Firmware bugs
-------------

- DFU can reset the application but then fails to perform the transfer.
  Possibly a timing issue. Possibly caused by the attempt to access the
  device qualifier being slow.

- since we don't implement stalling, retrieving the device qualifier
  times out, which takes a fairly long time.

Parts update
------------

- JST UB-MC5AB2R3-RS04-4S has been obsoleted 2015-03-02
  We can either use Hirose ZX62R-AB-5P or - more likely - move to Micro-B.

For future consideration (20140712 board errata)
------------------------------------------------

- Hirose ZX62R-AB-5P and JST UB-MC5AB2R3-RS04-4S have little "talons" that
  require slots in the board. They sort of fit without the slots, though.
- design issue: the RNG of CC2543 is unavailable during rfkill. What do to ?
  Options: use TRNG only for seed, then work with PRNG (like /dev/urandom);
  collect random bits while we can, then store them in the KL26; use other
  sources of randomness (ADC ?)
- 32.768 kHz crystal runs about 60 +/- 20 ppm faster than expected. This is
  counteracted by the FLL systematically running 558 ppm slow. So we have to
  detune the crystal to meet USB Full-Speed host tolerance (+/- 500 ppm).
  See fw/clock.c:clock_xtal_32k_fll for details.
- various items are under the battery holder CON4:
  - pin 1 (BATTERY) of CON5 (partially)
  - one SUB_CAP_A via (completely)
  - one SUB_CAP_B via (partially)
- consider moving C2 (VOUT33 silo) to top layer, to shorten distance to LDO
  (we don't have room to do this properly, maybe later)


Applied changes (former 20140712 board errata)
----------------------------------------------

- widen board by 2 x 0.5 mm on top and bottom (for OLED FPC)
- SW1 switches VSYS between VRF and GND, instead of VREF between VSYS and GND
- the following resulted implicitly from widening the board:
  - move TP6 (GND) and TP5 (VSYS) a bit towards the middle of the board, to
    have more room on the border for the mechanical support of the test
    fixture
  - move C2 away from border to increase clearance from bottom lower edge to
    silk from 0.35 mm to at least 0.5 mm, making it the same as R4/5/6.
    (Bottom side upper edge has a generous clearance of 1.5 mm.)
  - consider moving D2 (LED) a bit towards the center to increase top side
    lower edge to silk clearance from 0.75 mm to 1.0 mm of C9/18/27.
  - move C10/11/12/13 towards the center of the board to increase top side
    upper edge to silk clearance from 0.35 mm to at least 0.5 mm.
- boost converter U2 should also get orientation mark on copper layer
- there should be no traces under FC-135
- battery holder CON4 lacks orientation mark on copper layer
- various items are under the battery holder CON4:
  - V5 (partially) -> moved to main PCB
- indicate that all capacitors should be X5R or better. See also
  http://www.johansondielectrics.com/technical-notes/product-training/basics-of-ceramic-chip-capacitors.html
  http://www.johansondielectrics.com/technical-notes/general/ceramic-capacitor-aging-made-simple.html
- added small test point to PTD1, for lab testing


Applied changes (former 20140712 board errata)
----------------------------------------------

- equip R7 with a <= 300 kOhm pull-down, to prevent boost regulator from
  activating on reset (found to be drawing several 100 mA - to be investigated)
- the CC2543 oscillator may run a bit too fast with 10 pF for C23 and C24.
  Should use 12 pF or 15 pF, and indicate this choice in a clearer way in
  the schematics. 15 pF yield 2.00002 MHz (+10 ppm, but instrument tolerance
  is +/- 100 ppm) for a nominal 2 MHz clock.
- mention in schematics that LED current at 3.0 V is 4 mA


Hardware
--------

- test operation with real CR2032

- test operation with USB power

- measure current consumption of all subsystems

- consider adding bottom-side TP to LED. This will allow using the
  LED signal as trigger with immediate visual indication. Drawback:
  this may not be the easiest signal to access.


Firmware
--------

- implement nice "login" screen

- use SPI hardware for OLED and RF

- display: track bounding box of changes and only update changed area

- complete event systems, interrupts, etc.

- make RF driver send/receive

- test RSA/DH performance


Mechanical
----------

- case: consider shrinking the main PCB and let battery slide under the
  OLED, reducing case length by up to 4 mm - battery tolerance

- case top: add 4 cuts of ~10 x 0.5 mm for prying the case halves apart


Known bugs
----------

- display sometimes "jumps" in the RNG test, suggesting the memory
  pointer occasionally gets set incorrectly


Obsolete
--------

- wheel button reacts slowly. May be defective trace.

- when the wheel button is held during FRDM-KL25Z reset, the reset is not
  performed


For investigation
-----------------

- the only clock available for driving the RTC seems to be the 1 kHz
  LPO. Is this a problem ? (By reading the RTC prescaler value, one
  can obtain 1 cycle granularity.)

- the first bandgap (ADC) reading after reset is often bogus.

- temperature sensor seems a bit noisy, barely achieving stable values
  for whole degrees. This may have something to do with the slope of
  1.715 mV/K.

- seems that we could reverse the USB Micro AB receptacle, thus making
  it follow the top/bottom mount convention. Should we try to do this ?
