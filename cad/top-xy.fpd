#include "obj.inc"
#include "pcba.inc"
#include "parts.inc"


frame case {
	set gap = 0.2mm
	set wall = 2mm

	set d = gap+wall/2
	set out = gap+wall

	case_ll: vec @(-d, -d)
	QLL(case_, R_LEFT+d, wall)

	case_ul: vec @(-d, PCB_Y+d)
	QUL(case_, R_LEFT+d, wall)

	rl: vec @(PCB_X-R_RIGHT, -d)
	rc: vec rl(0mm, R_RIGHT+d)
	ru: vec rc(0mm, R_RIGHT+d)
	arc rc rl ru wall

	line case_llx rl wall
	line case_ulx ru wall
	line case_lly case_uly wall

	// for measurements

	outer_ll: vec @(-out, -out)
	outer_ul: vec @(-out, PCB_Y+out)
	outer_mr: vec @(PCB_X+out, PCB_Y/2)
}


frame channel {
	set gap = 0.2mm
	set wall = 1mm

	set r = PCB_LAN_R-gap-wall/2

	ac: vec @(0mm, -PCB_LAN_ECCENT/2)
	al: vec ac(-r, 0mm)
	ar: vec ac(r, 0mm)

	bc: vec @(0mm, PCB_LAN_ECCENT/2)
	bl: vec bc(-r, 0mm)
	br: vec bc(r, 0mm)

	arc ac al ar wall
	arc bc br bl wall

	line al bl wall
	line ar br wall
}


frame pcb {

	pcb_ll: vec @(0mm, 0mm)
	QLL(pcb_, R_LEFT, w)

	pcb_ul: vec @(0mm, PCB_Y)
	QUL(pcb_, R_LEFT, w)

	rl: vec @(PCB_X-R_RIGHT, 0mm)
	rc: vec rl(0mm, R_RIGHT)
	ru: vec rc(0mm, R_RIGHT)
	arc rc rl ru w

	line pcb_llx rl w
	line pcb_ulx ru w
	line pcb_lly pcb_uly w
}


frame lanyard {
	set r = PCB_LAN_R

	ac: vec @(0mm, -PCB_LAN_ECCENT/2)
	al: vec ac(-r, 0mm)
	ar: vec ac(r, 0mm)

	bc: vec @(0mm, PCB_LAN_ECCENT/2)
	bl: vec bc(-r, 0mm)
	br: vec bc(r, 0mm)

	arc ac al ar w
	arc bc br bl w

	line al bl w
	line ar br w
}


frame holes {
	table { x, y, r }
	    {  HOLE_1_X, HOLE_1_Y, HOLE_1_R }
	    {  HOLE_2_X, HOLE_2_Y, HOLE_2_R }
	    {  HOLE_3_X, HOLE_3_Y, HOLE_3_R }

	c: vec @(x, y)
	vec .(r, 0mm)
	circ c . w
}


frame parts {
}


package "ANELOK-TOP-XY"
set w = 5mil

frame pcb @

vec @(PCB_LAN_X, PCB_LAN_Y)
frame lanyard .

frame holes @

vec @(DISP_X, DISP_Y)
frame oled_xy .

vec @(LED_X, LED_Y)
frame led_xy .

vec @(WHEEL_X, WHEEL_Y)
frame tswa_xy .

frame case @

vec @(PCB_LAN_X, PCB_LAN_Y)
frame channel .

pcb_mr: vec @(PCB_X, PCB_Y/2)
measx pcb.pcb_ll >> pcb_mr -10mm
measx case.outer_ll >> case.outer_mr -15mm

measy pcb.pcb_ll >> pcb.pcb_ul 10mm
measy case.outer_ll >> case.outer_ul 15mm
