EESchema Schematic File Version 2
LIBS:pwr
LIBS:r
LIBS:led
LIBS:powered
LIBS:micro_usb_b
LIBS:usb_a_plug
LIBS:device_sot
LIBS:ybox-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "25 oct 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ZX62-B-5PA CON1
U 1 1 526AF038
P 6200 2900
F 0 "CON1" H 6200 3400 60  0000 C CNN
F 1 "ZX62-B-5PA" H 6200 2400 60  0000 C CNN
F 2 "" H 6200 2900 60  0000 C CNN
F 3 "" H 6200 2900 60  0000 C CNN
	1    6200 2900
	0    -1   -1   0   
$EndComp
$Comp
L ZX62-B-5PA CON2
U 1 1 526AF047
P 2700 4300
F 0 "CON2" H 2900 3800 60  0000 C CNN
F 1 "ZX62-B-5PA" H 2700 4800 60  0000 C CNN
F 2 "" H 2700 4300 60  0000 C CNN
F 3 "" H 2700 4300 60  0000 C CNN
	1    2700 4300
	-1   0    0    1   
$EndComp
$Comp
L USB_A_PLUG CON3
U 1 1 526AF056
P 7900 4350
F 0 "CON3" H 7650 3950 60  0000 C CNN
F 1 "1002-002-01100" H 7850 4800 60  0000 C CNN
F 2 "" H 7900 4350 60  0000 C CNN
F 3 "" H 7900 4350 60  0000 C CNN
	1    7900 4350
	1    0    0    1   
$EndComp
NoConn ~ 3300 4150
Text Label 4900 4000 0    60   ~ 0
GND
Text Label 4900 4300 0    60   ~ 0
DP
Text Label 4900 4450 0    60   ~ 0
DM
Text Label 4900 4600 0    60   ~ 0
VBUS
NoConn ~ 6050 3500
NoConn ~ 6200 3500
NoConn ~ 6350 3500
$Comp
L GND #PWR01
U 1 1 526AF1A7
P 6900 2550
F 0 "#PWR01" H 6900 2550 30  0001 C CNN
F 1 "GND" H 6900 2480 30  0001 C CNN
F 2 "" H 6900 2550 60  0000 C CNN
F 3 "" H 6900 2550 60  0000 C CNN
	1    6900 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 526AF1B4
P 1950 4800
F 0 "#PWR02" H 1950 4800 30  0001 C CNN
F 1 "GND" H 1950 4730 30  0001 C CNN
F 2 "" H 1950 4800 60  0000 C CNN
F 3 "" H 1950 4800 60  0000 C CNN
	1    1950 4800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 526AF1BA
P 8650 4800
F 0 "#PWR03" H 8650 4800 30  0001 C CNN
F 1 "GND" H 8650 4730 30  0001 C CNN
F 2 "" H 8650 4800 60  0000 C CNN
F 3 "" H 8650 4800 60  0000 C CNN
	1    8650 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4600 7300 4600
Wire Wire Line
	3300 4450 7300 4450
Wire Wire Line
	3300 4300 7300 4300
Wire Wire Line
	3300 4000 5300 4000
Wire Wire Line
	5300 4000 5300 4150
Wire Wire Line
	5300 4150 7300 4150
Wire Wire Line
	5900 3500 5900 4600
Connection ~ 5900 4600
Wire Wire Line
	6500 3500 6500 4150
Connection ~ 6500 4150
Wire Wire Line
	5900 2350 5900 2150
Wire Wire Line
	5900 2150 6900 2150
Wire Wire Line
	6900 2150 6900 2550
Wire Wire Line
	6500 2350 6500 2150
Connection ~ 6500 2150
Wire Wire Line
	6400 2350 6400 2150
Connection ~ 6400 2150
Wire Wire Line
	6250 2350 6250 2150
Connection ~ 6250 2150
Wire Wire Line
	6150 2350 6150 2150
Connection ~ 6150 2150
Wire Wire Line
	6000 2350 6000 2150
Connection ~ 6000 2150
Wire Wire Line
	1950 3800 1950 4800
Wire Wire Line
	1950 4000 2150 4000
Wire Wire Line
	2150 4100 1950 4100
Connection ~ 1950 4100
Wire Wire Line
	2150 4250 1950 4250
Connection ~ 1950 4250
Wire Wire Line
	2150 4350 1950 4350
Connection ~ 1950 4350
Wire Wire Line
	2150 4500 1950 4500
Connection ~ 1950 4500
Wire Wire Line
	2150 4600 1950 4600
Connection ~ 1950 4600
Wire Wire Line
	8450 4150 8650 4150
Wire Wire Line
	8650 4150 8650 4800
Wire Wire Line
	8450 4600 8650 4600
Connection ~ 8650 4600
$Comp
L R R1
U 1 1 526AF37D
P 4300 2550
F 0 "R1" H 4430 2600 60  0000 C CNN
F 1 "330" H 4450 2500 60  0000 C CNN
F 2 "" H 4300 2550 60  0000 C CNN
F 3 "" H 4300 2550 60  0000 C CNN
	1    4300 2550
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 526AF38C
P 4300 3200
F 0 "D1" H 4300 3300 60  0000 C CNN
F 1 "LED" H 4300 3100 60  0000 C CNN
F 2 "" H 4300 3200 60  0000 C CNN
F 3 "" H 4300 3200 60  0000 C CNN
	1    4300 3200
	0    1    1    0   
$EndComp
$Comp
L GND #PWR04
U 1 1 526AF3B7
P 4300 3600
F 0 "#PWR04" H 4300 3600 30  0001 C CNN
F 1 "GND" H 4300 3530 30  0001 C CNN
F 2 "" H 4300 3600 60  0000 C CNN
F 3 "" H 4300 3600 60  0000 C CNN
	1    4300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2800 4300 3000
Wire Wire Line
	4300 3400 4300 3600
Wire Wire Line
	4300 1800 4300 2300
Text Label 4300 2100 1    60   ~ 0
VBUS
$Comp
L DIODE-SOT-AXC D2
U 1 1 526AF4D2
P 4400 4600
F 0 "D2" H 4550 4650 60  0000 C CNN
F 1 "MMBD4448HW" H 4350 4450 60  0000 C CNN
F 2 "" H 4400 4600 60  0000 C CNN
F 3 "" H 4400 4600 60  0000 C CNN
	1    4400 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3300 4600 4200 4600
Text Label 3500 4600 0    60   ~ 0
VBUS_OUT
$Comp
L POWERED #FLG05
U 1 1 526AF660
P 1950 3600
F 0 "#FLG05" H 2150 3500 60  0001 C CNN
F 1 "POWERED" H 1950 3650 60  0000 C CNN
F 2 "" H 1950 3600 60  0000 C CNN
F 3 "" H 1950 3600 60  0000 C CNN
	1    1950 3600
	1    0    0    -1  
$EndComp
Connection ~ 1950 4000
$EndSCHEMATC
