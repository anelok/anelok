/*
 * script.h - Scripting interface
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SCRIPT_H
#define	SCRIPT_H

void scan_script(void);
void scan_setup(void);

#endif /* !SCRIPT_H */
