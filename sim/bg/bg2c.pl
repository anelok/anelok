#!/usr/bin/perl
#
# sim/bg/bg2c.pl - Convert background image configuration to C
#
# Written 2017 by Werner Almesberger
# Copyright 2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

$scale = 1;
while (<>) {
	s/#.*//;
	next if /^\s*$/;
	if (/^\s*scale\s+(\d+)\s*$/) {
		$scale = $1;
	} elsif (/^\s*image\s+"([^"]*)"\s*$/) {
		$image = $1;
	} elsif (/^\s*image_led\s+"([^"]*)"\s*$/) {
		$image_led = $1;
	} elsif (/^\s*origin\s+(\d+)\s+(\d+)\s*$/) {
		@origin = ($1, $2);
	} elsif (/^\s*top\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s*$/) {
		@top = ($1, $2, $3, $4);
	} elsif (/^\s*middle\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s*$/) {
		@middle = ($1, $2, $3, $4);
	} elsif (/^\s*bottom\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s*$/) {
		@bottom = ($1, $2, $3, $4);
	} else {
		die "parse error";
	}
}


sub dump
{
	local ($name) = @_;

	open(FILE, $name) || die "$name: $!";
	$bytes = -s FILE;
	my @d = unpack("C*", join("", <FILE>));
	close FILE;

	my $s = "";
	while ($#d > -1) {
		my @t = splice(@d, 0, 8);

		$s .= "\t\t\t\t  " unless $s eq "";
		$s .= '"';
		$s .= join("", map { sprintf("\\x%02x", $_) } @t);
		$s .= '"';
		$s .= "," if $#d == -1;
		$s .= "\n";
	}
	return $s;
}


sub buttons
{
	local (@b) = @_;

	return sprintf("{ %d, %d, %d, %d }", $b[0], $b[1], $b[2], $b[3]);
}


#
# @{[ ... ]} device thanks to
# http://stackoverflow.com/questions/3939919/can-perl-string-interpolation-perform-any-expression-evaluation
#

print <<"EOF";
{
	.scale		= $scale,
	.image_data	= (const uint8_t *)
			  @{[ &dump($image) ]}
	.image_bytes	= $bytes,
	.image_led_data	= (const uint8_t *)
			  @{[ &dump($image_led) ]}
	.image_led_bytes = $bytes,
	.xo		= @{[ $origin[0] + 0 ]},
	.yo		= @{[ $origin[1] + 0 ]},
	.buttons	= {
				{ 0 },
				@{[ &buttons(@top) ]},
				@{[ &buttons(@middle) ]},
				@{[ &buttons(@bottom) ]},
	}
},
EOF
