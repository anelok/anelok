/*
 * sim.h - Anelok UI simulator
 *
 * Written 2014-2017 by Werner Almesberger
 * Copyright 2014-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef SIM_H
#define	SIM_H

#include <stdbool.h>
#include <stdint.h>

#include "SDL.h"

#include "display.h"
#include "input.h"


extern SDL_Window *win;
extern SDL_Surface *surf;

extern bool headless;
extern bool run_viewer;
extern int sim_scale;

extern bool sim_no_slider;	/* used by input.c */


/* Setup */

struct button_rect {
	int x, y;
	int w, h;	/* w = 0: entry not used */
};

extern int sim_xo, sim_yo;
extern const char *bg_image, *bg_image_led;
extern const uint8_t *bg_image_data, *bg_image_led_data;
extern unsigned bg_image_bytes, bg_image_led_bytes;
extern struct button_rect buttons[area_end];


/* Panic handling in the simulated environment */

extern bool stay_on_panic;


bool panic_loop(void);

/* For scripting */

void time_step_ms(unsigned ms);

void button_down(enum area area);
void button_up(void);

#endif /* !SIM_H */
