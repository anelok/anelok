/*
 * sim.c - Anelok UI simulator
 *
 * Written 2014-2017 by Werner Almesberger
 * Copyright 2014-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

#include "tick.h"
#include "devcfg.h"
#include "display.h"
#include "console.h"
#include "icon.h"
#include "touch.h"
#include "input.h"
#include "alert.h"
#include "alerts.h"
#include "sel.h"
#include "auth.h"
#include "ui.h"
#include "sim.h"

#include "SDL.h"

#include "subst.h"
#include "screen.h"
#include "cpp.h"
#include "script.h"
#include "bg.h"
#include "sim.h"


extern bool leave_ui;

bool headless = 0;
bool run_viewer = 0;
int sim_scale = 1;

const char *bg_image = NULL;
const char *bg_image_led = NULL;
const uint8_t *bg_image_data = NULL;
const uint8_t *bg_image_led_data = NULL;
unsigned bg_image_bytes, bg_image_led_bytes;

bool sim_no_slider = 1;
bool stay_on_panic = 0;

struct button_rect buttons[area_end];

static bool script = 0;
static bool test_selection = 0;
static bool down = 0;
static uint8_t curr_pos;
static bool paused = 0;


/* ----- Delays ------------------------------------------------------------ */


void msleep(unsigned ms)
{
	struct timespec req = {
		.tv_sec	= ms / 1000,
		.tv_nsec = (ms % 1000) * 1000000,
	};

	if (!script)
		nanosleep(&req, NULL);
}


/* ----- Timekeeping ------------------------------------------------------- */


#define	SIM_TIME_STEP_TICKS	100

static bool simulated_time = 0;
static uint32_t ticks = 0;


uint32_t sim_ticks(void)
{
	if (simulated_time)
		return ticks;
	else
		return SDL_GetTicks();
}


uint16_t tick_now(void)
{
	return sim_ticks();
}


uint16_t tick_delta(uint16_t *t)
{
	uint16_t now = tick_now();
	uint16_t dt = 0;

	if (!paused)
		dt = now - *t;
	*t = now;
	return dt;
}


void time_step_ms(unsigned ms)
{
	uint32_t t = ms;

	while (t > SIM_TIME_STEP_TICKS) {
		ticks += SIM_TIME_STEP_TICKS;
		ui();
		t -= SIM_TIME_STEP_TICKS;
	}
	ticks += t;
}


/* ----- Event handling ---------------------------------------------------- */


static void touch_press(uint8_t pos)
{
	curr_pos = FB_Y - pos - 1;
	down = 1;
}


static void touch_release(void)
{
	down = 0;
}


static enum area button_area = area_none;


void button_down(enum area area)
{
	if (button_area)
		return;
	switch (area) {
	case area_top:
		touch_press(0);
		break;
	case area_middle:
		touch_press(FB_Y >> 1);
		break;
	case area_bottom:
		touch_press(FB_Y - 1);
		break;
	default:
		abort();
	}
	button_area  = area;
}


static void click(int x, int y)
{
	enum area area;

	for (area = 0; area != area_end; area++)
		if (x >= buttons[area].x &&
		    x < buttons[area].x + buttons[area].w &&
		    y >= buttons[area].y &&
		    y < buttons[area].y + buttons[area].h) {
			button_down(area);
			return;
		}
	touch_press(y / sim_scale);
}


void button_up(void)
{
	if (!button_area)
		return;
	touch_release();
	button_area = area_none;
}


static void release(void)
{
	if (bg_image || bg_image_data)
		button_up();
	else
		touch_release();
}


static void window_title(void)
{
	const char *s;

	s = paused ? "Paused" : "Anelok";
	SDL_SetWindowTitle(win, s);
}


static bool process_events(void)
{
	SDL_Event event;

	if (!SDL_PollEvent(&event)) {
		msleep(10);
		return 1;
	}

	switch (event.type) {
	case SDL_QUIT:
		return 0;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_p:
			paused = !paused;
			window_title();
			return 1;
		case SDLK_s:
			dump_screen();
			break;
#ifndef __EMSCRIPTEN__
		case SDLK_q:
			return 0;
#endif
		default:
			break;
		}
	}

	if (paused)
		return 1;

	switch (event.type) {
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_PLUS:
		case SDLK_EQUALS:
			sim_swipe(1);
			break;
		case SDLK_MINUS:
			sim_swipe(-1);
			break;
		case SDLK_a:
			alert_radio();
			break;
		case SDLK_c:
			alert_clear(al_radio);
			break;
		case SDLK_j:
		case SDLK_DOWN:
			button_down(area_bottom);
			break;
		case SDLK_k:
		case SDLK_UP:
			button_down(area_top);
			break;
		case SDLK_l:
		case SDLK_RIGHT:
			button_down(area_middle);
			break;
		case SDLK_r:
			toggle_switch();
			break;
		default:
			break;
		}
		break;
	case SDL_KEYUP:
		switch (event.key.keysym.sym) {
		case SDLK_j:
		case SDLK_UP:
		case SDLK_k:
		case SDLK_DOWN:
		case SDLK_l:
		case SDLK_RIGHT:
			button_up();
			break;
		default:
			break;
		}
		break;
	case SDL_MOUSEMOTION:
		if (event.motion.state & SDL_BUTTON_LMASK)
			click(event.motion.x, event.motion.y);
		else
			touch_release();
		break;
	case SDL_MOUSEBUTTONDOWN:
		if (event.button.state == SDL_BUTTON_LEFT)
			click(event.button.x, event.button.y);
		break;
	case SDL_MOUSEBUTTONUP:
		release();
		break;
	}
	return 1;
}


bool panic_loop(void)
{
	SDL_Event event;

	display_update();	/* update and expose events */

	if (!SDL_PollEvent(&event)) {
		msleep(10);
		return 1;
	}

	switch (event.type) {
	case SDL_QUIT:
		return 0;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_s:
			dump_screen();
			break;
		case SDLK_q:
			return 0;
		default:
			break;
		}
	default:
		break;
	}
	return 1;
}


bool touch_filter(uint8_t *pos)
{
	if (!process_events())
		leave_ui = 1;
	if (down)
		*pos = curr_pos;
	return down;
}


/* ----- Scripting --------------------------------------------------------- */


extern int yyparse(void);
extern FILE *yyin;


/* ----- Command-line processing ------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [-d] [-m memcard.bin] [-p] [-s screen_dump.bmp] [-t] [-v]\n"
"       %*s[-x scale] [cpp_options] [script]\n"
"\n"
"  cpp_options are -Dname[=value], -Uname, and -Idir\n"
    , name, (int) strlen(name) + 1, "");
	exit(1);
}


#ifdef __EMSCRIPTEN__

static void run_ui(void)
{
	if (!ui())
		emscripten_cancel_main_loop();
}

#endif


int main(int argc, char **argv)
{
	const char *setup = NULL;
	bool debug = 0;
	char opt[] = "-?";
	int c;

	while ((c = getopt(argc, argv, "b:dm:ps:tvx:D:I:SU:")) != EOF)
		switch (c) {
		case 'b':
			setup = optarg;
			break;
		case 'd':
			debug = 1;
			break;
		case 'm':
			sim_mmc_file = fopen(optarg, "r+");
			if (!sim_mmc_file) {
				perror(optarg);
				return 1;
			}
			break;
		case 'p':
			stay_on_panic = 1;
			break;
		case 's':
			screen_dump_name = optarg;
			break;
		case 't':
			test_selection = 1;
			break;
		case 'v':
			run_viewer = 1;
			break;
		case 'x':
			sim_scale = atoi(optarg);
			if (!sim_scale)
				usage(*argv);
			break;
		case 'S':
			sim_no_slider = 0;
			break;
		case 'D':
		case 'I':
		case 'U':
			opt[1] = c;
			add_cpp_arg(opt);
			add_cpp_arg(optarg);
			break;
		default:
			usage(*argv);
		}

	if (setup) {
		int fd, old_stdin;

		fd = open(setup, O_RDONLY);
		if (fd < 0) {
			perror(setup);
			exit(1);
		}
		old_stdin = dup(0);
		dup2(fd, 0);
		scan_setup();
		yyparse();
		dup2(old_stdin, 0);
	} else {
		select_bg(sim_scale);
	}

	switch (argc - optind) {
	case 1:
		add_cpp_arg("-include");
		add_cpp_arg("sim.init");
		run_cpp_on_file(strcmp(argv[optind], "-") ?
		    argv[optind] : NULL);
		script = 1;
		break;
	case 0:
		break;
	default:
		usage(*argv);
	}

	headless = script && !debug;

	devcfg_init();
	display_init();
	console_init();
	window_title();

	if (test_selection) {
		auth_try(DEFAULT_PIN);
		ui_is_on = 1;
		ui_select();
	} else {
		ui_off();
	}

	ui_init();

	if (script) {
		simulated_time = 1;
		scan_script();
		yyparse();
	} else {
#ifdef __EMSCRIPTEN__
		emscripten_set_main_loop(run_ui, 0, 1);
#else
		while (ui());
#endif
	}

	return 0;
}
