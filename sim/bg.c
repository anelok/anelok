/*
 * bg.c - Background image setup
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "sim.h"
#include "bg.h"


static const struct bg_config configs[] = {
#include "background-x1.inc"
#include "background-x2.inc"
        { 0 }
};


bool select_bg(int scale)
{
	const struct bg_config *cfg;

	for (cfg = configs; 1; cfg++) {
		if (!cfg->scale)
			return 0;
		if (cfg->scale == scale)
			break;
	}

	bg_image_data = cfg->image_data;
	bg_image_bytes = cfg->image_bytes;
	bg_image_led_data = cfg->image_led_data;
	bg_image_led_bytes = cfg->image_led_bytes;
	sim_xo = cfg->xo;
	sim_yo = cfg->yo;
	memcpy(buttons, cfg->buttons, sizeof(buttons));
	return 1;
}
