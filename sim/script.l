%{
/*
 * script.l - Simulator scripting language
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdio.h>
#include <stdlib.h>

#include "input.h"	/* for enum area */
#include "script.h"	/* for enum area */
#include "sim.h"

#include "y.tab.h"


void yyerror(const char *s);
int yywrap(void);

static int lineno = 1;
static int start_token = START_SCRIPT;


void scan_script(void)
{
	start_token = START_SCRIPT;
}


void scan_setup(void)
{
	start_token = START_SETUP;
}

%}

%%

%{
	/*
	 * Nice hack:
	 *
	 * http://www.gnu.org/software/bison/manual/bison.html#
	 *   Multiple-start_002dsymbols
	 */

	if (start_token) {
		int tmp = start_token;
		start_token = 0;
		return tmp;
	}
%}


bottom		return TOK_BOTTOM;
check		return TOK_CHECK;
dump		return TOK_DUMP;
middle		return TOK_MIDDLE;
release		return TOK_RELEASE;
top		return TOK_TOP;
wait		return TOK_WAIT;

image		return TOK_IMAGE;
image_led	return TOK_IMAGE_LED;
origin		return TOK_ORIGIN;
scale		return TOK_SCALE;

[0-9]+\.?[0-9]*	{ yylval.ms = strtod(yytext, NULL) * 1000;
		  return TIME; }
\"(\\[^\n\t]|[^\\"\n\t])*\" {
		  yylval.str = strdup(yytext + 1);
		  *strchr(yylval.str, '"') = 0;
		  return STRING; }

[ \t]		;
\n		lineno++;

^#\ [0-9]+\ \"[^"]*\"(\ [0-9]+)*\n {
		  lineno = strtol(yytext+2, NULL, 0); }

#.*\n		lineno++;

.		return *yytext;

%%

void yyerror(const char *s)
{
	fprintf(stderr, "%d: %s near \"%s\"\n", lineno, s, yytext);
	exit(1);
}


/* yywrap must be explicitly provided under emscripten */

int yywrap(void)
{
	return 1;
}
