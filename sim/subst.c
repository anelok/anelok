/*
 * subst.c - Miscellaneous substitute functions
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "sim.h"
#include "subst.h"


/* ----- panic ------------------------------------------------------------- */


#include "qa.h"


void panic(void)
{
	if (stay_on_panic) {
		while (panic_loop());
		abort();
	}
	fprintf(stderr, "Panic !\n");
	abort();
}


/* ----- reset_cpu --------------------------------------------------------- */


/*
 * We can't or at least shouldn't include misc.h, so we have to provide our own
 * prototype.
 */

void reset_cpu(void) __attribute__((noreturn));


void reset_cpu(void)
{
	fprintf(stderr, "CPU reset\n");
	exit(1);
}


/* ----- id_eq, id_get ----------------------------------------------------- */


#include "id.h"


#define	UID_MH	0		/* make simulator entry easy to spot */
#define	UID_ML	0xa2e10b	/* "ANELOK" */
#define	UID_L	0x513c1a70	/* "SIMULATO" */


bool id_eq(const struct id *id)
{
	return id->uidmh == UID_MH && id->uidml == UID_ML &&
	    id->uidl == UID_L;

}


void id_get(struct id *id)
{
	id->uidmh = UID_MH;
	id->uidml = UID_ML;
	id->uidl = UID_L;
}


/* ----- Power management -------------------------------------------------- */


#include "power.h"


void power_3v3(bool on)
{
}


void power_disp(bool on)
{
}


void power_disp_force_on(void)
{
}


/* ----- Touch sensor ------------------------------------------------------ */


#include "touch.h"


struct cal cal_a, cal_b;
struct cal standby_cal_a, standby_cal_b;


void touch_calm(bool power_drop)
{
}


/* ----- USB --------------------------------------------------------------- */


#include "usb-board.h"
#include "vusb.h"
#include "hid-type.h"


/* Nothing is connected to USB */


bool usb_a(void)
{
	return 0;
}


bool vusb_sense(void)
{
	return 0;
}


void hid_type(struct hid_type_ctx *ctx, const char *s,
    void (*fn)(void *user), void *user)
{
	printf("HID: %s\n", s);
	if (fn)
		fn(user);
}


/* ----- Memory card ------------------------------------------------------- */


#include "mmc-hw.h"
#include "mmc.h"


FILE *sim_mmc_file = NULL;


bool card_present(void)
{
	return sim_mmc_file;
}


bool mmc_init(void)
{
	return sim_mmc_file;
}


bool mmc_read_cid(uint8_t *cid)
{
	return 0;
}


/* --- Memory card blocks --- */


static bool mmc_ok;


bool mmc_begin_read(uint32_t addr)
{
	if (!sim_mmc_file)
		return 0;
	mmc_ok = 1;
	if (fseek(sim_mmc_file, addr, SEEK_SET) < 0)
		return 0;
	return ftell(sim_mmc_file) == addr;
}


uint8_t mmc_read(void)
{
	int c;

	assert(sim_mmc_file);
	c = fgetc(sim_mmc_file);
	if (c < 0) {
		mmc_ok = 0;
		return 0;
	}
	return c & 0xff;	// make Coverity happy
}


bool mmc_end_read(void)
{
	assert(sim_mmc_file);
	return mmc_ok;
}


/* ----- CC2543 ------------------------------------------------------------ */


#include "ccdbg.h"


uint16_t ccdbg_get_chip_id(struct ccdbg *ccdbg)
{
	return 0x43ff;	/* valid chip ID, nonsensical version */
}


#include "cc.h"


struct ccdbg ccdbg;

static bool rf_switch = 1;


bool cc_on(void)
{
	return rf_switch;
}


bool cc_acquire(void)
{
	return rf_switch; /* just in case */
}


void cc_release(void)
{
}


void toggle_switch(void)
{
	rf_switch = !rf_switch;
}


/* ----- RF communication -------------------------------------------------- */


#include "rf.h"


uint8_t rf_poll(void *buf, uint8_t size)
{
	return 0;
}


bool rf_send(const void *buf, uint8_t cmd)
{
	return 1;
}


void rf_init(void)
{
}


/* ----- Communication handler --------------------------------------------- */


#include "input.h"
#include "account.h"
#include "comm.h"


static struct hid_type_ctx type;


bool comm_send(struct account_record *rec)
{
	hid_type(&type, account_text(rec), NULL, NULL);
	input_refresh();
	return 1;
}


enum comm comm_mode(struct account_record *rec)
{
	return account_field_type(rec) == field_pw ? comm_hid : comm_none;
}


/* ----- Events ------------------------------------------------------------ */


#include "event.h"


uint32_t events;


bool __event_consume(enum event n)
{
	uint32_t mask = 1 << n;

	if (!(events & mask))
		return 0;
	events &= ~mask;
	return 1;
}


/* ----- TweetNaCl --------------------------------------------------------- */


void randombytes(uint8_t *bytes, uint64_t len);

void randombytes(uint8_t *bytes, uint64_t len)
{
	abort();
}
