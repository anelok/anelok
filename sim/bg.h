/*
 * bg.h - Background image setup
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef BG_H
#define	BG_H

#include <stdbool.h>
#include <stdint.h>

#include "sim.h"


struct bg_config {
	int scale;		/* 0 for end-of-list */
	const uint8_t *image_data;
	unsigned image_bytes;
	const uint8_t *image_led_data;
	unsigned image_led_bytes;
	int xo, yo;
	struct button_rect buttons[area_end];
};


bool select_bg(int scale);

#endif /* !BG_H */
