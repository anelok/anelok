%{
/*
 * script.y - Simulator scripting language
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>

#include "ui.h"
#include "screen.h"
#include "sim.h"

#include "y.tab.h"


#define	TMP_DUMP_NAME	"_tmp.bmp"

#define	VIEWER		"qiv"
#define	VIEWER_ARGS	"-R"


#define	BUF_SIZE	1024


static bool file_equal(const char *name_a, const char *name_b)
{
	int fd_a, fd_b;
	ssize_t got_a, got_b;
	char buf_a[BUF_SIZE], buf_b[BUF_SIZE];
	bool match = 0;

	fd_a = open(name_a, O_RDONLY);
	if (fd_a < 0) {
		perror(name_a);
		exit(1);
	}
	fd_b = open(name_b, O_RDONLY);
	if (fd_b < 0) {
		perror(name_b);
		exit(1);
	}

	while (1) {
		got_a = read(fd_a, buf_a, sizeof(buf_a));
		if (got_a < 0) {
			perror(name_a);
			exit(1);
		}
		got_b = read(fd_b, buf_b, got_a ? got_a : 1);
		if (got_b < 0) {
			perror(name_b);
			exit(1);
		}
		if (!got_a && !got_b) {
			match = 1;
			break;
		}
		if (got_a != got_b)
			break;
		if (memcmp(buf_a, buf_b, got_a))
			break;
	}
	(void) close(fd_a);
	(void) close(fd_b);
	return match;
}


static void do_run_viewer(const char *a, const char *b)
{
	if (fork())
		return;
	execlp(VIEWER, VIEWER, VIEWER_ARGS, a, b, NULL);
	perror(VIEWER);
	exit(1);
}

%}


%union {
	enum area area;
	char *str;
	int ms;
	struct button_rect rect;
};


%token		START_SCRIPT START_SETUP
%token		TOK_TOP TOK_MIDDLE TOK_BOTTOM
%token		TOK_RELEASE
%token		TOK_CHECK TOK_DUMP TOK_WAIT
%token		TOK_IMAGE TOK_IMAGE_LED TOK_ORIGIN TOK_SCALE

%token	<ms>	TIME
%token	<str>	STRING

%type	<area>	button
%type	<ms>	time_expr primary_time int
%type	<rect>	rect


%%


all:
	START_SCRIPT script
	| START_SETUP setup
	;

script:
	| script command
		{
			ui();
		}
	;

command:
	button
		{
			button_down($1);
		}
	| TOK_RELEASE
		{
			button_up();
		}
	| TOK_CHECK STRING
		{
			dump_screen_file(TMP_DUMP_NAME);
			if (!file_equal(TMP_DUMP_NAME, $2)) {
				if (run_viewer)
					do_run_viewer(TMP_DUMP_NAME, $2);
				yyerror("file mismatch");
				exit(1);
			}
			free($2);
			
		}
	| TOK_DUMP STRING
		{
			dump_screen_file($2);
			free($2);
		}
	| TOK_WAIT time_expr
		{
			if ($2 < 0)
				yyerror("negative time");
			time_step_ms($2);
		}
	;

button:
	TOK_TOP
		{
			$$ = area_top;
		}
	| TOK_MIDDLE
		{
			$$ = area_middle;
		}
	| TOK_BOTTOM
		{
			$$ = area_bottom;
		}
	;

time_expr:
	primary_time
		{
			$$ = $1;
		}
	| time_expr '+' primary_time
		{
			$$ = $1 + $3;
		}
	| time_expr '-' primary_time
		{
			$$ = $1 - $3;
		}
	;

primary_time:
	TIME
		{
			$$ = $1;
		}
	| '(' time_expr ')'
		{
			$$ = $2;
		}
	;

setup:
	| setup setting
	;

setting:
	TOK_SCALE int
		{
			sim_scale = $2;
		}
	| TOK_IMAGE STRING
		{
			bg_image = $2;
		}
	| TOK_IMAGE_LED STRING
		{
			bg_image_led = $2;
		}
	| TOK_TOP rect
		{
			buttons[area_top] = $2;	
		}
	| TOK_MIDDLE rect
		{
			buttons[area_middle] = $2;	
		}
	| TOK_BOTTOM rect
		{
			buttons[area_bottom] = $2;	
		}
	| TOK_ORIGIN int int
		{
			sim_xo = $2;
			sim_yo = $3;
		}
	; 

rect:
	int int int int
		{
			$$.x = $1;
			$$.y = $2;
			$$.w = $3;
			$$.h = $4;
		}
	;

int:
	TIME
		{
			if ($1 % 1000) {
				yyerror("integer expected");
				exit(1);
			}
			$$ = $1 / 1000;
		}
	;
