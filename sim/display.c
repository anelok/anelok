/*
 * sim/display.c - Display simulator based on SDL_gfx
 *
 * Written 2014-2017 by Werner Almesberger
 * Copyright 2014-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "SDL.h"

#include "qa.h"
#include "tick.h"
#include "led.h"
#include "display.h"

#include "sim.h"
#include "subst.h"


#define	FB_X	128
#define	FB_Y	64

#define	BLACK		0, 0, 0
#define	WHITE		255, 255, 255


SDL_Window *win;
SDL_Surface *surf;
int sim_xo = 0, sim_yo = 0;

static SDL_Surface *surf_led = NULL;
static SDL_Renderer *rend;
static SDL_Texture *tex;

static bool rotate;
static bool led_is_on = 0;


/* ----- Drawing primitives ------------------------------------------------ */


static void sdl_blit(SDL_Surface *src, SDL_Rect *srcrect,
    SDL_Surface *dst, SDL_Rect *dstrect)
{
	if (SDL_BlitSurface(src, srcrect, dst, dstrect)) {
		fprintf(stderr, "SDL_BlitSurface failed: %s\n",
		    SDL_GetError());
		exit(1);
	}
}


static void pixel(int x, int y, uint8_t r, uint8_t g, uint8_t b)
{
	SDL_Rect rect = {
		.x = sim_xo + x * sim_scale,
		.y = sim_yo + y * sim_scale,
		.w = sim_scale,
		.h = sim_scale
	};

	if (x < 0 || x >= FB_X || y < 0 || y >= FB_Y)
		oops();
	SDL_FillRect(surf, &rect, SDL_MapRGB(surf->format, r, g, b));
}


static void update(void)
{
	if (headless)
		return;
	if (surf_led && led_is_on) {
		SDL_Rect rect = {
			.x = sim_xo,
			.y = sim_yo,
			.w = FB_X * sim_scale,
			.h = FB_Y * sim_scale
		};
		sdl_blit(surf, &rect, surf_led, &rect);
		SDL_UpdateTexture(tex, NULL, surf_led->pixels, surf_led->pitch);
	} else {
		SDL_UpdateTexture(tex, NULL, surf->pixels, surf->pitch);
	}
	SDL_RenderClear(rend);
	SDL_RenderCopy(rend, tex, NULL, NULL);
	SDL_RenderPresent(rend);
}


/* ----- LED --------------------------------------------------------------- */


void led(bool on)
{
	static uint32_t last;
	uint32_t now;

	now = sim_ticks();
	if (now < last + 40)
		msleep(last + 40 - now);
	led_is_on = on;
	update();
	last = sim_ticks();
}


void led_toggle(void)
{
	led(!led_is_on);
}


/* ----- OLED display ------------------------------------------------------ */


const struct rect clip_all = {
	.x	= 0,
	.y	= 0,
	.w	= FB_X,
	.h	= FB_Y,
};


void display_clear(void)
{
	display_clear_rect(0, 0, FB_X - 1, FB_Y - 1);
}


void display_set(uint8_t x, uint8_t y)
{
	if (rotate)
		pixel(FB_X - 1 - x, FB_Y - 1 - y, WHITE);
	else
		pixel(x, y, WHITE);
}


void display_clr(uint8_t x, uint8_t y)
{
	if (rotate)
		pixel(FB_X - 1 - x, FB_Y - 1 - y, BLACK);
	else
		pixel(x, y, BLACK);
}


void display_blit_clip(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y, const struct rect *clip)
{
	int8_t x1 = x + w;
	int8_t y1 = y + h;
	uint8_t t;

	REQUIRE(x >= clip->x);	/* @@@ lazy */
	if (x1 > clip->x + clip->w)
		x1 = clip->x + clip->w;
	if (x >= x1)
		return;
	w = x1 - x;

	if (y < clip->y) {
		p += span * (clip->y - y);
		y = clip->y;
	}
	if (y1 > clip->y + clip->h)
		y1 = clip->y + clip->h;
	if (y >= y1)
		return;

	while (y != y1) {
		for (t = 0; t != w; t++) {
			if (p[t >> 3] & (1 << (t & 7)))
				display_set(x + t, y);
		}
		y++;
		p += span;
	}
}


void display_blit(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y)
{
	display_blit_clip(p, w, h, span, x, y, &clip_all);
}


void display_clear_rect(uint8_t xa, uint8_t ya, uint8_t xb, uint8_t yb)
{
	SDL_Rect r;

	if (rotate) {
		r.x = sim_xo + (FB_X - xb - 1) * sim_scale;
		r.y = sim_yo + (FB_Y - yb - 1) * sim_scale;
	} else {
		r.x = sim_xo + xa * sim_scale;
		r.y = sim_yo + ya * sim_scale;
	}
	r.w = (xb - xa + 1) * sim_scale;
	r.h = (yb - ya + 1) * sim_scale;

	SDL_FillRect(surf, &r, SDL_MapRGB(surf->format, BLACK));
}


void display_rotate(bool on)
{
	/*
	 * In Anelok, the OLED is rotated, so when the device is rotated,
	 * the display isn't and vice versa.
	 */
	rotate = !on;
}


bool display_update(void)
{
	update();
	return 0; /* the simulator updates its display very quickly */
}


void display_dim(bool dim)
{
	/* too lazy to implement real dimming */
}


void display_on(bool clear)
{
	if (clear)
		display_clear();
	display_update();
}


void display_off(void)
{
}


static void init_video(void)
{
	win = SDL_CreateWindow("Anelok", SDL_WINDOWPOS_UNDEFINED,
	    SDL_WINDOWPOS_UNDEFINED, surf->w, surf->h, 0);
	if (!win) {
		fprintf(stderr, "SDL_CreateWindow: %s\n", SDL_GetError());
		exit(1);
	}

	rend = SDL_CreateRenderer(win, -1, 0);
	if (!rend) {
		fprintf(stderr, "SDL_CreateRenderer: %s\n", SDL_GetError());
		exit(1);
	}

	/*
	 * Use SDL_CreateTexture because SDL_CreateTextureFromSurface does not
	 * detect the pixel format of the surface. Documentation doesn't say
	 * if it's even supposed to.
	 */
	tex = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBX8888,
	    SDL_TEXTUREACCESS_STREAMING, surf->w, surf->h);
	if (!tex) {
		fprintf(stderr, "SDL_CreateTexture: %s\n",
		    SDL_GetError());
		exit(1);
	}
}


static SDL_Surface *new_surface(int w, int h)
{
	SDL_Surface *s;

	s = SDL_CreateRGBSurface(0, w, h, 32,
	    0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
	if (s)
		return s;
	fprintf(stderr, "SDL_CreateRGBSurface: %s\n", SDL_GetError());
	exit(1);
}


static SDL_Surface *new_surface_from_img(const char *file)
{
	SDL_Surface *img, *s;

	img = SDL_LoadBMP(file);
	if (!img) {
		fprintf(stderr, "SDL_LoadBMP: %s\n", SDL_GetError());
		exit(1);
	}
	s = SDL_ConvertSurfaceFormat(img, SDL_PIXELFORMAT_RGBX8888, 0);
	SDL_FreeSurface(img);
	return s;
}


static SDL_Surface *new_surface_from_img_data(const uint8_t *data,
    unsigned bytes)
{
	SDL_Surface *img, *s;

	img = SDL_LoadBMP_RW(SDL_RWFromConstMem(data, bytes), 1);
	if (!img) {
		fprintf(stderr, "SDL_LoadBMP_RW: %s\n", SDL_GetError());
		exit(1);
	}
	s = SDL_ConvertSurfaceFormat(img, SDL_PIXELFORMAT_RGBX8888, 0);
	SDL_FreeSurface(img);
	return s;
}


void display_init(void)
{ 
	/*
	 * SDL 2.0 introduced the problem that, in a multi-head setup, the
	 * simulator window would always open on the first screen, irrespective
	 * of the value of DISPLAY. See also
	 * https://bugzilla.libsdl.org/show_bug.cgi?id=2192
	 *
	 * This was fixed with
	 * https://hg.libsdl.org/SDL/rev/3d2c0f659ad3
	 *
	 * Unfortunately, if SDL uses X RandR, the problem still exists.
	 * Explicitly asking SDL to refrain from using X RandR makes it fall
	 * back to the correct version.
	 */

	SDL_SetHint(SDL_HINT_VIDEO_X11_XRANDR, "0");

	if (SDL_Init(headless ? 0 : SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	if (bg_image)
		surf = new_surface_from_img(bg_image);
	else if (bg_image_data)
		surf = new_surface_from_img_data(bg_image_data, bg_image_bytes);
	else
		surf = new_surface(FB_X * sim_scale, FB_Y * sim_scale);

	if (bg_image_led)
		surf_led = new_surface_from_img(bg_image_led);
	else if (bg_image_led_data)
		surf_led = new_surface_from_img_data(bg_image_led_data,
		    bg_image_led_bytes);

	if (!headless)
		init_video();
}
