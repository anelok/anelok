/*
 * sim/backtrace-emcc.c - No stack trace for emscripten
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stddef.h>

#include "backtrace.h"


void backtrace_start(struct backtrace *ctx)
{
}


const void *backtrace_next(struct backtrace *ctx)
{
	return NULL;
}
