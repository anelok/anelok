The work in this directory and the directories below it is under the
copyright of various authors. Authorship is indicated either directly
in the files containing the respective work or in the file AUTHORS.


GPLv2+
------

Unless otherwise indicated, all code is distributed under the terms of
the GNU GENERAL PUBLIC LICENSE, Version 2:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

For your convenience, a copy of the complete license has been included
in the file COPYING.GPLv2.


LGPLv2.1+
---------

Some code may be considered "library" to be distributed under the more
lenient GNU LESSER GENERAL PUBLIC LICENSE, Version 2.1. Such code is
only licensed under the LGPL if explicitly marked so. By default, all
code is under the GPL.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

For your convenience, a copy of the complete LGPL has been included in
the file COPYING.LGPLv21.


CC-BY-SA 3.0
------------

All files that are not considered code, such as documents, schematic
symbols, board layout, case design, logos, and other graphical elements,
are distributed under the terms of the Creative Commons
Attribution-ShareAlike 3.0 Unported license (cc-by-sa 3.0):

    This work is licensed under the Creative Commons Attribution-Share
    Alike 3.0 Unported License. To view a copy of this license, visit
    http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California, 94105, USA.

The complete text of the license is available at the following
location:
http://creativecommons.org/licenses/by-sa/3.0/legalcode


More information about the respective license can be obtained at the
following locations:

http://www.gnu.org/licenses/gpl-2.0.html
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
http://creativecommons.org/licenses/by-sa/3.0/


3-clause BSD
------------

The following files originate from the Freescale KSDK and are covered
by a 3-clause BSD license:

fw/sdk/MKL46Z4.h: Freescale Semiconductor, Inc.
fw/sdk/core_cm0plus.h: ARM Ltd.
fw/sdk/core_cmInstr.h: ARM Ltd.


Freescale license:

**     Copyright (c) 1997 - 2014 Freescale Semiconductor, Inc.
**     All rights reserved.
**
**     Redistribution and use in source and binary forms, with or without modification,
**     are permitted provided that the following conditions are met:
**
**     o Redistributions of source code must retain the above copyright notice, this list
**       of conditions and the following disclaimer.
**
**     o Redistributions in binary form must reproduce the above copyright notice, this
**       list of conditions and the following disclaimer in the documentation and/or
**       other materials provided with the distribution.
**
**     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**       contributors may be used to endorse or promote products derived from this
**       software without specific prior written permission.
**
**     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**     http:                 www.freescale.com
**     mail:                 support@freescale.com


ARM license:

Copyright (c) 2009 - 2014 ARM LIMITED

   All rights reserved.
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
   - Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   - Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
   - Neither the name of ARM nor the names of its contributors may be used
     to endorse or promote products derived from this software without
     specific prior written permission.
   *
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.


FatFs license
-------------

The FatFs code in fw/fatfs/ is covered by the following license:

 FatFs module is an open source software to implement FAT file system to
 small embedded systems. This is a free software and is opened for education,
 research and commercial developments under license policy of following trems.

  Copyright (C) 2014, ChaN, all right reserved.

 * The FatFs module is a free software and there is NO WARRANTY.
 * No restriction on use. You can use, modify and redistribute it for
   personal, non-profit or commercial product UNDER YOUR RESPONSIBILITY.
 * Redistributions of source code must retain the above copyright notice.


Public domain
-------------

The TweetNaCl code in crypter/ is in the public domain.
http://tweetnacl.cr.yp.to/


CC0 / Public domain
-------------------

The uNaCl code in fw/db/crypto/unacl/ is in the public domain (some says CC0).
http://munacl.cryptojedi.org/
