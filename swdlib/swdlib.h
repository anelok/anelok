/*
 * swdlib/swdlib.h - SWD protocol driver
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SWDLIB_H
#define	SWDLIB_H

#include <stdbool.h>
#include <stdint.h>

#if 0
#define DPRINTF(fmt, ...)	fprintf(stderr, fmt, ##__VA_ARGS__)
#else
#define DPRINTF(fmt, ...)
#endif


enum swd_response {
	SWD_OK		= 0,
	SWD_WAIT,
	SWD_FAULT,
	SWD_ERROR,
	SWD_PARITY,
	SWD_TIMEOUT,
};


struct swd_ops {
	void (*send)(void *ctx, uint32_t data, uint8_t bits);
	uint32_t (*recv)(void *ctx, uint8_t bits);
	void (*reset)(void *ctx, bool active);
	void (*sleep_1s)(void *ctx);
	void (*report)(const char *fmt, ...)
	    __attribute__((format (printf, 1, 2)));
};

struct swd {
	const struct swd_ops *ops;
	void *ctx;
	uint8_t ap;		/* currently selected AP */
	uint8_t ap_bank;	/* currently selected bank */
};


#define swd_report(fmt, ...)	swd->ops->report(fmt, ##__VA_ARGS__)

void swd_report_error(struct swd *swd, enum swd_response res);

enum swd_response swd_ap_read(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t *val);
enum swd_response swd_ap_write(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t val);

bool swd_read_8(struct swd *swd, volatile const uint8_t *addr, uint8_t *result);
bool swd_write_8(struct swd *swd, volatile uint8_t *addr, uint8_t val);

bool swd_read_32(struct swd *swd, volatile const uint32_t *addr,
    uint32_t *result);
bool swd_write_32(struct swd *swd, volatile uint32_t *addr, uint32_t val);

void swd_idle(struct swd *swd, uint16_t cycles);

/*
 * A session is as follows:
 *
 * - swd_setup
 * - any operations preceding DAP activation (optional)
 * - swd_init or custom DAP activation
 * - swd_open
 * - any operations preceding read/write operations (optional)
 *
 * - read/write operations (optional)
 *
 * - swd_release (optional)
 * - swd_close
 */

void swd_setup(struct swd *swd, const struct swd_ops *ops, void *ctx);
bool swd_init(struct swd *swd);
bool swd_open(struct swd *swd);
bool swd_release(struct swd *swd); /* release from reset */
void swd_close(struct swd *swd);

void swd_sleep_1s(struct swd *swd);

#endif /* !SWDLIB_H */
