/*
 * swdlib/kinetis.c - Kinetis-specific SWD functions
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>

//#include "dap.h"
#include "swdlib.h"
#include "../fw/base/regs.h"
#include "kinetis.h"


/* ----- Identification ---------------------------------------------------- */


bool kinetis_print_uid(struct swd *swd)
{
	uint32_t mid_high, mid_low, low;

	if (!swd_read_32(swd, &SIM_UIDMH, &mid_high))
		return 0;
	if (!swd_read_32(swd, &SIM_UIDML, &mid_low))
		return 0;
	if (!swd_read_32(swd, &SIM_UIDL, &low))
		return 0;
	swd_report("UID %04x-%08x-%08x\n", mid_high & 0xffff, mid_low, low);
        return 1;
}


bool kinetis_identify_soc(struct swd *swd)
{
	uint32_t id, fcfg1;
	uint8_t v;

	/* SDID */

	if (!swd_read_32(swd, &SIM_SDID, &id))
		return 0;
	swd_report("SDID 0x%08x: ", (unsigned) id);

	/* FCFG1 */

	if (!swd_read_32(swd, &SIM_FCFG1, &fcfg1))
		return 0;

	/* Kinetis series */

	v = (id & SIM_SDID_SERIESID_MASK) >> SIM_SDID_SERIESID_SHIFT;
	switch (v) {
	case 1:
		swd_report("Kinetis KL");
		break;
	default:
		swd_report("???\n");
		return 0;
	}

	/* Kinetis family */

	v = (id & SIM_SDID_FAMID_MASK) >> SIM_SDID_FAMID_SHIFT;
	switch (v) {
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
		swd_report("%u", v);
		break;
	default:
		swd_report("?");
		break;
	}

	/* Kinetis sub-family */

	v = (id & SIM_SDID_SUBFAMID_MASK) >> SIM_SDID_SUBFAMID_SHIFT;
	if (v <= 7)
		swd_report("%u", v);
	else
		swd_report("?");

	/* device revision */

	v = (id & SIM_SDID_REVID_MASK) >> SIM_SDID_REVID_SHIFT;
	swd_report(" rev %u", v);

	/* die number */

	v = (id & SIM_SDID_DIEID_MASK) >> SIM_SDID_DIEID_SHIFT;
	swd_report(".%u", v);

	/* program Flash size */

	v = (fcfg1 & SIM_FCFG1_PFSIZE_MASK) >> SIM_FCFG1_PFSIZE_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 8 kB");
		break;
	case 1:
		swd_report(", 16 kB");
		break;
	case 3:
		swd_report(", 32 kB");
		break;
	case 5:
		swd_report(", 64 kB");
		break;
	case 7:
		swd_report(", 128 kB");
		break;
	case 9:
		swd_report(", 256 kB");
		break;
	default:
		swd_report(", #%x", v);
	}
	swd_report(" Flash");

	/* RAM size */

	v = (id & SIM_SDID_SRAMSIZE_MASK) >> SIM_SDID_SRAMSIZE_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 0.5 kB RAM");
		break;
	default:
		swd_report(", %u kB RAM", 1 << (v - 1));
		break;
	}

	/* pincount */

	v = (id & SIM_SDID_PINID_MASK) >> SIM_SDID_PINID_SHIFT;
	switch (v) {
	case 0:
		swd_report(", 16-pin");
		break;
	case 1:
		swd_report(", 24-pin");
		break;
	case 2:
		swd_report(", 32-pin");
		break;
	case 3:
		swd_report(", 36-pin");
		break;
	case 4:
		swd_report(", 48-pin");
		break;
	case 5:
		swd_report(", 64-pin");
		break;
	case 6:
		swd_report(", 80-pin");
		break;
	case 8:
		swd_report(", 100-pin");
		break;
	case 11:
		swd_report(", WLCSP");
		break;
	default:
		swd_report("??? package");
		break;
	}

	swd_report("\n");
	return 1;
}
