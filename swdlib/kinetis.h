/*
 * swdlib/kinetis.h - Kinetis-specific SWD functions
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef KINETIS_H
#define	KINETIS_H

#include "swdlib.h"

bool kinetis_print_uid(struct swd *swd);
bool kinetis_identify_soc(struct swd *swd);

#endif /* !KINETIS_H */
