/*
 * swdlib/nrfids.h - nFF51 chip IDs
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef NRFIDS_H
#define	NRFIDS_H

#include <stdint.h>


struct nrfid {
	const char *pkg;	/* NULL at last entry */
	const uint16_t *ids;	/* last entry is 0 */
};

extern const struct nrfid nrfids[];

#endif /* !NRFIDS_H */
