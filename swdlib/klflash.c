/*
 * swdlib/klflash.c - KL2x Flash programming
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "../fw/base/regs.h"

#include "dap.h"
#include "swdlib.h"
#include "klflash.h"


/* ----- Flash mass erase -------------------------------------------------- */


bool kl_mass_erase(struct swd *swd)
{
	enum swd_response res;
	uint32_t val;

	res = swd_ap_read(swd, MDM_AP, MDM_AP_CONTROL, &val);
	if (res)
		goto fail;
	val |= MDM_AP_CONTROL_Mass_Erase;
	res = swd_ap_write(swd, MDM_AP, MDM_AP_CONTROL, val);
	if (res)
		goto fail;

	/*
	 * Magic idle sequence.
	 *
	 * Results for a KL25 in 32-QFN with 128 kB Flash:
	 * - at 60k idle cycles, mass-erase never works when secure.
	 * - at 61k, it works on every second try.
	 * - at 65k, it seems to work all the time.
	 * Let's keep a healthy safety margin.
	 *
	 * Update:
	 * KL25 2N97F and KL26 1N15J need just one idle cycle. The wait is
	 * probably t_{ersall}(max) = 650 ms.
	 *
	 * Without idle, the operation fails silently. Without the sleep, the
	 * acknowledgement never makes it.
	 */
	swd_idle(swd, 1);
	swd_sleep_1s(swd);

	res = swd_ap_read(swd, MDM_AP, MDM_AP_STATUS, &val);
	if (res)
		goto fail;
	if (!(val & MDM_AP_STATUS_Mass_Erase_Ack)) {
		swd_report("mass erase not acknowledged\n");
		return 0;
	}
	do {
		res = swd_ap_read(swd, MDM_AP, MDM_AP_CONTROL, &val);
		if (res)
			goto fail;
	} while (res & MDM_AP_CONTROL_Mass_Erase);
	return 1;

fail:
	swd_report_error(swd, res);
	return 0;
}


/* ----- Chip protection check --------------------------------------------- */


bool kl_flash_security(struct swd *swd, bool *can_erase, bool *secure,
    bool *backdoor)
{
	enum swd_response res;
	uint32_t val;

	res = swd_ap_read(swd, MDM_AP, MDM_AP_STATUS, &val);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	if (can_erase)
		*can_erase = val & MDM_AP_STATUS_Mass_Erase_Enable;
	if (secure)
		*secure = val & MDM_AP_STATUS_System_Security;
	if (backdoor)
		*backdoor = val & MDM_AP_STATUS_Backdoor_Enable;
	return 1;
}


/* ----- Kinetis Flash programming ----------------------------------------- */


#define	WFB(r, v)	swd_write_8(swd, &(r), (v))
#define	RFB(r, v)	swd_read_8(swd, &(r), (v))
#define	WFW(r, v)	swd_write_32(swd, (volatile uint32_t *) &(r), (v))


static bool kl_flash_ready(struct swd *swd, bool *ready)
{
	uint8_t s;
	uint8_t i;
	uint32_t v;

	if (!RFB(FTFA_FSTAT, &s))
		return 0;
	if (!(s & FTFA_FSTAT_CCIF_MASK)) {
		*ready = 0;
		return 1;
	}
	if (!(s & 0x7f)) {
		*ready = 1;
		return 1;
	}
	for (i = 1; i != 0x80; i <<= 1)
		if (s & i)
			switch (i) {
			case FTFA_FSTAT_MGSTAT0_MASK:
				swd_report("flash error\n");
				break;
			case FTFA_FSTAT_FPVIOL_MASK:
				swd_report("protection violation\n");
				break;
			case FTFA_FSTAT_ACCERR_MASK:
				swd_report("access error\n");
				break;
			case FTFA_FSTAT_RDCOLERR_MASK:
				swd_report("read collision\n");
				break;
			default:
				break;
			}
	for (i = 0; i != 16; i += 4) {
		if (!swd_read_32(swd, (void *) (0x40020000 + i), &v))
			return 0;
		swd_report("%x: %08x\n", i, (unsigned) v);
	}
	return 0;
}


static bool kl_flash_run(struct swd *swd)
{
	bool ok, ready = 0;

//	WF(FTFA_FSTAT, 0x7f);
	ok = WFB(FTFA_FSTAT, FTFA_FSTAT_CCIF_MASK);
	while (ok && !ready)
		ok = ok && kl_flash_ready(swd, &ready);
	return ok;
}


enum {
	PGM4	= 0x06,
	PGMCHK	= 0x02,
};

enum {
	MARGIN_NORMAL	= 0x00,
	MARGIN_USER	= 0x01,
	MARGIN_FACTORY	= 0x02,
};


static bool kl_write_flash(struct swd *swd, uint32_t addr, uint32_t data)
{
	bool ok;

	ok = WFW(FTFA_FCCOB3, addr | PGM4 << 24);
	ok = ok && WFW(FTFA_FCCOB7, data);
	ok = ok && kl_flash_run(swd);
	return ok;
}


#if 0
static bool kl_check_flash(struct swd *swd, uint32_t addr, uint32_t data)
{
	bool ok;

	ok = WFW(FTFA_FCCOB3, addr | PGMCHK << 24);
	ok = ok && WFW(FTFA_FCCOB7, MARGIN_USER << 24);
	ok = ok && WFW(FTFA_FCCOBB, data);
	ok = ok && kl_flash_run(swd);
	return ok;
}
#endif


bool kl_program_flash(struct swd *swd,
    uint32_t addr, const uint32_t *buf, uint32_t words,
    void (*progress)(void *user, bool verify, uint16_t pos), void *user)
{
	bool ready;
	uint32_t i, got;

	do {
		if (!kl_flash_ready(swd, &ready))
			return 0;
	} while (!ready);

	for (i = 0; i != words; i++) {
		if (progress)
			progress(user, 0, i);
		if (!kl_write_flash(swd, addr + 4 * i, buf[i])) {
			swd_report("at 0x%08x\n", (unsigned) addr + 4 * i);
			return 0;
		}
	}

	if (progress)
		progress(user, 0, words);
	for (i = 0; i != words; i++) {
		if (progress)
			progress(user, 1, i);
		if (!swd_read_32(swd, (uint32_t *) addr + i, &got))
			return 0;
		if (got != buf[i]) {
			swd_report("0x%08x: expected 0x%08x got 0x%08x\n",
			    (unsigned) (addr + 4 * i), (unsigned) buf[i],
			    (unsigned) got);
			return 0;
		}
//		kl_check_flash(swd, addr+(i << 2), buf[i]);
	}
	if (progress)
		progress(user, 1, words);
	return 1;
}


bool kl_read_flash(struct swd *swd,
    uint32_t addr, uint32_t *buf, uint32_t words)
{
	uint32_t i = 0;

	for (i = 0; i != words; i++)
		if (!swd_read_32(swd, (uint32_t *) addr + i, buf + i))
			return 0;
	return 1;
}
