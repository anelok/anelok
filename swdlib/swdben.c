/*
 * swdlib/swdben.c - Ben Nanonote UBB driver for SWD
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h> /* for sleep, usleep */

#include <ubb/ubb.h>
#include "swdlib.h"
#include "ben.h"
#include "swdben.h"


static inline void ben_swd_pulse(struct ben_swd *ben)
{
	SET(ben->clk);
	CLR(ben->clk);
}


static void ben_swd_send(void *ctx, uint32_t data, uint8_t bits)
{
	struct ben_swd *ben = ctx;
	uint8_t i;

	DPRINTF(">>> 0x%x, %u\n", data, bits);
	if (ben->input) {
		ben_swd_pulse(ben);
		DPRINTF("->-\n");
		OUT(ben->dio);
		ben->input = 0;
	}
	for (i = 0; i != bits; i++) {
		if ((data >> i) & 1)
			SET(ben->dio);
		else
			CLR(ben->dio);
		ben_swd_pulse(ben);
	}
}


static uint32_t ben_swd_recv(void *ctx, uint8_t bits)
{
	struct ben_swd *ben = ctx;
	uint32_t val = 0;
	uint8_t i;

	if (!ben->input) {
		IN(ben->dio);
		DPRINTF("-<-\n");
		ben_swd_pulse(ben);
		ben->input = 1;
	}
	for (i = 0; i != bits; i++) {
		val |= PIN(ben->dio) << i;
		ben_swd_pulse(ben);
	}
	DPRINTF("<<< 0x%x, %u\n", val, bits);
	return val;
}


static void ben_swd_reset(void *ctx, bool active)
{
	struct ben_swd *ben = ctx;

	DPRINTF("reset %u\n", active);
	if (active) {
		OUT(ben->reset);
		usleep(1);
	} else {
		IN(ben->reset);
		usleep(10); /* takes about 2.5 us to raise */
	}
}


static void sleep_1s(void *ctx)
{
	sleep(1);
}


static const struct swd_ops ben_swd_ops = {
	.send		= ben_swd_send,
	.recv		= ben_swd_recv,
	.reset		= ben_swd_reset,
	.sleep_1s	= sleep_1s,
	.report		= report,
};


bool ben_swd_ubb_open(uint32_t reset, uint32_t dio, uint32_t clk)
{
	if (ubb_open(~(reset | dio | clk))) {
		fprintf(stderr, "ubb_open failed\n");
		return 0;
	}

	/* drain power from target */

	ubb_power(0);

	CLR(reset);
	CLR(clk);
	CLR(dio);

	OUT(reset);
	OUT(clk);
	OUT(dio);

	sleep(1);

	/*
	 * Set pull-up on nRESET. The two states of nRESET are strong drive to
	 * ground and weak pull to VDD. This allows the target to command its
	 * own wired-or resets without the risk of having to fight the
	 * programmer.
	 */
	PDPULLC = reset;

	/* allow Ben's C45 to charge */

	SET(clk);
	SET(dio);

	sleep(1);

	/* power up */

	ubb_power(1);

	usleep(200 * 1000);

	return 1;
}


bool ben_swd_open(struct ben_swd *ben, struct swd *swd,
    uint32_t reset, uint32_t dio, uint32_t clk,
    bool (*init)(struct swd *swd),
    bool (*open)(struct swd *swd))
{
	ben->reset = reset;
	ben->dio = dio;
	ben->clk = clk;

	if (!ben_swd_ubb_open(reset, dio, clk))
		return 0;

	ben->input = 0;

	ben->swd = swd;

	swd_setup(swd, &ben_swd_ops, ben);
	if (!init(swd))
		return 0;
	if (!open(swd))
		return 0;

	return 1;
}


bool ben_swd_release(struct ben_swd *ben)
{
	if (swd_release(ben->swd))
		return 1;
	fprintf(stderr, "swd_release failed\n");
	return 0;
}


void ben_swd_close(struct ben_swd *ben)
{
	swd_close(ben->swd);
//	ubb_power(0);
	ubb_close(ben->clk);
}
