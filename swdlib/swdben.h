/*
 * swdlib/swdben.h - Ben Nanonote UBB driver for SWD
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SWDBEN_H
#define	SWDBEN_H


#include <stdint.h>

#include "swdlib.h"


struct ben_swd {
	uint32_t reset, dio, clk;
	bool input;
	struct swd *swd;
};


bool ben_swd_ubb_open(uint32_t reset, uint32_t dio, uint32_t clk);
bool ben_swd_open(struct ben_swd *ben, struct swd *swd,
    uint32_t reset, uint32_t dio, uint32_t clk,
    bool (*init)(struct swd *swd), bool (*open)(struct swd *swd));
bool ben_swd_release(struct ben_swd *ben);
void ben_swd_close(struct ben_swd *ben);

#endif /* !SWDBEN_H */
