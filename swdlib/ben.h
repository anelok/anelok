/*
 * swdlib/ben.h - Ben Nanonote UBB driver
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BEN_H
#define	BEN_H

#include <stdbool.h>
#include <stdint.h>


#ifndef DPRINTF
#if 0
#define DPRINTF(fmt, ...)       fprintf(stderr, fmt, ##__VA_ARGS__)
#else
#define DPRINTF(fmt, ...)
#endif
#endif


enum protection {
	prot_none,
	prot_rev,	/* reversible */
	prot_irrev,	/* irreversible */
};


struct target_ops {
	void (*assign)(const char *s);
	bool (*identify)(void);
	bool (*erase)(void);
	bool (*write)(const char *file, uint32_t base, enum protection prot);
	void *(*read)(uint32_t base, uint32_t *size);
	void (*run)(void);
};


extern const struct target_ops cc_target_ops;
extern const struct target_ops kl_target_ops;
extern const struct target_ops nrf_target_ops;


void report(const char *fmt, ...);

typedef bool (*flash_fn)(void *dsc, uint32_t addr,
    const uint32_t *buf, uint32_t words, enum protection prog);

bool do_flash(void *dsc, const char *name, flash_fn fn, uint32_t base,
    enum protection prot);

#endif /* !BEN_H */
