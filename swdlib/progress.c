/*
 * swdlib/progress.c - Progress bar
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "swdlib.h"
#include "progress.h"


#define	BAR_WIDTH	78


void progress(void *user, bool verify, uint16_t pos)
{
	struct progress *ctx = user;
	struct swd *swd = ctx->swd;

	if (!pos)
		ctx->bar = 0;
	if (pos == ctx->words) {
		swd_report("|%c", verify ? '\n' : '\r');
		return;
	}
	if (pos / ctx->div == ctx->bar) {
		swd_report("%c\b", "/-\\|"[pos & 3]);
	} else {
		swd_report(verify ? "#" : "-");
		ctx->bar++;
	}
}


void progress_init(struct progress *ctx, struct swd *swd, uint32_t words)
{
	ctx->swd = swd;
	ctx->words = words;
	ctx->div = words < BAR_WIDTH - 1 ? 1 :
	    (words + BAR_WIDTH - 1 - 1) / (BAR_WIDTH - 1);
	ctx->bar = 0;
}
