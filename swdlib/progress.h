/*
 * swdlib/progress.c - Progress bar
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef PROGRESS_H
#define	PROGRESS_H

#include <stdint.h>

#include "swdlib.h"


struct progress {
	struct swd *swd;
	uint32_t words;
	uint8_t div;
	uint8_t bar;
};


void progress(void *user, bool verify, uint16_t pos);
void progress_init(struct progress *ctx, struct swd *swd, uint32_t words);

#endif /* !PROGRESS_H */
