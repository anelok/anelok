/*
 * swdlib/ben.c - Ben Nanonote UBB driver
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ben.h"


/* ----- Diagnostics (same for CC2543, KL2x, and nRF) ---------------------- */


void report(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fflush(stderr);
}


/* ----- Platform-independent file load and flash -------------------------- */


static void *read_file(const char *name, uint32_t *bytes)
{
	int fd;
	struct stat st;
	void *buf;
	off_t size;
	ssize_t got;

	fd = open(name, O_RDONLY);
	if (fd < 0) {
		perror(name);
		return NULL;
	}
	if (fstat(fd, &st) < 0) {
		perror(name);
		return NULL;
	}
	*bytes = st.st_size;
	size = (st.st_size + 3) & ~3;
	buf = malloc(size);
	if (!buf) {
		perror("malloc");
		return NULL;
	}
	got = read(fd, buf, st.st_size);
	if (got < 0) {
		perror(name);
		return NULL;
	}
	if (got != *bytes) {
		fprintf(stderr, "%s: short read %u < %u\n",
		    name, (unsigned) got, (unsigned) *bytes);
		return NULL;
	}
	return buf;
}


bool do_flash(void *dsc, const char *name, flash_fn fn, uint32_t base,
    enum protection prot)
{
	void *buf;
	uint32_t bytes;

	buf = read_file(name, &bytes);
	if (!buf)
		return 0;
	return fn(dsc, base, (const uint32_t *) buf, (bytes + 3) >> 2, prot);
}


/* ----- Verify Flash content ---------------------------------------------- */


static bool verify(const struct target_ops *ops, const char *name,
    uint32_t base)
{
	uint32_t *file_buf, *flash_buf;
	uint32_t file_bytes, flash_bytes;
	unsigned i;

	file_buf = read_file(name, &file_bytes);
	if (!file_buf)
		return 0;
	
	flash_buf = ops->read(base, &flash_bytes);
	if (!flash_buf)
		return 0;

	if (flash_bytes < file_bytes) {
		fprintf(stderr,
		    "file %s is longer (%u bytes) than flash (%u)\n",
		    name, (unsigned) file_bytes, (unsigned) flash_bytes);
		return 0;
	}
	for (i = 0; i != (file_bytes + 3) >> 2; i++)
		if (file_buf[i] != flash_buf[i]) {
			fprintf(stderr, "0x%08x: expected 0x%08x got 0x%08x\n",
			    base + (i << 2), (unsigned) file_buf[i],
			    (unsigned) flash_buf[i]);
			return 0;
		}
	return 1;
}


/* ----- Main -------------------------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [common] [-P|-I] [file.bin [base]]\n"
"       %s [common] -d\n"
"       %s [common] -r\n"
"       %s [common] -e\n"
"       %s [common] -v file.bin [base]\n\n"
"Common items:\n"
"  [signal=pin] [-c]\n\n"
"  -c  target is CC25xx (default: KL2x)\n"
"  -n  target is nRFxx (default: KL2x)\n"
"  -d  dump Flash content (in binary)\n"
"  -e  erase whole Flash\n"
"  -r  reset and run target\n"
"  -v  compare Flash content with file\n"
"  -P  file sets reversible chip protection\n"
"  -I  file sets irreversible chip protection\n"
	    , name, name, name, name, name);
	exit(1);
}


static void not_supported(void)
{
	fprintf(stderr, "operation not supported by target driver\n");
	exit(1);
}


int main(int argc, char *const *argv)
{
	const struct target_ops *ops = &kl_target_ops;
	enum {
		mode_default = 0,
		mode_dump,
		mode_erase,
		mode_run,
		mode_verify,
	} mode = mode_default;
	enum protection prot = prot_none;
	int c;
	bool ok = 1;
	const void *buf;
	char *end;
	uint32_t base = 0;
	uint32_t bytes;

	while ((c = getopt(argc, argv, "cndervPI")) != EOF)
		switch (c) {
		case 'c':
			ops = &cc_target_ops;
			break;
		case 'n':
			ops = &nrf_target_ops;
			break;
		case 'd':
			if (mode)
				usage(*argv);
			mode = mode_dump;
			break;
		case 'e':
			if (mode)
				usage(*argv);
			mode = mode_erase;
			break;
		case 'r':
			if (mode)
				usage(*argv);
			mode = mode_run;
			break;
		case 'v':
			if (mode)
				usage(*argv);
			mode = mode_verify;
			break;
		case 'P':
			if (prot != prot_none)
				usage(*argv);
			prot = prot_rev;
			break;
		case 'I':
			if (prot != prot_none)
				usage(*argv);
			prot = prot_irrev;
			break;
		default:
			usage(*argv);
		}

	while (argc != optind) {
		if (!strchr(argv[optind], '='))
			break;
		if (!ops->assign)
			not_supported();
		ops->assign(argv[optind]);
		optind++;
	}

	switch (argc - optind) {
	case 2:
		base = strtoul(argv[optind + 1], &end, 0);
		if (*end)
			usage(*argv);
		/* fall through */
	case 1:
		if (mode != mode_default && mode != mode_verify)
			usage(*argv);
		break;
	case 0:
		if (mode == mode_verify)
			usage(*argv);
		break;
	default:
		usage(*argv);
	}

	switch (mode) {
	case mode_default:
		if (argc == optind) {
			if (!ops->identify)
				not_supported();
			ok = ops->identify();
		} else {
			if (!ops->write)
				not_supported();
			ok = ops->write(argv[optind], base, prot);
		}
		break;
	case mode_dump:
		if (!ops->read)
			not_supported();
		buf = ops->read(0, &bytes);
		if (!buf)
			return 1;
		fwrite(buf, 1, bytes, stdout);
		break;
	case mode_verify:
		if (!ops->read || !ops->write)
			not_supported();
		ok = verify(ops, argv[optind], base);
		break;
	case mode_erase:
		if (!ops->erase)
			not_supported();
		ok = ops->erase();
		break;
	case mode_run:
		if (!ops->run)
			not_supported();
		ops->run();
		break;
	default:
		abort();
	}

	return !ok;
}
