/*
 * swdlib/brflash.c - nRF51 Flash programming
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef NRFLASH_H
#define NRFLASH_H

#include <stdbool.h>
#include <stdint.h>

#include "swdlib.h"


bool nrf_erase_all(struct swd *swd);

bool nrf_program_flash(struct swd *swd,
    uint32_t addr, const uint32_t *buf, uint32_t words,
    void (*progress)(void *user, bool verify, uint16_t pos), void *user);
bool nrf_read_flash(struct swd *swd,
    uint32_t addr, uint32_t *buf, uint32_t words);

#endif /* !NRFLASH_H */
