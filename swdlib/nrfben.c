/*
 * swdlib/nrfben.c - Ben Nanonote UBB driver for SWD (nRF)
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> /* for usleep */

#include "nrf51.h"
#include "nrf51_bitfields.h"

#include <ubb/ubb.h>
#include "ubbmap.h"
#include "progress.h"
#include "swdlib.h"
#include "nrfids.h"
#include "nrflash.h"
#include "swdben.h"
#include "ben.h"


static struct ubbmap map[] = {
	{ "dio",	UBB_DAT1 },
	{ "clk",	UBB_DAT0 },
	{ NULL }
};


#define	NRF_RESET	SWD_DIO
#define	SWD_DIO		ubbmap_pin(map, "dio")
#define	SWD_CLK		ubbmap_pin(map, "clk")


/* ----- Operations backend ------------------------------------------------ */


static bool nrf_program(struct swd *swd, uint32_t addr,
    const uint32_t *buf, uint32_t words, enum protection prot)
{
	struct progress ctx;

	if (!nrf_erase_all(swd))
		return 0;
	progress_init(&ctx, swd, words);
	return nrf_program_flash(swd, addr, buf, words, progress, &ctx);
}


static void *read_flash(struct swd *swd, uint32_t base, uint32_t flash_words)
{
	uint32_t words;
	uint32_t *buf;

	words = flash_words - (base >> 2);
	buf = calloc(words, sizeof(uint32_t));
	if (!buf) {
		perror("calloc");
		return NULL;
	}

	if (!nrf_read_flash(swd, base, buf, words)) {
		free(buf);
		return NULL;
	}

	return buf;
}


bool nrf_identify(struct swd *swd, uint32_t *flash_words)
{
	uint32_t config;
	uint16_t hwid;
	const struct nrfid *pkg;
	const uint16_t *id;
	uint32_t code_page_bytes, code_pages;
	uint32_t ram_block_bytes, ram_blocks;
	uint32_t uid_low, uid_high;
	uint32_t mac_low, mac_high, addr_type;

	if (!swd_read_32(swd, &NRF_FICR->CONFIGID, &config))
		return 0;
	hwid = (config & FICR_CONFIGID_HWID_Msk) >> FICR_CONFIGID_HWID_Pos;
	for (pkg = nrfids; pkg->pkg; pkg++)
		for (id = pkg->ids; *id; id++)
			if (*id == hwid)
				goto out;
out:
	swd_report("HWID 0x%04x (%s)\n", (unsigned) hwid,
	    pkg->pkg ? pkg->pkg : "???");

	if (!swd_read_32(swd, &NRF_FICR->CODEPAGESIZE, &code_page_bytes))
		return 0;
	if (!swd_read_32(swd, &NRF_FICR->CODESIZE, &code_pages))
		return 0;
	swd_report("Flash %u bytes * %u pages = %u bytes\n",
	    (unsigned) code_page_bytes, (unsigned) code_pages,
	    (unsigned) (code_page_bytes * code_pages));
	if (flash_words)
		*flash_words = (code_page_bytes / 4) * code_pages;

	if (!swd_read_32(swd, &NRF_FICR->SIZERAMBLOCKS, &ram_block_bytes))
		return 0;
	if (!swd_read_32(swd, &NRF_FICR->NUMRAMBLOCK, &ram_blocks))
		return 0;
	swd_report("RAM %u bytes * %u blocks = %u bytes\n",
	    (unsigned) ram_block_bytes, (unsigned) ram_blocks,
	    (unsigned) (ram_block_bytes * ram_blocks));

	if (!swd_read_32(swd, &NRF_FICR->DEVICEID[0], &uid_low))
		return 0;
	if (!swd_read_32(swd, &NRF_FICR->DEVICEID[1], &uid_high))
		return 0;
	swd_report("UID %08x-%08x\n", (unsigned) uid_high, (unsigned) uid_low);

	if (!swd_read_32(swd, &NRF_FICR->DEVICEADDR[0], &mac_low))
		return 0;
	if (!swd_read_32(swd, &NRF_FICR->DEVICEADDR[1], &mac_high))
		return 0;
	if (!swd_read_32(swd, &NRF_FICR->DEVICEADDRTYPE, &addr_type))
		return 0;
	swd_report("MAC %02x:%02x:%02x:%02x:%02x:%02x (%s)\n",
	    ((unsigned) mac_high >> 8) & 0xff,
	    (unsigned) mac_high & 0xff,
	    ((unsigned) mac_low >> 24) & 0xff,
	    ((unsigned) mac_low >> 16) & 0xff,
	    ((unsigned) mac_low >> 8) & 0xff,
	    (unsigned) mac_low & 0xff,
	    addr_type & FICR_DEVICEADDRTYPE_DEVICEADDRTYPE_Msk ?
	    "random" : "public");
	
	return 1;
}


/* ----- ben_swd_open wrapper ---------------------------------------------- */


static bool nrf_init(struct swd *swd)
{
	uint8_t i;

	/*
	 * nRF51 needs a total of 150 cycles with SWDIO = 1: 100 to wake up
	 * the DAP plus the 50 required by ARM.
	 *
	 * See section 11.1.2 in "nRF51 Series Reference Manual" Version 3.0
	 *  (dsv nrfrm)
	 */
	for (i = 0; i != 4; i++)
		swd->ops->send(swd->ctx, 0xffffffff, 25);
	return swd_init(swd);
}


static bool nrf_open(struct ben_swd *ben, struct swd *swd)
{
	return ben_swd_open(ben, swd, NRF_RESET, SWD_DIO, SWD_CLK,
	    nrf_init, swd_open);
}


/* ----- Target operations ------------------------------------------------- */


static void nrf_op_assign(const char *s)
{
	ubbmap_assign(map, s);
}


static bool nrf_op_identify(void)
{
	struct ben_swd ben;
	struct swd swd;
	bool ok = 0;

	if (!nrf_open(&ben, &swd))
		return 0;
#if 0
	bool can_erase, secure, backdoor;

	if (!kl_flash_security(&swd, &can_erase, &secure, &backdoor))
		goto out;
	fprintf(stderr, "Mass-erase: %s, secure: %s, backdoor: %s\n",
	    can_erase ? "yes" : "no", secure ? "yes" : "no",
	    backdoor ? "yes" : "no");
	if (secure) {
		ok = 1;
		goto out;
	}
#endif
	if (!ben_swd_release(&ben))
		return 0;
	ok = nrf_identify(&swd, NULL);

//out:
	ben_swd_close(&ben);
	return ok;
}


static bool nrf_op_erase(void)
{
	struct ben_swd ben;
	struct swd swd;
	bool ok;

	if (!nrf_open(&ben, &swd))
		return 0;
	ok = nrf_erase_all(&swd);
	ben_swd_close(&ben);
	return ok;
}


static bool nrf_op_write(const char *file, uint32_t base, enum protection prot)
{
	struct ben_swd ben;
	struct swd swd;
	bool ok = 0;

	if (!nrf_open(&ben, &swd))
		return 0;

	if (!ben_swd_release(&ben))
		return 0;
	if (!nrf_identify(&swd, NULL))
		goto out;

	ok = do_flash(&swd, file, (flash_fn) nrf_program, base, prot);
out:
	ben_swd_close(&ben);

	return ok;
}


static void *nrf_op_read(uint32_t base, uint32_t *size)
{
	struct ben_swd ben;
	struct swd swd;
	void *res = NULL;
	uint32_t flash_words;

	if (!nrf_open(&ben, &swd))
		return NULL;
	if (!ben_swd_release(&ben))
		return NULL;
	if (!nrf_identify(&swd, &flash_words))
		goto out;

	if (base > (flash_words << 2)) {
		fprintf(stderr, "base %u > bytes %u\n",
		    (unsigned) base, flash_words << 2);
		return NULL;
	}

	res = read_flash(&swd, base, flash_words);
	*size = flash_words * 4 - base;

out:
	ben_swd_close(&ben);

	return res;
}


static void nrf_op_run(void)
{
	char ch;

	ben_swd_ubb_open(NRF_RESET, SWD_DIO, SWD_CLK);
	SET(NRF_RESET);
	usleep(100 * 1000);
	CLR(NRF_RESET);
	usleep(100 * 1000);
	SET(NRF_RESET);
	fprintf(stderr, "Press [Enter] to exit\n");
	read(0, &ch, 1);
	ubb_close(SWD_CLK);
}


const struct target_ops nrf_target_ops = {
	.assign		= nrf_op_assign,
	.identify	= nrf_op_identify,
	.erase		= nrf_op_erase,
	.write		= nrf_op_write,
	.read		= nrf_op_read,
	.run		= nrf_op_run,
};
