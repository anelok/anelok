/*
 * swdlib/nrflash.c - nRF51 Flash programming
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "nrf51.h"
#include "nrf51_bitfields.h"

#include "swdlib.h"
#include "nrflash.h"


/*
 * ERASEALL takes 22.3 ms, writes usecs.
 * Experiments showed that a Ben has to wait about 300 cycles for ERASEALL.
 */

#define	MAX_WAIT	10000


/* ----- Helper functions -------------------------------------------------- */


static int8_t flash_busy(struct swd *swd)
{
	uint32_t ready;

	if (!swd_read_32(swd, &NRF_NVMC->READY, &ready))
		return -1;
	return !(ready & NVMC_READY_READY_Msk);
}


static bool flash_wait(struct swd *swd)
{
	int8_t busy;
	uint16_t tries = MAX_WAIT;

	while (tries--) {
		busy = flash_busy(swd);
		if (!busy)
			return 1;
		if (busy < 0)
			return 0;
	}
	swd_report("timeout\n");
	return 0;
}


static bool flash_config(struct swd *swd, uint32_t wen)
{
	return swd_write_32(swd, &NRF_NVMC->CONFIG, wen << NVMC_CONFIG_WEN_Pos);
}


/* ----- Flash mass erase -------------------------------------------------- */


bool nrf_erase_all(struct swd *swd)
{
	if (!flash_wait(swd))
		return 0;
	if (!flash_config(swd, NVMC_CONFIG_WEN_Een))
		return 0;
	if (!swd_write_32(swd, &NRF_NVMC->ERASEALL,
	    NVMC_ERASEALL_ERASEALL_Erase << NVMC_ERASEALL_ERASEALL_Pos))
		return 0;
	if (!flash_wait(swd))
		return 0;
	if (!flash_config(swd, NVMC_CONFIG_WEN_Ren))
		return 0;
	return 1;
}


/* ----- nRF51 Flash programming ------------------------------------------- */


bool nrf_program_flash(struct swd *swd,
    uint32_t addr, const uint32_t *buf, uint32_t words,
    void (*progress)(void *user, bool verify, uint16_t pos), void *user)
{
	uint32_t i, got;

	if (!flash_wait(swd))
		return 0;
	if (!flash_config(swd, NVMC_CONFIG_WEN_Wen))
		return 0;

	for (i = 0; i != words; i++) {
		if (progress)
			progress(user, 0, i);
		if (!flash_wait(swd))
			return 0;
		if (!swd_write_32(swd, (uint32_t *) addr + i, buf[i]))
			return 0;
	}
	if (!flash_wait(swd))
		return 0;
	if (!flash_config(swd, NVMC_CONFIG_WEN_Ren))
		return 0;

	if (progress)
		progress(user, 0, words);
	for (i = 0; i != words; i++) {
		if (progress)
			progress(user, 1, i);
		if (!swd_read_32(swd, (uint32_t *) addr + i, &got))
			return 0;
		if (got != buf[i]) {
			swd_report("0x%08x: expected 0x%08x got 0x%08x\n",
			    (unsigned) (addr + 4 * i), (unsigned) buf[i],
			    (unsigned) got);
			return 0;
		}
	}
	if (progress)
		progress(user, 1, words);
	return 1;
}


bool nrf_read_flash(struct swd *swd,
    uint32_t addr, uint32_t *buf, uint32_t words)
{
	uint32_t i = 0;

	for (i = 0; i != words; i++)
		if (!swd_read_32(swd, (uint32_t *) addr + i, buf + i))
			return 0;
	return 1;
}
