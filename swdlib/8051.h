/*
 * swdlib/8051.h - 8051 instruction set (partial)
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef I8051_H
#define	I8051_H

enum {
	INC_DPTR	= 0xa3,		/* INC DPTR */
	MOV_A_dir	= 0xe5,		/* MOV A,direct */
	MOV_A_u8	= 0x74,		/* MOV A,#data */
	MOV_dir_A	= 0xf5,		/* MOV direct,A */
	MOV_dir_u8	= 0x75,		/* MOV direct,#data */
	MOV_DPRT_u16	= 0x90,		/* MOV DPTR,#data16 (hi, lo) */
	MOVX_A_DPTR	= 0xe0,		/* MOVX A,@DPTR */
	MOVX_DPTR_A	= 0xf0,		/* MOVX @DPTR,A */
};

#endif /* !I8051_H */
