/*
 * swdlib/swdlib.c - Experimental Anelok firmware
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Resources:
 *
 * ARM's documentation (coresight, v5 debug spec, v5.1 supplement):
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0480f/index.html
 *
 * ARM Debug Interface v5 -- Architecture Specification
 * 
 * http://www.pjrc.com/arm/pdf/doc/ARM_debug.pdf
 * http://hackipedia.org/Hardware/CPU/ARM/pdf,%20Cortex/IHI0031A_ARM_debug_interface_v5.pdf
 * http://www.pjrc.com/arm/pdf/doc/ARM_debug.sup.pdf
 *
 * ARM DSTREAM SWD timing diagram:
 * http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0499d/BEHCBIJE.html
 *
 * AHB-AP (ARM):
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0480f/CIHEBBEI.html
 *
 * Cortex M1 DAP:
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0413c/Babbbebi.html
 *
 * SiLab's improved variant (includes valid IDCODEs):
 * http://www.silabs.com/Support%20Documents/TechnicalDocs/AN0062.pdf
 *
 * OpenOCD
 * http://openocd.sourceforge.net/doc/doxygen/html/arm__adi__v5_8c_source.html
 */

#include <stdbool.h>
#include <stdint.h>

#include "dap.h"
#include "swdlib.h"


#define	MAX_TRIES	100	/* number of times we WAIT or FAULT */


/* ----- Wrappers for platform operations ---------------------------------- */


static inline void swd_send(struct swd *swd, uint32_t data, uint8_t bits)
{
	swd->ops->send(swd->ctx, data, bits);
}


static inline uint32_t swd_recv(struct swd *swd, uint8_t bits)
{
	return swd->ops->recv(swd->ctx, bits);
}


static inline void swd_reset(struct swd *swd, bool active)
{
	return swd->ops->reset(swd->ctx, active);
}


void swd_sleep_1s(struct swd *swd)
{
	return swd->ops->sleep_1s(swd->ctx);
}


/* ----- Request and Acknowledge ------------------------------------------- */


static enum swd_response swd_request(struct swd *swd,
    bool ap, bool rd, uint8_t reg)
{
	uint32_t val;
	bool parity;

	parity = ap ^ rd ^ (reg & 1) ^ (reg >> 1);
	val = 1			/* Start */
	    | ap << 1		/* APnDP */
	    | rd << 2		/* RnW */
	    | reg << 3		/* A[2:3] */
	    | parity << 5	/* Parity */
	    | 0 << 6		/* Stop */
	    | 1 << 7;		/* Park */
	swd_send(swd, val, 8);

	val = swd_recv(swd, 3);
	switch (val) {
	case 1:
		return SWD_OK;
	case 2:
		return SWD_WAIT;
	case 4:
		return SWD_FAULT;
	default:
		swd_report("ACK %u\n", (unsigned) val);
		return SWD_ERROR;
	}
}


/* ----- DP/AP register read/write ----------------------------------------- */


static bool parity32(uint32_t val)
{
	val ^= val >> 16;
	val ^= val >> 8;
	val ^= val >> 4;
	val ^= val >> 2;
	val ^= val >> 1;
	return val & 1;
}


static enum swd_response swd_abort(struct swd *swd)
{
	const uint32_t val =
	    SWD_DP_ABORT_STKERRCLR | SWD_DP_ABORT_WDERRCLR |
	    SWD_DP_ABORT_ORUNERRCLR;
	enum swd_response res;

	res = swd_request(swd, 0, 0, SWD_DP_ABORT >> 2);
	if (res) {
		swd_report("ABORT failed: ");
		swd_report_error(swd, res);
		return res;
	}
	swd_send(swd, val, 32);
	swd_send(swd, parity32(val), 1);
	return res;
}


static enum swd_response swd_read(struct swd *swd, bool ap, uint8_t reg,
    uint32_t *val)
{
	enum swd_response res;
	bool parity;
	uint8_t tries = MAX_TRIES;

	while (1) {
		res = swd_request(swd, ap, 1, reg >> 2);
		*val = swd_recv(swd, 32);
		parity = swd_recv(swd, 1);

		switch (res) {
		case SWD_OK:
			if (parity32(*val) != parity)
				return SWD_PARITY;
			DPRINTF("%s[%u] -> 0x%08x\n",
			    ap ? "AP" : "DP", reg, *val);
			return SWD_OK;
		case SWD_WAIT:
			break;
		case SWD_FAULT:
			res = swd_abort(swd);
			if (res)
				return res;
			break;
		default:
			swd_report_error(swd, res);
			return res;
		}
		if (!tries--)
			return SWD_TIMEOUT;
	}
}


static enum swd_response swd_write(struct swd *swd, bool ap, uint8_t reg,
    uint32_t val)
{
	enum swd_response res;
	uint8_t tries = MAX_TRIES;

	DPRINTF("%s[%u] = 0x%08x\n", ap ? "AP" : "DP", reg, val);
	while (1) {
		res = swd_request(swd, ap, 0, reg >> 2);
		swd_send(swd, val, 32);
		swd_send(swd, parity32(val), 1);

		switch (res) {
		case SWD_OK:
			return SWD_OK;
		case SWD_WAIT:
			break;
		case SWD_FAULT:
			res = swd_abort(swd);
			if (res)
				return res;
			break;
		default:
			swd_report_error(swd, res);
			return res;
		}
		if (!tries--)
			return SWD_TIMEOUT;
	}
}


/* ----- AP register read/write -------------------------------------------- */


static enum swd_response swd_ap_force_select(struct swd *swd,
    uint8_t ap, uint8_t bank)
{
	enum swd_response res;

	res = swd_write(swd, 0, SWD_DP_SELECT,
	    ap << SWD_DP_SELECT_APSEL_SHIFT |
	    bank << SWD_DP_SELECT_APBANKSEL_SHIFT);
	if (res)
		return res;
	swd->ap = ap;
	swd->ap_bank = bank;
	return SWD_OK;
}


static enum swd_response swd_ap_select(struct swd *swd,
    uint8_t ap,  uint8_t bank)
{
	if (swd->ap == ap && swd->ap_bank == bank)
		return SWD_OK;
	return swd_ap_force_select(swd, ap, bank);
}


enum swd_response swd_ap_read(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t *val)
{
	enum swd_response res;

	res = swd_ap_select(swd, ap, reg >> 4);
	if (res)
		return res;
	res = swd_read(swd, 1, reg & 0xc, val);
	if (res)
		return res;
	return swd_read(swd, 0, SWD_DP_RDBUF, val);
}


enum swd_response swd_ap_write(struct swd *swd,
    uint8_t ap, uint8_t reg, uint32_t val)
{
	enum swd_response res;

	res = swd_ap_select(swd, ap, reg >> 4);
	if (res)
		return res;
	return swd_write(swd, 1, reg & 0xc, val);
}


/* ----- AHB read/write ---------------------------------------------------- */


static enum swd_response ahb_wait_idle(struct swd *swd)
{
	enum swd_response res;
	uint32_t s;

	do {
		res = swd_ap_read(swd, AHB_AP, AHB_AP_CSW, &s);
		if (res)
			return res;
	} while ((s & AHB_AP_CSW_TrInProg) || !(s & AHB_AP_CSW_DbgStatus));
	return 0;
}


static enum swd_response ahb_read_8(struct swd *swd,
    uint32_t addr, uint8_t *val)
{
	enum swd_response res;
	uint32_t tmp;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    AHB_AP_CSW_Size_8 | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	res = swd_ap_read(swd, AHB_AP, AHB_AP_DRW, &tmp);
	*val = tmp;
	return res;
}


static enum swd_response ahb_write_size(struct swd *swd,
    uint32_t addr, uint32_t val, uint32_t size)
{
	enum swd_response res;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    size | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	return swd_ap_write(swd, AHB_AP, AHB_AP_DRW, val);
}


static enum swd_response ahb_write_8(struct swd *swd,
    uint32_t addr, uint8_t val)
{
	return ahb_write_size(swd, addr, val, AHB_AP_CSW_Size_8);
}


static enum swd_response ahb_read_32(struct swd *swd,
    uint32_t addr, uint32_t *val)
{
	enum swd_response res;

	res = ahb_wait_idle(swd);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_CSW,
	    AHB_AP_CSW_Size_32 | AHB_AP_CSW_Prot_DEFAULT | AHB_AP_CSW_SBO);
	if (res)
		return res;
	res = swd_ap_write(swd, AHB_AP, AHB_AP_TAR, addr);
	if (res)
		return res;
	return swd_ap_read(swd, AHB_AP, AHB_AP_DRW, val);
}


static enum swd_response ahb_write_32(struct swd *swd,
    uint32_t addr, uint32_t val)
{
	return ahb_write_size(swd, addr, val, AHB_AP_CSW_Size_32);
}


/* ----- Helper functions -------------------------------------------------- */


void swd_report_error(struct swd *swd, enum swd_response res)
{
	switch (res) {
	case SWD_WAIT:
		swd_report("WAIT\n");
		break;
	case SWD_FAULT:
		swd_report("FAULT\n");
		break;
	case SWD_ERROR:
		swd_report("ERROR\n");
		break;
	case SWD_PARITY:
		swd_report("PARITY\n");
		break;
	case SWD_TIMEOUT:
		swd_report("PARITY\n");
		break;
	default:
		swd_report("unknown error %d\n", res);
		break;
	}
}


static void swd_sync(struct swd *swd)
{
	/* reset JTAG and SWD by sending > 50 cycles */

	swd_send(swd, 0xffffffff, 25);
	swd_send(swd, 0xffffffff, 26);
}


void swd_idle(struct swd *swd, uint16_t cycles)
{
	while (cycles >= 32) {
		swd_send(swd, 0, 32);
		cycles -= 32;
	}
	if (cycles)
		swd_send(swd, 0, cycles);
}


static void status(struct swd *swd)
{
	enum swd_response res;
	uint32_t s;

	res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &s);
	if (res)
		swd_report_error(swd, res);
	DPRINTF("status 0x%08x\n", s);
}


/* ----- SWD initialization ------------------------------------------------ */


bool swd_init(struct swd *swd)
{
	enum swd_response res;
	uint32_t id;

	/* activate the SWD interface */

	swd_sync(swd);
	swd_send(swd, 0xe79e, 16);	/* JTAG-to-SWD select sequence */
	swd_sync(swd);
	swd_idle(swd, 8);		/* ADIv5.1: at least one idle cycle */

	/* check that we're talking */

	res = swd_read(swd, 0, SWD_DP_IDCODE, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}

	switch (id) {
	case IDCODE_M0:
		swd_report("Cortex M0\n");
		break;
	case IDCODE_M0_PLUS:
		swd_report("Cortex M0+\n");
		break;
	case IDCODE_M3_M4:
		swd_report("Cortex M3/M4\n");
		break;
	default:
		swd_report("unknown IDCODE 0x%08x\n", (unsigned) id);
		return 0;
	}

	/* clear all the SWD sticky error flags */

	res = swd_abort(swd);
	if (res)
		return 0;

	/* initialize AP and register bank selection */

	res = swd_ap_force_select(swd, 0, 0);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}

	return 1;
}


/* ----- Target read/write ------------------------------------------------- */


bool swd_read_8(struct swd *swd, volatile const uint8_t *addr, uint8_t *result)
{
	enum swd_response res;

	while (1) {
		res = ahb_read_8(swd, (uint32_t) addr, result);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_write_8(struct swd *swd, volatile uint8_t *addr, uint8_t val)
{
	enum swd_response res;

//fprintf(stderr, "0x%08x <- %02x\n", (uint32_t) addr, val);
	while (1) {
		res = ahb_write_8(swd, (uint32_t) addr, val);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_read_32(struct swd *swd, volatile const uint32_t *addr,
    uint32_t *result)
{
	enum swd_response res;

	while (1) {
		res = ahb_read_32(swd, (uint32_t) addr, result);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 0;
	}
}


bool swd_write_32(struct swd *swd, volatile uint32_t *addr, uint32_t val)
{
	enum swd_response res;

	while (1) {
		res = ahb_write_32(swd, (uint32_t) addr, val);
		if (!res)
			return 1;
		/* @@@ recover */
		swd_report_error(swd, res);
		return 1;
	}
}


/* ----- Open/close SWD ---------------------------------------------------- */


static bool identify_aps(struct swd *swd)
{
	enum swd_response res;
	uint32_t id;

	res = swd_ap_read(swd, MDM_AP, MDM_AP_IDR, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	swd_report("AP #%u ID 0x%08x (%s)\n", MDM_AP, (unsigned) id,
	    id == MDM_AP_ID ? "MDM-AP" : "???");

	status(swd);

	res = swd_ap_read(swd, AHB_AP, AHB_AP_IDR, &id);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	swd_report("AP #%u ID 0x%08x (%s)\n", AHB_AP, (unsigned) id,
	    id == AHB_AP_ID || id == AHB_AP_ID_M0_PLUS ? "AHB-AP" : "???");

	status(swd);
	return 1;
}


#define	CTRL_STAT_TRIES	100000


static bool dp_ctrl_stat(struct swd *swd, uint32_t req, uint32_t mask, bool on)
{
	enum swd_response res;
	uint32_t v;
	unsigned i;

	res = swd_write(swd, 0, SWD_DP_CTRL_STAT, req);
	if (res)
		goto error;

	for (i = 0; i != CTRL_STAT_TRIES; i++) {
		res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &v);
		if (res)
			goto error;
		if ((v & mask) == mask)
			return 1;
	}

	swd_report("DP_CTRL_STAT timeout: expected 0x%x/0x%x, got 0x%x\n",
	    on ? (unsigned) mask : 0, (unsigned) mask, (unsigned) v);
	return 0;

error:
	swd_report_error(swd, res);
	return 0;
}


static bool activate_debug(struct swd *swd)
{
	uint32_t ctrl = SWD_DP_CTRL_CDBGPWRUPREQ | SWD_DP_CTRL_CSYSPWRUPREQ;
	uint32_t ack = SWD_DP_CTRL_CDBGPWRUPACK | SWD_DP_CTRL_CSYSPWRUPACK;
	enum swd_response res;
	uint32_t v;

	/* power up debug and system */

	if (!dp_ctrl_stat(swd, ctrl, ack, 1))
		return 0;

	/* reset debug */

	res = swd_write(swd, 0,
	    SWD_DP_CTRL_STAT, ctrl | SWD_DP_CTRL_CDBGRSTREQ);
	if (res)
		goto error;
	res = swd_read(swd, 0, SWD_DP_CTRL_STAT, &v);
	if (res)
		goto error;

	/* CDBGRSTREQ not implemented */
	if (!(v & SWD_DP_CTRL_CDBGRSTACK)) {
		res = swd_write(swd, 0,
			SWD_DP_CTRL_STAT, ctrl | SWD_DP_CTRL_ORUNDETECT);
		if (res)
			goto error;
		return 1;
	}

	/* remove reset request */

	return dp_ctrl_stat(swd,
	    ctrl | SWD_DP_CTRL_ORUNDETECT, SWD_DP_CTRL_CDBGRSTACK, 0);

error:
	swd_report_error(swd, res);
	return 0;
}


bool swd_open(struct swd *swd)
{
	if (!activate_debug(swd))
		return 0;
	status(swd);

	if (!identify_aps(swd))
		return 0;

	return 1;
}


void swd_setup(struct swd *swd, const struct swd_ops *ops, void *ctx)
{
	swd->ops = ops;
	swd->ctx = ctx;
}


bool swd_release(struct swd *swd)
{
	enum swd_response res;

	swd_reset(swd, 0);

	res = swd_ap_write(swd, MDM_AP, MDM_AP_CONTROL,
	    MDM_AP_CONTROL_Core_Hold_Reset);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}
	status(swd);

	return 1;
}


void swd_close(struct swd *swd)
{
	swd_idle(swd, 1000);
}
