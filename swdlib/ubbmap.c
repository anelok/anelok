/*
 * swdlib/ubbmap.c - UBB signal mapping
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>	/* for strcasecmp, strncasecmp */

#include <ubb/ubb.h>
#include "ubbmap.h"


static const struct ubbmap ubb[] = {
	{ "cmd",	UBB_CMD },
	{ "clk",	UBB_CLK },
	{ "dat0",	UBB_DAT0 },
	{ "dat1",	UBB_DAT1 },
	{ "dat2",	UBB_DAT2 },
	{ "dat3",	UBB_DAT3 },
	{ NULL, }
};


static void usage(const struct ubbmap *map)
{
	fprintf(stderr, "assignment syntax:\n  ");
	while (map->name) {
		fprintf(stderr, "%s%s", map->name, map[1].name ? "|" : "");
		map++;
	}
	fprintf(stderr, "=CMD|CLK|DAT[0-3]\n");
	fprintf(stderr, "  CMD|CLK|DAT[0-3]=0|1\n");
	exit(1);
}


static uint32_t lookup(const struct ubbmap *map, const char *name)
{
	while (map->name) {
		if (!strcasecmp(map->name, name))
			return map->pin;
		map++;
	}
	return 0;
}


static bool set_io(const char *s, unsigned len, bool on)
{
	const struct ubbmap *walk;

	for (walk = ubb; walk->name; walk++)
		if (strlen(walk->name) == len &&
		    !strncasecmp(walk->name, s, len))
			break;
	if (!walk->name)
		return 0;

	if (ubb_open(~walk->pin)) {
		fprintf(stderr, "ubb_open failed\n");
		exit(1);
	}
	if (on)
		SET(walk->pin);
	else
		CLR(walk->pin);
	OUT(walk->pin);
	ubb_close(UBB_ALL);
	return 1;
}


void ubbmap_assign(struct ubbmap *map, const char *s)
{
	struct ubbmap *walk;
	const char *eq = strchr(s, '=');
	unsigned len;

	if (!eq)
		usage(map);

	len = eq - s;
	if (!strcmp(eq + 1, "0") && set_io(s, len, 0))
		return;
	if (!strcmp(eq + 1, "1") && set_io(s, len, 1))
		return;

	for (walk = map; walk->name; walk++)
		if (strlen(walk->name) == len &&
		    !strncasecmp(walk->name, s, len))
			break;
	if (!walk->name)
		usage(map);

	walk->pin = lookup(ubb, eq + 1);
	if (!walk->pin)
		usage(map);
}


uint32_t ubbmap_pin(const struct ubbmap *map, const char *name)
{
	uint32_t pin;

	pin = lookup(map, name);
	if (!pin)
		abort();
	return pin;
}
