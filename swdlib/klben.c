/*
 * swdlib/klben.c - Ben Nanonote UBB driver for SWD (KL2x)
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> /* for usleep */

#include "../fw/base/regs.h"

#include <ubb/ubb.h>
#include "ubbmap.h"
#include "progress.h"
#include "dap.h"
#include "swdlib.h"
#include "klflash.h"
#include "kinetis.h"
#include "swdben.h"
#include "ben.h"


static struct ubbmap map[] = {
	{ "reset",	UBB_CLK },
	{ "dio",	UBB_DAT1 },
	{ "clk",	UBB_DAT0 },
	{ NULL }
};


#define	KL_RESET	ubbmap_pin(map, "reset")
#define	SWD_DIO		ubbmap_pin(map, "dio")
#define	SWD_CLK		ubbmap_pin(map, "clk")

#define	FLASH_WORDS	32768	/* @@@ */


/* ----- Operations backend ------------------------------------------------ */


#define	FSEC	0x40c


static enum protection decode_fsec(const uint32_t *buf, uint32_t off,
    uint32_t words)
{
	uint8_t fsec;

	if (off > FSEC)
		return prot_none;
	if (off + (words << 2) < FSEC)
		return prot_none;
	fsec = buf[(FSEC - off) >> 2];
	if ((fsec & 3) == 2)
		return	prot_none;
	if (((fsec >> 4) & 2) == 2) /* mass erase disabled */
		return prot_irrev;
	else
		return prot_rev;
}


static bool compare_fsec(struct swd *swd,
    enum protection expect, enum protection has)
{
	if (expect == has)
		return 1;
	swd_report("binary has ");
	switch (has) {
	case prot_none:
		swd_report("no");
		break;
	case prot_rev:
		swd_report("reversible");
		break;
	case prot_irrev:
		swd_report("irreversible");
		break;
	default:
		abort();
	}
	swd_report(" protection, expected ");
	switch (expect) {
	case prot_none:
		swd_report("none");
		break;
	case prot_rev:
		swd_report("reversible");
		break;
	case prot_irrev:
		swd_report("irreversible");
		break;
	default:
		abort();
	}
	swd_report("\n");
	return 0;
}


static bool kl_program(struct swd *swd, uint32_t addr,
    const uint32_t *buf, uint32_t words, enum protection prot)
{
	struct progress ctx;
	enum protection fsec_prot;

	/*
	 * @@@ dirty hack to enable incremental programming
	 */
	const char *s = getenv("SWD_OFF");
	uint32_t off = 0;

	if (s) {
		off = strtoul(s, NULL, 0);
		if (off & 3) {
			fprintf(stderr, "SWD_OFF must be word-aligned\n");
			return 0;
		}
		addr += off ;
		buf += off >> 2;
		words -= off >> 2;
		fprintf(stderr, "0x%x + %u\n",
		    (unsigned) addr, (unsigned) words << 2);
	}

	fsec_prot = decode_fsec(buf, addr, words);
	if (!compare_fsec(swd, prot, fsec_prot))
		return 0;
	if (!addr)
		if (!kl_mass_erase(swd))
			return 0;
	progress_init(&ctx, swd, words);
	return kl_program_flash(swd, addr, buf, words, progress, &ctx);
}


static void *read_flash(struct swd *swd, uint32_t base, uint32_t *size)
{
	uint32_t words;
	uint32_t *buf;

	if (base > (FLASH_WORDS << 2)) {
		fprintf(stderr, "base %u > bytes %u\n",
		    (unsigned) base, FLASH_WORDS << 2);
		return NULL;
	}

	words = FLASH_WORDS - (base >> 2);
	buf = calloc(words, sizeof(uint32_t));
	if (!buf) {
		perror("calloc");
		return NULL;
	}

	if (!kl_read_flash(swd, base, buf, words)) {
		free(buf);
		return NULL;
	}

	*size = words << 2;
	return buf;
}


/* ---- ben_swd_open wrapper ----------------------------------------------- */


static bool open_and_hold_reset(struct swd *swd)
{
	enum swd_response res;

	if (!swd_open(swd)) {
		fprintf(stderr, "swd_open failed\n");
		return 0;
	}

	res = swd_ap_write(swd, MDM_AP, MDM_AP_CONTROL,
	    MDM_AP_CONTROL_Core_Hold_Reset |
	    MDM_AP_CONTROL_System_Reset_Request);
	if (res) {
		swd_report_error(swd, res);
		return 0;
	}

	return 1;
}


static bool kl_open(struct ben_swd *ben, struct swd *swd)
{
	return ben_swd_open(ben, swd, KL_RESET, SWD_DIO, SWD_CLK,
	    swd_init, open_and_hold_reset);
}


/* ----- Target operations ------------------------------------------------- */


static void kl_op_assign(const char *s)
{
	ubbmap_assign(map, s);
}


static bool kl_op_identify(void)
{
	struct ben_swd ben;
	struct swd swd;
	bool can_erase, secure, backdoor;
	bool ok = 0;

	if (!kl_open(&ben, &swd))
		return 0;
	if (!kl_flash_security(&swd, &can_erase, &secure, &backdoor))
		goto out;
	fprintf(stderr, "Mass-erase: %s, secure: %s, backdoor: %s\n",
	    can_erase ? "yes" : "no", secure ? "yes" : "no",
	    backdoor ? "yes" : "no");
	if (secure) {
		ok = 1;
		goto out;
	}
	if (!ben_swd_release(&ben))
		return 0;
	ok = kinetis_identify_soc(&swd);
	ok = ok && kinetis_print_uid(&swd);

out:
	ben_swd_close(&ben);
	return ok;
}


static bool kl_op_erase(void)
{
	struct ben_swd ben;
	struct swd swd;
	bool ok;

	if (!kl_open(&ben, &swd))
		return 0;
	ok = kl_mass_erase(&swd);
	ben_swd_close(&ben);
	return ok;
}


static bool kl_op_write(const char *file, uint32_t base, enum protection prot)
{
	struct ben_swd ben;
	struct swd swd;
	bool can_erase, secure;
	bool ok = 0;

	if (!kl_open(&ben, &swd))
		return 0;

	if (!kl_flash_security(&swd, &can_erase, &secure, NULL))
		goto out;
	if (!can_erase) {
		fprintf(stderr, "Flash cannot be erased\n");
		goto out;
	}
	if (secure) {
		fprintf(stderr, "Chip is \"secure\" - please erase with -e\n");
		goto out;
	}

	if (!ben_swd_release(&ben))
		return 0;
	if (!kinetis_identify_soc(&swd))
		goto out;

	ok = do_flash(&swd, file, (flash_fn) kl_program, base, prot);
out:
	ben_swd_close(&ben);

	return ok;
}


static void *kl_op_read(uint32_t base, uint32_t *size)
{
	struct ben_swd ben;
	struct swd swd;
	void *res = NULL;

	if (!kl_open(&ben, &swd))
		return NULL;
	if (!ben_swd_release(&ben))
		return 0;
	if (!kinetis_identify_soc(&swd))
		goto out;

	res = read_flash(&swd, base, size);

out:
	ben_swd_close(&ben);

	return res;
}


static void kl_op_run(void)
{
	char ch;

	ben_swd_ubb_open(KL_RESET, SWD_DIO, SWD_CLK);
	SET(KL_RESET);
	usleep(100 * 1000);
	CLR(KL_RESET);
	usleep(100 * 1000);
	SET(KL_RESET);
	fprintf(stderr, "Press [Enter] to exit\n");
	read(0, &ch, 1);
	ubb_close(SWD_CLK);
}


const struct target_ops kl_target_ops = {
	.assign		= kl_op_assign,
	.identify	= kl_op_identify,
	.erase		= kl_op_erase,
	.write		= kl_op_write,
	.read		= kl_op_read,
	.run		= kl_op_run,
};
