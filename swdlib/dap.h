/*
 * swdlib/dap.h - Debug Access Port definitions
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef DAP_H
#define	DAP_H

/*
 * ARM's documentation about IDCODE is misleading at best. The real values
 * can be found in
 * http://www.silabs.com/Support%20Documents/TechnicalDocs/AN0062.pdf
 *
 * IDCODE for M0:
 * http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka16088.html
 */


#define	IDCODE_M0	0x0bb11477
#define	IDCODE_M0_PLUS	0x0bc11477
#define	IDCODE_M3_M4	0x2ba01477


#define	AHB_AP	0	/* AHB-AP at #0 */
#define	MDM_AP	1	/* MDM-AP at #1 */

#define	AHB_AP_ID		0x04770021
#define	AHB_AP_ID_M0_PLUS	0x04770031
#define	MDM_AP_ID		0x001c0020


/* http://infocenter.arm.com/help/topic/com.arm.doc.ddi0413c/Bcfdfigg.html */

/* SWD-DP registers */
enum {
	SWD_DP_IDCODE		= 0 << 2,	/* read */
	SWD_DP_ABORT		= 0 << 2,	/* write */
	SWD_DP_CTRL_STAT	= 1 << 2,	/* read/write */
	SWD_DP_RESEND		= 2 << 2,	/* read */
	SWD_DP_SELECT		= 2 << 2,	/* write */
	SWD_DP_RDBUF		= 3 << 2,	/* read */
};

/* SWD_DP_ABORT fields */

enum {
	SWD_DP_ABORT_DAPABORT	= 1,	/* abort DAP transaction */
	SWD_DP_ABORT_STKCMPCLR	= 2,	/* clear STICKYCMP */
	SWD_DP_ABORT_STKERRCLR	= 4,	/* clear STICKYERR */
	SWD_DP_ABORT_WDERRCLR	= 8,	/* clear WDATAERR */
	SWD_DP_ABORT_ORUNERRCLR	= 0x10,	/* clear STICKYORUN */
};

/* SWD_DP_CTRL_STAT fields */

enum {
	SWD_DP_CTRL_ORUNDETECT	= 1,	/* overrun detection */
	SWD_DP_CTRL_STICKYORUN	= 2,	/* overrun occurred */
	SWD_DP_CTRL_TRNMODE_PASS = 0,	/* pass to AP */
	SWD_DP_CTRL_TRNMODE_VRFY = 8,	/* pushed verify */
	SWD_DP_CTRL_TRNMODE_CMP	= 10,	/* pushed compare */
	SWD_DP_CTRL_STICKYCMP	= 0x10,	/* match occurred */
	SWD_DP_CTRL_STICKYERR	= 0x20,	/* AHB-Lite bus error */
	SWD_DP_CTRL_READOK	= 0x40,	/* RDBUFF was okay */
	SWD_DP_CTRL_WDATAERR	= 0x80,	/* write discarded */
	SWD_DP_CTRL_MASKLANE_0	= 0x100,	/* 0x......ff */
	SWD_DP_CTRL_MASKLANE_1	= 0x200,	/* 0x....ff.. */
	SWD_DP_CTRL_MASKLANE_2	= 0x400,	/* 0x..ff.... */
	SWD_DP_CTRL_MASKLANE_3	= 0x800,	/* 0xff...... */
	SWD_DP_CTRL_TRNCNT_MASK	= 0xfff000,	/* transaction counter */
	SWD_DP_CTRL_TRNCNT_SHIFT = 12,
	SWD_DP_CTRL_CDBGRSTREQ	= 1 << 26,	/* debug reset request */
	SWD_DP_CTRL_CDBGRSTACK	= 1 << 27,	/* CDBGRSTACK ack */
	SWD_DP_CTRL_CDBGPWRUPREQ = 1 << 28,	/* debug power-up request */
	SWD_DP_CTRL_CDBGPWRUPACK = 1 << 29,	/* CDBGPWRUPREQ ack */
	SWD_DP_CTRL_CSYSPWRUPREQ = 1 << 30,	/* system power-up request */
	SWD_DP_CTRL_CSYSPWRUPACK = 1 << 31	/* CSYSPWRUPREQ ack */
};

/* SWD_DP_SELECT fields */

enum {
	SWD_DP_SELECT_CTRLSEL	= 1,		/* 0: CTRL/STAT, 1: WCR */
	SWD_DP_SELECT_APBANKSEL_MASK = 0xf0,	/* select register window */
	SWD_DP_SELECT_APBANKSEL_SHIFT = 4,
	SWD_DP_SELECT_APSEL_MASK = 0xff000000,	/* select access port (0) */
	SWD_DP_SELECT_APSEL_SHIFT = 24
};

/* http://infocenter.arm.com/help/topic/com.arm.doc.ddi0413c/Chdjibcg.html */

/* AHB-AP registers */

enum {
	AHB_AP_CSW	= 0x00,	/* Control/Status Word */
	AHB_AP_TAR	= 0x04,	/* Transfer Address */
	AHB_AP_DRW	= 0x0c,	/* Data Read/Write */
	AHB_AP_DB0	= 0x10,	/* Banked Data 0 */
	AHB_AP_DB1	= 0x14,	/* Banked Data 1 */
	AHB_AP_DB2	= 0x18,	/* Banked Data 2 */
	AHB_AP_DB3	= 0x1c,	/* Banked Data 3 */
	AHB_AP_BASE	= 0xf8,	/* Debug base address */
	AHB_AP_IDR	= 0xfc,	/* Identification Register */
};

/* AHB_AP_CSW fields */

enum {
	AHB_AP_CSW_Size_8	= 0,	/* 8 bits */
	AHB_AP_CSW_Size_16	= 1,	/* 16 bits */
	AHB_AP_CSW_Size_32	= 2,	/* 32 bits */
	AHB_AP_CSW_Size_MASK	= 7,
	AHB_AP_CSW_AddrInc_off	= 0,	/* auto increment off */
	AHB_AP_CSW_AddrInc_sng	= 0x10,	/* increment, single */
	AHB_AP_CSW_AddrInc_pack	= 0x20,	/* increment, packed */
	AHB_AP_CSW_AddrInc_MASK	= 0x30,
	AHB_AP_CSW_DbgStatus	= 0x40,	/* AHB transfers permitted */
	AHB_AP_CSW_TrInProg	= 0x80,	/* Transfer in progress */
	AHB_AP_CSW_Prot_MASK	= 0x0f000000,	/* HPROT[3:0] */
	AHB_AP_CSW_Prot_SHIFT	= 24,
	AHB_AP_CSW_Prot_DEFAULT	= (3 << AHB_AP_CSW_Prot_SHIFT),
	AHB_AP_CSW_SBO		= 1 << 30, /* Reserved, must write 1 */
};

/* MDM-AP registers */

enum {
	MDM_AP_STATUS	= 0x00,
	MDM_AP_CONTROL	= 0x04,
	MDM_AP_IDR	= 0xfc,
};

/* MDM_AP_CONTROL fields */

enum {
	MDM_AP_CONTROL_Mass_Erase	= 1 << 0,
	MDM_AP_CONTROL_Debug_Disable	= 1 << 1,
	MDM_AP_CONTROL_Debug_Request	= 1 << 2,
	MDM_AP_CONTROL_System_Reset_Request = 1 << 3,
	MDM_AP_CONTROL_Core_Hold_Reset	= 1 << 4,
	MDM_AP_CONTROL_VLLDBGREQ	= 1 << 5,
	MDM_AP_CONTROL_VLLDBGACK	= 1 << 6,
	MDM_AP_CONTROL_LLS_Status_Ack	= 1 << 7,
};

/* MDM_AP_STATUS fields */

enum {
	MDM_AP_STATUS_Mass_Erase_Ack	= 1 << 0,
	MDM_AP_STATUS_Flash_Ready	= 1 << 1,
	MDM_AP_STATUS_System_Security	= 1 << 2,
	MDM_AP_STATUS_System_Reset	= 1 << 3,
	MDM_AP_STATUS_Mass_Erase_Enable	= 1 << 5,
	MDM_AP_STATUS_Backdoor_Enable	= 1 << 6,
	MDM_AP_STATUS_LP_Enabled	= 1 << 7,
	MDM_AP_STATUS_VLPx		= 1 << 8,
	MDM_AP_STATUS_LSS_Exit		= 1 << 9,
	MDM_AP_STATUS_VLLSx_Exit	= 1 << 10,
	MDM_AP_STATUS_Core_Halted	= 1 << 16,
	MDM_AP_STATUS_Core_SLEEPDEEP	= 1 << 17,
	MDM_AP_STATUS_Core_SLEEPING	= 1 << 18,
};

#endif /* !DAP_H */
