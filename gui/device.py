#!/usr/bin/python
#
# gui/device.py - USB interface
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


# References:
# - PyUSB:
#   https://github.com/walac/pyusb/blob/master/docs/tutorial.rst


from struct import *
import usb.core
from usb.util import *


anelok_vid = 0x20b7	# Qi-Hardware
anelok_pid = 0xae70	# AnELOk

ANELOK_READ_DB	= (CTRL_TYPE_VENDOR | CTRL_IN,	4)
ANELOK_WRITE_DB = (CTRL_TYPE_VENDOR | CTRL_OUT,	4)
ANELOK_CANCEL	= (CTRL_TYPE_VENDOR | CTRL_IN,	5)

DB_SIZE = 16 * 1024

XFER_SIZE = 64
TIMEOUT_MS = 1000

class Usb:
	def cancel(self):
		res = self.dev.ctrl_transfer(ANELOK_CANCEL[0], ANELOK_CANCEL[1],
		    0, 0, 0, TIMEOUT_MS)
		if res != array.array("B"):
			raise Exception("ANELOK_CANCEL failed")

	def read(self):
		self.cancel()
		s = ""
		while len(s) < DB_SIZE:
			res = self.dev.ctrl_transfer(ANELOK_READ_DB[0],
			    ANELOK_READ_DB[1], len(s), 0, XFER_SIZE, TIMEOUT_MS)
			if len(res) != XFER_SIZE:
				raise Exception("Short transfer")
			s += "".join([ chr(x) for x in res ])
		self.cancel()

		t = ""
		while len(s):
			if len(s) < 2:
				raise Exception("Record too short")
			n = unpack_from(">H", s)[0]
			if n == 0:
				break
			if len(s) < 2 + n:
				raise Exception("Record truncated")
			t += s[0:2 + n]
			s = s[2 + n:]
		return t
		
	def write(self, db):
		s = db + pack("BB", 0, 0)
		while len(s) & 3:
			s += pack("B", 0)

		pos = 0
		self.cancel()
		while len(s):
			t = s[:XFER_SIZE]
			res = self.dev.ctrl_transfer(ANELOK_WRITE_DB[0],
			    ANELOK_WRITE_DB[1], pos, 0, t, TIMEOUT_MS)
			if res != len(t):
				raise Exception("Short transfer")
			pos += XFER_SIZE
			s = s[XFER_SIZE:]
		self.cancel()
			
	def __init__(self):
		self.dev = usb.core.find(idVendor = anelok_vid,
		    idProduct = anelok_pid)
#		self.dev.set_configuration()
		if self.dev is None:
			raise Exception("Device not found")
