#!/usr/bin/python
#
# gui/account.py - Account page
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import pygtk
import gtk

import device
from account_db import Account_db, Record
import versioning
from tools import *


#
# Accounts appear in three different types of structures:
#
# 1) In the actual account database:
#
#    foo ...
#    dir/bar ...
#    dir/baz ...
#    ...
#
#    There are no entries for directories per se, since directories are
#    part of the path names in the records.
#
# 2) In the lists returned by Account_db.list:
#
#    List for base = None: [ foo ..., dir ... ]
#
#    List for base = "dir" node: [ bar ..., baz ...]
#
#    These lists contain both "leaf nodes" (account records) of the respective
#    directory and its immediate subdirectories. Each element is of the class
#    Node.
#
# 3) Account_page.map:
#
#    [ foo, [ bar, baz ] ]
#
#    Each list element is either a list or a Node.
#


def is_list(x):
	return isinstance(x, list)

def is_prefix(x, y):
	return y[0:len(x)] == x


class Account_page:

	# ----- Determine the path of any item in the tree --------------------

	def tree_dir_path_name(self, tree, level):
		for entry in tree:
			if is_list(entry):
				found = self.tree_dir_path_name(entry, level)
				if found is not None:
					return found
			else:
				return "/".join(entry.path.split("/")[0:level])
		return None

	def tree_dir_path_find(self, tree, node, level):
		for entry in tree:
			if entry is node:
				return self.tree_dir_path_name(node, level)
			if is_list(entry):
				found = self.tree_dir_path_find(entry, node,
				    level + 1)
				if found is not None:
					return found
		return None

	def tree_path(self, node):
		if is_list(node):
			return self.tree_dir_path_find(self.map, node, 1)
		else:
			return node.path


	# ----- Account and directory tree tree structure ---------------------

	#
	# Lookup a path in the tree. Returns:
	#
	# - a node if the path identifies an existing record node,
	# - a list if the path identifies an existing directory,
        # - False if the path tries to use a node as both directory and record,
        # - None if nothing was found.
	#
	# @@@ a bit clumsy. We could do it all with just one loop.
	#

	def tree_lookup(self, tree, path):
		for node in tree:
			if is_list(node):
				for entry in node:
					if is_list(entry):
						continue
					if is_prefix(path, entry.path + "/") \
					    and entry.path.rfind("/") == \
					    len(path):
						return node
				found = self.tree_lookup(node, path)
				if found is not None:
					return found
			else:
				if node.path == path:
					return node
				if is_prefix(node.path + "/", path):
					return False
		return None

	def build_tree(self, tree_base = None, pw_base = None):
		tree = []
		for node in self.db.list(pw_base):
			if not node:
				self.treestore.append(tree_base, [ "???" ])
				tree.append(None)
				continue
			w = self.treestore.append(tree_base, [ node.name ])
			if node.is_directory:
				tree.append(self.build_tree(w, node))
			else:
				tree.append(node)
		return tree

	# ----- Detail page ---------------------------------------------------

	def refresh(self):
		# @@@ known bug: collapses the tree
		# @@@ should at least open the path to the new/current entry
		self.selected_node = None
		self.show_node(None)
		self.treestore.clear()
		self.map = self.build_tree()

	def update(self, refresh = True):
		r = self.selected_node.rec
		r.update(self.selected_node.fields)
		if refresh:
			self.refresh()
		else:
			self.revert_button.set_sensitive(r.has_changed())

	def validate_name(self, s):
		if not Record.name_is_valid(s):
			return False
		path = "/".join(self.selected_node.path.split("/")[0:-1] + [s])
		clash = self.tree_lookup(self.map, path)
		if clash is not None and clash is not self.selected_node:
			return False
# update node
# update tree
# update revert button
		return True

	def set_name(self, s):
		node = self.selected_node
		if is_list(node):
			pass	# directory
		else:
			node.fields.name = s
		self.update()
		return True

	def set_url(self, s):
		self.selected_node.fields.url = s
		self.update(False)
		return True

	def set_login(self, s):
		self.selected_node.fields.login = s
		self.update(None)
		return True

	def set_pw(self, s):
		self.selected_node.fields.pw = s
		self.update(False)
		return True

	def revert(self, button, node):
		node.rec.revert()
		self.refresh()

	def show_node(self, node):
		if self.details:
			self.detail_box.remove(self.details)
		if node is None:
			self.details = None
			return
		self.details = gtk.Frame()
		self.detail_box.pack_end(self.details)

		vbox = gtk.VBox(False)

		tab = gtk.Table(2, 4)
#		tab.set_border_width(5)
		vbox = gtk.VBox(False)

# @@@ should use "changed", but this yields (in gtk.main):
# GtkWarning: IA__gdk_window_get_display: assertion 'GDK_IS_WINDOW (window)' failed
# GtkWarning: IA__gdk_cursor_new_for_display: assertion 'GDK_IS_DISPLAY (display)' failed
# GtkWarning: IA__gdk_window_set_cursor: assertion 'GDK_IS_WINDOW (window)' failed
# GtkWarning: IA__gdk_cursor_unref: assertion 'cursor != NULL' failed

		add_label(tab, 0, 0, "Name")
		add_entry(tab, 1, 0, node.fields.name,
		    validate = self.validate_name, activate = self.set_name)
		add_label(tab, 0, 1, "URL")
		add_entry(tab, 1, 1, node.fields.url, validate = self.set_url)
		add_label(tab, 0, 2, "Login")
		add_entry(tab, 1, 2, node.fields.login,
		    validate = self.set_login)
		add_label(tab, 0, 3, "Password")
		add_entry(tab, 1, 3, node.fields.pw, validate = self.set_pw)

		vbox.pack_start(tab, True, False)

		self.revert_button = gtk.Button("Revert")
		self.revert_button.connect("clicked", self.revert, node)
		vbox.pack_end(self.revert_button, False, False)
		
		self.details.add(vbox)

		self.detail_box.show_all()
		self.revert_button.set_sensitive(node.rec.has_changed())

	# ----- New record ----------------------------------------------------

	#
	# Analyze a path considering the currently selected node, which acts as
	# kind of a "current directory". We return the following information,
	# in this order:
	#
	# - the full path name, e.g., "foo", "dir/bar",
	# - the directory prefix, e.g., "", "dir",
	# - the "basename" of the records, e.g., "foo", "bar",
	# - the node before which is should be inserted, or None
	# - the node after which is should be inserted, or None
	#
	# Note that both "before" and "after" can be None, in which case the
	# record should be appended at the end of the account list.
	#

	def analyze_path(self, s, selected_node = None):
		after = None
		before = None
		if s[0] == "/":
			prefix = ""
			s = s[1:]
		elif selected_node is None:
			prefix = ""
		else:
			if is_list(selected_node):
				levels = 0
				before = selected_node
				while is_list(before):
					before = before[0]
					levels += 1
				prefix = before.path
				for i in range(0, levels):
					pos = prefix.rfind("/")
					prefix = prefix[0:pos]

			else:
				prefix = selected_node.path
				pos = prefix.rfind("/")
				if pos == -1:
					prefix = ""
				else:
					prefix = prefix[0:pos]
				after = selected_node
		if prefix == "":
			name = s
		else:
			name = prefix + "/" + s
		return name, prefix, s, before, after

	#
	# Returns:
	#
	# - "" if no item is selected or the selected item is at the top level
	#   (root directory),
	# - the path leading to the selected item, with a trailing slash
	#

	def cwd(self):
		if self.selected_node is None:
			return ""
		path = self.tree_path(self.selected_node)
		if is_list(self.selected_node):
			return path + "/"
		pos = path.rfind("/")
		if pos == -1:
			return ""
		return path[0:pos + 1]

	def validate_new(self, s):
		if s == "":
			return True
		if not Record.name_is_valid(s):
			return False
		return self.tree_lookup(self.map, self.cwd() + s) is None

	def find_treepath(self, tree, path):
		i = 0
		for entry in tree:
			if is_list(entry):
				found = self.find_treepath(entry, path)
				if found is not None:
					return tuple([ i ] + list(found))
			else:
				if entry.path == path:
					return ( i, )
			i += 1
		return None

	def set_cursor(self, path):
		treepath = self.find_treepath(self.map, path)
		self.tv.expand_to_path(treepath)
		self.tv.set_cursor(treepath)

	def new_record(self, s):
		self.new_record_entry.clear()
		path, _, _, before, after = \
		    self.analyze_path(s, self.selected_node)

		self.db.new_record(path, after, before)

		self.refresh()
		self.selected_node = self.tree_lookup(self.map, path)
		self.show_node(self.selected_node)
		self.set_cursor(path)

	# ----- Account list / tree -------------------------------------------

	def select_node(self, treepath):
		if treepath is None:
			return None
		tree = self.map
		for p in treepath:
			tree = tree[p]
		return tree

	def cursor_changed(self, treeview):
		(path, focus) = treeview.get_cursor();
		self.selected_node = self.select_node(path)
		if is_list(self.selected_node):
			self.show_node(None)
		else:
			self.show_node(self.selected_node)

	def show_list(self, treestore):
		vbox = gtk.VBox(False)

		self.tv = gtk.TreeView(treestore)
		self.tv.set_headers_visible(False)
#		self.tv.set_reorderable(True)
		tvc = gtk.TreeViewColumn()
		self.tv.append_column(tvc)

		self.tv.connect("cursor_changed", self.cursor_changed)

		cell = gtk.CellRendererText()
		tvc.pack_start(cell, True)
		tvc.add_attribute(cell, 'text', 0)

		self.tv.set_search_column(0)

		scrolled_window = gtk.ScrolledWindow()
		scrolled_window.set_policy(gtk.POLICY_NEVER,
		    gtk.POLICY_AUTOMATIC)
		scrolled_window.add_with_viewport(self.tv)

		self.new_record_entry = Entry(activate = self.new_record,
		    validate = self.validate_new, on_empty = "Add account")

		vbox.pack_start(scrolled_window, expand = True, fill = True)
		vbox.pack_end(self.new_record_entry, expand = False)

		return vbox

	# ----- Menu bar ------------------------------------------------------

	def menu_new(self, w, data):
		# @@@ save on change
		self.db.new()
		self.refresh()

	def menu_load(self, w, data):
		# @@@ save on change
		chooser = gtk.FileChooserDialog("Load account database",
		    action = gtk.FILE_CHOOSER_ACTION_OPEN,
		    buttons= (
			gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_OPEN, gtk.RESPONSE_OK))
			# @@@ record "new no change" state
		res = chooser.run()
		if res == gtk.RESPONSE_OK:
			self.db.load(self.key_page.keys,
			    chooser.get_filename())
			self.refresh()
		chooser.destroy()

	def menu_save(self, w, data):
		if self.db_file:
			versioning.version(self.db_file)
			self.db.save(self.key_page.keys.writer(),
			    self.key_page.keys.readers(), self.db_file)
		else:
			self.menu_save_as(w, data)
		# @@@ record "new no change" state

	def menu_save_as(self, w, data):
		chooser = gtk.FileChooserDialog("Save account database",
		    action = gtk.FILE_CHOOSER_ACTION_SAVE,
		    buttons= (
			gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		if self.db_file:
			chooser.set_current_name(self.db_file)
		res = chooser.run()
		if res == gtk.RESPONSE_OK:
			self.db_file = chooser.get_filename()
			versioning.version(self.db_file)
			self.db.save(self.key_page.keys.writer(),
			    self.key_page.keys.readers(), self.db_file)
		chooser.destroy()

	def menu_fetch(self, w, data):
		usb = device.Usb()
		s = usb.read()
		self.db.parse(s, self.key_page.keys)
		self.refresh()

	def menu_store(self, w, data):
		usb = device.Usb()
		s = self.db.pack(self.key_page.keys.writer(),
		    self.key_page.keys.readers())
		usb.write(s)

	def menu_quit(self, w, data):
		# @@@ save on change
		gtk.main_quit()

	def make_menu(self):
		menu_items = (
		    ( "/File",		None,	None,	0,	"<Branch>" ),
		    ( "/File/New",	None,	self.menu_new,
							0,	"<Item>" ),
		    ( "/File/Load",	None,	self.menu_load,
							0,	"<Item>" ),
		    ( "/File/Save",	None,	self.menu_save,
							0,	"<Item>" ),
		    ( "/File/Save as",	None,	self.menu_save_as,
							0,	"<Item>" ),
		    ( "/File/sep",	None,	None,	0,	"<Separator>" ),
		    ( "/File/Quit",	None,	self.menu_quit,
							0,	"<Item>" ),
		    ( "/Device",	None,	None,	0,	"<Branch>" ),
		    ( "/Device/Fetch",	None,	self.menu_fetch,
							0,	"<Item>" ),
		    ( "/Device/Store",	None,	self.menu_store,
							0,	"<Item>" )
		)
		# must keep reference in instance to prevent deletion
		self.item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>")
		self.item_factory.create_items(menu_items)
		return self.item_factory.get_widget("<main>")

	# ----- Initialization ------------------------------------------------

	def __init__(self, key_page, db_file = None):
		self.db = Account_db()
		self.key_page = key_page
		self.details = None
		self.selected_node = None	# node or list
		self.db_file = db_file

		if db_file:
			self.db.load(self.key_page.keys, db_file)

		vbox = gtk.VBox(False)
		hbox = gtk.HBox()
		self.detail_box = gtk.VBox()

		self.treestore = gtk.TreeStore(str)
		self.map = self.build_tree()

		list = self.show_list(self.treestore)

		hbox.pack_start(list, expand = False)
		hbox.pack_start(self.detail_box, expand = True, fill = True)

		menu = self.make_menu()
		vbox.pack_start(menu, expand = False)
		vbox.pack_start(hbox, expand = True)

		self.w = vbox
