#!/usr/bin/python
#
# gui/tools.py - GUI tools
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import pygtk
import gtk


def add_label(tab, col, row, s, clicked = None, selectable = False):
	label = gtk.Label(s)
	label.set_alignment(0, 0)
	label.set_selectable(selectable)

	evbox = gtk.EventBox()
	evbox.add(label)
	if clicked is not None:
		evbox.connect("button-press-event", clicked)

#	tab.attach_defaults(label, col, col + 1, row, row + 1)
	tab.attach(evbox, col, col + 1, row, row + 1,
	    gtk.FILL, gtk.EXPAND | gtk.FILL, 6, 6)
#	    gtk.EXPAND | gtk.FILL, gtk.EXPAND | gtk.FILL, 6, 6)

	return label


def add_button(tab, col, row, s, clicked = None):
	button = gtk.Button(s)
	button.set_alignment(0, 0)

	if clicked is not None:
		button.connect("button-press-event", clicked)

	tab.attach(button, col, col + 1, row, row + 1,
	    gtk.FILL, gtk.EXPAND | gtk.FILL, 0, 6)
	return button


class Entry(gtk.Entry):

	def set_text_color(self, color):
		self.modify_text(self.get_state(), gtk.gdk.color_parse(color))

	def set_bg_color(self, color):
		self.modify_base(self.get_state(), gtk.gdk.color_parse(color))

	def set_on_empty(self):
		self.empty = self.get_text() == ""
		if self.empty:
			self.set_text(self.on_empty)
			self.set_text_color("grey")

	def focus_in(self, widget, event):
		if self.empty:
			self.set_text("")
			self.set_text_color("black")

	def focus_out(self, widget, event):
		self.set_on_empty()

	def clear(self):
		self.set_text("")
		self.empty = True
		self.get_toplevel().set_focus(None)

	def cancel(self):
		if self.original is None:
			self.set_text("")
			self.empty = True
		else:
			self.set_text(self.original)
			self.empty = self.original == ""
		self.get_toplevel().set_focus(None)

	def activate_handler(self, widget):
		s = self.get_text()
		if s == "":
			self.clear()
			return False
		if self.validate is not None and not self.validate(s):
			return False
		if self.activate is not None:
			self.activate(s)
		return True

	def changed(self, widget):
		if not self.has_focus():
			return
		if self.validate is None:
			return
		s = self.get_text()
		if self.validate(s):
			self.set_bg_color("white")
		else:
			self.set_bg_color("pink")

	def key_press(self, widget, event):
		key = gtk.gdk.keyval_name(event.keyval)
		if key == "Escape":
			self.cancel()

	def __init__(self, s = None, activate = None, validate = None,
	    on_empty = None):
		gtk.Entry.__init__(self)

		self.on_empty = on_empty
		self.empty = False
		self.activate = activate
		self.validate = validate
		self.original = s

		if s is None:
			s = ""
		self.set_text(s)
		if activate:
			self.connect("activate", self.activate_handler)
		if validate:
			self.connect("changed", self.changed)
		if on_empty is not None:
			self.connect("focus-in-event", self.focus_in)
			self.connect("focus-out-event", self.focus_out)
			self.set_on_empty()
		self.connect("key-press-event", self.key_press)


def add_entry(tab, col, row, s, activate = None, validate = None,
    on_empty = None):
	entry = Entry(s, activate, validate)
	tab.attach_defaults(entry, col, col + 1, row, row + 1)
