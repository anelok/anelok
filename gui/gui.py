#!/usr/bin/python
#
# gui/gui.py - User interface
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import pygtk
import gtk

from key import Key_page
from account import Account_page


# References:
#
# - "hello world" framework
#   http://www.pygtk.org/pygtk2tutorial/ch-GettingStarted.html#sec-HelloWorld
# - notebook example
#   http://www.pygtk.org/pygtk2tutorial/sec-Notebooks.html
# - notebook reference
#   https://developer.gnome.org/pygtk/stable/class-gtknotebook.html#method-gtknotebook--set-tab-label-packing
# - tree widget intro
#   http://www.pygtk.org/pygtk2tutorial/ch-TreeViewWidget.html
# - scrolled window (for treeview)
#   http://www.pygtk.org/pygtk2tutorial/sec-ScrolledWindows.html
#   http://www.pygtk.org/pygtk2reference/class-gtkscrolledwindow.html#constructor-gtkscrolledwindow
# - making TreeView cells editable
#   http://faq.pygtk.org/index.py?file=faq13.010.htp&req=show
#


class Gui:

	def delete_event(self, widget, event, data = None):
		return False

	def destroy(self, widget, data = None):
		gtk.main_quit()

	def __init__(self, key_file = None, db_file = None):
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_default_size(gtk.gdk.screen_width() / 3,
		    gtk.gdk.screen_height() / 3)

		self.window.connect("delete_event", self.delete_event)
		self.window.connect("destroy", self.destroy)

		self.key_page = Key_page(key_file)
		self.pw_page = Account_page(self.key_page, db_file)

		nb = gtk.Notebook()

		tab = gtk.Frame("Key")
		tab.set_shadow_type(gtk.SHADOW_NONE)
		nb.append_page(self.key_page.w, tab)

		tab = gtk.Frame("Accounts")
		tab.set_shadow_type(gtk.SHADOW_NONE)
		nb.append_page(self.pw_page.w, tab)

#		self.button = gtk.Button("Hello World")
#		self.button.connect("clicked", self.hello, None)

##		self.button.connect_object("clicked", gtk.Widget.destroy,
#		    self.window)
		self.window.add(nb)
#		self.button.show()

		self.window.show_all()

		gtk.main()
