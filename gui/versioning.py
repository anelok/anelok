#!/usr/bin/python
#
# gui/versioning.py - File name versioning
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import re
import os


def version(file):
	if not os.path.isfile(file):	
		return
	m = re.search("(.*)(\.[^/]*)$", file)
	if m:
		base = m.group(1)
		ext = m.group(2)
	else:
		base = file
		ext = ""
	i = 0
	while True:
		name = "%s~%d%s" % (base, i, ext)
		if not os.path.isfile(name):	
			break
		i += 1
	os.rename(file, name)
