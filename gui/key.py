#!/usr/bin/python
#
# gui/key.py - Key page
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import pygtk
import gtk

#import device
import base32
from keyfile import Keyfile, Key, Hash
import versioning
from tools import *


class Key_page:

	def get_pw_response(self, entry, dialog, res):
		dialog.response(res)

	def get_pw(self):
		dialog = gtk.Dialog("Password", None,
		    flags = gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
		    buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
		        gtk.STOCK_OK, gtk.RESPONSE_OK))
		entry = gtk.Entry()
		entry.set_visibility(False)
		entry.connect("activate", self.get_pw_response, dialog,
		    gtk.RESPONSE_OK)
		dialog.vbox.pack_start(entry)
		dialog.show_all()
		res = dialog.run()
		if res != gtk.RESPONSE_OK:
			dialog.destroy()
			return None
		pw = entry.get_text()
		dialog.destroy()
		return pw

	def load(self, key_file):
		s = self.keys.load(key_file)
		hash = None
		while True:
			try:
				self.keys.parse(s, hash)
				self.hash = hash
				self.update_menu_for_pw()
				return True
			except Exception:
				if hash is None and self.hash is not None:
					hash = self.hash
					continue
				pw = self.get_pw()
				if pw is None or pw == "":
					return False
				hash = Hash(pw, s)

	# ----- Helper functions ----------------------------------------------

	def name_exists(self, name, skip = None):
		for key in self.keys:
			if key is not skip and not key.deleted and \
			    key.name == name:
				return True
		return False

	# ----- Detail page ---------------------------------------------------

	def validate_name(self, s):
		if s == "":
			return False
		if self.name_exists(s, self.selected):
			return False
		self.selected.name = s
		self.treestore.set_value(self.selected.iter, 4, s)
		self.update_revert()
		return True

	def validate_pubkey(self, s):
		if s == "":
			self.selected.pubkey = None
			self.selected.set_reader(False)
			self.treestore.set_value(self.selected.iter, 2, False)
			self.treestore.set_value(self.selected.iter, 3, False)
			return True 
		try:
			new = self.selected.pubkey is None
			base32.decode(s)[0:32]
			self.selected.pubkey = base32.decode(s)[0:32]
			if new:
				self.selected.set_reader(True)
				self.treestore.set_value(self.selected.iter, 2,
				    True)
				self.treestore.set_value(self.selected.iter, 3,
				    True)
			self.update_revert()
			return True
		except Exception:
			return False

	def generate_key(self, widget, event):
		key = self.selected
		first_writer = self.keys.writer() is None
		key.generate(key.name)
		if first_writer:
			key.set_writer(True)
		key.set_reader(True)
		self.update()

	def update(self):
		self.refresh()
		self.show_key(self.selected)

	def update_revert(self):
		self.revert_button.set_sensitive(self.selected.has_changed())

	def revert(self, button, key):
		key.revert()
		self.show_key(key)

	def delete(self, button, key):
		key.delete(undelete = key.deleted)
		self.treestore.set_value(key.iter, 5, key.deleted)
		self.show_key(key)

	def show_key(self, key):
		if self.details:
			self.detail_box.remove(self.details)
		if key is None:
			self.details = None
			return

		self.details = gtk.Frame()
		self.detail_box.pack_end(self.details)

		vbox = gtk.VBox(False)

		tab = gtk.Table(2, 3)
#		tab.set_border_width(5)
		vbox = gtk.VBox(False)

		add_label(tab, 0, 0, "Name")
		if key.deleted:
			add_label(tab, 1, 0, key.name)
		else:
			add_entry(tab, 1, 0, key.name,
			    validate = self.validate_name)

		add_label(tab, 0, 1, "Secret key")
		if key.pubkey is None:
			if not key.deleted:
				add_button(tab, 1, 1, "Generate",
				    clicked = self.generate_key)
		elif key.seckey is None:
			add_label(tab, 1, 1, "(unknown)")
		else:
			add_label(tab, 1, 1, "(hidden)")

		add_label(tab, 0, 2, "Public key")
		if key.seckey is None:
			if key.pubkey is None:
				s = ""
			else:
				s = base32.encode(key.pubkey)
			if key.deleted:
				add_label(tab, 1, 2, s)
			else:
				add_entry(tab, 1, 2, s,
				    validate = self.validate_pubkey)
		else:
			add_label(tab, 1, 2, base32.encode(key.pubkey, True),
			    selectable = True)

		vbox.pack_start(tab, True, False)

		hbox = gtk.HBox(False)

		self.revert_button = gtk.Button("Revert")
		self.revert_button.connect("clicked", self.revert, key)
		hbox.pack_end(self.revert_button, False, False)
		self.update_revert()

		if key.deleted:
			self.delete_button = gtk.Button("Undelete")
			self.delete_button.set_sensitive(
			    not self.name_exists(key.name))
		else:
			self.delete_button = gtk.Button("Delete")
		self.delete_button.connect("clicked", self.delete, key)
		hbox.pack_end(self.delete_button, False, False)

		vbox.pack_end(hbox, False, False)

		self.details.add(vbox)

		self.detail_box.show_all()

	# ----- Menu bar ------------------------------------------------------

	def menu_new(self, w, data):
		# @@@ save on change
		self.show_key(None)
		self.keys.new()
		self.selected = None
		self.refresh()

	def menu_load(self, w, data):
		# @@@ save on change
		chooser = gtk.FileChooserDialog("Load key list",
		    action = gtk.FILE_CHOOSER_ACTION_OPEN,
		    buttons= (
			gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_OPEN, gtk.RESPONSE_OK))
			# @@@ record "new no change" state
		res = chooser.run()
		if res == gtk.RESPONSE_OK:
			self.load(chooser.get_filename())
			self.refresh()
		chooser.destroy()

	def menu_set_pw(self, w, data):
		pw = self.get_pw()
		if pw is None:
			return
		if pw == "":
			# @@@ ask for confirmation
			self.hash = None
		else:
			self.hash = Hash(pw)
		self.update_menu_for_pw()

	def menu_save(self, w, data):
		if self.key_file:
			versioning.version(self.key_file)
			self.keys.save(self.key_file, self.hash)
		else:
			self.menu_save_as(w, data)
		# @@@ record "new no change" state

	def menu_save_as(self, w, data):
		chooser = gtk.FileChooserDialog("Save key list",
		    action = gtk.FILE_CHOOSER_ACTION_SAVE,
		    buttons= (
			gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
			gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		if self.key_file:
			chooser.set_current_name(self.key_file)
		res = chooser.run()
		if res == gtk.RESPONSE_OK:
			self.key_file = chooser.get_filename()
			versioning.version(self.key_file)
			self.keys.save(self.key_file, self.hash)
		chooser.destroy()

	def menu_quit(self, w, data):
		# @@@ save on change
		gtk.main_quit()

	def update_menu_for_pw(self):
		# If files were specified on the command line we don't have a
		# window or menu when loading the keys, but create it later.
		if self.w is None:
			return
		label = self.item_factory.get_widget("/File/Set password"). \
		    get_child()
		if self.hash is None:
			label.set_text("Set password")
		else:
			label.set_text("Change password")

	def make_menu(self):
		menu_items = (
		    ( "/File",		None,	None,	0,	"<Branch>" ),
		    ( "/File/New",	None,	self.menu_new,
							0,	"<Item>" ),
		    ( "/File/Load",	None,	self.menu_load,
							0,	"<Item>" ),
		    ( "/File/sep1",	None,	None,	0,	"<Separator>" ),
		    ( "/File/Set password", None, self.menu_set_pw,
							0,	"<Item>" ),
		    ( "/File/Save",	None,	self.menu_save,
							0,	"<Item>" ),
		    ( "/File/Save as",	None,	self.menu_save_as,
							0,	"<Item>" ),
		    ( "/File/sep2",	None,	None,	0,	"<Separator>" ),
		    ( "/File/Quit",	None,	self.menu_quit,
							0,	"<Item>" ),
		)
		# must keep reference in instance to prevent deletion
		self.item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>")
		self.item_factory.create_items(menu_items)
		self.update_menu_for_pw()
		return self.item_factory.get_widget("<main>")

	# ----- New key -------------------------------------------------------

	def new_key(self, s):
		self.new_key_entry.clear()
		key = Key()
		key.name = s
		self.keys.append(key)
		self.refresh()
		self.tv.set_cursor(len(self.keys) - 1)

	# ----- Key list ------------------------------------------------------

	def refresh(self):
		self.treestore.clear()
		self.build_list()
		row = 0
		for key in self.keys:
			if key is self.selected:
				self.tv.set_cursor(row)
				return
			row += 1

	def click_writer(self, cell, path):
		key = self.keys[int(path)]
		writer = self.keys.writer()
		if writer is not None:
			writer.set_writer(False)
		key.set_writer(True)
		self.refresh()

	def click_reader(self, cell, path):
		key = self.keys[int(path)]
		key.set_reader(not key.is_reader)
		self.refresh()

	def cursor_changed(self, treeview):
                (path, focus) = treeview.get_cursor();
		self.selected = self.keys[path[0]]
		self.show_key(self.selected)

	def validate_new(self, s):
		if s == "":
			return True
		if not Key.name_is_valid(s):
			return False

		return not self.name_exists(s)

# Setting tooltips is bizarrely complicated. Using the approach described
# here:
#
# http://mailman.daa.com.au/pipermail/pygtk/2010-December/019279.html
# http://mailman.daa.com.au/pipermail/pygtk/2010-December/019280.html

	def query_tooltip(self, tv, x, y, keyboard_tip, tooltip):
		res = tv.get_tooltip_context(x, y, keyboard_tip)
		if res is None:
			return False
		model, path, iter = res

        	bin_x, bin_y = tv.convert_widget_to_bin_window_coords(x, y)
		res = tv.get_path_at_pos(bin_x, bin_y)
		if res is None:
			return False
		path, column, cell_x, cell_y = res

		if hasattr(column, "tip"):
			tooltip.set_text(column.tip)
		else:
			return False

		tv.set_tooltip_cell(tooltip, path, None, None)
		return True

	def show_list(self, treestore):
		vbox = gtk.VBox(False)

		self.tv = gtk.TreeView(treestore)
		self.tv.set_headers_visible(False)
#		self.tv.set_reorderable(True)

		self.tv.connect("cursor_changed", self.cursor_changed)

		cell = gtk.CellRendererToggle()
		cell.set_radio(True)
		cell.set_property("activatable", True)
		cell.connect("toggled", self.click_writer)
		tvc = gtk.TreeViewColumn("Writer", cell)
		tvc.add_attribute(cell, 'visible', 0)
		tvc.add_attribute(cell, 'active', 1)
		self.tv.append_column(tvc)
		tvc.tip = "Key used to write"

		cell = gtk.CellRendererToggle()
		cell.set_property("activatable", True)
		cell.connect("toggled", self.click_reader)
		tvc = gtk.TreeViewColumn("Reader", cell)
		tvc.add_attribute(cell, 'visible', 2)
		tvc.add_attribute(cell, 'active', 3)
		self.tv.append_column(tvc)
		tvc.tip = "Keys allowed to read"

		cell = gtk.CellRendererText()
		tvc = gtk.TreeViewColumn("Name", cell)
		tvc.add_attribute(cell, 'text', 4)
		tvc.add_attribute(cell, 'strikethrough', 5)
		self.tv.append_column(tvc)

		self.tv.set_search_column(0)

		self.tv.set_has_tooltip(True)
		self.tv.connect("query-tooltip", self.query_tooltip)

		scrolled_window = gtk.ScrolledWindow()
		scrolled_window.set_policy(gtk.POLICY_NEVER,
		    gtk.POLICY_AUTOMATIC)
		scrolled_window.add_with_viewport(self.tv)

		self.new_key_entry = Entry(activate = self.new_key,
		    validate = self.validate_new, on_empty = "Add key")

		vbox.pack_start(scrolled_window, expand = True, fill = True)
		vbox.pack_end(self.new_key_entry, expand = False)

                return vbox

	def build_list(self):
		for key in self.keys:
			if key.name is None:
				name = base32.encode(key.pubkey, True)
			else:
				name = key.name
			key.iter = self.treestore.append(None,
			    ( key.seckey is not None,
			      key is self.keys.writer(),
			      key.pubkey is not None,
			      key in self.keys.readers(),
			      name, key.deleted ))

	def make_key_page(self):
		vbox = gtk.VBox(False)
		hbox = gtk.HBox()
		self.detail_box = gtk.VBox()

		self.treestore = gtk.TreeStore(bool, bool, bool, bool, str,
		    bool)
		self.build_list()

		list = self.show_list(self.treestore)

		if len(self.keys):
			writer = self.keys.writer()
			if writer is None:
				self.tv.set_cursor(0)
			else:
				n = 0
				for key in self.keys:
					if key is writer:
						self.tv.set_cursor(n)
						break
					n += 1

		hbox.pack_start(list, expand = False)
		hbox.pack_start(self.detail_box, expand = True, fill = True)

		menu = self.make_menu()
		vbox.pack_start(menu, expand = False)
		vbox.pack_start(hbox, expand = True)

		return vbox

	# ----- Initialization ------------------------------------------------

	def __init__(self, key_file = None):
		self.keys = Keyfile()
		self.key_file = key_file
		self.details = None
		self.selected = None
		self.hash = None
		self.w = None
		if key_file:
			if not self.load(key_file):
				raise Exception("Cannot load key file")
		self.w = self.make_key_page()
