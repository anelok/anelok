#!/usr/bin/python
#
# gui/anelok-gui.py - Host-side GUI for managing accounts
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


from sys import argv
import sys
from os.path import dirname, realpath

sys.path.insert(0, dirname(realpath(argv[0])) + "/../crypter")

from gui import Gui


def usage():
	print >>sys.stderr, "usage: " + argv[0] + " [keyfile [pw_db]]"
	exit(1)


if len(argv) > 1:
	key_file = argv[1]
else:
	key_file = None
if len(argv) > 2:
	db_file = argv[2]
else:
	db_file = None
if len(argv) > 3:
	usage()

Gui(key_file, db_file)
