.PHONY:		all dsv AUTHORS
.PHONY:		build clean spotless wipe

all:		build

dsv:
		dsv setup BOOKSHELF BOOKSHELF2 BOOKSHELF3

AUTHORS:	authors.add
		{ echo "*** MACHINE-GENERATED. DO NOT EDIT ! ***"; echo; \
		  cat authors.add; \
		  ../wernermisc/bin/authors; } >AUTHORS || { rm -f $@: exit 1; }

# ----- Firmware build and cleanup --------------------------------------------

DIRS = crypter fw sim

build:
		for n in $(DIRS); do make -C $$n || exit 1; done

clean:
		for n in $(DIRS); do make -C $$n clean; done

spotless:
		for n in $(DIRS); do make -C $$n spotless; done

wipe:		spotless
		make -C crypter wipe

# ----- Coverity submission ---------------------------------------------------

#
# Example content for Makefile.coverity:
#
# COV_BUILD = /home/sys/cov/cov-analysis-linux64-7.7.0.4/bin/cov-build
# COV_UPLOAD = qippl $(1) tmp/coverity/
# COV_URL = http://downloads.qi-hardware.com/people/werner/tmp/coverity/$(1)
# COV_PROJECT = Anelok
# COV_USER = werner@almesberger.net
# COV_TOKEN = eeThi6Yi8Ag2-KeoPh2ie2t
#
# (The token in this example is just some random garbage.)
#

-include Makefile.coverity

COV_FILE = anelok.bz2

coverity:
		@[ "$(COV_BUILD)" -a "$(COV_UPLOAD)" -a "$(COV_URL)" \
		    -a "$(COV_PROJECT)" -a "$(COV_USER)" -a "$(COV_TOKEN)" ] \
		    || { echo "Makefile.coverity is missing or incomplete" \
		    1>&2; exit 1; }
		$(MAKE) wipe
		rm -rf cov-int
		$(COV_BUILD) --dir cov-int make
		tar caf $(COV_FILE) cov-int
		$(call COV_UPLOAD,$(COV_FILE))
		curl --data "project=$(COV_PROJECT)&token=$(COV_TOKEN)&email=$(COV_USER)&url=$(call COV_URL,$(COV_FILE))" \
		    https://scan.coverity.com/builds
