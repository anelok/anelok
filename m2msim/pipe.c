/*
 * m2msim/pipe.c - Faux pair of connected UARTs, for simulation
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#include "uart.h"
#include "tracing.h"
#include "pipe.h"


/* ----- Pipe operations --------------------------------------------------- */


static bool pipe_may_tx(void *ctx)
{
	struct pipe_side *side = ctx;

	return side->peer->empty && !side->peer->break_in;
}


static void pipe_tx(void *ctx, uint8_t data)
{
	struct pipe_side *side = ctx;

	trace_tx(side, data);

	assert(side->peer->empty);
	side->peer->rx_buf = data;
	side->peer->empty = 0;
}


static void pipe_send_break(void *ctx)
{
	struct pipe_side *side = ctx;

	trace_tx_break(side);

	side->peer->break_in = 1;
}


static bool pipe_poll_rx(void *ctx, uint8_t *v)
{
	struct pipe_side *side = ctx;

	if (side->empty)
		return 0;

	if (side->ops && side->ops->drop &&
	    side->ops->drop(side->user, side->rx_buf)) {
		side->empty = 1;
		return 0;
	}

	trace_rx(side, side->rx_buf);

	*v = side->rx_buf;
	side->empty = 1;
	return 1;
}


static bool pipe_poll_error(void *ctx, bool *is_break)
{
	struct pipe_side *side = ctx;

	if (side->error) {
		trace_rx_error(side);

		side->error = 0;
		*is_break = 0;
		return 1;
	}
	if (side->break_in) {
		if (side->ops && side->ops->drop_break &&
		    side->ops->drop_break(side->user)) {
			side->break_in = 0;
			return 0;
		}

		trace_rx_break(side);

		side->break_in = 0;
		*is_break = 1;
		return 1;
	}
	return 0;
}


struct uart_ops pipe_ops = {
	.poll_rx	= pipe_poll_rx,
	.poll_error	= pipe_poll_error,
	.may_tx		= pipe_may_tx,
	.tx		= pipe_tx,
	.send_break	= pipe_send_break,
};


static void side_init(struct pipe_side *side)
{
	side->empty = 1;
	side->break_in = side->error = 0;
}


void pipe_init(struct pipe_ctx *pipe,
    const struct pipe_sim_ops *ops_a, void *user_a,
    const struct pipe_sim_ops *ops_b, void *user_b)
{
	side_init(&pipe->a);
	side_init(&pipe->b);
	pipe->a.name = 'A';
	pipe->b.name = 'B';
	pipe->a.peer = &pipe->b;
	pipe->b.peer = &pipe->a;
	pipe->a.ops = ops_a;
	pipe->a.user = user_a;
	pipe->b.ops = ops_b;
	pipe->b.user = user_b;
}
