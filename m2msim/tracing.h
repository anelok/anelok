/*
 * m2msim/tracing.h - Tracing of protocol activity
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TRACING_H
#define	TRACING_H

#include <stdbool.h>
#include <stdint.h>

#include "pipe.h"
#include "m2m.h"
#include "m2msim.h"


extern bool tracing;


void trace_rx(const struct pipe_side *side, uint8_t data);
void trace_tx(const struct pipe_side *side, uint8_t data);
void trace_tx_break(const struct pipe_side *side);
void trace_rx_break(const struct pipe_side *side);
void trace_rx_error(const struct pipe_side *side);

void trace_drop(const struct side *side, uint8_t data);
void trace_drop_break(const struct side *side);

void trace_rx_state(const struct m2m *m2m, const char *state, int line);
void trace_tx_state(const struct m2m *m2m, const char *state, int line);
void trace_link_state(const struct m2m *m2m, const char *state, int line);

void trace_side(const struct side *side, const char *fmt, ...);
void trace_m2m(const struct m2m *m2m, const char *fmt, ...);

void trace_div(void);

#endif /* TRACING_H */
