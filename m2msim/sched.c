/*
 * m2msim/sched.c - Simulation schedules
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>

#include "m2msim.h"


/* We keep this in a separate file to avoid namespace pollution */

/* TX callbacks */

#define	tx(data)	{ ev_tx, data },
#define	tx_done()	{ ev_tx_done, 0 },
#define	tx_fail()	{ ev_tx_fail, 0 },

/* TX API */

#define	send(type)	{ ev_send, type },
#define	tx_end()	{ ev_tx_end, 0 },
#define	tx_cancel()	{ ev_tx_cancel, 0 },

/* RX callbacks */

#define	begin(type)	{ ev_begin, type },
#define	rx(data)	{ ev_rx, data },
#define	rx_fail()	{ ev_rx_fail, 0 },

/* RX API */

#define	rx_end()	{ ev_rx_end, 0 },
#define	rx_cancel()	{ ev_rx_cancel, 0 },

/* Link callbacks */

#define	drop(data)	{ ev_drop, data },
#define	drop_break()	{ ev_drop_break, 0 },

/* Other events */

#define	timeout()	{ ev_timeout, 0 },

/* Meta-events */

#define	chain		{ ev_chain, 0 },
#define	start_a()	{ ev_start_a, 0 },
#define	start_a_rx()	{ ev_start_a_rx, 0 },
#define	start_a_tx()	{ ev_start_a_tx, 0 },
#define	start_b()	{ ev_start_b, 0 },
#define	start_b_rx()	{ ev_start_b_rx, 0 },
#define	start_b_tx()	{ ev_start_b_tx, 0 },


#define	TITLE(text)	title = text;
#define	BEGIN(name)	events_##name = (struct event []) {
#define	END		{ ev_end } };
#define	HALT(name)	running_##name = 0;


const char *title;
const struct event *events_a;
const struct event *events_a_rx;
const struct event *events_a_tx;
const struct event *events_b;
const struct event *events_b_rx;
const struct event *events_b_tx;
bool running_a = 1;
bool running_a_rx = 1;
bool running_a_tx = 1;
bool running_b = 1;
bool running_b_rx = 1;
bool running_b_tx = 1;


/*
 * We have to call run_schedule from there because the compound literals we use
 * only live as long as their enclosing block and there is no way to make them
 * "static".
 *
 * See also http://stackoverflow.com/q/21882564
 */

bool setup_and_run_schedule(const char *name)
{
	#include SCHEDULE

	return run_schedule(name);
}
