/*
 * m2msim/m2msim.h - M2M protocol test harness
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "tracing.h"
#include "pipe.h"
#include "m2m.h"
#include "m2msim.h"


/* ----- Diagnostics ------------------------------------------------------- */


static const char *tx_state_name[] = {
	[M2M_TX_IDLE]	= "TX_IDLE",
	[M2M_TX_WAKE]	= "TX_WAKE",
	[M2M_TX_WOKEN]	= "TX_WOKEN",
	[M2M_TXING]	= "TXING",
	[M2M_ACK_WAIT]	= "ACK_WAIT",
	[M2M_TX_FLUSH]	= "TX_FLUSH",
};

static const char *rx_state_name[] = {
	[M2M_RX_IDLE]	= "RX_IDLE",
	[M2M_RX_BEGIN]	= "RX_BEGIN",
	[M2M_RX]	= "RX",
};

static const char *event_name[] = {
	[ev_end]	= "end",
	[ev_tx]		= "tx",
	[ev_tx_done]	= "tx_done",
	[ev_tx_fail]	= "tx_fail",
	[ev_send]	= "send",
	[ev_tx_end]	= "tx_end",
	[ev_tx_cancel]	= "tx_cancel",
	[ev_begin]	= "begin",
	[ev_rx]		= "rx",
	[ev_rx_fail]	= "rx_fail",
	[ev_rx_end]	= "rx_end",
	[ev_rx_cancel]	= "rx_cancel",
	[ev_drop]	= "drop",
	[ev_drop_break]	= "drop_break",
	[ev_timeout]	= "timeout",
	[ev_chain]	= "chain",
	[ev_start_a]	= "start_a",
	[ev_start_a_rx]	= "start_a_rx",
	[ev_start_a_tx]	= "start_a_tx",
	[ev_start_b]	= "start_b",
	[ev_start_b_rx]	= "start_b_rx",
	[ev_start_b_tx]	= "start_b_tx",
};


static void dump_event(FILE *file, const struct event *ev)
{
	switch (ev ->ev) {
	case ev_tx:
	case ev_send:
	case ev_begin:
	case ev_rx:
	case ev_drop:
		fprintf(file, "%s(%02x)", event_name[ev->ev], ev->data);
		break;
	case ev_end:
	case ev_tx_done:
	case ev_tx_fail:
	case ev_tx_end:
	case ev_tx_cancel:
	case ev_rx_fail:
	case ev_rx_end:
	case ev_rx_cancel:
	case ev_drop_break:
	case ev_timeout:
	case ev_chain:
	case ev_start_a:
	case ev_start_a_rx:
	case ev_start_a_tx:
	case ev_start_b:
	case ev_start_b_rx:
	case ev_start_b_tx:
		fprintf(file, "%s", event_name[ev->ev]);
		break;
	default:
		abort();
	}
}


static void dump_events(const struct event *list, const struct event *curr)
{
	const struct event *p = list;

	while (1) {
		if (p == curr)
			fprintf(stderr, "    ==> ");
		else
			fprintf(stderr, "\t");
		if (p->ev == ev_chain || (p != list && p[-1].ev == ev_chain))
			fprintf(stderr, "    ");
		dump_event(stderr, p);
		fprintf(stderr, "\n");
		if (p->ev == ev_end)
			return;
		p++;
	}
}


static void dump_vars(const struct side *side)
{
	const struct m2m *m2m = &side->m2m;

	fprintf(stderr, "%c: in {%s%s%s} out {%s%s%s} type %02x timeout %d\n",
	    side->name,
	    m2m->in_ack ? "ack" : "", m2m->in_ack && m2m->in_nak ? "," : "",
	    m2m->in_nak ? "nak" : "",
	    m2m->out_ack ? "ack" : "", m2m->out_ack && m2m->out_nak ? "," : "",
	    m2m->out_nak ? "nak" : "",
	    m2m->type, side->timeout);
}


/* ----- Timer ------------------------------------------------------------- */


static void start_timer(void *ctx, uint16_t ms)
{
	struct side *side = ctx;

	(void) ms;
	side->timeout = 0;
}


static bool poll_timer(void *ctx)
{
	struct side *side = ctx;

	if (!side->timeout)
		return 0;
	side->timeout = 0;
	return 1;
}


static struct m2m_timer_ops timer_ops = {
	.start	= start_timer,
	.poll	= poll_timer,
};


/* ----- Test-driven actions ----------------------------------------------- */


static bool try_act(struct m2m *m2m, const struct event *ev)
{
	struct side *side = m2m->rx_ctx;

	switch (ev->ev) {
	case ev_end:
	case ev_tx:
	case ev_tx_done:
	case ev_tx_fail:
	case ev_begin:
	case ev_rx:
	case ev_rx_fail:
	case ev_drop:
	case ev_drop_break:
		return 0;
	case ev_send:
		trace_m2m(m2m, "api send(%02x)", ev->data);
		m2m_send(m2m, ev->data);
		return 1;
	case ev_tx_end:
		trace_m2m(m2m, "api tx_end");
		m2m_tx_end(m2m);
		return 1;
	case ev_tx_cancel:
		trace_m2m(m2m, "api tx_cancel");
		m2m_tx_cancel(m2m);
		return 1;
	case ev_rx_end:
		trace_m2m(m2m, "api rx_end");
		m2m_rx_end(m2m);
		return 1;
	case ev_rx_cancel:
		trace_m2m(m2m, "api rx_cancel");
		m2m_rx_cancel(m2m);
		return 1;
	case ev_timeout:
		trace_m2m(m2m, "timeout");
		side->timeout = 1;	
		return 1;
	case ev_start_a:
		trace_m2m(m2m, "start A");
		running_a = 1;
		return 1;
	case ev_start_a_rx:
		trace_m2m(m2m, "start A.RX");
		running_a_rx = 1;
		return 1;
	case ev_start_a_tx:
		trace_m2m(m2m, "start A.TX");
		running_a_tx = 1;
		return 1;
	case ev_start_b:
		trace_m2m(m2m, "start B");
		running_b = 1;
		return 1;
	case ev_start_b_rx:
		trace_m2m(m2m, "start B.RX");
		running_b_rx = 1;
		return 1;
	case ev_start_b_tx:
		trace_m2m(m2m, "start B.TX");
		running_b_tx = 1;
		return 1;
	default:
		abort();
	}
}


/* ----- Manage expectations ----------------------------------------------- */


static void expect_data(struct side *side, bool tx,
    enum event_type ev, uint8_t data)
{
	const struct event **p = tx ? side->p_tx : side->p_rx;

	if ((*p)->ev == ev && (*p)->data == data) {
		(*p)++;
		return;
	}
	fflush(stdout);
	fprintf(stderr, "%c: expected %s(%02x), got ", side->name,
	    event_name[ev], data);
	dump_event(stderr, *p);
	fprintf(stderr, "\n");
	exit(1);
}


static void expect(struct side *side, bool tx, enum event_type ev)
{
	const struct event **p = tx ? side->p_tx : side->p_rx;

	if ((*p)->ev == ev) {
		(*p)++;
		return;
	}
	fflush(stdout);
	fprintf(stderr, "%c: expected %s, got ", side->name, event_name[ev]);
	dump_event(stderr, *p);
	fprintf(stderr, "\n");
	exit(1);
}


static void chain(struct side *side, bool tx)
{
	const struct event **p = tx ? side->p_tx : side->p_rx;

	if ((*p)->ev != ev_chain)
		return;
	(*p)++;
	if (try_act(&side->m2m, *p)) {
		(*p)++;
		return;
	}
	fprintf(stderr, "cannot chain ");
	dump_event(stderr, *p);
	fprintf(stderr, "\n");
	exit(1);
}


/* ----- Line callbacks ---------------------------------------------------- */


static bool line_drop(void *ctx, uint8_t data)
{
	struct side *side = ctx;
	const struct event **p = side->p_rx;

	if ((*p)->ev != ev_drop || (*p)->data != data)
		return 0;
	trace_drop(side, data);
	(*p)++;
	return 1;
}


static bool line_drop_break(void *ctx)
{
	struct side *side = ctx;
	const struct event **p = side->p_rx;

	if ((*p)->ev != ev_drop_break)
		return 0;
	trace_drop_break(side);
	(*p)++;
	return 1;
}


static const struct pipe_sim_ops sim_pipe_ops = {
	.drop		= line_drop,
	.drop_break	= line_drop_break,
};


/* ----- Receiver callbacks ------------------------------------------------ */


static void cb_begin(void *ctx, uint8_t type)
{
	struct side *side = ctx;

	expect_data(side, 0, ev_begin, type);
	trace_side(side, "cb begin(%02x)", type);
	chain(side, 0);
}


static void cb_rx(void *ctx, uint8_t data)
{
	struct side *side = ctx;

	expect_data(side, 0, ev_rx, data);
	trace_side(side, "cb rx(%02x)", data);
	chain(side, 0);
}


static void cb_rx_fail(void *ctx)
{
	struct side *side = ctx;

	expect(side, 0, ev_rx_fail);
	trace_side(side, "cb rx_fail");
	chain(side, 0);
}


static struct m2m_rx_ops rx_ops = {
	.begin	= cb_begin,
	.rx	= cb_rx,
	.fail	= cb_rx_fail,
};


/* ----- Sender callbacks -------------------------------------------------- */


static uint8_t cb_tx(void *ctx)
{
	struct side *side = ctx;
	uint8_t data = (*side->p_tx)->data;

	expect(side, 1, ev_tx);
	trace_side(side, "cb tx(%02x)", data);
	chain(side, 1);
	return data;
}


static void cb_tx_done(void *ctx)
{
	struct side *side = ctx;

	expect(side, 1, ev_tx_done);
	trace_side(side, "cb tx_done");
	chain(side, 1);
}


static void cb_tx_fail(void *ctx)
{
	struct side *side = ctx;

	expect(side, 1, ev_tx_fail);
	trace_side(side, "cb tx_fail");
	chain(side, 1);
}


static struct m2m_tx_ops tx_ops = {
	.tx	= cb_tx,
	.done	= cb_tx_done,
	.fail	= cb_tx_fail,
};


/* ----- Test setup and execution ------------------------------------------ */


static bool act(struct m2m *m2m, const struct event **ev)
{
	assert((*ev)->ev != ev_chain);
	if (!try_act(m2m, *ev))
		return 0;
	(*ev)++;
	return 1;
}


bool run_schedule(const char *name)
{
	const struct event *pa_rx = events_a ? events_a : events_a_rx;
	const struct event *pa_tx = events_a ? events_a : events_a_tx;
	const struct event *pb_rx = events_b ? events_b : events_b_rx;
	const struct event *pb_tx = events_b ? events_b : events_b_tx;
	struct side a = {
		.name = 'A',
		.timeout = 0,
		.p_rx = &pa_rx,
		.p_tx = pa_rx == pa_tx ? &pa_rx : &pa_tx,
	};
	struct side b = {
		.name = 'B',
		.timeout = 0,
		.p_rx = &pb_rx,
		.p_tx = pb_rx == pb_tx ? &pb_rx : &pb_tx,
	};
	struct pipe_ctx pipe;
	bool change;
	int which = 0;
	bool ok = 1;

	pipe_init(&pipe, &sim_pipe_ops, &a, &sim_pipe_ops, &b);

	m2m_init(&a.m2m, &pipe_ops, &pipe.a, &timer_ops, &a);
	m2m_tx_setup(&a.m2m, &tx_ops, &a);
	m2m_rx_setup(&a.m2m, &rx_ops, &a);

	m2m_init(&b.m2m, &pipe_ops, &pipe.b, &timer_ops, &b);
	m2m_tx_setup(&b.m2m, &tx_ops, &b);
	m2m_rx_setup(&b.m2m, &rx_ops, &b);

	printf("%s: %s\n", name, title);

	/* @@@ add more fine-grained control over sequencing */
	do {
		int i;

		trace_div();
		change = 0;
		for (i = 0; i != 6 ; i++) {
			switch (which++ % 6) {
			case 0:
				change = m2m_poll(&a.m2m);
				break;
			case 1:
				change = m2m_poll(&b.m2m);
				break;
			case 2:
				if ((events_a || events_a_rx) &&
				    (running_a || running_a_rx))
					change = act(&a.m2m, &pa_rx);
				break;
			case 3:
				if (events_a_tx && running_a_tx)
					change = act(&a.m2m, &pa_tx);
				break;
			case 4:
				if ((events_b || events_b_rx) &&
				    (running_b || running_b_rx))
					change = act(&b.m2m, &pb_rx);
				break;
			case 5:
				if (events_b_tx && running_b_tx)
					change = act(&b.m2m, &pb_tx);
				break;
			default:
				abort();
			}
			if (change)
				break;
		}
	} while (change);

	if (a.m2m.rx_state) {
		fprintf(stderr, "A: final RX state is %s, should be %s\n",
		    rx_state_name[a.m2m.rx_state], rx_state_name[0]);
		ok = 0;
	}
	if (a.m2m.tx_state) {
		fprintf(stderr, "A: final TX state is %s, should be %s\n",
		    tx_state_name[a.m2m.tx_state], tx_state_name[0]);
		ok = 0;
	}
	if (b.m2m.rx_state) {
		fprintf(stderr, "B: final RX state is %s, should be %s\n",
		    rx_state_name[b.m2m.rx_state], rx_state_name[0]);
		ok = 0;
	}
	if (b.m2m.tx_state) {
		fprintf(stderr, "B: final TX state is %s, not should be %s\n",
		    tx_state_name[b.m2m.tx_state], tx_state_name[0]);
		ok = 0;
	}
	if (pa_rx->ev != ev_end) {
		fprintf(stderr, "Schedule A.RX did not finish:\n");
		dump_events(events_a ? events_a : events_a_rx, pa_rx);
		ok = 0;
	}
	if (pa_tx->ev != ev_end && events_a_tx) {
		fprintf(stderr, "Schedule A.TX did not finish:\n");
		dump_events(events_a ? events_a : events_a_tx, pa_tx);
		ok = 0;
	}
	if (pb_rx->ev != ev_end) {
		fprintf(stderr, "Schedule B.RX did not finish:\n");
		dump_events(events_b ? events_b : events_b_rx, pb_rx);
		ok = 0;
	}
	if (pb_tx->ev != ev_end && events_b_tx) {
		fprintf(stderr, "Schedule B.TX did not finish:\n");
		dump_events(events_b ? events_b : events_b_tx, pb_tx);
		ok = 0;
	}
	if (!ok) {
		dump_vars(&a);
		dump_vars(&b);
	}
	return ok;
}


static void usage(const char *name)
{
	fprintf(stderr, "usage: %s [-q]\n", name);
	exit(1);
}


int main(int argc, char **argv)
{
	const char *slash;
	bool ok;
	int c;

	while ((c = getopt(argc, argv, "q")) != EOF)
		switch (c) {
		case 'q':
			tracing = 0;
			break;
		default:
			usage(*argv);
		}

	if (argc != optind)
		usage(*argv);

	slash = strrchr(*argv, '/');
	ok = setup_and_run_schedule(slash ? slash + 1 : *argv);
	return !ok;
}
