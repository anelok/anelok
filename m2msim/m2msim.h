/*
 * m2msim/m2msim.h - Simulation schedules
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef M2MSIM_H
#define	M2MSIM_H

#include <stdbool.h>
#include <stdint.h>

#include "m2m.h"


enum event_type {
	ev_end,
	ev_tx,
	ev_tx_done,
	ev_tx_fail,
	ev_send,
	ev_tx_end,
	ev_tx_cancel,
	ev_begin,
	ev_rx,
	ev_rx_fail,
	ev_rx_end,
	ev_rx_cancel,
	ev_drop,
	ev_drop_break,
	ev_timeout,
	ev_chain,	/* call next event from callback */
	ev_start_a,
	ev_start_a_rx,
	ev_start_a_tx,
	ev_start_b,
	ev_start_b_rx,
	ev_start_b_tx,
};

struct event {
	enum event_type ev;
	uint8_t data;
};

struct side {
	char name;
	struct m2m m2m;
	bool timeout;
	const struct event **p_rx, **p_tx;
};


extern const char *title;
extern const struct event *events_a;
extern const struct event *events_a_rx;
extern const struct event *events_a_tx;
extern const struct event *events_b;
extern const struct event *events_b_rx;
extern const struct event *events_b_tx;
extern bool running_a;
extern bool running_a_rx;
extern bool running_a_tx;
extern bool running_b;
extern bool running_b_rx;
extern bool running_b_tx;

bool run_schedule(const char *name);
bool setup_and_run_schedule(const char *name);

#endif /* M2MSIM_H */
