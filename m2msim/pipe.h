/*
 * m2msim/pipe.h - Faux pair of connected UARTs, for simulation
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PIPE_H
#define	PIPE_H

#include <stdbool.h>
#include <stdint.h>

#include "uart.h"


struct pipe_sim_ops {
	bool (*drop)(void *ctx, uint8_t data);
	bool (*drop_break)(void *ctx);
};

struct pipe_side {
	char name;
	uint8_t rx_buf;
	bool empty;
	bool break_in;
	bool error;
	struct pipe_side *peer;
	const struct pipe_sim_ops *ops;
	void *user;
};

struct pipe_ctx {
	struct pipe_side a, b;
};


extern struct uart_ops pipe_ops;


void pipe_init(struct pipe_ctx *pipe,
    const struct pipe_sim_ops *ops_a, void *user_a,
    const struct pipe_sim_ops *ops_b, void *user_b);

#endif /* PIPE_H */
