/*
 * m2msim/tracing.c - Tracing of protocol activity
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "m2m.h"
#include "pipe.h"
#include "tracing.h"


bool tracing = 1;


static void indent_side(const struct side *side, bool end)
{
	switch (side->name) {
	case 'A':
		printf(end ? "" : "\t");
		break;
	case 'B':
		printf(end ? "\t\t\t" : "\t\t");
		break;
	default:
		printf(end ? "??? " : "???\t\t");
		break;
	}
}


static void indent_m2m(const struct m2m *m2m, bool end)
{
	indent_side(m2m->rx_ctx, end);
}


static void indent_pipe(const struct pipe_side *side)
{
	switch (side->name) {
	case 'A':
		printf("\t");
		break;
	case 'B':
		printf("\t\t");
		break;
	default:
		printf("???\t");
		break;
	}
}


/* ----- Pipe activity ----------------------------------------------------- */


void trace_rx(const struct pipe_side *side, uint8_t data)
{
	if (!tracing)
		return;
	indent_pipe(side);
	if (side->name == 'A')
		printf("%02x <--\n", data);
	else
		printf("--> %02x\n", data);
}


void trace_tx(const struct pipe_side *side, uint8_t data)
{
	if (!tracing)
		return;
	indent_pipe(side);
	if (side->name == 'A')
		printf("%02x -->\n", data);
	else
		printf("<-- %02x\n", data);
}


void trace_rx_break(const struct pipe_side *side)
{
	if (!tracing)
		return;
	indent_pipe(side);
	if (side->name == 'A')
		printf("BRK <--\n");
	else
		printf("--> BRK\n");
}


void trace_tx_break(const struct pipe_side *side)
{
	if (!tracing)
		return;
	indent_pipe(side);
	if (side->name == 'A')
		printf("BRK -->\n");
	else
		printf("<-- BRK\n");
}


void trace_rx_error(const struct pipe_side *side)
{
	if (!tracing)
		return;
	indent_pipe(side);
	printf("ERROR\n");
}


/* ----- Trace induced data loss ------------------------------------------- */


void trace_drop(const struct side *side, uint8_t data)
{
	if (!tracing)
		return;
	indent_side(side, 0);
	printf("drop %02x\n", data);
}


void trace_drop_break(const struct side *side)
{
	if (!tracing)
		return;
	indent_side(side, 0);
	printf("drop break\n");
}


/* ----- State changes ----------------------------------------------------- */


void trace_rx_state(const struct m2m *m2m, const char *state, int line)
{
	if (!tracing)
		return;
	indent_m2m(m2m, 1);
	printf("%s:%d\n", state, line);
}


void trace_tx_state(const struct m2m *m2m, const char *state, int line)
{
	if (!tracing)
		return;
	indent_m2m(m2m, 1);
	printf("%s:%d\n", state, line);
}


void trace_link_state(const struct m2m *m2m, const char *state, int line)
{
	if (!tracing)
		return;
	indent_m2m(m2m, 0);
	printf("%s:%d\n", state, line);
}


/* ----- API activity ------------------------------------------------------ */


void trace_side(const struct side *side, const char *fmt, ...)
{
	va_list ap;

	if (!tracing)
		return;
	indent_side(side, 1);
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	printf("\n");
}


void trace_m2m(const struct m2m *m2m, const char *fmt, ...)
{
	va_list ap;

	if (!tracing)
		return;
	indent_m2m(m2m, 1);
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	printf("\n");
}


/* ----- Division ---------------------------------------------------------- */


void trace_div(void)
{
	int i;

	if (!tracing)
		return;
	for (i = 0; i != 30; i++)
		printf("- ");
	putchar('\n');
}
