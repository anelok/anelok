#!/usr/bin/perl
#
# bom/xml2bom.pl - Convert an eeschema (KiCAD 4) BOM to <qty domain name>
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

while (<>) {
	if (/<comp ref="(.*?)"/) {
		$ref = $1;
		undef $val;
		next;
	}
	if (/<value>\s*(.*?)\s*<\/value>/) {
		$val = $1;
		$val =~ s/\s+/_/g;
		next;
	}
	next unless /<\/comp>/;
	die "$ref has no value" unless defined $val;
	print "1 BOM ${ref}_$val\n";
}
