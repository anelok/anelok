#!/usr/bin/perl
#
# bom/csv2docdb.pl - Convert a "shopping list" from CSV to eeshow annotations
#
# Written 2017 by Werner Almesberger
# Copyright 2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

while (<>) {
	die unless /^\d+,"([^"]+)",.*\d,"(.*)"$/;
	for (split(",", $1)) {
		print "$_\n$2\n(no link)\n\n" if $2 ne "";
	}
}
