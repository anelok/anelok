#!/usr/bin/perl
#
# bom/lst2bom.pl - Convert an eeschema (KiCAD) BOM to <qty domain name>
#
# Written 2014-2015 by Werner Almesberger
# Copyright 2014-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


while (<>) {
	last if /^#End Cmp/;
	next unless /^\|\s+([A-Z]+\d+)\s+([^;]*?)\s*;/;
	print "1 BOM $1_$2\n";
}
