update=Sat Sep 21 02:00:42 2013
last_client=pcbnew
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[eeschema]
version=1
LibDir=
NetFmtName=
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=../../kicad-libs/components/pwr
LibName2=../../kicad-libs/components/r
LibName3=../../kicad-libs/components/c
LibName4=../../kicad-libs/components/gencon
LibName5=../../kicad-libs/components/powered
LibName6=../../kicad-libs/components/er-oled-fpc30
LibName7=../../kicad-libs/components/testpoint
LibName8=../../kicad-libs/components/tswa
LibName9=../../kicad-libs/components/device_sot
LibName10=../../kicad-libs/components/8_10-card
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill="    1.295400"
PadDrillOvalY="    1.295400"
PadSizeH="    3.000000"
PadSizeV="    3.000000"
PcbTextSizeV="    1.500000"
PcbTextSizeH="    1.500000"
PcbTextThickness="    0.300000"
ModuleTextSizeV="    1.000000"
ModuleTextSizeH="    1.000000"
ModuleTextSizeThickness="    0.150000"
SolderMaskClearance="    0.000000"
SolderMaskMinWidth="    0.000000"
DrawSegmentWidth="    0.100000"
BoardOutlineThickness="    0.100000"
ModuleOutlineThickness="    0.150000"
[pcbnew/libraries]
LibDir=
LibName1=../../kicad-libs/modules/stdpass
LibName2=../../kicad-libs/modules/pads-array
LibName3=../../kicad-libs/modules/tswa
LibName4=../../kicad-libs/modules/sot
LibName5=../../kicad-libs/modules/er-oled-fpc30
