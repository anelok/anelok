#!/usr/bin/perl
#
# fsm2gv.pl - Convert simple FSM description to Graphviz
#
# Written 2017 by Werner Almesberger
# Copyright 2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# TODO:
# - add access to node / edge attributes
# - add "width" override
# - not sure if state annotations should continue to be in parentheses or not
#

sub flush
{
	return unless defined $cond;
	$s =~ s/^\s*//;
	$s =~ s/\s*$//;
	my @a = split(/\s*,\s*/, $s);
#print STDERR "$state $cond |", join("|", @a), "|\n";
	die "minimum edge is condition -> state" unless $#a >= 0;
	my $to = pop(@a);
	push(@{ $edges{$state}{$to} }, [ $cond, @a ]);
	undef $s;
}


while (<>) {
	chop;
	s/#.*$//;
	next if /^\s*$/;
	if (/^(\?)?([A-Za-z0-9_]+)(\(.*?\))?/) {
		&flush;
		$state = $2;
		push(@states, $state);
		$trans{$state} = $1 eq "?";
		$label{$state} = $3;
		undef $cond;
		$_ = $';
	}
	if (/^\s*(\S.*?)\s*->/) {
		&flush;
		$cond = $1;
		$_ = $';
	}
	$s .= $_;
}
&flush;

print <<"EOF";
digraph G {
	rankdir = LR;
	node	[ shape = circle, fixedsize = true, width = 1.0 ];
	$states[0] [ shape = doublecircle ];
EOF

for $state (@states) {
	my @a = ();

	push(@a, "label = \"$state\\n$label{$state}\"")
	    if defined $label{$state};
	push(@a, "shape=diamond") if $trans{$state};
	print "$state [ ", join(", ", @a), "]\n" if $#a > -1;
	for $to (keys %{ $edges{$state} }) {
		print "$state -> $to [ label = <\n"; 
		print "\t<TABLE border=\"0\" cellspacing=\"1\" cellpadding=\"0\">\n";
		for (@{ $edges{$state}{$to} }) {
			my @a = @{ $_ };
			my $cond = shift @a;
	#
	# A quirk of *dot is that "align" affects the previous element, not the
	# current one. Left-aligning TR seems to make the graph slightly nicer,
	# for no clear reason.
	#
			print "\t<TR align=\"left\"><TD align=\"left\">";
#			print "\t<TR><TD align=\"left\">";
			print join("<BR align=\"left\"/>", "<B>$cond</B>", @a);
			print "<BR align=\"left\"/></TD></TR>\n";
		}
		print "\n</TABLE>> ];\n";
	}
}

print "}\n";
