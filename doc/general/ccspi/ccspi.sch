EESchema Schematic File Version 2
LIBS:cc2543
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "1 jun 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CC2543 U?
U 1 1 538BB505
P 5500 4000
F 0 "U?" H 4550 5050 60  0000 C CNN
F 1 "CC2543" H 5500 4000 60  0000 C CNN
F 2 "~" H 5500 4000 60  0000 C CNN
F 3 "~" H 5500 4000 60  0000 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
Text Notes 5650 2250 0    80   ~ 16
SS
Text Notes 4900 2250 0    80   ~ 16
MO
Text Notes 3500 3550 0    80   ~ 16
MI
Text Notes 3800 4600 0    80   ~ 16
C
Text Notes 3200 4450 0    80   ~ 16
SS
Text Notes 3800 3550 0    80   ~ 16
MI
Text Notes 3200 4600 0    80   ~ 16
MO
Text Notes 3200 4300 0    80   ~ 16
C
Text Notes 3800 3850 0    80   ~ 16
SS
Text Notes 4900 6050 0    80   ~ 16
MI
Text Notes 4900 2450 0    80   ~ 16
MO
Text Notes 5850 2250 0    80   ~ 16
C
Wire Notes Line
	5100 5900 5100 6100
Wire Notes Line
	3100 4100 3400 4100
Wire Notes Line
	3100 6100 3100 4100
Wire Notes Line
	5100 6100 3100 6100
Wire Notes Line
	6000 2100 6000 2300
Wire Notes Line
	3400 3600 3700 3600
Wire Notes Line
	3400 4100 3400 5900
Wire Notes Line
	3400 5900 5100 5900
Wire Notes Line
	3400 3600 3400 2100
Wire Notes Line
	3400 2100 6000 2100
Wire Notes Line
	3700 4700 4000 4700
Wire Notes Line
	5150 2300 5150 2500
Wire Notes Line
	3700 4700 3700 2300
Wire Notes Line
	3700 2300 6000 2300
Wire Notes Line
	4000 4700 4000 2500
Wire Notes Line
	4000 2500 5150 2500
Text Notes 3500 2250 0    100  ~ 20
Alt 2
Text Notes 3800 2450 0    100  ~ 20
Alt 3
Text Notes 3200 6050 0    100  ~ 20
Normal
Wire Notes Line
	4200 4250 4000 4250
Wire Notes Line
	3700 4250 3400 4250
Wire Notes Line
	4200 4400 4000 4400
Wire Notes Line
	3700 4400 3400 4400
Wire Notes Line
	4200 4550 4000 4550
Wire Notes Line
	3700 4550 3400 4550
Wire Notes Line
	4200 3500 4000 3500
Wire Notes Line
	5000 2700 5000 2500
Wire Notes Line
	5900 2700 5900 2300
Wire Notes Line
	5750 2700 5750 2300
Wire Notes Line
	4200 3800 4000 3800
Wire Notes Line
	5000 5300 5000 5900
Text Notes 2300 1600 0    120  ~ 24
Cheat-sheet for CC2543 SPI pin assignment
$EndSCHEMATC
