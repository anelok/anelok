Selection of the MCU

-------------------------------------------------------------------------------

Search at Digi-Key:

Keyword: OTG -> 2808
Integrated Circuits (ICs) / Embedded - Microcontrollers -> 2487
In stock -> 613
Core Processor: ARM* -> 235
Voltage - Supply (min): < 2.0 V -> 183
Voltage - Supply (max): >= 3.3 V -> 152
Package / Case: <= 64 pins -> 29
Package / Case: no BGA -> 26

"-> N" indicates the number of hits remaining. At this point, all the
chips has >= 64 kB Flash and there were no T&R packagings. I sorted
by unit price for 100 units:

1.59	Freescale KL2:  MKL24Z64VFM4	M0+ 48 MHz, 32-VFQFN
2.04	Freescale KL2:  MKL25Z128VFM4	M0+ 48 MHz, 32-VFQFN
2.15	Freescale KL2:  MKL25Z128VFT4	M0+ 48 MHz, 48-VFQFN
2.21	Freescale KL2:  MKL25Z128VLH4	M0+ 48 MHz, 64-LQFP
2.55	Freescale K20:  MK20DX128VFM5	M4 50 MHz, 32-VFQFN
... more Freescale K20, KL4 ...
6.11	TI Tiva C:	TM4C1233D5PMI	M4F 80 MHz, 64-LQFP
6.67	STM32F2:	STM32F205RCT6	M3 120 MHz, 64-LQFP
7.01	TI Tiva C:	TM4C1237E6PMI	M4F 80 MHz, 64-LQFP
... more STM and TI chips in 64-LQFP ...

So this left three families: Freescale Kinetis, and there especially
the KL2 sub-series, then TI Tiva C, and finally STM32F2.

The TM4C1233D5PMI seems to have only a USB device [1], so the first
suitable TM4C in this list would be the TM4C1237E6PMI. But this isn't
correct either, and it would really be the TM4C1237D5PM, with
Digi-Key list as non-USB while TI list it as OTG.

From the STM32 family, there's also the STM32F205RBT6, which is
currently out of stock.

Some of the chips don't have pricing for large volumes. For 1000
units, picking the cheapest with at least 48 pins of each group:

MKL25Z128VFT4	USD 2.15	(@100)	index	1
STM32F205RBT6	USD 4.46	(@1000)		2.07
TM4C1237D5PMI	USD 4.60	(@1000)		2.14
STM32F205RCT6	USD 4.87	(@1000)		2.27

I eliminated chips with 32 pins because my current estimates suggest
the device needs around 25 pins already for I/O, and some are
invariably "lost" for reset and power, bringing the total number of
pins used to at least 33. It may still be possible to sequeeze this
into a 32 pin budget but let's see about this later.

Characteristics:

Chip		Core	Speed	Package	Flash	RAM	Features
---------------	-------	-------	-------	-------	-------	----------------------
MKL25Z128VFT4	M0+	 48 MHz	48-QFN	128 kB	 16 kB	USB-Reg, ID
STM32F205RBT6	M3	120 MHz	64-LQFP	128 kB	 68 kB	2 x USB, RNG, SDIO, ID
TM4C1237D5PMI	M4F	 80 MHz	64-LQFP	 64 kB	 24 kB	FPU
STM32F205RCT6	M3	120 MHz	64-LQFP	256 kB	100 kB	2 x USB, RNG, SDIO, ID

Current consumption at 48 MHz (peripherals off/static) should be
around 5 mA for the MKL25Z128VFT4, 10 mA for the STM32F205RBT6,
and 24 mA for the TM4C1237D5PMI.

The KL2 series has a USB voltage regulator which such a feature is
missing in the others. A voltage regulator is particularly
attractive for the RF dongle. The MCU in the RF dongle should also
be small. As we can see above, the KL2 series has members in 32-QFN
packages. The STM32 series has the STM32F103 in 36-QFN which has
USB device (not OTG) and up to 128 kB of Flash. So it seems that
one could find chips for both types of systems in the same family.

The STM32F2 series has a built-in random number generator that
produces about 32 Mbps. We also have one in the transceiver at
2 Mbps. The STM32F2 series also contains members that have built-in
hardware crypto (DES, 3DES, AES, MD5, SHA1, HMAC), but that doesn't
seem overly usefulf or out purposes.

KL2 and STM32 have a unique chip ID. I haven't found one in a quick
look through the TM4C documentation.

From this comparison, it seems that Freescale KL2 and STM32F2
are the main candidates. The TI chip is slightly more expensive
than the STM32F2 but is weak in terms of features and draws a lot
of power. It also has an FPU we have no use for.

The STM32F2 can run very quickly, but we probably wouldn't want to
make use of that, due to power consumption. The STM32F2 series
goes up to 1 MB of Flash while the KL2 reaches its limit at 128
kB.

All have an RTC, but the KL2 has no separate supply for it. This
needs more investigation.
