\documentclass[12pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{xfrac}
\usepackage{changepage}
\usepackage[hidelinks,bookmarksnumbered=true]{hyperref}

\newenvironment{indented}{\trivlist\item\begin{adjustwidth}{1em}{}}%
{\end{adjustwidth}\endtrivlist}

\renewcommand\arraystretch{1.1}


\begin{document}

\title{Anelok Inter-SoC Protocol}
\author{Werner Almesberger
  \url{werner@almesberger.net}}
\maketitle

The Anelok architecture, both in the Anelok password safe and in the
Y-Box, uses two microcontrollers: one is a general-purpose SoC (System
on a Chip) that performs most of the higher-level functions of the device,
such as USB communication, user interface, etc. The other microcontroller
is a specialized RF transceiver that implements part of the RF communication
stack.

Both are connected through the extended SPI interface described in this
document, with the general-purpose SoC acting as master and the RF SoC
acting as slave.

% -----------------------------------------------------------------------------

\section{Hardware constraints}

We currently use Freescale Kinetis L series chips as master and Texas
Instruments CC254x series RF SoCs as slave. We may also consider the ST
STM32 series for future design variants.

Interrupt capabilities depend on the chip family. Table \ref{intcap}
summarizes the interrupt capabilities of the chips mentioned above.

\def\Y{$\bullet$}
\def\N{$\circ$}

\begin{table}[htp]
\centering
\begin{tabular}{l|cc|ccc}
  Chip family & \multicolumn{2}{c|}{Level-triggered} &
  \multicolumn{3}{c}{Edge-triggered} \\
  & High & Low & Rising & Falling & Both \\
  \hline
  Kinetis L & \Y & \Y & \Y & \Y & \Y \\
  STM 32    & \N & \N & \Y & \Y & \Y \\
  CC254x    & \N & \N & \Y & \Y & \N \\
\end{tabular}
\label{intcap}
\caption{Interrupt capabilities of chips used or considered for Anelok.}
\end{table}

The protocol must be designed to be at least compatible with the
capabilities of the chips we use. In order to be able to use different
chips in the future, it would be desirable if the protocol could also
work with systems that support only level-triggered or only edge-triggered
interrupts.

The SPI slave must be given enough time to set up its SPI engine
before the master performs a transfer. Likewise, the slave must also
be able to defer further transfers if it is still processing a prior
transfer or is busy for some other reason.

Since SPI itself does not provide this kind of flow control mechanism,
we have to add one on top of it.

% -----------------------------------------------------------------------------

\section{Interface hardware}

Master and slave are connected by an SPI interface plus any additional
signals that are needed for in-circuit flashing the RF SoC. Besides the
usual four SPI lines, two additional signals are needed: an interrupt
from the slave to the master and a transfer request signal from master
to slave.
Figure \ref{system} shows how master and slave are connected.

\begin{figure}[htb]
\centering
\includegraphics{system.pdf}
\label{system}
\caption{Signals for handshake and SPI interface between master
  (Kinetis L or similar) and slave (RF SoC).}
\end{figure}

nSEL is active-low. We choose nINT and nREQ to be active-low as well.
There are no other devices on
this SPI bus. The design allows for sharing the SPI bus but not the
interrupt and request lines.

The CC2543 is remote-flashed using the debug interface, which has three
signal: nRESET, debug clock DC, and debug data DD. We use DD for the
interrupt and DC for request.

Voltage levels and related parameters are determined by the chips used
in the design and are therefore not specified in this document. The
Anelok system generally operates either at battery voltage or at a
nominal 3.3 V, with both SoCs using the same supply voltage.

% -----------------------------------------------------------------------------

\section{SPI protocol}

While the handshake protocol described in section \ref{handshake} does not
constrain the configuration of the SPI interface, we need to define a
common setup. We choose to operate the SPI interface as follows:

\begin{indented}
\begin{tabular}{ll}
  SPI mode:  & 0 \hfill \\
  Word size: & 8 bits \\
  Bit order: & most significant bit first \\
\end{tabular}
\end{indented}

Due to using mode 0, we must ensure that the slave has completed setting
up its SPI engine at the time the master lowers nSEL, since it must output
the first bit even before the first clock pulse. This may seem to be an
inconvenience but being forced to clearly separate handshake from the SPI
transfer actually helps to avoid depending on -- usually poorly
documented -- hardware implementation details later on.

Timing details are to be defined, depending on the characteristics of
the chips involved. Table \ref{spitiming} lists the most important
timing parameters of the two chips we are currently using.

\begin{table}[hbp]
\centering
\begin{tabular}{l|cccc}
  Chip family & Role & SPI clock & nSEL falling to SCLK & SCLK to nSEL rising \\
              &      & MHz (max) & ns (min) & ns (min) \\
  \hline
  Kinetis KL26 & Master & 12 & \sfrac{1}{2} SPI clock & \sfrac{1}{2} SPI clock \\
  CC2543       & Slave  & 4  & 63 & 63 \\
\end{tabular}
\label{spitiming}
\caption{Characteristics of the SPI interfaces of chips we use. For KL26,
  we assume that SPI0 is used and therefore
  $\textrm{f}_\textrm{periph} = \textrm{f}_\textrm{BUS} = 24 \textrm{MHz}$.}
\end{table}

% -----------------------------------------------------------------------------

\section{Transfer handshake}
\label{handshake}

Since the slave may not be able to set up its SPI engine such that it
could accept a transfer at any time and without prior warning,
we use a simple handshake protocol that allows master and slave to
synchronize the timing of the actual SPI transfer.

Both sides can initiate a transfer and in each transfer either side can
send data.


\subsection{Slave initiates transfer}

The slave initiates a transfer by signaling an interrupt, as shown in
figure \ref{slaveinit}. The master then acknowledges the interrupt and
performs the SPI transaction without further synchronization.

\begin{figure}[htb]
\centering
\includegraphics{slave-init.pdf}
\label{slaveinit}
\caption{SPI transfer requested by slave:
  1) slave sets up SPI, arms nSEL falling interrupt (see text),
     and lowers nINT.
  2) master detects falling nINT, sets up SPI, drops first nREQ and then
     nSEL, and performs the SPI transfer.
  3) on nSEL falling, the slave raises nINT again.
  4) the master waits until nINT is high (inactive) again and then
     raises nREQ.
  5) the slave can send the next interrupt when nREQ has risen.}
\end{figure}

The interrupt is acknowledged by the master sending data. If the SPI
bus is not shared, data transmission can be detected by directly
monitoring nSEL for a falling edge. Alternatively, the
slave could use the transmit or reception indication an SPI device
usually provides.

Since the slave can arm an interrupt on nREQ high immediately after
seeing nSEL fall, the master must ensure that nREQ is low and has
stabilized before lowering nSEL. Else, a slave using level-triggered
interrupts could see nREQ still high and thus lose synchronization.

The master must keep nREQ low until a) the slave has de-asserted the interrupt
b) the master is ready to receive the next interrupt.
The slave can generate the next interrupt any time after the master has
de-asserted nREQ. If the slave is able to immediately handle another SPI
transfer, I can generate the next interrupt even before the SPI transfer
has finished.


\subsection{Master initiates transfer}

The master initiates a transfer by lowering nREQ and waiting until
the slave acknowledges this by asserting nINT. The master then
proceeds by sending the data and the transfer continues as in the
previous scenario. This is depicted in figure \ref{masterinit}.

\begin{figure}[htb]
\centering
\includegraphics{master-init.pdf}
\label{masterinit}
\caption{SPI transfer requested by master:
  1) master sets up SPI and lowers nREQ.
  2) on nREQ falling, slave sets up SPI, arms nSEL falling interrupt
     (see text), and lowers nINT.
  3) master detects falling nINT and starts sending data.
  Steps 4) to 6) are the same as steps 3) to 5) of transfers initiated
     by the slave.}
\end{figure}


\subsection{Both sides simultaneously initiate a transfer}

Master and slave may try to initiate a transfer at the same time, as
shown in figure \ref{race}. In this case, the other side's acknowledgement
may appear to arrive before the request.

\begin{figure}[htb]
\centering
\includegraphics{race.pdf}
\label{race}
\caption{If master and slave try to initiate a transfer at the same time,
  nINT may be asserted before (A) or after (B) nREQ.}
\end{figure}

A slave that is about to assert nINT can simply ignore nREQ for the
purpose of handshaking since it not involved at the beginning of
slave-initiated transfers.

The master must take into account that it may receive the nINT
interrupt before is has asserted nREQ. In particular, if it
uses a mixture of interrupts and polling, it must not count the
interrupt as one permission to send and nINT being low after
asserting nREQ as another one.

Furthermore, the slave must never try to consider falling nREQ as
an acknowledgement of nINT. While this would work in a normal
slave-initiated transfer, it could make the slave de-assert nINT
before the master has detected it if the master was about to
initiate a transfer as well.

\end{document}
