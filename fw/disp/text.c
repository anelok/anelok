/*
 * fw/text.c - Text rendering
 *
 * Written 2013-2016 by Werner Almesberger
 * Copyright 2013-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>
#include <string.h>

#include "display.h"
#include "text.h"

#define	static	static const
#include "font_small.xbm"
#include "font_medium.xbm"
#undef static


const struct font font_small = {
	.data	= font_small_bits,
	.w	= 5,
	.h	= 8,
	.cx	= 8,
	.cy	= 8,
	.mx	= 16,
};

const struct font font_medium = {
	.data	= font_medium_bits,
	.w	= 6,
	.h	= 10,
	.cx	= 8,
	.cy	= 10,
	.mx	= 16,
};

#include "font_10x20.h"


void text_char_clip(const struct font *f, char ch, uint8_t x, int8_t y,
    const struct rect *clip)
{
	uint8_t px, py;

	px = (ch - ' ') % f->mx;
	py = (ch - ' ') / f->mx;
	display_blit_clip(f->data + px * (f->cx / 8) +
	    py * (f->mx * f->cx / 8 * f->cy),
	    f->w, f->h, f->mx * f->cx / 8, x, y, clip);
}


void text_char(const struct font *f, char ch, uint8_t x, int8_t y)
{
	text_char_clip(f, ch, x, y, &clip_all);
}


uint8_t text_str_clip(const struct font *f, const char *s,
    uint8_t x, int8_t y, const struct rect *clip)
{
	while (*s) {
		if (x >= FB_X)
			return x;
		text_char_clip(f, *s++, x, y, clip);
		x += f->w + 1;
	}
	return x;
}


uint8_t text_str(const struct font *f, const char *s, uint8_t x, int8_t y)
{
	return text_str_clip(f, s, x, y, &clip_all);
}


uint8_t text_str_fake(const struct font *f, const char *s, uint8_t x)
{
	uint16_t len = strlen(s);

	if (x > FB_X)
		return x;
	if ((FB_X - x) / len < (f->w + 1))
		return FB_X;
	return x + (f->w + 1) * len;
}


uint8_t text_nl(const struct font *f)
{
	return f->h;
}
