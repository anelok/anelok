/*
 * fw/console.h - Printf-style text output
 *
 * Written 2013, 2015, 2017 by Werner Almesberger
 * Copyright 2013, 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CONSOLE_H
#define	CONSOLE_H

#include <stdint.h>


#define	CONSOLE_FONT	font_small


void console_char(char ch);
void console_string(const char *s);
void console_printf(const char *fmt, ...)
    __attribute__((format(printf, 1, 2)));
void console_goto(uint8_t nx, uint8_t ny);
void console_goto_relative(int8_t rx, int8_t ry);
void console_update(void);
void console_update_async(void);
void console_clear(void);
void console_window(uint8_t x0, uint8_t x1);
void console_init(void);

#endif /* !CONSOLE_H */
