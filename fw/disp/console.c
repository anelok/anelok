/*
 * fw/console.c - Printf-style text output
 *
 * Written 2013-2015, 2017 by Werner Almesberger
 * Copyright 2013-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stddef.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

#include "event.h"
#include "fmt.h"
#include "display.h"
#include "text.h"
#include "console.h"


#define	X_STEP	(CONSOLE_FONT.w + 1)
#define	Y_STEP	(CONSOLE_FONT.h)


static uint8_t win_x0, win_x1;
static uint8_t x, y;
static bool changed;


static void nl(void)
{
	x = win_x0;
	if (y < FB_Y)
		y += Y_STEP;
}


void console_char(char ch)
{
	if (y >= FB_Y)
		return;
	if (ch == '\n') {
		nl();
		return;
	}
	if (ch == '\t') {
		x = x - (x % (8 * X_STEP)) + 8 * X_STEP;
		/*
		 * handle line wraps immediately, in case we have a sequence of
		 * tabs that would overflow uint8_t
		 */
		if (x > win_x1 - X_STEP)
			nl();
		return;
	}
	if (x > win_x1 - X_STEP)
		nl();
	text_char(&CONSOLE_FONT, ch, x, y);
	x += X_STEP;
	changed = 1;
}


void console_string(const char *s)
{
	while (*s)
		console_char(*s++);
}


static void out_char(void *user, char ch)
{
	console_char(ch);
}


void console_printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vformat(out_char, NULL, fmt, ap);
	va_end(ap);
}


void console_goto(uint8_t nx, uint8_t ny)
{
	x = nx;
	y = ny;
}


void console_goto_relative(int8_t rx, int8_t ry)
{
	x += rx;
	y += ry;
}


void console_update(void)
{
	if (changed)
		display_update();
	changed = 0;
}


void console_update_async(void)
{
	if (changed)
		event_set_interrupt(ev_console);
}


void console_clear(void)
{
	display_clear();
	x = win_x0;
	y = 0;
	changed = 1;
}


void console_window(uint8_t x0, uint8_t x1)
{
	win_x0 = x0;
	win_x1 = x1;
	if (x < win_x0 || x > win_x1)
		x = win_x0;
}


void console_init(void)
{
	display_on(1);
	x = y = 0;
	win_x0 = 0;
	win_x1 = FB_X - 1;
	changed = 0;
}
