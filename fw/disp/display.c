/*
 * fw/display.c - OLED display driver
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "qa.h"
#include "tick.h"
#include "gpio.h"
#include "board.h"
#include "display.h"


#define	PAGES		(FB_Y / 8)


const struct rect clip_all = {
	.x	= 0,
	.y	= 0,
	.w	= FB_X,
	.h	= FB_Y,
};


static uint8_t fb[FB_X * PAGES];

static bool change = 1;
static bool rotate;


static void reset(void)
{
	msleep(20);	/* "Power Stabilized - Delay Recommended" */
	gpio_begin(DISP_nRES);
	gpio_clr(DISP_nRES);
	msleep(1);	/* "3us Delay Minimum */
	gpio_set(DISP_nRES);
	gpio_end(DISP_nRES);
}


static void spi_begin(void)
{
	gpio_begin(DISP_nCS);
	gpio_clr(DISP_nCS);
	gpio_begin(DISP_SDIN);
	gpio_begin(DISP_SCLK);
}


static void spi_end(void)
{
	gpio_set(DISP_nCS);
	gpio_end(DISP_nCS);
	gpio_end(DISP_SDIN);
	gpio_end(DISP_SCLK);
}


static void spi_send(uint8_t v)
{
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (v & mask)
			gpio_set(DISP_SDIN);
		else
			gpio_clr(DISP_SDIN);
		gpio_set(DISP_SCLK);
		gpio_clr(DISP_SCLK);
	}
}


static void dnc(bool data)
{
	gpio_begin(DISP_DnC);
	if (data)
		gpio_set(DISP_DnC);
	else
		gpio_clr(DISP_DnC);
	gpio_end(DISP_DnC);
}


static void cmd1(uint8_t cmd)
{
	dnc(0);
	spi_begin();
	spi_send(cmd);
	spi_end();
}


static void cmd2(uint8_t cmd, uint8_t arg)
{
	dnc(0);
	spi_begin();
	spi_send(cmd);
	spi_send(arg);
	spi_end();

}


void display_clear(void)
{
	memset(fb, 0, sizeof(fb));
	change = 1;
}


static inline void set_pixel(uint8_t x, uint8_t y)
{
	fb[x + (y >> 3) * FB_X] |= 1 << (y & 7);
}


void display_set(uint8_t x, uint8_t y)
{
	if (x >= FB_X || y >= FB_Y)
		oops();
	if (rotate)
		set_pixel(FB_X - 1 - x, FB_Y - 1 - y);
	else
		set_pixel(x, y);
	change = 1;
}


static inline void clr_pixel(uint8_t x, uint8_t y)
{
	fb[x + (y >> 3) * FB_X] &= ~(1 << (y & 7));
}


void display_clr(uint8_t x, uint8_t y)
{
	if (x >= FB_X || y >= FB_Y)
		oops();
	if (rotate)
		clr_pixel(FB_X - 1 - x, FB_Y - 1 - y);
	else
		clr_pixel(x, y);
	change = 1;
}


static void blit_normal(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, uint8_t y)
{
	uint8_t *f;
	uint8_t i, mask;

	while (h) {
		f = fb + x + (y >> 3) * FB_X;
		mask = 1 << (y & 7);
		for (i = 0; i != w; i++) {
			if (p[i >> 3] & (1 << (i & 7)))
				*f |= mask;
			f++;
		}
		p += span;
		y++;
		h--;
	}
}


static void blit_rotated(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, uint8_t y)
{
	uint8_t *f;
	uint8_t i, mask;

	y = FB_Y - y;
	while (h) {
		y--;
		f = fb + (FB_X - x) + (y >> 3) * FB_X;
		mask = 1 << (y & 7);
		for (i = 0; i != w; i++) {
			f--;
			if (p[i >> 3] & (1 << (i & 7)))
				*f |= mask;
		}
		p += span;
		h--;
	}
}


void display_blit_clip(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y, const struct rect *clip)
{
	uint8_t xend = clip->x + clip->w;
	uint8_t yend = clip->y + clip->h;

	if (x < clip->x) {
		uint8_t dx = clip->x - x;

		REQUIRE(!(dx & 7));
		if (dx >= w)
			return;
		w -= (clip->x - x);
		p += dx >> 3;
		x = clip->x;
	}
	if (x >= xend)
		return;
	if (x + w > xend)
		w = xend - x;

	if (y < clip->y) {
		uint8_t dy = clip->y - y;

		if (dy >= h)
			return;
		h -= dy;
		p += span * dy;
		y = clip->y;
	}
	if (y >= yend)
		return;
	if (y + h > yend)
		h = yend - y;

	if (rotate)
		blit_rotated(p, w, h, span, x, y);
	else
		blit_normal(p, w, h, span, x, y);
	change = 1;
}


void display_blit(const uint8_t *p, uint8_t w, uint8_t h, uint16_t span,
    uint8_t x, int8_t y)
{
	display_blit_clip(p, w, h, span, x, y, &clip_all);
}


void display_clear_rect(uint8_t xa, uint8_t ya, uint8_t xb, uint8_t yb)
{
	uint8_t x;

	while (ya != yb + 1) {
		for (x = xa; x != xb + 1; x++)
			display_clr(x, ya);
		ya++;
	}
	change = 1;
}


void display_rotate(bool on)
{
	rotate = on;
}


bool display_update(void)
{
	const uint8_t *p = fb;
	uint8_t page, col;

	if (!change)
		return 0;
	for (page = 0; page != PAGES; page++) {
		cmd1(0xb0 | page);
		cmd1(0x10);
		cmd1(0x00);
		dnc(1);
		spi_begin();
		for (col = 0; col != FB_X; col++)
			spi_send(*p++);
		spi_end();
	}
	change = 0;
	return 1;
}


void display_dim(bool dim)
{
	msleep(1);
	cmd2(0x81, dim ? 0x0 : 0xf0);
	msleep(1);
}


void display_on(bool clear)
{
	reset();

	cmd1(0xae);		/* turn off oled panel */
	cmd2(0xd5, 0x80);	/* display clock divide ratio */
	cmd2(0xa8, 0x3f);	/* multiplex ratio  = 1/64 */
	cmd2(0xd3, 0x00);	/* display offset = 0 */
	cmd2(0x8d, 0x14);	/* enable charge pump */

	msleep(10);

	cmd1(0x40);		/* address line */
	cmd1(0xa6);		/* normal display */
	cmd1(0xa4);		/* display RAM */
	cmd1(0xa1);		/* re-map 128 to 0 */
	cmd1(0xc8);		/* scan direction 64 to 0 */
	cmd2(0xda, 0x12);	/* COM pins hardware configuration */
	cmd2(0x81, 0xf0);	/* contrast */
	cmd2(0xd9, 0xf1);	/* pre-charge period */
	cmd2(0xdb, 0x40);	/* set vcomh */

	msleep(10);

	if (clear)
		display_clear();
	display_update();

	msleep(100);		/* "100ms Delay Recommended" */

	cmd1(0xaf);		/* turn on panel */
}


void display_off(void)
{
	cmd1(0xae);
	cmd2(0x8d, 0x10);
	msleep(100);		/* "100ms Delay Recommended" */
}


void display_init(void)
{
	gpio_init_out(DISP_nRES, 0);
	gpio_init_out(DISP_SCLK, 0);
	gpio_init_out(DISP_SDIN, 0);
	gpio_init_out(DISP_DnC, 0);
	gpio_init_out(DISP_nCS, 1);
}
