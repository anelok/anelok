/*
 * fw/al_radio.c - Radio alert
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "alert.h"
#include "icon.h" /* for main_x* */
#include "console.h"
#include "input.h"
#include "std.h"
#include "alerts.h"


static struct std std;


/* ----- Display update ---------------------------------------------------- */


static void radio_show(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("Radio alert");
	console_update();
}


/* ----- Return to previous screen ----------------------------------------- */


static bool radio_back(void *user)
{
	alert_end();
	input_pop();
	return 1;
}


/* ----- Callback ---------------------------------------------------------- */


static const struct std_ops radio_ops = {
	.back		= radio_back,
	.show		= radio_show,
};


static bool alert_radio_setup(void *user)
{
	input_push();
	std_init(&std, &radio_ops, NULL);
	return 0;
}


/* ----- Post alert -------------------------------------------------------- */


void alert_radio(void)
{
	alert_set(al_radio, alert_radio_setup, NULL);
}
