/*
 * fw/ui/sel.c - Selection
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "display.h"
#include "cursor.h"
#include "sel.h"


/* ----- Helper functions -------------------------------------------------- */


static uint8_t size(struct sel *sel)
{
	return sel->ops->size(sel->user);
}


/* ----- Draw screen content ----------------------------------------------- */


static void redraw(struct sel *sel, int16_t y)
{
	if (sel->ops->display_begin)
		sel->ops->display_begin(sel->user);
	cursor_rewind(sel->cursor);
	while (1) {
		uint8_t h;

		h = size(sel);
		if (y + h >= 0)
			sel->ops->display(sel->user, y,
			    cursor_pos(sel->cursor) == sel->selected);
		if (!cursor_next(sel->cursor))
			break;
		y += h;
		if (y >= FB_Y)
			break;
	}
	if (sel->ops->display_end)
		sel->ops->display_end(sel->user);
}


static void show_middle(struct sel *sel, uint8_t curr_h)
{
	int16_t y;

	y = (FB_Y >> 1) - (curr_h >> 1) - sel->curr_y + sel->yoff;
	redraw(sel, y);
}


static void sel_update(struct sel *sel, bool partial)
{
	uint16_t y, left;
	uint8_t h;

	/* find current entry */

	cursor_rewind(sel->cursor);
	y = sel->curr_y + (size(sel) >> 1);

	left = y;
	while (1) {
		h = size(sel);
		if (h > left)
			break;
		left -= h;
		REQUIRE(cursor_next(sel->cursor));
	}

	sel->selected = cursor_pos(sel->cursor);

	if (partial)
		return;

	cursor_rewind(sel->cursor);

	show_middle(sel, h);
}


cursor_pos_type sel_selected(struct sel *sel)
{
	sel_update(sel, 1);
	return sel->selected;
}


void sel_show(struct sel *sel)
{
	sel_update(sel, 0);
}


/* ----- Setup ------------------------------------------------------------- */


static void center(struct sel *sel)
{
	uint16_t y;
	uint8_t h;

	cursor_rewind(sel->cursor);
	sel->curr_y = -(size(sel) >> 1);
	y = 0;
	while (1) {
		h = size(sel);
		if (y + h >= sel->height >> 1)
			break;
		if (!cursor_next(sel->cursor))
			break;
		y += h;
		sel->curr_y += h;
	}

	sel->curr_y += h >> 1;
}


void sel_init(struct sel *sel, struct cursor *cursor,
    const struct sel_ops *ops, void *user)
{
	uint8_t h;

	sel->ops = ops;
	sel->user = user;

	sel->cursor = cursor;
	cursor_rewind(cursor);

	sel->selected = 0;

	h = size(sel);
	sel->height = h;
	sel->span = (h + 1) >> 1;

	while (cursor_next(sel->cursor)) {
		h = size(sel);
		sel->height += h;
		sel->span += h;
	}
	sel->span -= (h + 1) >> 1;

	center(sel);
}


void sel_position(struct sel *sel, cursor_pos_type top, cursor_pos_type curr)
{
	cursor_pos_type pos = 0;
	uint16_t y0, y;
	uint8_t h;

	REQUIRE(top <= curr);
	cursor_rewind(sel->cursor);
	h = size(sel);
	sel->curr_y = -(h >> 1);
	y = 0;
	while (pos != top && cursor_next(sel->cursor)) {
		h = size(sel);
		y += h;
		pos++;
	}
	y0 = y;
	while (pos != curr && cursor_next(sel->cursor)) {
		h = size(sel);
		y += h;
		pos++;
	}
	sel->curr_y += y + (h >> 1);
	sel->yoff = sel->curr_y + (h >> 1) - y0 - (FB_Y >> 1);
}


/* ----- Movements: steps -------------------------------------------------- */


static bool get_y(struct sel *sel, cursor_pos_type pos, uint16_t *res)
{
	int16_t y;
	uint8_t h;

	cursor_rewind(sel->cursor);
	h = size(sel);
	y = -(h >> 1);
	while (cursor_pos(sel->cursor) != pos) {
		h = size(sel);
		if (!cursor_next(sel->cursor))
			return 0;
		y += h;
	}
	*res = y + (h >> 1);
	return 1;
}


bool sel_up(struct sel *sel)
{
	if (!sel->selected)
		return 0;
	sel->yoff = 0;
	return get_y(sel, sel->selected - 1, &sel->curr_y);
}


bool sel_down(struct sel *sel)
{
	sel->yoff = 0;
	return get_y(sel, sel->selected + 1, &sel->curr_y);
}


/* ----- Movements: swiping ------------------------------------------------ */


static uint8_t limit_swipe(struct sel *sel, int8_t dir, uint8_t dist)
{
	uint16_t y;
	uint8_t h, limit;

	cursor_rewind(sel->cursor);
	h = size(sel) >> 1;
	y = h >> 1;
	while (cursor_pos(sel->cursor) != sel->selected + dir) {
		h = size(sel);
		if (!cursor_next(sel->cursor))
			return dist;
		y += h;
	}
	y -= h >> 1;
	limit = dir == 1 ? y - sel->curr_y : sel->curr_y - y;
	limit += h >> 2;	/* @@@ tune this */
	return dist > limit ? limit : dist;
}


static bool swipe_up(struct sel *sel, uint8_t delta)
{
	if (!sel->curr_y)
		return 0;

	delta = limit_swipe(sel, -1, delta);

	if (sel->curr_y < delta)
		sel->curr_y = 0;
	else
		sel->curr_y -= delta;
	return 1;
}


static bool swipe_down(struct sel *sel, uint8_t delta)
{
	if (sel->curr_y == sel->span)
		return 0;

	delta = limit_swipe(sel, 1, delta);

	if (sel->curr_y + delta < sel->span)
		sel->curr_y += delta;
	else
		sel->curr_y = sel->span;
	return 1;
}


bool sel_swipe(struct sel *sel, int8_t delta)
{
	if (!delta)
		return 0;
	sel->yoff = 0;		/* @@@ should "burn off" yoff gently */
	delta += delta;		/* accelerate: one position = 2 pixels */
	if (delta < 0)
		return swipe_up(sel, -delta);
	else
		return swipe_down(sel, delta);
}


/* ----- Searching --------------------------------------------------------- */


bool sel_find(struct sel *sel, bool (*fn)(void *user), void *user)
{
	cursor_rewind(sel->cursor);
	while (!fn(user))
		if (!cursor_next(sel->cursor))
			return 0;
	sel->selected = cursor_pos(sel->cursor);
	REQUIRE(get_y(sel, sel->selected, &sel->curr_y));
	return 1;
}
