/*
 * fw/ui/dir.h - Directory cache
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef DIR_H
#define	DIR_H

#include <stdbool.h>
#include <stdint.h>


typedef uint8_t dir_type;
typedef uint16_t account_type;


bool dir_show_entry(dir_type cwd, account_type n);
bool dir_is_sub(dir_type cwd, account_type n);
void dir_get_sub(dir_type cwd, account_type n, const char *name,
    char res[MAX_DIR_NAME + 1]);
dir_type dir_enter(dir_type cwd, account_type n);
dir_type dir_leave(dir_type cwd);

void dir_cache_begin(void);
void dir_cache_add(const char *name);
void dir_cache_end(void);

#endif /* !DIR_H */
