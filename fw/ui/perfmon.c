/*
 * fw/ui/perfmon.c - Performance monitoring and reporting
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "tick.h"
#include "fmt.h"
#include "display.h"
#include "text.h"
#include "console.h"
#include "perfmon.h"


#define	POS_X	64
#define	POS_Y	50


bool perfmon_enabled = 0;

static uint16_t t0, t1;


void perfmon_enable(bool on)
{
	perfmon_enabled = on;
}


void perfmon_do_start(void)
{
	t0 = tick_now();
}


static void count(void *user, char c)
{
	uint8_t *n = user;

	(*n)++;
}


void perfmon_do_show(void)
{
	uint8_t n = 0;

	t1 = tick_now();

	format(count, &n, "%d", t1 - t0);
	display_clear_rect(POS_X, POS_Y,
	    POS_X + (CONSOLE_FONT.w + 1) * n - 1,
	    POS_Y + CONSOLE_FONT.h - 1);
	console_goto(POS_X, POS_Y);
	console_printf("%u", t1 - t0);
	console_update();
}
