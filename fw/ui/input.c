/*
 * fw/input.c - Input events
 *
 * Written 2014-2017 by Werner Almesberger
 * Copyright 2014-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "display.h"
#include "input.h"


#define	STACK_LEVELS	2


static struct {
	const struct input_ops *ops;
	void *user;
} stack[STACK_LEVELS], *curr = stack;

static bool rotate = 0;
static bool suspended = 0;


/* ----- Position indicator ------------------------------------------------ */


static uint8_t last_pos = 0;


static void clear_pos(void)
{
	uint8_t x = rotate ? 0 : FB_X - 1;
	uint8_t y;

	if (!last_pos)
		return;
	for (y = last_pos - 1; y != last_pos + 2; y++)
		display_clr(x, y);
	last_pos = 0;
}


static void show_pos(uint8_t pos)
{
	uint8_t mid = pos ? pos < FB_Y - 1 ? pos : FB_Y - 2 : 1;
	uint8_t x = rotate ? 0 : FB_X - 1;
	uint8_t y;

	clear_pos();
	for (y = mid - 1; y != mid + 2; y++)
		display_set(x, y);
	last_pos = mid;
}


/* ----- Timer ------------------------------------------------------------- */


static uint32_t input_timer_ms;
static bool input_timer_on = 0;


void input_set_timer(uint16_t ms)
{
	input_timer_on = 1;
	input_timer_ms = ms;
}


void input_cancel_timer(void)
{
	input_timer_on = 0;
}


bool input_next_timer(uint16_t *ms)
{
	if (suspended || !input_timer_on)
		return 0;
	*ms = input_timer_ms;
	return 1;
}


static void input_time_update(uint16_t delta_ms)
{
	if (!suspended && input_timer_on && input_timer_ms <= delta_ms) {
		input_timer_on = 0;
		if (curr->ops && curr->ops->timer)
			curr->ops->timer(curr->user);	
	} else {
		input_timer_ms -= delta_ms;
	}
}


/* ----- Input event detection --------------------------------------------- */


/*
 * TO DO:
 * - consider rejecting last few samples before release
 */


#define	TAP_MIN_MS	  20
#define	TAP_MAX_MS	 200
#define	PRESS_MIN_MS	 300
#define	PRESS_MAX_MS	1500
#define	EXTENDED_MAX_MS	3000

#define	SWIPE_MIN_MS	50
#define	SWIPE_MIN_MOVE	10

#define	BLACKOUT_MS	200


static enum state {
	s_idle,
	s_cancel,
	s_tap_wait,
	s_tap_maybe,
	s_press_wait,
	s_extended,
	s_swipe,
	s_blackout,
} state = s_idle;

static uint8_t curr_area;
static uint8_t start_pos;
static uint32_t input_ms;	/* duration of touch timer */


enum area input_pressed(void)
{
	switch (state) {
	case s_idle:
	case s_cancel:
	case s_blackout:
	case s_swipe:
		return area_none;
	case s_tap_wait:
	case s_tap_maybe:
	case s_press_wait:
	case s_extended:
		return curr_area;
	default:
		oops();
	}
}


void input_select(const struct input_ops *ops, void *user)
{
	input_cancel_timer();
	curr->ops = ops;
	curr->user = user;
}


#define	AREA_GAP	10
#define	AREA_SIZE	((FB_Y - 2 * AREA_GAP) / 3)


static enum area pick_area(uint8_t pos)
{
	if (pos < AREA_SIZE)
		return area_top;
	if (pos >= FB_Y - AREA_SIZE)
		return area_bottom;
	if (pos >= AREA_SIZE + AREA_GAP && pos < 2 * (AREA_SIZE + AREA_GAP))
		return area_middle;
	return area_none;
}


static bool start_swipe(uint8_t pos)
{
	int8_t d = pos - start_pos;

	if (input_ms < SWIPE_MIN_MS)
		return 0;
	if (d > -SWIPE_MIN_MOVE && d < SWIPE_MIN_MOVE)
		return 0;
	if (curr->ops) {
		if (state == s_press_wait || state == s_extended)
			if (curr->ops->cancel)
				curr->ops->cancel(curr_area, curr->user);
		if (curr->ops->swipe_begin)
			curr->ops->swipe_begin(pick_area(pos), curr->user);
	}
	state = s_swipe;
	return 1;
}


void sim_swipe(int8_t d)
{
	if (curr->ops) {
		if (curr->ops->swipe_begin)
			curr->ops->swipe_begin(0, curr->user);
		if (curr->ops->swipe_move)
			curr->ops->swipe_move(d, curr->user);
	}
}


void input_rotate(bool on)
{
	rotate = on;
}


void input_press(uint8_t pos, uint32_t delta_ms)
{
	const struct input_ops *ops = curr->ops;
	enum area area;

	input_time_update(delta_ms);
	input_ms += delta_ms;
	if (rotate)
		pos = FB_Y - 1 - pos;
	area = pick_area(pos);

#ifdef SIMULATOR
	/*
	 * @@@ Hack: we assume "SIMULATOR == no slider". But one Mk 2 board and
	 * all Mk 3 boards have buttons, too. The main purpose of this test is
	 * to suppress drawing the position marker while the display is powered
	 * down. In the simulator, powering down has no effect, and we
	 * therefore got position markers when the screen on the real hardware
	 * would be blank.
	 */
	extern bool sim_no_slider;

	if (!sim_no_slider)
		show_pos(pos);
#else
	show_pos(pos);
#endif

	switch (state) {
	case s_idle:
		state = s_tap_wait;
#ifdef SIMULATOR
		/*
		 * This _may_ disable the slider. Without background image, the
		 * slider functionality is still available. Enabling the
		 * background image changes the way input events are processed,
		 * and the slider functionality is then no longer available.
		 */

		if (sim_no_slider) {
			curr_area = area;
			state = s_tap_maybe;
		}
#endif
		input_ms = 0;
		start_pos = pos;
		break;
	case s_cancel:
		/* swiping is not affected by tap/press cancellation */
		// coverity[check_return]
		start_swipe(pos);
		break;
	case s_tap_wait:
		if (start_swipe(pos))
			break;
		if (input_ms <= TAP_MIN_MS)
			break;
		if (area == area_none) {
			state = s_cancel;
			break;
		}
		curr_area = area;
		state = s_tap_maybe;
		break;
	case s_tap_maybe:
		if (start_swipe(pos))
			break;
		if (input_ms <= TAP_MAX_MS &&
		    !(ops && ops->expedite &&
		    ops->expedite(area, curr->user)))
			break;
		if (area != curr_area) {
			state = s_cancel;
			break;
		}
		if (ops && ops->begin)
			ops->begin(area, curr->user);
		state = s_press_wait;
		break;
	case s_press_wait:
		if (start_swipe(pos))
			break;
		if (input_ms <= PRESS_MAX_MS && area == curr_area)
			break;
		if (ops && ops->extend && ops->extend(curr_area, curr->user)) {
			state = s_extended;
			break;
		}
		state = s_cancel;
		if (ops && ops->timeout &&
		    ops->timeout(curr_area, curr->user))
			break;
		if (ops && ops->cancel)
			ops->cancel(curr_area, curr->user);
		break;
	case s_extended:
		if (start_swipe(pos))
			break;
		if (input_ms <= EXTENDED_MAX_MS && area == curr_area)
			break;
		state = s_cancel;
		if (ops && ops->timeout &&
		    ops->timeout(curr_area, curr->user))
			break;
		if (ops && ops->cancel)
			ops->cancel(curr_area, curr->user);
		break;
	case s_swipe:
		if (ops && ops->swipe_move)
			ops->swipe_move(pos - start_pos, curr->user);
		break;
	case s_blackout:
		input_ms = 0;
		break;
	default:
		oops();
	}
}


void input_release(uint32_t delta_ms)
{
	const struct input_ops *ops = curr->ops;

	input_time_update(delta_ms);
	input_ms += delta_ms;
	clear_pos();
	switch (state) {
	case s_idle:
		break;
	case s_cancel:
		break;
	case s_tap_wait:
		break;
	case s_tap_maybe:
		if (input_ms > TAP_MAX_MS)
			break;
		if (ops && ops->tap)
			ops->tap(curr_area, curr->user);
		break;
	case s_press_wait:
		if (input_ms > PRESS_MAX_MS) {
			if (ops && ops->cancel)
				ops->cancel(curr_area, curr->user);
			break;
		}
		if (ops && ops->press)
			ops->press(curr_area, curr->user);
		break;
	case s_extended:
		if (input_ms > EXTENDED_MAX_MS) {
			if (ops && ops->cancel)
				ops->cancel(curr_area, curr->user);
			break;
		}
		if (ops && ops->extended)
			ops->extended(curr_area, curr->user);
		break;
	case s_swipe:
		if (ops && ops->swipe_end)
			ops->swipe_end(curr_area, curr->user);
		break;
	case s_blackout:
		if (input_ms < BLACKOUT_MS)
			return;
		break;
	default:
		oops();
	}
	state = s_idle;
}


/* ----- Suspend/resume ---------------------------------------------------- */


void input_suspend(void)
{
	suspended = 1;
	if (curr->ops && curr->ops->suspend)
		curr->ops->suspend(curr->user);
}


void input_resume(void)
{
	if (state == s_tap_wait) {
		state = s_blackout;
		input_ms = 0;
	}
	if (curr->ops && curr->ops->resume)
		curr->ops->resume(curr->user);
	suspended = 0;
}


/* ----- Refresh the display content --------------------------------------- */


void input_refresh(void)
{
	if (curr->ops && curr->ops->refresh)
		curr->ops->refresh(curr->user);
}


/* ----- Stack for layered users (e.g., regular use vs. alerts) ------------ */


void input_push(void)
{
	REQUIRE(curr < stack + STACK_LEVELS - 1);
	curr++;
}


void input_pop(void)
{
	REQUIRE(curr != stack);
	curr--;
}


void input_rewind(void)
{
	curr = stack;
}
