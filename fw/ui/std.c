/*
 * fw/ui/std.c - Standard button layout
 *
 * Written 2015-2017 by Werner Almesberger
 * Copyright 2015-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "perfmon.h"
#include "auth.h"
#include "alert.h"
#include "input.h"
#include "icon.h"
#include "ui.h"
#include "std.h"


/* ----- Display update ---------------------------------------------------- */


static bool have_back(struct std *ctx)
{
	if (!ctx->ops->back)
		return 0;
	if (ctx->ops->have_back)
		return ctx->ops->have_back(ctx->user);
	return 1;
}


static bool have_setup(struct std *ctx)
{
	if (!ctx->ops->setup)
		return 0;
	if (ctx->ops->have_setup)
		return ctx->ops->have_setup(ctx->user);
	return 1;
}


static void show_icons(struct std *ctx)
{
	const struct icon *top, *middle, *bottom;

	if (ctx->bold == area_top) {
		if (alert_pending()) {
			top = &icon_alert_bold;
		} else if (have_back(ctx) &&
		    (!ctx->extended || !have_setup(ctx))) {
			top = &icon_back_bold;
		} else if (have_setup(ctx)) {
			top = &icon_setup_bold;
		} else {
			top = NULL;
		}
	} else {
		if (alert_pending()) {
			top = &icon_alert;
		} else if (have_back(ctx)) {
			top = &icon_back;
		} else if (have_setup(ctx)) {
			top = &icon_setup;
		} else {
			top = NULL;
		}
	}

	if (ctx->ops->middle_icon) {
		middle = ctx->ops->middle_icon(ctx->bold == area_middle,
		    ctx->user);
	} else {
		middle = NULL;
	}

	if (ctx->extended && auth_valid() && ctx->bold == area_bottom) {
		bottom = &icon_zap_bold;
	} else {
		bottom = ctx->bold == area_bottom ? &icon_off_bold : &icon_off;
	}

	icon_bar(top, middle, bottom);
}


static void show(struct std *ctx)
{
	if (ctx->ops->show)
		ctx->ops->show(ctx->user);
	show_icons(ctx);
}


/* ----- Input callbacks -------------------------------------------------- */


static void std_tap(enum area area, void *user)
{
	struct std *ctx = user;

	perfmon_start();
	if (ctx->ops->tap && ctx->ops->tap(area, ctx->user)) {
		perfmon_show();
		return;
	}
	show(ctx);
	perfmon_show();
}


static void std_begin(enum area area, void *user)
{
	struct std *ctx = user;

	ctx->bold = area;
	show(ctx);
}


static void std_cancel(enum area area, void *user)
{
	struct std *ctx = user;

	ctx->bold = area_none;
	ctx->extended = 0;
	show(ctx);
}


static void std_press(enum area area, void *user)
{
	struct std *ctx = user;

	ctx->bold = area_none;
	ctx->extended = 0;

	switch (area) {
	case area_top:
		// @@@ add guard time
		if (alert_run_next())
			return;
		if (have_back(ctx)) {
			if (ctx->ops->back(ctx->user))
				return;
			break;
		}
		if (have_setup(ctx) && ctx->ops->setup(ctx->user))
			return;
		break;
	case area_middle:
		if (ctx->ops->middle_press && ctx->ops->middle_press(ctx->user))
			return;
		break;
	case area_bottom:
		ui_off();
		return;
	default:
		oops();
	}
	show(ctx);
}


static bool std_extend(enum area area, void *user)
{
	struct std *ctx = user;

	switch (area) {
	case area_top:
		if (!have_setup(ctx))
			return 0;
		break;
	case area_middle:
		return 0;
	case area_bottom:
		if (!auth_valid())
			return 0;
		break;
	default:
		oops();
	}

	ctx->extended = 1;
	show(ctx);
	return 1;
}


static void std_extended(enum area area, void *user)
{
	struct std *ctx = user;

	REQUIRE(ctx->extended);
	ctx->bold = area_none;
	ctx->extended = 0;

	switch (area) {
	case area_top:
		if (have_setup(ctx)) {
			if (ctx->ops->setup(ctx->user))
				return;
			break;
		}
		if (have_back(ctx) && ctx->ops->back(ctx->user))
			return;
		break;
	case area_middle:
		break;
	case area_bottom:
		if (auth_valid())
			auth_zap();
		ui_off();
		return;
	default:
		oops();
	}
	show(ctx);
}


static bool std_timeout(enum area area, void *user)
{
	switch (area) {
	case area_top:
	case area_middle:
		return 0;
	case area_bottom:
		std_extended(area, user);
		return 1;
	default:
		oops();
	}
}


static bool std_expedite(enum area area, void *user)
{
	struct std *ctx = user;

	if (ctx->ops->expedite)
		return ctx->ops->expedite(area, ctx->user);
	return 0;
}


static void std_swipe_begin(enum area area, void *user)
{
	struct std *ctx = user;

	if (ctx->ops->swipe_begin)
		ctx->ops->swipe_begin(area, ctx->user);
	show(ctx);
}


static void std_swipe_move(int8_t delta, void *user)
{
	struct std *ctx = user;

	if (ctx->ops->swipe_move && ctx->ops->swipe_move(delta, ctx->user))
		show(ctx);
}



static void std_swipe_end(enum area area, void *user)
{
	struct std *ctx = user;

	if (ctx->ops->swipe_end)
		ctx->ops->swipe_end(area, ctx->user);
	show(ctx);
}


static void std_timer(void *user)
{
	struct std *ctx = user;

	if (ctx->ops->timer)
		ctx->ops->timer(ctx->user);
}


static void std_suspend(void *user)
{
	struct std *ctx = user;

	if (ctx->ops->suspend)
		ctx->ops->suspend(ctx->user);
}


static void std_resume(void *user)
{
	struct std *ctx = user;

	if (ctx->ops->resume)
		ctx->ops->resume(ctx->user);
	show(ctx);
}


static void std_refresh(void *user)
{
	struct std *ctx = user;

	show(ctx);
}


static const struct input_ops std_ops = {
	.tap		= std_tap,
	.begin		= std_begin,
	.cancel		= std_cancel,
	.press		= std_press,
	.extend		= std_extend,
	.extended	= std_extended,
	.timeout	= std_timeout,
	.expedite	= std_expedite,
	.swipe_begin	= std_swipe_begin,
	.swipe_move	= std_swipe_move,
	.swipe_end	= std_swipe_end,
	.timer		= std_timer,
	.suspend	= std_suspend,
	.resume		= std_resume,
	.refresh	= std_refresh,
};


/* ----- Initialization ---------------------------------------------------- */


/*
 * Area		Tap		Press			Extended
 * ------------	---------------	-----------------------	-----------------------
 * Top		user-defined	back / setup /none	setup / back / none
 * Middle	user-defined	user-defined		-
 * Bottom	user-defined	off			zap / off
 *
 * xxx / yyy = use xxx if available, else use yyy
 */


void std_init(struct std *ctx, const struct std_ops *ops, void *user)
{
	ctx->ops = ops;
	ctx->user = user;

	ctx->bold = area_none;
	ctx->extended = 0;

	input_select(&std_ops, ctx);
	show(ctx);
}


/* ----- Simulator support ------------------------------------------------- */


void sim_std_show(struct std *ctx)
{
	show(ctx);
}
