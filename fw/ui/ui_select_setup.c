/*
 * fw/ui_select_setup.c - UI: administrative tasks at account selection
 *
 * Written 2015-2017 by Werner Almesberger
 * Copyright 2015-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "textsel.h"
#include "ui.h"


static struct textsel textsel;


/* ----- Task: not implemented --------------------------------------------- */


static bool sorry(void)
{
	ui_sorry();
	return 1;
}


/* ----- Task: show public key --------------------------------------------- */


#include "account.h"
#include "base32.h"
#include "console.h"


static void proceed(void)
{
	textsel_static_proceed(&textsel);
}


struct pubkey_ctx {
	const uint8_t *p;
	uint8_t n;
};


static bool pubkey_in(void *user, uint8_t *byte)
{
	struct pubkey_ctx *ctx = user;

	if (ctx->p == my_pk + crypto_box_PUBLICKEYBYTES)
		return 0;
	*byte = *ctx->p++;
	return 1;
}


static void pubkey_out(void *user, char ch)
{
	struct pubkey_ctx *ctx = user;
	bool nl = !(ctx->n % 12);

	if (ctx->n && !(ctx->n & 3))
		console_char(nl ? '\n' : '-');

	/*
	 * Increase space between lines by one pixel, for better visual
	 * separation of all the numbers and upper-case letters.
	 */
	if (nl)
		console_goto_relative(0, 1);

	console_char(ch);
	ctx->n++;
}


static void do_show_pubkey(void *user)
{
	struct pubkey_ctx ctx = {
		.p = my_pk,
		.n = 0
	};

	console_window(main_x0, main_x1);
	console_clear();
	console_printf("Public key:\n\n");
	base32_encode(pubkey_in, pubkey_out, &ctx);
	console_update();
}


static bool show_pubkey(void)
{
	ui_show(do_show_pubkey, NULL, proceed);
	return 1;
}


/* ----- Tasks ------------------------------------------------------------- */


static const struct textsel_entry tasks[] = {
	{ "Add account",	sorry		},
	{ "Add directory",	sorry		},
//	{ "Delete directory",	sorry		},	if in subdirectory
	{ "Public key",		show_pubkey	},
	{ "Change code",	sorry		},
	{ NULL,			NULL		},
};


/* ----- Callbacks --------------------------------------------------------- */


static bool select_setup_back(void *user)
{
	ui_select();
	return 1;
}


static bool select_setup_select(void *user, void *user_entry)
{
	bool (*fn)(void) = user_entry;

	return fn();
}


static const struct textsel_ops select_setup_ts_ops = {
	.back		= select_setup_back,
	.select		= select_setup_select,
};


bool ui_select_setup(void *user)
{
	pm_set_policy(&pm_policy_default);
	textsel_static_init(&textsel, &select_setup_ts_ops, NULL, tasks);
	return 1;
}
