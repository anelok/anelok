/*
 * fw/ui/record.c - Display possibly long records
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "input.h"
#include "display.h"
#include "text.h"
#include "record.h"


#define	SCROLL_MS	200

/*
 * At 200 ms, 16 bits are good for about 3.6 hours of scrolling. If there is
 * an overrun, the worst that will happen is that scrolling will skip.
 */

static uint16_t scroll;
static bool by_timer = 0;


void record_display_begin(void *user)
{
	scroll = by_timer ? scroll + 1 : 0;
	input_cancel_timer();
}


void record_display_end(void *user)
{
	by_timer = 0;
}


/*
 * @@@ This is a little ugly since it duplicates the design choice of the
 * caller.
 */

#define	TEXT_X	(font->w + 1)	// @@@ dupliat


void record_display(const char *text, const struct font *font,
    uint8_t x, int8_t y, const struct rect *clip)
{
	uint8_t end, len;
	uint16_t offset;
	uint8_t bar_x;
	uint8_t from, to, i;

	end = text_str_fake(font, text, x);
	if (end < clip->x + clip->w) {
		text_str_clip(font, text, x, y, clip);
		return;
	}

	len = strlen(text);
	offset = scroll % (len + 1);	// + 1 for vertical bar

	if (offset != len)
		x = text_str_clip(font, text + offset, x, y, clip);

	input_set_timer(SCROLL_MS);

	bar_x = x + TEXT_X / 2;
	if (bar_x >= clip->x + clip->w)
		return;

	/*
	 * @@@ display_* is rather strict about valid coordinates, so we have
	 * to do careful range checking. Maybe more permissive variants of
	 * display_* would be usedful for cases like this one ?
	 */
	from = y < 1 ? 0: y - 1;
	to = y + font->h + 1;
	if (to >= FB_Y)
		to = FB_Y;
	if (from >= to)
		return;

	for (i = from; i != to; i++)
		display_set(bar_x, i);

	if (x + TEXT_X < clip->x + clip->w)
		text_str_clip(font, text, x + TEXT_X, y, clip);
}


void record_display_timer(void)
{
	by_timer = 1;
}

