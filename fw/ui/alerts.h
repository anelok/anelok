/*
 * fw/ui/alerts.h - Alert components
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ALERTS_H
#define	ALERTS_H

void alert_radio(void);

#endif /* !ALERTS_H */
