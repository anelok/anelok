/*
 * fw/ui/perfmon.h - Performance monitoring and reporting
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PERFMON_H
#define	PERFMON_H

#include <stdbool.h>


extern bool perfmon_enabled;


void perfmon_enable(bool on);

void perfmon_do_start(void);
void perfmon_do_show(void);


/*
 * Note: perfmon_start may be called without matching perfmon_show. In this
 * case, the start time of the measured interval is set to the current time.
 */

static inline void perfmon_start(void)
{
	if (perfmon_enabled)
		perfmon_do_start();
}


static inline void perfmon_show(void)
{
	if (perfmon_enabled)
		perfmon_do_show();
}

#endif /* !PERFMON_H */
