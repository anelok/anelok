/*
 * fw/ui_off.c - UI: pseudo-UI to simulate device off
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "display.h"
#include "icon.h"
#include "text.h"
#include "alert.h"
#include "input.h"
#include "auth.h"
#include "fmt.h"
#include "chill.h"
#include "ui.h"


bool ui_is_on;


/* ----- Input callbacks --------------------------------------------------- */


#define	FONT	font_10x20


static void draw_chill(void)
{
	uint16_t ms;
	unsigned t = chill_left_s(&ms);
	char buf[10];
	char *p = buf;
	uint8_t w;

	display_clear();

	if (t >= 24 * 3600)
		format(add_char, &p, "%ud %02uh", t / 24 / 3600,
		    (t / 3600) % 24);
	else
		format(add_char, &p, "%02u:%02u:%02u", t / 3600,
		    (t / 60) % 60, t % 60);

	w = text_str_fake(&FONT, buf, 0);
	text_str(&FONT, buf,
	    (FB_X - w) >> 1,
	    (FB_Y - FONT.h) >> 1);

	input_set_timer(ms);
}


static void off_begin(enum area area, void *user)
{
	if (area == area_middle) {
		pm_busy();
		ui_is_on = 1;
		if (chill_left_s(NULL))
			draw_chill();
		else
			icon_draw(&icon_banner,
			    (FB_X - icon_banner.w) >> 1,
			    (FB_Y - icon_banner.h) >> 1);
	}
}


static void off_cancel(enum area area, void *user)
{
	if (area == area_middle)
		ui_off();
}


static void off_press(enum area area, void *user)
{
	if (area == area_middle) {
		if (chill_left_s(NULL)) {
			ui_off();
			return;
		}
		if (auth_valid())
			ui_select();
		else
			ui_login();
	}
}


static bool off_extend(enum area area, void *user)
{
	if (area == area_bottom)
		auth_zap();
	return 0;
}


static void off_timer(void *user)
{
	if (ui_is_on)	// @@@ slightly paranoid
		draw_chill();
}


static const struct input_ops off_ops = {
	.begin		= off_begin,
	.cancel		= off_cancel,
	.press		= off_press,
	.extend		= off_extend,
	.timer		= off_timer,
};


void ui_off(void)
{
	/* clean up alerts */
	input_rewind();
	alert_end();

	input_select(&off_ops, NULL);
	ui_is_on = 0;
	pm_sleep(PM_OFF);
}
