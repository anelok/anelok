/*
 * fw/ui/cursor.c - Cursors in lists
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "qa.h"
#include "cursor.h"


void cursor_rewind(struct cursor *cursor)
{
	if (cursor->ops->rewind && cursor->ops->rewind(cursor->user)) {
		cursor->pos = 0;
		return;
	}
	REQUIRE(cursor->ops->prev);
	while (cursor->pos) {
		REQUIRE(cursor->ops->prev(cursor->user));
		cursor->pos--;
	}
}


bool cursor_prev(struct cursor *cursor)
{
	cursor_pos_type pos;

	if (!cursor->pos)
		return 0;
	if (cursor->ops->prev && cursor->ops->prev(cursor->user)) {
		cursor->pos--;
		return 1;
	}
	pos = cursor->pos - 1;
	REQUIRE(cursor->ops->rewind);
	REQUIRE(cursor->ops->rewind(cursor->user));
	for (cursor->pos = 0; cursor->pos != pos; cursor->pos++)
		REQUIRE(cursor->ops->next(cursor->user));
	return 1;
}


bool cursor_next(struct cursor *cursor)
{
	if (!cursor->ops->next(cursor->user))
		return 0;
	cursor->pos++;
	return 1;
}


bool cursor_goto(struct cursor *cursor, cursor_pos_type pos)
{
	if (cursor->pos == pos)
		return 1;
	if (cursor->pos > pos) {
		if (pos >= cursor->pos >> 1 && cursor->ops->prev) {
			while (cursor->pos != pos) {
				if (!cursor->ops->prev(cursor->user))
					goto from_top;
				cursor->pos--;
			}
			return 1;
		}
from_top:
		cursor_rewind(cursor);
	}
	while (cursor->pos != pos) {
		if (!cursor->ops->next(cursor->user))
			return 0;
		cursor->pos++;
	}
	return 1;
}


void cursor_sync(struct cursor *cursor)
{
	cursor_pos_type pos = cursor->pos;

	cursor_rewind(cursor);
	REQUIRE(cursor_goto(cursor, pos));
}


void cursor_init(struct cursor *cursor,
    const struct cursor_ops *ops, void *user)
{
	REQUIRE(ops->next);
	cursor->ops = ops;
	cursor->user = user;
	cursor->pos = 0;
}
