/*
 * fw/ui/textsel.h - Selection from text list
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TEXTSEL_H
#define	TEXTSEL_H

#include <stdbool.h>

#include "std.h"
#include "cursor.h"
#include "icon.h"
#include "sel.h"


struct textsel_entry {
	const char *text;	/* NULL if last entry */
	void *user_entry;
};


struct textsel_access_ops {
	const char *(*text)(void *user);
	void *(*entry)(void *user);
};

struct textsel_ops {
	bool (*have_back)(void *user);
	bool (*have_setup)(void *user);

	bool (*back)(void *user);
	bool (*setup)(void *user);
	bool (*select)(void *user, void *user_entry);

	const struct icon *(*middle_icon)(bool bold, void *user);
		/* icon of middle button */

	void (*suspend)(void *user, void *user_entry);
	void (*resume)(void *user, void *user_entry);
};


struct textsel {
	const struct textsel_ops *ops;
	void *user;
	const struct textsel_access_ops *access_ops;
	void *access_user;

	struct cursor *cursor;

	struct std std;
	struct sel sel;

	uint8_t last;				/* delta at last swipe */

	cursor_pos_type selected_entry;

	/* textsel_static only */
	struct cursor static_cursor;
	const struct textsel_entry *list;	/* top of the list */
	const struct textsel_entry *curr;	/* for list travel */
};


bool textsel_find(struct textsel *ctx, bool (*fn)(void *user), void *user);

void textsel_dyn_proceed(struct textsel *ctx);
void textsel_dyn_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, struct cursor *cursor,
    const struct textsel_access_ops *access_ops, void *access_user);

void textsel_static_proceed(struct textsel *ctx);
void textsel_static_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, const struct textsel_entry *list);

void sim_ts_show(struct textsel *ctx);

#endif /* !TEXTSEL_H */
