/*
 * fw/ui_login_setup.c - UI: administrative tasks on the login screen
 *
 * Written 2015-2017 by Werner Almesberger
 * Copyright 2015-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "icon.h"
#include "textsel.h"
#include "ui.h"


static struct textsel textsel;


void login_setup_proceed(void)
{
	textsel_static_proceed(&textsel);
}


/* ----- Callbacks --------------------------------------------------------- */


static bool login_setup_back(void *user)
{
	ui_login();
	return 1;
}


static bool login_setup_select(void *user, void *user_entry)
{
	bool (*fn)(void) = user_entry;

	return fn();
}


static void login_setup_suspend(void *user, void *user_entry)
{
	ui_off();
}


static const struct textsel_ops login_setup_ts_ops = {
	.back		= login_setup_back,
	.select		= login_setup_select,
	.suspend	= login_setup_suspend,
};


void ui_login_setup(void)
{
	pm_set_policy(&pm_policy_default);
	textsel_static_init(&textsel, &login_setup_ts_ops, NULL,
	    login_setup_tasks);
}
