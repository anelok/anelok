/*
 * fw/ui_account.c - UI: account access
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "pm.h"
#include "display.h"
#include "text.h"
#include "icon.h"
#include "cursor.h"
#include "input.h"
#include "sel.h"
#include "std.h"
#include "account.h"
#include "comm.h"
#include "record.h"
#include "ui.h"


#define	FONT		font_medium
#define	LINE_Y		(FONT.h + 1)
#define	TEXT_X		(FONT.w + 1)
#define	MARKER_Y	((FONT.h - icon_current.h - 1) >> 1)

static bool reveal;

static struct std std;
static struct sel sel;
static struct account_record record;
static uint8_t last;	/* delta at last swipe */


/* ----- Callbacks --------------------------------------------------------- */


static inline enum field_type current_field_type(void)
{
	cursor_goto(&record.cursor, sel_selected(&sel));
	return account_field_type(&record);
}


/* ----- Selection operations ---------------------------------------------- */


static void hline(uint8_t x0, uint8_t x1, int8_t y)
{
	if (y < 0 || y >= FB_Y)
		return;
	while (x0 <= x1) {
		display_set(x0, y);
		x0++;
	}
}


static void display_record(uint8_t x, int8_t y)
{
	struct rect clip = {
		.x	= x,
		.y	= 0,
		.w	= main_x1 - x + 1,
		.h	= FB_Y,
	};

	record_display(account_text(&record), &FONT, x, y, &clip);
}


static void account_display(void *user, int8_t y, bool selected)
{
	uint8_t x = main_x0 + TEXT_X;
	enum field_type field = account_field_type(&record);
	enum pw_flags flags = account_flags(&record);

	if (selected)
		icon_draw(&icon_current, main_x0, y + MARKER_Y);
	switch (field) {
	case field_name:
		hline(main_x0, main_x1, y + LINE_Y);
		/* fall through */
	case field_url:
		display_record(x, y);
		return;
	case field_login:
		text_str(&FONT, "User", x, y);
		break;
	case field_pw:
		text_str(&FONT, "Password", x, y);
		if (flags & pw_inaccessible)
			return;
		if ((flags & pw_hidden) && !reveal)
			return;
		break;
	default:
		oops();
	}
	display_record(x, y + LINE_Y);
}


static uint8_t account_size(void *user)
{
	switch (account_field_type(&record)) {
	case field_name:
		return LINE_Y + 2;
	case field_url:
		return LINE_Y;
	default:
		return LINE_Y * 2;
	}
}


static const struct sel_ops account_sel_ops = {
	.size		= account_size,
	.display_begin	= record_display_begin,
	.display_end	= record_display_end,
	.display	= account_display,
};


/* ----- Input actions ----------------------------------------------------- */


static void account_show(void *user)
{
	display_clear();
	sel_show(&sel);
}


static bool account_tap(enum area area, void *user)
{
	switch (area) {
	case area_top:
		sel_up(&sel);
		break;
	case area_middle:
		break;
	case area_bottom:
		sel_down(&sel);
		break;
	default:
		break;
	}
	return 0;
}


static bool account_middle_press(void *user)
{
	enum field_type field = current_field_type();
	enum pw_flags flags = account_flags(&record);

	if (field != field_pw)
		return 0;
	if ((flags & pw_hidden) && !reveal) {
		reveal = 1;
		return 0;
	}

	switch (comm_mode(&record)) {
	case ui_comm_hid:
		return comm_send(&record);
	default:
		break;
	}

	if (flags & pw_type) {
		ui_sorry();
		return 1;
	}
	if (flags & pw_radio) {
		ui_sorry();
		return 1;
	}
	if (flags & pw_inaccessible) {
		ui_sorry();
		return 1;
	}
	return 0;
}


static const struct icon *account_middle_icon(bool bold, void *user)
{
	enum field_type field = current_field_type();
	enum pw_flags flags = account_flags(&record);

	if (field != field_pw)
		return 0;
	if ((flags & pw_hidden) && !reveal)
		return bold ? &icon_eye_bold : &icon_eye;

	switch (comm_mode(&record)) {
	case ui_comm_hid:
		return bold ? &icon_keyboard_bold : &icon_keyboard;
	default:
		break;
	}

#if 0
	if (flags & pw_type)
		return bold ? &icon_keyboard_bold : &icon_keyboard;
#endif
	if (flags & pw_radio)
		return bold ? &icon_radio_bold : &icon_radio;
	if (flags & pw_inaccessible)
		return bold ? &icon_exchange_bold : &icon_exchange;
	return NULL;
}


static void account_swipe_begin(enum area area, void *user)
{
	last = 0;
}


static bool account_swipe_move(int8_t delta, void *user)
{
	bool change;

	change = sel_swipe(&sel, delta - last);
	last = delta;
	return change;
}


static void account_timer(void *user)
{
	record_display_timer();
	input_refresh();
}


static const struct std_ops account_std_ops = {
	.show		= account_show,
	.back		= ui_select_proceed,
	.setup		= ui_sorry_dummy,	// ui_account_setup
	.tap		= account_tap,
	.middle_press	= account_middle_press,
	.middle_icon	= account_middle_icon,
	.swipe_begin	= account_swipe_begin,
	.swipe_move	= account_swipe_move,
	.timer		= account_timer,
};


/* ----- Initialization ---------------------------------------------------- */


static uint8_t find_data(struct account_record *rec)
{
	uint8_t n = 0;

	cursor_rewind(&rec->cursor);
	while (1) {
		switch (account_field_type(rec)) {
		case field_login:
		case field_pw:
			return n;
		default:
			break;
		}
		if (!cursor_next(&rec->cursor))
			break;
		n++;
	}
	return 0;
}


void ui_account(struct account_list *list)
{
	reveal = 0;

	pm_set_policy(&pm_policy_peruse);
	account_record_open(list, &record);
	sel_init(&sel, &record.cursor, &account_sel_ops, NULL);
	sel_position(&sel, 0, find_data(&record));
	std_init(&std, &account_std_ops, NULL);
}
