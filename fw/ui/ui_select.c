/*
 * fw/ui_select.c - UI: account selection
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "pm.h"
#include "display.h"
#include "console.h"
#include "icon.h"
#include "cursor.h"
#include "textsel.h"
#include "auth.h"
#include "account.h"
#include "dbconf.h"
#include "dir.h"
#include "ui.h"


static struct textsel textsel;
static struct account_list list;
static struct cursor filter_cursor;
static dir_type cwd;
static account_type account;


static void ui_select_changed(void);


/* ----- Access operations ------------------------------------------------- */


static const char *select_text(void *user)
{
	static char sub[MAX_DIR_NAME + 1];
	const char *name, *sep;

	name = account_name(&list);
	if (dir_is_sub(cwd, account)) {
		dir_get_sub(cwd, account, name, sub);
		return sub;
	} else {
		sep = strrchr(name, SEP_CHAR);
		return sep ? sep + 1 : name;
	}
}


static const struct textsel_access_ops select_access_ops = {
	.text	= select_text,
};


/* ----- Textsel operations ------------------------------------------------ */


static bool select_have_back(void *user)
{
	return cwd;
}


static bool find_pos(void *user)
{
	cursor_pos_type *pos = user;

	return cursor_pos(&list.cursor) == *pos;
}


static bool select_back(void *user)
{
	cursor_pos_type pos;

	/*
	 * To select the entry of this subdirectory in the parent directory we
	 * use a little trick: since the filter selecting the content of this
	 * directory is still in place, we can find the first entry simply by
	 * rewinding.
	 *
	 * Then we set up the parent, including changing the filter (which also
	 * changes how std counts entires, so we couldn't just copy "pos" into
	 * textsel.std.selected) and find the entry again.
	 */

	cursor_rewind(&filter_cursor);
	pos = cursor_pos(&list.cursor);
	cwd = dir_leave(cwd);
	/*
	 * @@@ ui_select_changed does too much. We could simply call
	 * textsel_dyn_proceed after textsel_find (without even calling
	 * sim_std_show), and it would still work. FFS.
	 */
	ui_select_changed();
	textsel_find(&textsel, find_pos, &pos);
	return 1;
}


static bool select_select(void *user, void *user_entry)
{
	cursor_sync(&filter_cursor);
	if (dir_is_sub(cwd, account)) {
		cwd = dir_enter(cwd, account);
		ui_select_changed();
	} else {
		ui_account(&list);
	}
	return 1;
}


static const struct icon *select_middle_icon(bool bold, void *user)
{
	cursor_sync(&filter_cursor);
	return dir_is_sub(cwd, account) ?
	    bold ? &icon_folder_bold : &icon_folder :
	    bold ? &icon_next_bold : &icon_next;
}


static const struct textsel_ops select_ts_ops = {
	.have_back	= select_have_back,
	.setup		= ui_select_setup,
	.back		= select_back,
	.select		= select_select,
	.middle_icon	= select_middle_icon,
};


/* ----- Path filter ------------------------------------------------------- */


static bool settle(void)
{
	while (!dir_show_entry(cwd, account)) {
		if (!cursor_next(&list.cursor))
			return 0;
		account++;
	}
	return 1;
}


static bool filter_rewind(void *user)
{
	cursor_rewind(&list.cursor);
	account = 0;
	return settle();
}


static bool filter_next(void *user)
{
	if (!cursor_next(&list.cursor))
		return 0;
	account++;
	return settle();
}


static const struct cursor_ops filter_ops = {
	.rewind	= filter_rewind,
	.next	= filter_next,
};


/* ----- List is empty ----------------------------------------------------- */


static void show_empty(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("No accounts");
	console_update();
}


/* ----- Setup ------------------------------------------------------------- */


bool ui_select_proceed(void *dummy)
{
	pm_set_policy(&pm_policy_default);
	textsel_static_proceed(&textsel);
	return 1;
}


static void busy(void)
{
	display_clear();
	icon_center(&icon_busy);
	display_update();
}


static void ui_select_changed(void)
{
	cursor_init(&filter_cursor, &filter_ops, NULL);
	textsel_dyn_init(&textsel, &select_ts_ops, NULL,
	    &filter_cursor, &select_access_ops, NULL);
}


void ui_select(void)
{
	bool ok;

	pm_set_policy(&pm_policy_default);

	display_dim(1);
	busy();
	ok = account_list_open(&list);
	display_dim(0);

	if (!ok) {
		auth_zap();
		ui_show(show_empty, NULL, NULL);
		return;
	}

	cwd = 0;
	dir_cache_begin();
	cursor_rewind(&list.cursor);
	do dir_cache_add(account_name(&list));
	while (cursor_next(&list.cursor));
	dir_cache_end();

	ui_select_changed();
}
