/*
 * fw/ui/record.h - Display possibly long records
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef RECORD_H
#define	RECORD_H

#include <stdint.h>

#include "display.h"
#include "text.h"


/*
 * We don't use the "user" argument, but it gives us compatibility with
 * sel_ops. We could use a record context structure instead of having global
 * variables in record.c, but given we currently can have only one set of
 * scrolling records anyway (and would lack timer coordination if there was
 * more than one), that would be wasteful.
 */

void record_display_begin(void *user);
void record_display_end(void *user);
void record_display(const char *text, const struct font *font,
    uint8_t x, int8_t y, const struct rect *clip);
void record_display_timer(void);

#endif /* !RECORD_H */
