/*
 * fw/ui/alert.h - User alerts
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ALERT_H
#define	ALERT_H

#include <stdbool.h>


enum alert {
	al_radio,
	al_end,		/* must be last */
};


/*
 * Callback: return 1 to process more alerts, 0 to stop processing alerts.
 * If returning 0, the end of the alerts must be signaled by calling alert_end.
 */

void alert_set(enum alert alert, bool (*fn)(void *user), void *user);
void alert_clear(enum alert alert);

bool alert_pending(void);
/* Return 1 if any alerts were processed */
bool alert_run_next(void);
void alert_end(void);

#endif /* !ALERT_H */
