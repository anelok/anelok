/*
 * fw/ui/chill.c - Forced wait after failed login
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>

#include "misc.h"
#include "chill.h"


/*
 * @@@ This implementation is only for demonstrational purposes. A long wait
 * could simply be defeated by resetting or power-cycling the device.
 *
 * The proper implementation should store the current delay in persistent
 * storage, and use that delay after each reset. Furthermore, it should
 * write the delay value before using the PIN entered, so that resetting the
 * device during the login process will count as a failed login, too.
 */

static uint32_t chill_tab_s[] = {
	0, 0,			// the first three tries have no penalty
	60, 120, 300, 600,	// 1, 2, 5, 10 minutes
	3600, 3600,		// two tries at one hour each
	24 * 3600		// wait a day
};

static const uint32_t *next_chill = chill_tab_s;
static uint32_t chill_s = 0;
static uint16_t chill_ms = 0;


void chill_pass(void)
{
	chill_s = chill_ms = 0;
	next_chill = chill_tab_s;
}


void chill_fail(void)
{
	chill_s = *next_chill;
	chill_ms = 0;
	if (next_chill < ARRAY_END(chill_tab_s) - 1)
		next_chill++;
}


void chill_update(uint16_t ms)
{
	uint32_t s;

	chill_ms += ms;
	s = chill_ms / 1000;
	chill_ms -= 1000 * s;
	chill_s = s < chill_s ? chill_s - s : 0;
}


uint32_t chill_left_s(uint16_t *change_ms)
{
	if (change_ms)
		*change_ms = 1000 - chill_ms;
	return chill_s;
}
