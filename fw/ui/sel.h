/*
 * fw/ui/sel.h - Selection (generic)
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SEL_H
#define	SEL_H

#include <stdbool.h>
#include <stdint.h>

#include "cursor.h"


struct sel_ops {
	uint8_t (*size)(void *user);	/* entry height, in pixels */
	void (*top)(void *user);
	void (*display_begin)(void *user);
	void (*display_end)(void *user);
	void (*display)(void *user, int8_t y, bool selected);
};

struct sel {
	const struct sel_ops *ops;	/* list operations */
	void *user;		/* user context */

	struct cursor *cursor;

	uint16_t span;		/* distance we can travel */
	uint16_t height;	/* total height */

	cursor_pos_type selected; /* currently selected item */
	uint16_t curr_y;	/* virtual y position; 0 <= curr_y <= span */
	int16_t yoff;		/* cursor offset from screen center */
};


void sel_init(struct sel *sel, struct cursor *cursor,
    const struct sel_ops *ops, void *user);
void sel_position(struct sel *sel, cursor_pos_type top, cursor_pos_type curr);
void sel_show(struct sel *sel);
cursor_pos_type sel_selected(struct sel *sel);
bool sel_up(struct sel *sel);
bool sel_down(struct sel *sel);
bool sel_swipe(struct sel *sel, int8_t delta);

bool sel_find(struct sel *sel, bool (*fn)(void *user), void *user);

#endif /* !SEL_H */
