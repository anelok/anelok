/*
 * fw/ui/icon.c - Icons
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "display.h"
#include "icon.h"

#define	static	static const
#include "icons/bullet.xbm"
#include "icons/circle.xbm"
#include "icons/12x12/delete.xbm"
#include "icons/12x12/delete_bold.xbm"
#include "icons/12x12/enter.xbm"
#include "icons/12x12/enter_bold.xbm"
#include "icons/hand.xbm"
#include "icons/12x12/next.xbm"
#include "icons/12x12/next_bold.xbm"
#include "icons/12x12/off.xbm"
#include "icons/12x12/off_bold.xbm"
#include "icons/12x12/setup.xbm"
#include "icons/12x12/setup_bold.xbm"
#include "icons/sorry.xbm"
#include "icons/current.xbm"
#include "icons/banner.xbm"
#include "icons/12x12/back.xbm"
#include "icons/12x12/back_bold.xbm"
#include "icons/12x12/eye.xbm"
#include "icons/12x12/eye_bold.xbm"
#include "icons/12x12/radio.xbm"
#include "icons/12x12/radio_bold.xbm"
#include "icons/12x12/keyboard.xbm"
#include "icons/12x12/keyboard_bold.xbm"
#include "icons/12x12/exchange.xbm"
#include "icons/12x12/exchange_bold.xbm"
#include "icons/12x12/alert.xbm"
#include "icons/12x12/alert_bold.xbm"
#include "icons/12x12/zap.xbm"
#include "icons/12x12/zap_bold.xbm"
#include "icons/busy.xbm"
#include "icons/12x12/folder.xbm"
#include "icons/12x12/folder_bold.xbm"
#undef static


#define	ICON(name) \
	const struct icon icon_##name = { \
	    name##_width, name##_height, name##_bits }


ICON(bullet);
ICON(circle);
ICON(delete);
ICON(delete_bold);
ICON(enter);
ICON(enter_bold);
ICON(hand);
ICON(next);
ICON(next_bold);
ICON(off);
ICON(off_bold);
ICON(setup);
ICON(setup_bold);
ICON(sorry);
ICON(current);
ICON(banner);
ICON(back);
ICON(back_bold);
ICON(eye);
ICON(eye_bold);
ICON(radio);
ICON(radio_bold);
ICON(keyboard);
ICON(keyboard_bold);
ICON(exchange);
ICON(exchange_bold);
ICON(alert);
ICON(alert_bold);
ICON(zap);
ICON(zap_bold);
ICON(busy);
ICON(folder);
ICON(folder_bold);


#define	POS_WIDTH	2	/* offset for position marker (1 pixel for */
				/* the marker, 1 pixel for visual separation) */
#define	DIV_WIDTH	2	/* width of the icon divider line */
#define	ICON_DIV_GAP	3	/* gap between icons and divider */
#define	MAIN_GAP	2	/* space to leave on non-icon side of divider */


uint8_t main_x0, main_x1;

static bool rotated = 0;


void icon_draw_clip(const struct icon *icon, uint8_t x, int8_t y,
    const struct rect *clip)
{
	display_blit_clip(icon->bits, icon->w, icon->h, (icon->w + 7) >> 3,
	    x, y, clip);
}

void icon_draw(const struct icon *icon, uint8_t x, int8_t y)
{
	icon_draw_clip(icon, x, y, &clip_all);
}


void icon_center(const struct icon *icon)
{
	icon_draw(icon,
	    (main_x0 + main_x1 - icon->w) >> 1,
	    (FB_Y - icon->h) >> 1);
}


void icon_bar(const struct icon *top, const struct icon *mid,
    const struct icon *bot)
{
	uint8_t w = 0;
	uint8_t icon_x, div_x;
	uint8_t x, y;

	if (top)
		w = top->w;
	if (mid && mid->w > w)
		w = mid->w;
	if (bot && bot->w > w)
		w = bot->w;

	if (rotated) {
		main_x1 = FB_X - 1;
		if (!w) {
			main_x0 = POS_WIDTH;
			return;
		}
		icon_x = POS_WIDTH;
		div_x = icon_x + w + ICON_DIV_GAP;
		display_clear_rect(POS_WIDTH, 0, div_x - 1, FB_Y - 1);
		main_x0 = div_x + DIV_WIDTH + MAIN_GAP;
	} else {
		main_x0 = 0;
		if (!w) {
			main_x1 = FB_X - 1 - POS_WIDTH;
			return;
		}
		icon_x = FB_X - w - POS_WIDTH;
		display_clear_rect(icon_x - ICON_DIV_GAP, 0,
		    FB_X - POS_WIDTH - 1, FB_Y - 1);
		div_x = icon_x - ICON_DIV_GAP - DIV_WIDTH;
		main_x1 = div_x - MAIN_GAP - 1;
	}

	if (top)
		icon_draw(top, icon_x, 0);
	if (mid)
		icon_draw(mid, icon_x, (FB_Y - mid->h) >> 1);
	if (bot)
		icon_draw(bot, icon_x, FB_Y - bot->h);

	for (x = div_x; x != div_x + DIV_WIDTH; x++)
		for (y = 0; y != FB_Y; y++)
			display_set(x, y);
}


void icon_rotate(bool on)
{
	rotated = on;
}
