/*
 * fw/ui_show.c - UI: display some fixed content
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "display.h"
#include "std.h"
#include "ui.h"


static struct std std;
static void (*show_fn)(void *user);
static void (*go_back)(void);
static void *show_user;


/* ----- Display update ---------------------------------------------------- */


static void show_show(void *user)
{
	display_clear();
	show_fn(show_user);
}


/* ----- Input callbacks --------------------------------------------------- */


static bool show_back(void *user)
{
	go_back();
	return 1;
}


static bool show_have_back(void *user)
{
	return go_back;
}


static const struct std_ops show_ops = {
	.show		= show_show,
	.back		= show_back,
	.have_back	= show_have_back,
};


void ui_show(void (*show)(void *user), void *user, void (*back)(void))
{
	pm_set_policy(&pm_policy_peruse);
	show_fn = show;
	show_user = user;
	go_back = back;
	std_init(&std, &show_ops, NULL);
}
