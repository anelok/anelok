/*
 * fw/ui/chill.h - Forced wait after failed login
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CHILL_H
#define	CHILL_H

#include <stdbool.h>
#include <stdint.h>


void chill_pass(void);
void chill_fail(void);

void chill_update(uint16_t ms);

/*
 * chill_left_s returns the number of seonds left to chill. The number of
 * milliseconds until the next second is returned in change_ms (unless NULL).
 */
uint32_t chill_left_s(uint16_t *change_ms);

#endif /* !CHILL_H */
