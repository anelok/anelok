/*
 * fw/ui/comm.h - Communication handler
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef COMM_H
#define	COMM_H

#include <stdbool.h>

#include "account.h"


enum comm {
	comm_none	= 0,
	comm_hid,
};


extern enum comm comm;


bool comm_send(struct account_record *rec);
enum comm comm_mode(struct account_record *rec);
void comm_update(enum comm new);

#endif /* COMM_H */
