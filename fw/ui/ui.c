/*
 * fw/ui/ui.c - User interface main loop
 *
 * Written 2013-2016 by Werner Almesberger
 * Copyright 2013-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "touch.h"
#include "tick.h"
#include "led.h"
#include "event.h"
#include "pm.h"
#include "input.h"
#include "display.h"
#include "chill.h"
#include "auth.h"
#include "ui.h"


#define	BLINK_INTERVAL_MS	5000
#define	BLINK_ON_MS		1

bool leave_ui = 0;


static uint16_t t;
static uint32_t t_idle = 0;
static uint32_t t_last_blink = 0;


static void blink(void)
{
	led(1);
	msleep(BLINK_ON_MS);
	led(0);
	t_last_blink = t_idle;
}


bool ui(void)
{
	uint16_t dt;
	uint8_t pos;

	if (leave_ui)
		return 0;
	dt = tick_delta(&t);
	auth_delta(dt);	/* @@@ ugly */
	chill_update(dt);
	if (touch_filter(&pos)) {
		input_press(FB_Y - pos - 1, dt);
		t_idle = t_last_blink = 0;
		if (ui_is_on) {
			pm_busy();
		} else {
			if (input_pressed() == area_bottom && auth_valid())
				blink();
		}
	} else {
		input_release(dt);
		t_idle += dt;
		pm_idle(t_idle);
		switch (pm_state()) {
		case pm_active:
			break;
		case pm_standby:
			/*
			 * @@@ we do this mainly to force an authentication
			 * test. Authentication should be checked closer to
			 * the database, e.g., timed-out authentication should
			 * invalidate the key(s) and any access should simply
			 * fail to return valid results, upon which the caller
			 * can then return to login.
			 */
			if (ui_is_on)
				ui_off();
#ifndef SIMULATOR
			msleep(50);
#endif
			/* fall through */
		default:
			if (!auth_valid())
				break;
			if (t_idle - t_last_blink < BLINK_INTERVAL_MS)
				break;
			blink();
			break;
		}
	}
	if (display_update())
		msleep(6);
	else
		msleep(10);

	if (event_consume(ev_refresh))
		input_refresh();

	return 1;
}


void ui_init(void)
{
	t = tick_now();
}
