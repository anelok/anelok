/*
 * fw/ui/cursor.h - Cursors in lists
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CURSOR_H
#define	CURSOR_H

#include <stdbool.h>
#include <stdint.h>


typedef uint16_t cursor_pos_type;

/*
 * Either rewind or prev must be implemented. An unimplemented function can be
 * NULL or return 0.
 */

struct cursor_ops {
	bool (*rewind)(void *user);	/* optional */
	bool (*prev)(void *user);	/* optional */
	bool (*next)(void *user);	/* required */
};

struct cursor {
	const struct cursor_ops *ops;
	void *user;
	cursor_pos_type pos;
};


static inline cursor_pos_type cursor_pos(const struct cursor *cursor)
{
	return cursor->pos;
}

void cursor_rewind(struct cursor *cursor);
bool cursor_prev(struct cursor *cursor);
bool cursor_next(struct cursor *cursor);

bool cursor_goto(struct cursor *cursor, cursor_pos_type pos);

/*
 * cursor_sync rewinds the cursor then goes to the previous position. This is
 * necessary when accessing the first or last element of a list if a
 * cursor_prev or cursor_next has been attempted on that element and this
 * operation caused a state change.
 *
 * Normally, no such state change should occur. However, when stacking cursors
 * and filtering results (like we do in ui_select), the unfiltered cursor may
 * move over additional entries while the filtered cursor stays. In this case,
 * the state of the underlying list will reflect the position of the unfiltered
 * and not the filtered cursor and a call to cursor_sync is needed to send both
 * to the same place.
 */

void cursor_sync(struct cursor *cursor);

/*
 * To avoid unexpected callbacks, cursor_init does not rewind the list.
 */

void cursor_init(struct cursor *cursor,
    const struct cursor_ops *ops, void *user);

#endif /* !CURSOR_H */
