/*
 * fw/std.h - Standard button layout
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef STD_H
#define	STD_H

#include <stdbool.h>
#include <stdint.h>

#include "input.h"


/*
 * "back", "setup", "tap", and "middle_press" return 1 if leaving the current
 * screen, 0 if staying. In the latter case, the screen content will be
 * redrawn.
 *
 * "swipe_move" returns 1 if a display update may be necessary.
 */

struct std_ops {
	void (*show)(void *user);

	bool (*have_back)(void *user);
	bool (*have_setup)(void *user);
	bool (*back)(void *user);
	bool (*setup)(void *user);

	bool (*tap)(enum area area, void *user);	/* quick tap */
	bool (*expedite)(enum area area, void *user);	/* tap equals long */
	bool (*middle_press)(void *user);		/* long press */
	const struct icon *(*middle_icon)(bool bold, void *user);
						/* icon of middle button */

	void (*swipe_begin)(enum area area, void *user);
	bool (*swipe_move)(int8_t delta, void *user);
	void (*swipe_end)(enum area area, void *user);

	void (*timer)(void *user);

	void (*suspend)(void *user);
	void (*resume)(void *user);
};


struct std {
	const struct std_ops *ops;
	void *user;

	enum area bold;
	bool extended;
};


void std_init(struct std *ctx, const struct std_ops *ops, void *user);

void sim_std_show(struct std *ctx);

#endif /* !STD_H */
