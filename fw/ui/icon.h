/*
 * fw/ui/icon.c - Icons
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef ICON_H
#define	ICON_H

#include <stdbool.h>
#include <stdint.h>

#include "display.h"


struct icon {
	uint8_t	w;	/* width */
	uint8_t	h;	/* height */
	const unsigned char *bits;
};


extern const struct icon icon_bullet;
extern const struct icon icon_circle;
extern const struct icon icon_delete;
extern const struct icon icon_delete_bold;
extern const struct icon icon_enter;
extern const struct icon icon_enter_bold;
extern const struct icon icon_hand;
extern const struct icon icon_next;
extern const struct icon icon_next_bold;
extern const struct icon icon_off;
extern const struct icon icon_off_bold;
extern const struct icon icon_setup;
extern const struct icon icon_setup_bold;
extern const struct icon icon_sorry;
extern const struct icon icon_current;
extern const struct icon icon_banner;
extern const struct icon icon_back;
extern const struct icon icon_back_bold;
extern const struct icon icon_eye;
extern const struct icon icon_eye_bold;
extern const struct icon icon_radio;
extern const struct icon icon_radio_bold;
extern const struct icon icon_keyboard;
extern const struct icon icon_keyboard_bold;
extern const struct icon icon_exchange;
extern const struct icon icon_exchange_bold;
extern const struct icon icon_alert;
extern const struct icon icon_alert_bold;
extern const struct icon icon_zap;
extern const struct icon icon_zap_bold;
extern const struct icon icon_busy;
extern const struct icon icon_folder;
extern const struct icon icon_folder_bold;


/* Main display area, updated by icon_bar */
extern uint8_t main_x0, main_x1;


void icon_draw_clip(const struct icon *icon, uint8_t x, int8_t y,
    const struct rect *clip);
void icon_draw(const struct icon *icon, uint8_t x, int8_t y);
void icon_center(const struct icon *icon);
void icon_bar(const struct icon *top, const struct icon *mid,
    const struct icon *bot);
void icon_rotate(bool on);

#endif /* !ICON_H */
