/*
 * fw/ui/alert.c - User alerts
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "event.h"
#include "alert.h"


static struct {
	bool (*fn)(void *user);	/* NULL if inactive */
	void *user;
} alerts[al_end];


static bool in_alert = 0;


static void refresh(void)
{
	event_set_interrupt(ev_refresh); /* @@@ */
}


void alert_set(enum alert alert, bool (*fn)(void *user), void *user)
{
	REQUIRE((unsigned) alert < al_end);
	alerts[alert].fn = fn;
	alerts[alert].user = user;
	refresh();
}


void alert_clear(enum alert alert)
{
	REQUIRE((unsigned) alert < al_end);
	alerts[alert].fn = NULL;
	refresh();
}


bool alert_pending(void)
{
	uint8_t i;

	if (in_alert)
		return 0;
	for (i = 0; i != al_end; i++)
		if (alerts[i].fn)
			return 1;
	return 0;
}


bool alert_run_next(void)
{
	bool (*fn)(void *user);
	uint8_t i;
	bool found = 0;

	if (in_alert)
		return 0;
	for (i = 0; i != al_end; i++) {
		fn = alerts[i].fn;
		if (!fn)
			continue;
		alert_clear(i);
		found = 1;
		if (fn(alerts[i].user))
			continue;
		in_alert = 1;
		break;
	}
	if (found)
		refresh();
	return found;
}


void alert_end(void)
{
	in_alert = 0;
	refresh();
}
