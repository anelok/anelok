/*
 * fw/input.h - Input events
 *
 * Written 2014-2017 by Werner Almesberger
 * Copyright 2014-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef INPUT_H
#define	INPUT_H

#include <stdbool.h>
#include <stdint.h>


enum area {
	area_none,
	area_top,
	area_middle,
	area_bottom,
	area_end		/* must be last */
};

struct input_ops {
	void (*tap)(enum area area, void *user);	/* quick tap */
	void (*begin)(enum area area, void *user);	/* begin long press */
	void (*cancel)(enum area area, void *user);	/* press cancelled */
	void (*press)(enum area area, void *user);	/* press confirmed */
	bool (*extend)(enum area area, void *user); /* begin extended press */
	void (*extended)(enum area area, void *user);	/* extended press */
	bool (*timeout)(enum area area, void *user);	/* long/ext exceeded */
	bool (*expedite)(enum area area, void *user);	/* tap equals long */
	void (*swipe_begin)(enum area area, void *user);
	void (*swipe_move)(int8_t delta, void *user);
	void (*swipe_end)(enum area area, void *user);

	void (*timer)(void *user);

	/*
	 * Power management. Refresh screen after resume.
	 *
	 * Note that a call to input_suspend is not necessarily followed by a
	 * call to input_resume, e.g., if the system goes to a deeper
	 * power-saving mode during suspend.
	 *
	 * Users of the input system will typically only implement either
	 * suspend or resume. For example, the login dialog just directly
	 * turns off the device from the suspend handler.
	 */
	void (*suspend)(void *user);
	void (*resume)(void *user);

	void (*refresh)(void *user);
};


void sim_swipe(int8_t d);

void input_rotate(bool on);

enum area input_pressed(void);

void input_select(const struct input_ops *ops, void *user);
void input_press(uint8_t pos, uint32_t delta_ms);
void input_release(uint32_t delta_ms);
void input_suspend(void);
void input_resume(void);
void input_refresh(void);

void input_set_timer(uint16_t ms);
void input_cancel_timer(void);
bool input_next_timer(uint16_t *ms);

void input_push(void);
void input_pop(void);
void input_rewind(void);

#endif /* !INPUT_H */
