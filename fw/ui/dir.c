/*
 * fw/ui/dir.c - Path operations
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "qa.h"
#include "dbconf.h"
#include "dir.h"


static dir_type parent[MAX_DIRS + 1];
static dir_type dir[MAX_ACCOUNTS];
static char names[(MAX_DIRS - 1) * MAX_DIR_NAME];
static dir_type n_dirs;
static account_type n_accounts;
static account_type first[MAX_DIRS + 1];


/* ----- Helper functions -------------------------------------------------- */


static char *strnchr(char *s, char c, uint8_t n)
{
	char *p;

	p = memchr(s, c, n);
	if (p)
		return p;
	return s + n;
}


/* ----- Entry classification ---------------------------------------------- */


bool dir_show_entry(dir_type cwd, account_type n)
{
	dir_type d;

	if (dir[n] == cwd)
		return 1;
	for (d = 1; d != n_dirs + 1; d++)
		if (first[d] == n && parent[d] == cwd)
			return 1;
	return 0;
}


bool dir_is_sub(dir_type cwd, account_type n)
{
	return dir[n] != cwd;
}


void dir_get_sub(dir_type cwd, account_type n, const char *name,
    char res[MAX_DIR_NAME + 1])
{
	const char *p = name;
	const char *sep;
	dir_type d;

	for (d = cwd; d; d = parent[d]) {
		p = strchr(p , SEP_CHAR);
		REQUIRE(p);
		p++;
	}

	sep = strchr(p, SEP_CHAR);
	REQUIRE(sep);

	memcpy(res, p, sep - p);
	res[sep - p] = 0;
}


dir_type dir_enter(dir_type cwd, account_type n)
{
	dir_type d;

	d = dir[n];
	REQUIRE(d != cwd);
	while (d) {
		if (parent[d] == cwd)
			return d;
		d = parent[d];
	}
	oops();
}


dir_type dir_leave(dir_type cwd)
{
	REQUIRE(cwd);
	return parent[cwd];
}


/* ----- Directory cache --------------------------------------------------- */


static dir_type get_dir(dir_type cwd, const char *name, uint8_t name_len)
{
	char *p;
	const char *curr;
	dir_type d;

	p = names;
	for (d = 1; d != n_dirs + 1; d++) {
		curr = p;
		p = strnchr(p, 0, MAX_DIR_NAME) + 1;
		if (parent[d] != cwd)
			continue;
		if (!strncmp(curr, name, name_len) && !curr[name_len])
			return d;
	}
	memcpy(p, name, name_len);
	if (name_len != MAX_DIR_NAME)
		p[name_len] = 0;
	n_dirs++;
	first[n_dirs] = n_accounts;
	parent[n_dirs] = cwd;
	return n_dirs;
}


void dir_cache_begin(void)
{
	REQUIRE(MAX_DIRS < (dir_type) ~0);
	REQUIRE(MAX_ACCOUNTS <= (account_type) ~0);
	n_dirs = 0;
	n_accounts = 0;
	/* @@@ dynamically allocate "names" */
}


void dir_cache_add(const char *name)
{
	const char *p = name;
	const char *sep;
	dir_type cwd = 0;

	p = name;
	while (1) {
		sep = strchr(p, SEP_CHAR);
		if (!sep)
			break;
		REQUIRE(sep - p <= MAX_DIR_NAME);
		cwd = get_dir(cwd, p, sep - p);
		p = sep + 1;
	}
	dir[n_accounts] = cwd;
	n_accounts++;
}


void dir_cache_end(void)
{
#if 0
#include <stdio.h>
	uint16_t i;

	for (i = 0; i != n_dirs + 1; i++)
		fprintf(stderr, "dir %d parent %d first %d\n",
		    i, parent[i], first[i]);
	for (i = 0; i != n_accounts; i++)
		fprintf(stderr, "acc %d dir %d\n", i, dir[i]);
#endif
	/* @@@ free "names" */
}
