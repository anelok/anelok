/*
 * fw/ui/textsel.c - Selection from text list
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "display.h"
#include "icon.h"
#include "text.h"
#include "input.h"
#include "record.h"
#include "std.h"
#include "cursor.h"
#include "sel.h"
#include "textsel.h"


#define	FONT		font_medium
#define	LINE_Y		(FONT.h + 1)
#define	TEXT_X		(FONT.w + 1)
#define	MARKER_Y	((FONT.h - icon_current.h - 1) >> 1)


/* ----- Cursor operations ------------------------------------------------- */


static bool textsel_cursor_rewind(void *user)
{
	struct textsel *ctx = user;

	ctx->curr = ctx->list;
	return 1;
}


static bool textsel_cursor_prev(void *user)
{
	struct textsel *ctx = user;

	ctx->curr--;
	return 1;
}


static bool textsel_cursor_next(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->curr[1].text)
		return 0;
	ctx->curr++;
	return 1;
}


static const struct cursor_ops textsel_cursor_ops = {
	.rewind	= textsel_cursor_rewind,
	.prev	= textsel_cursor_prev,
	.next	= textsel_cursor_next,
};


/* ----- Access operations ------------------------------------------------- */


static const char *textsel_access_text(void *user)
{
	struct textsel *ctx = user;

	return ctx->curr->text;
}


static void *textsel_access_entry(void *user)
{
	struct textsel *ctx = user;

	return (void *) ctx->curr->user_entry;
}


static const struct textsel_access_ops textsel_access_ops = {
	.text	= textsel_access_text,
	.entry	= textsel_access_entry,
};


/* ----- Selection operations ---------------------------------------------- */


static uint8_t textsel_size(void *user)
{
	return LINE_Y;
}


static void textsel_display(void *user, int8_t y, bool selected)
{
	struct textsel *ctx = user;
	uint8_t x = main_x0 + TEXT_X;
	struct rect clip = {
		.x	= x,
		.y	= 0,
		.w	= main_x1 - x + 1,
		.h	= FB_Y,
        };

	if (selected) {
		icon_draw(&icon_current, main_x0, y + MARKER_Y);
		ctx->selected_entry = cursor_pos(ctx->cursor);
	}
	
	record_display(ctx->access_ops->text(ctx->access_user),
	    &FONT, x, y, &clip);
}


static const struct sel_ops textsel_sel_ops = {
	.size		= textsel_size,
	.display_begin	= record_display_begin,
	.display_end	= record_display_end,
	.display	= textsel_display,
};


/* ----- Display update ---------------------------------------------------- */


static inline void *current_user_entry(struct textsel *ctx)
{
	cursor_goto(ctx->cursor, ctx->selected_entry);
	if (!ctx->access_ops->entry)
		return NULL;
	return ctx->access_ops->entry(ctx->access_user);
}


/* ----- Standard interface actions ---------------------------------------- */


static void textsel_show(void *user)
{
	struct textsel *ctx = user;

	display_clear();
	sel_show(&ctx->sel);
}


static bool textsel_have_back(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->back)
		return 0;
	if (ctx->ops->have_back)
		return ctx->ops->have_back(ctx->user);
	return 1;
}


static bool textsel_have_setup(void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->setup)
		return 0;
	if (ctx->ops->have_setup)
		return ctx->ops->have_setup(ctx->user);
	return 1;
}


static bool textsel_back(void *user)
{
	struct textsel *ctx = user;

	return ctx->ops->back(ctx->user);
}


static bool textsel_setup(void *user)
{
	struct textsel *ctx = user;

	return ctx->ops->setup(ctx->user);
}


static bool textsel_middle_press(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->select)
		return ctx->ops->select(ctx->user, current_user_entry(ctx));
	return 0;
}


static bool textsel_tap(enum area area, void *user)
{
	struct textsel *ctx = user;

	switch (area) {
	case area_top:
		sel_up(&ctx->sel);
		break;
	case area_middle:
		return textsel_middle_press(user);
	case area_bottom:
		sel_down(&ctx->sel);
		break;
	default:
		break;
	}
	return 0;
}


static bool textsel_expedite(enum area area, void *user)
{
	return area == area_middle;
}


static const struct icon *textsel_middle_icon(bool bold, void *user)
{
	struct textsel *ctx = user;

	if (!ctx->ops->select)
		return NULL;
	if (ctx->ops->middle_icon) {
		cursor_goto(ctx->cursor, ctx->selected_entry);
		return ctx->ops->middle_icon(bold, ctx->user);
	}
	return bold ? &icon_next_bold : &icon_next;
}


static void textsel_swipe_begin(enum area area, void *user)
{
	struct textsel *ctx = user;

	ctx->last = 0;
}


static bool textsel_swipe_move(int8_t delta, void *user)
{
	struct textsel *ctx = user;
	bool change;

	change = sel_swipe(&ctx->sel, delta - ctx->last);
	ctx->last = delta;
	return change;
}


static void textsel_timer(void *user)
{
	record_display_timer();
	input_refresh();
}


static void textsel_suspend(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->suspend)
		ctx->ops->suspend(ctx->user, current_user_entry(ctx));
}


static void textsel_resume(void *user)
{
	struct textsel *ctx = user;

	if (ctx->ops->resume)
		ctx->ops->resume(ctx->user, current_user_entry(ctx));
}


static const struct std_ops textsel_std_ops = {
	.show		= textsel_show,
	.have_back	= textsel_have_back,
	.have_setup	= textsel_have_setup,
	.back		= textsel_back,
	.setup		= textsel_setup,
	.tap		= textsel_tap,
	.expedite	= textsel_expedite,
	.middle_press	= textsel_middle_press,
	.middle_icon	= textsel_middle_icon,
	.swipe_begin	= textsel_swipe_begin,
	.swipe_move	= textsel_swipe_move,
	.timer		= textsel_timer,
	.suspend	= textsel_suspend,
	.resume		= textsel_resume,
};


/* ----- Searching --------------------------------------------------------- */


bool textsel_find(struct textsel *ctx, bool (*fn)(void *user), void *user)
{
	bool res;

	res = sel_find(&ctx->sel, fn, user);
	sim_std_show(&ctx->std);	// @@@
	return res;
}


/* ----- Initialization ---------------------------------------------------- */


void textsel_dyn_proceed(struct textsel *ctx)
{
	std_init(&ctx->std, &textsel_std_ops, ctx);
}


void textsel_dyn_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, struct cursor *cursor,
    const struct textsel_access_ops *access_ops, void *access_user)
{
	ctx->ops = ops;
	ctx->user = user;
	ctx->access_ops = access_ops;
	ctx->access_user = access_user;
	ctx->cursor = cursor;

	sel_init(&ctx->sel, cursor, &textsel_sel_ops, ctx);
	textsel_dyn_proceed(ctx);
}


void textsel_static_proceed(struct textsel *ctx)
{
	textsel_dyn_proceed(ctx);
}


void textsel_static_init(struct textsel *ctx, const struct textsel_ops *ops,
    void *user, const struct textsel_entry *list)
{
	ctx->list = ctx->curr = list;
	cursor_init(&ctx->static_cursor, &textsel_cursor_ops, ctx);
	textsel_dyn_init(ctx, ops, user, &ctx->static_cursor,
	    &textsel_access_ops, ctx);
}


/* ----- Simulator support ------------------------------------------------- */


void sim_ts_show(struct textsel *ctx)
{
	sim_std_show(&ctx->std);
}
