/*
 * fw/ui_sorry.c - UI: general error indication
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "pm.h"
#include "display.h"
#include "std.h"
#include "icon.h"
#include "ui.h"


static struct std std;


/* ----- Display update ---------------------------------------------------- */


static void sorry_show(void *user)
{
	display_clear();
	icon_center(&icon_sorry);
}


/* ----- Standard interface callbacks -------------------------------------- */


static void sorry_suspend(void *user)
{
	ui_off();
}


static const struct std_ops sorry_ops = {
	.show		= sorry_show,
	.suspend	= sorry_suspend,
};


void ui_sorry(void)
{
	std_init(&std, &sorry_ops, NULL);
}


bool ui_sorry_dummy(void *dummy)
{
	pm_set_policy(&pm_policy_default);
	ui_sorry();
	return 1;
}
