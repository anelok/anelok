#!/usr/bin/perl
#
# fontify.pl - Convert BDF font to X bitmap (extended for Anelok use)
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# The BDF format is documented in Wikipedia:
# https://en.wikipedia.org/wiki/Glyph_Bitmap_Distribution_Format
#


sub usage
{
	print STDERR "usage $0 name [file ...]\n";
	exit(1);
}


$name = shift @ARGV;
&usage unless defined $name;

while (<>) {
	if ($bits) {
		die unless /^([0-9A-F]+)/;
		push(@{ $c[$code] }, $1);
		$bits--;
		next;
	}
	if (/^FONTBOUNDINGBOX\s+(\d+)\s+(\d+)\s+(-?\d+)\s+(-?\d+)/) {
		@box = ($1, $2, $3, $4);
		next;
	}
	if (/^STARTCHAR/) {
		@bbx = @box;
		undef $code;
		next;
	}
	if (/^BBX\s+(\d+)\s+(\d+)\s+(-?\d+)\s+(-?\d+)/) {
		@bbx = ($1, $2, $3, $4);
		next;
	}
	if (/^ENCODING\s+(\d+)/) {
		die if defined $c{$1};
		$code = $1;
		next;
	}
	if (/^BITMAP/) {
		@{ $bbx[$code] } = @bbx;
		$bits = $bbx[1];
		next;
	}
}


($w, $h) = ($bbx[0] - 1, $bbx[1] - 1);

$cx = ($w + 7) & ~7;
$cy = $h;
$mx = 96;

$offset = 1;

print "/* MACHINE-GENERATED. DO NOT EDIT ! */\n\n";
print "#define ${name}_width ", $cx * 96, "\n";
print "#define ${name}_height ", $cy, "\n";
print "static const uint8_t ${name}_bits[] = {\n";

$nib = "0123456789ABCDEF";
     #  0000000011111111
     #  0000111100001111
     #  0011001100110011
     #  0101010101010101
$rev = "084C2A6E195D3B7F";

$n = 0;
for ($y = $offset; $y != $h + $offset; $y++) {
	for ($c = 32; $c != 128; $c++) {
		my $s = $c[$c == 127 ? 32 : $c][$y];

		die "no character for code $c" unless defined $s;
		$s = join("", map(reverse(split(//)), split(/..\K/, $s)));
		eval "\$s =~ y/$nib/$rev/";
		while (length $s) {
			print "  " unless $n;
			if ($n == 12) {	# XBM compatibility
				print "\n  ";
				$n = 0;
			}
			print "0x", substr($s, 0, 2), ", ";
			$s = substr($s, 2);
			$n++;
		}
	}
}
print "\n  };\n";

print "const struct font ${name} = {\n";
print "\t.data\t= ${name}_bits,\n";
print "\t.w\t= $w,\n";
print "\t.h\t= $h,\n";
print "\t.cx\t= $cx,\n";
print "\t.cy\t= $cy,\n";
print "\t.mx\t= $mx\n";
print "};\n";
