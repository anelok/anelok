/*
 * fw/ybox.c - Y-Box DFU-capable bootloader
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "board.h"
#include "misc.h"
#include "cc.h"
#include "led.h"
#include "clock.h"
#include "flash.h"
#include "usb-board.h"
#include "usb.h"
#include "dfu.h"
#include "dfu-wait.h"


static void (*run_app)(void) =
    (void (*)(void)) (APP_BASE | 1); /* | 1 for Thumb */


static void wait_clock(void)
{
	gpio_begin(RF_CLK);
	gpio_fn(RF_CLK, 4);	/* TPM_CLKIN0 */
	gpio_end(RF_CLK);

	SIM_SOPT2 |= SIM_SOPT2_TPMSRC(1);
				/* use MCGFLLCLK or MCGPLLCLK/2 for TPM */
	SIM_SCGC6 |= SIM_SCGC6_TPM0_MASK;

	TPM0_SC = 0;		/* disable counter */
	TPM0_CNT = 0;		/* clear counter */
	TPM0_MOD = 100;		/* count 100 cycles */
	TPM0_SC =
	    TPM_SC_TOF_MASK |	/* clear overflow */
	    TPM_SC_CMOD(2) |	/* use external clock */
	    TPM_SC_PS(0);	/* prescale by 1 */

	while (!(TPM0_STATUS & TPM_STATUS_TOF_MASK));

	TPM0_SC = 0;		/* disable counter */
	SIM_SCGC6 &= ~SIM_SCGC6_TPM0_MASK;
				/* clock gate */
}


static void transition_clock(void)
{
	/*
	 * Switch back to using the internal clock while we reset the CC25xx.
	 * It's up to the application to make contact after the reset.
	 */

	MCG_C6 &= ~MCG_C6_CME0_MASK;
	clock_internal();
	cc_release();

	wait_clock();

	clock_external();

	/* enable clock monitor again */

	MCG_C6 |= MCG_C6_CME0_MASK;
}


int main(void)
{
	led_init();

	cc_init();

	mdelay(10); /* debounce (if we're coming from a LOCS0 reset) */
	if (!cc_on()) {
		led(1);
		while (!cc_on());
		led(0);
	}
	mdelay(10); /* debounce */

	if (!cc_acquire())
		panic();
	cc_clock();

	usb_init();
	usb_begin_device();

	/*
	 * Enable clock monitor. Due to MCG_C2.LOCRE0 being set, this will
	 * reset when the clock fails. The clock will fail when rfkill is
	 * asserted.
	 *
	 * @@@ Should we skip DFU after reset by rfkill ? We can find out by
	 * checking RCM_SRS0.LOC
	 */

	MCG_C6 |= MCG_C6_CME0_MASK;

	dfu_init();
	flash_init();
	cc_led(1);
	while (dfu_wait());
	cc_led(0);
	usb_end_device();

	/*
	 * @@@ set FTFA_FPROTx to protect boot loader:
	 *
	 * 1) Write application that trashes the boot loader
	 * 2) and indicates success/failure via LED(s)
	 * 3) Set FPROT and run application agaib
	 * 4) Change application to trash something immediately after boot
	 *    loader (should be allowed)
	 */

	transition_clock();

	run_app();

	reset_cpu();
}
