/*
 * fw/ep0.c - EP0 extension protocol
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdint.h>

#include "cc/op.h"

#include "led.h"
#include "rf.h"
#include "usb.h"
#include "touch.h"
#include "ep0.h"


extern uint8_t rx_buf[128];
extern uint8_t rx_len;

static uint8_t rf_cmd;
static uint8_t rf_msg[128];


static void send_cmd(void *user)
{
	rf_send(rf_msg, rf_cmd);
}


static void clear_rx_buf(void *user)
{
	rx_len = 0;
}


static bool my_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;
	uint16_t size;
	uint16_t touch_res;

	switch (req) {
	case YBOX_PUT:
		if (setup->wLength < 1)
			return 0;
		if (setup->wLength > sizeof(rf_msg))
			return 0;
		usb_recv(&eps[0], rf_msg, setup->wLength, send_cmd, NULL);
/*
 * @@@ rf_msg overflows and overwrites rf_cmd. something must be very wrong
 * in the USB driver.
 */
		rf_cmd = setup->wLength | (setup->wValue ? 0x80 : 0);
#if 0
		led(!!setup->wValue);
		rf_msg[0] = setup->wIndex;
		rf_send(rf_msg, 0x81);
#endif
#if 0
		if (setup->wLength < 1)
			return 0;
		if (setup->wLength > MAX_PSDU)
			return 0;
		buf[0] = AT86RF230_BUF_WRITE;
		buf[1] = setup->wLength;
		size = setup->wLength + 2;
		usb_recv(&eps[0], buf + 2, setup->wLength, do_buf_write, NULL);
#endif
		return 1;
	case YBOX_GET:
		size = rx_len < setup->wLength ? rx_len : setup->wLength;
		usb_send(&eps[0], rx_buf, size, clear_rx_buf, NULL);
		return 1;
	case YBOX_POKE:
		rf_msg[0] = OP_POKE;
		rf_msg[1] = setup->wIndex;
		rf_msg[2] = setup->wIndex >> 8;
		rf_msg[3] = setup->wValue;
		rf_send(rf_msg, 0x84);
		return 1;
	case YBOX_PEEK:
		rf_msg[0] = OP_PEEK;
		rf_msg[1] = setup->wIndex;
		rf_msg[2] = setup->wIndex >> 8;
		rf_send(rf_msg, 0x83);
		usb_send(&eps[0], NULL, 0, NULL, NULL);
		return 1;
	case YBOX_TOUCH:
		usb_send(&eps[0], &touch_res,
		    touch_read(&touch_res) ? 2 : 0, NULL, NULL);
		return 1;
	default:
		return 0;
	}
}


void ep0_init(void)
{
	user_setup = my_setup;
}
