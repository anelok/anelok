/*
 * fw/flash.c - Board-specific flash functions
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "dfu.h"
#include "flash.h"
#include "board.h"


const struct dfu_flash_ops *dfu_flash_ops = &plat_flash_ops;


static void my_set_interface(int nth)
{
#if DFU_ALT_SETTINGS > 1
	if (nth)
		dfu_flash_ops = &cc_flash_ops;
	else
		dfu_flash_ops = &plat_flash_ops;
#endif /* DFU_ALT_SETTINGS > 1 */
}


#if DFU_ALT_SETTINGS > 1

static const uint8_t string_descriptor_0[] = {
	4,				/* blength */
	USB_DT_STRING,			/* bDescriptorType */
	LE(USB_LANGID_ENGLISH_US)	/* wLANGID[0]  */
};


static const uint8_t string_descriptor_1[] = {
	2 + DFU_ALT_NAME_0_LEN,		/* blength */
	USB_DT_STRING,			/* bDescriptorType */
	DFU_ALT_NAME_0			/* bString */
};


static const uint8_t string_descriptor_2[] = {
	2 + DFU_ALT_NAME_1_LEN,		/* blength */
	USB_DT_STRING,			/* bDescriptorType */
	DFU_ALT_NAME_1			/* bString */
};


static bool my_get_descriptor(uint8_t type, uint8_t index,
     const uint8_t **reply, uint8_t *size)
{
	switch (type) {
	case DFU_DT_FUNCTIONAL:
		return dfu_my_descr(type, index, reply, size);
	case USB_DT_STRING:
		break;
	default:
		return 0;
	}
	switch (index) {
	case 0:
		*reply = string_descriptor_0;
		*size = sizeof(string_descriptor_0);
		return 1;
	case DFU_ALT_NAME_0_IDX:
		*reply = string_descriptor_1;
		*size = sizeof(string_descriptor_1);
		return 1;
	case DFU_ALT_NAME_1_IDX:
		*reply = string_descriptor_2;
		*size = sizeof(string_descriptor_2);
		return 1;
	default:
		return 0;
	}
}

#endif /* DFU_ALT_SETTINGS > 1*/


void flash_init(void)
{
	user_set_interface = my_set_interface;
#if DFU_ALT_SETTINGS > 1
	user_get_descriptor = my_get_descriptor;
#endif
}
