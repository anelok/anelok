/*
 * fw/common/dfu-wait.h - Wait-for-DFU loop
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef DFU_WAIT_H
#define	DFU_WAIT_H

#include <stdbool.h>


bool dfu_wait(void);

#endif /* !DFU_WAIT_H */
