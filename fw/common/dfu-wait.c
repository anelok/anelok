/*
 * fw/common/dfu-wait.c - Wait-for-DFU loop
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "led.h"
#include "usb-board.h"
#include "usb.h"
#include "dfu.h"
#include "dfu-wait.h"


static volatile uint32_t *app = (volatile uint32_t *) APP_BASE;


static bool got_application(void)
{
	return *app != 0xffffffff;
}


bool dfu_wait(void)
{
	uint32_t i;

	/*
	 * Wait for enumeration
	 *
	 * The delay of this loop is about 3.00 s
	 */

	led(0);

	for (i = 0; i != 2900 * 1000; i++) {
		usb_poll_device();
		if (usb_enumerated)
			goto enumerated;
	}
	return !got_application();

enumerated:
	/*
	 * Wait for DFU
	 *
	 * The delay of this loop is about 1.86 s
	 */

	led(1);
	i = 0;
	while (i != 1200 * 1000) {
		if (!usb_enumerated)
			return 1;
		if (dfu.state == dfuIDLE && got_application())
			i++;
		else
			i = 0;
		usb_poll_device();
	}

	/* done */

	led(0);

	return 0;
}
