#!/usr/bin/perl
print("#ifdef VECS_ASSIGN\n");
for (@ARGV) {
	/=/ || die;
	print "[$`_IRQn + 16]	= ${'}_isr,\n";
}
print("#endif /* VECS_ASSIGN */\n\n");
print("#undef VECS_ASSIGN\n");

print("#ifdef VECS_DECLARE\n");
for (@ARGV) {
	/=/ || die;
	print "void ${'}_isr(void);\n";
EOF
}
print("#endif /* VECS_DECLARE*/\n\n");
print("#undef VECS_DECLARE\n");

print("#ifdef VECS_DEFAULT\n");
for (@ARGV) {
	/=/ || die;
	print <<"EOF";
	__asm__("${'}_isr\\t= default_handler\\n");
	__asm__("\\t.weak\\t${'}_isr\\n");
EOF
}
print("#endif /* VECS_DEFAULT */\n\n");
print("#undef VECS_DEFAULT\n");
