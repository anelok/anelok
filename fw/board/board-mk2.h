/*
 * fw/board/board-mk2.h - GPIOs of Mk 2 Anelok board
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_MK2_H
#define	BOARD_MK2_H

#include "gpio.h"


/* ----- Power distribution ------------------------------------------------ */

#define	PWR_EN		GPIO_ID(D, 2)
#define	DISP_ON		GPIO_ID(B, 16)
#define	CARD_ON		GPIO_ID(B, 3)

/* ----- LED --------------------------------------------------------------- */

#define	LED		GPIO_ID(B, 0)

/* ----- Display ----------------------------------------------------------- */

#define	DISP_SCLK	GPIO_ID(C, 5)
#define	DISP_SDIN	GPIO_ID(C, 6)
#define	DISP_DnC	GPIO_ID(C, 2)
#define	DISP_nRES	GPIO_ID(C, 0)
#define	DISP_nCS	GPIO_ID(B, 17)

/* ----- Capacitative sensor ----------------------------------------------- */

#define	CAP_A		GPIO_ID(A, 1)
#define	CAP_B		GPIO_ID(A, 2)

/* ----- USB --------------------------------------------------------------- */

#define	USB_ID		GPIO_ID(E, 21)
#define	USB_nSENSE	GPIO_ID(C, 1)

/* ----- Memory card ------------------------------------------------------- */

#define	CARD_DAT2	GPIO_ID(E, 29)
#define	CARD_DAT3	GPIO_ID(E, 30)
#define	CARD_CMD	GPIO_ID(E, 24)
#define	CARD_CLK	GPIO_ID(E, 25)
#define	CARD_DAT0	GPIO_ID(B, 2)
#define	CARD_DAT1	GPIO_ID(B, 1)

#define	CARD_SW		GPIO_ID(C, 3)

/* ----- RF ---------------------------------------------------------------- */

#define	RF_MOSI		GPIO_ID(D, 7)
#define	RF_MISO		GPIO_ID(D, 6)
#define	RF_SCLK		GPIO_ID(D, 5)
#define	RF_nSEL		GPIO_ID(D, 3)

#define	RF_DD		GPIO_ID(D, 4)
#define	RF_DC		GPIO_ID(C, 7)
#define	RF_nRESET	GPIO_ID(D, 0)

#define	VRF		GPIO_ID(C, 4)

#define	RF_nINT		RF_DD
#define	RF_nREQ		RF_DC

/* ----- USB --------------------------------------------------------------- */

#define	DFU_ALT_SETTINGS 2

#endif /* !BOARD_MK2_H */
