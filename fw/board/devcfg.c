/*
 * fw/board/devcfg.c - Configuration specific to individual device
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "misc.h"
#include "id.h"
#include "devcfg.h"


struct devcfg_db {
	struct id id;
	struct devcfg cfg;
};



static const struct devcfg_db devcfg_db[] = {
#include "../devcfg.inc"
	/* default */
	{ .id = { 0, 0, 0 }, .cfg = { 0, 0, 0, 0, 0, 0, 0, 0 } },
};

const struct devcfg *devcfg = &(ARRAY_END(devcfg_db) - 1)->cfg;


void devcfg_init(void)
{
	const struct devcfg_db *db;

	for (db = devcfg_db; db != ARRAY_END(devcfg_db); db++) {
		devcfg = &db->cfg;
		if (id_eq(&db->id))
			break;
	}
}
