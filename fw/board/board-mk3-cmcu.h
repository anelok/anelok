/*
 * fw/board/board-mk3-cmcu.h - GPIOs of cMCU in Anelok Mk 3 board
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_MK3_CMCU_H
#define	BOARD_MK3_CMCU_H

#include "gpio.h"


/* ----- Power distribution ------------------------------------------------ */

#define	VRF_OFF		GPIO_ID(B, 1)
#define	VBAT_SENSE	GPIO_ID(E, 30)

/* ----- LED --------------------------------------------------------------- */

#define	LED		GPIO_ID(D, 6)

/* ----- USB --------------------------------------------------------------- */

#define	USB_ID		GPIO_ID(E, 0)
#define	USB_nSENSE	GPIO_ID(C, 5)

/* ----- sMCU --------------------------------------------------------------- */

#define	MCU_STCR	GPIO_ID(A, 1)
#define	MCU_SRCT	GPIO_ID(A, 2)

#define	MCU_SxCx_TX	MCU_SRCT
#define	MCU_SxCx_RX	MCU_STCR

#define	SMCU_SWD_CLK	MCU_SRCT
#define	SMCU_SWD_DIO	MCU_STCR

/* ----- RF ---------------------------------------------------------------- */

#define	RF_CTRR		GPIO_ID(C, 4)
#define	RF_CRRT		GPIO_ID(C, 3)

#define	RF_SWDIO	GPIO_ID(C, 2)
#define	RF_SWDCLK	GPIO_ID(C, 1)

#define	RF_nRESET	RF_SWDIO

#define	VRF		GPIO_ID(B, 0)

/* ----- USB --------------------------------------------------------------- */

/*
 * @@@ We need to generalize common/flash.c before we can enable alt settings.
 */
// #define	DFU_ALT_SETTINGS 2

#define	oops	panic

#endif /* !BOARD_MK3_CMCU_H */
