/*
 * fw/board/devcfg.h - Configuration specific to individual device
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef DEVCFG_H
#define	DEVCFG_H

#include <stdbool.h>
#include <stdint.h>

#include "id.h"


struct devcfg {
	const char *name;	/* board name (NULL for unknown) */
	uint16_t thresh;	/* touch threshold (0 for default) */
	uint16_t scale;		/* touch scaling (0 for default) */
	int16_t offset;		/* touch offset (0 for default) */
	uint32_t cal_a;		/* fixed calibration data */
	uint32_t cal_b;
	bool stay_on;		/* never stop the boost converter */
	bool tactile;		/* has tactile switches instead of slider */
};


extern const struct devcfg *devcfg;


void devcfg_init(void);


#endif /* !DEVCFG_H */
