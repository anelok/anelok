/*
 * fw/board/board.h - Board-specific definitions for Anelok
 *
 * Written 2014-2015, 2017 by Werner Almesberger
 * Copyright 2014-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_H
#define	BOARD_H

#if defined(ANELOK_MK2) || defined(BOOT_MK2)
#include "board-mk2.h"
#elif defined(ANELOK_MK3_SMCU) || defined(BOOT_MK3_SMCU)
#include "board-mk3-smcu.h"
#elif defined(ANELOK_MK3_CMCU) || defined(BOOT_MK3_CMCU)
#include "board-mk3-cmcu.h"
#elif defined(ANELOK_MK3_NRF) || defined(BOOT_MK3_NRF)
#include "board-mk3-nrf.h"
#elif defined(YBOX) || defined(BOOT_YBOX)
#include "board-ybox.h"
#else
#error Please define board type
#endif


/* ----- USB --------------------------------------------------------------- */

/*
 * Allow board-*.h to override things.
 */

#ifndef	USB_VENDOR
#define	USB_VENDOR	0x20b7	/* Qi Hardware */
#endif

#ifndef USB_PRODUCT
#define	USB_PRODUCT	0xae70	/* AnELok, device 0 */
#endif
				/* - --           - */
#define	DFU_USB_VENDOR	USB_VENDOR
#define	DFU_USB_PRODUCT	USB_PRODUCT

#ifndef	BOARD_MAX_mA
#define	BOARD_MAX_mA	120	/* limited by KL26's USB VREG */
#endif

#ifndef EP0_SIZE
#define	EP0_SIZE	64	/* Full-Speed */
#endif

#ifndef NUM_EPS
#define	NUM_EPS		2
#endif

/*
 * Define DFU interface names. They're only used if DFU_ALT_SETTINGS is
 * defined.
 */

#define	DFU_ALT_NAME_0	'K', 0, 'L', 0, '2', 0, '6', 0
#define	DFU_ALT_NAME_0_IDX 1
#define	DFU_ALT_NAME_0_LEN 8

#define	DFU_ALT_NAME_1	'C', 0, 'C', 0, '2', 0, '5', 0, '4', 0, '3', 0
#define	DFU_ALT_NAME_1_IDX 2
#define	DFU_ALT_NAME_1_LEN 12

#endif /* !BOARD_H */
