/*
 * fw/board/board-mk3-smcu.h - GPIOs of sMCU in Anelok Mk 3 board
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_MK3_SMCU_H
#define	BOARD_MK3_SMCU_H

#include "gpio.h"


/* ----- Power distribution ------------------------------------------------ */

#define	DISP_ON		GPIO_ID(B, 1)
#define	CARD_ON		GPIO_ID(C, 3)

#define	VHIGH		GPIO_ID(C, 7)

/* ----- LED --------------------------------------------------------------- */

#define	LED		GPIO_ID(B, 0)

/* ----- Display ----------------------------------------------------------- */

#define	DISP_SCLK	GPIO_ID(C, 5)
#define	DISP_SDIN	GPIO_ID(C, 6)
#define	DISP_DnC	GPIO_ID(D, 4)
#define	DISP_nRES	GPIO_ID(D, 5)
#define	DISP_nCS	GPIO_ID(D, 6)

/* ----- Tactile switches -------------------------------------------------- */

#define	CAP_A		GPIO_ID(A, 4)
#define	CAP_B		GPIO_ID(C, 1)
#define	CAP_C		GPIO_ID(C, 2)

/* ----- Memory card ------------------------------------------------------- */

#define	CARD_DAT2	GPIO_ID(E, 19)
#define	CARD_DAT3	GPIO_ID(E, 17)
#define	CARD_CMD	GPIO_ID(E, 16)
#define	CARD_CLK	GPIO_ID(E, 0)
#define	CARD_DAT0	GPIO_ID(E, 1)
#define	CARD_DAT1	GPIO_ID(E, 18)

#define	CARD_SW		GPIO_ID(D, 7)

/* ----- cMCU --------------------------------------------------------------- */

#define	MCU_SRCT	GPIO_ID(A, 1)
#define	MCU_STCR	GPIO_ID(A, 2)

#define	MCU_SxCx_TX	MCU_STCR
#define	MCU_SxCx_RX	MCU_SRCT

#define	CMCU_SWD_CLK	MCU_STCR
#define	CMCU_SWD_DIO	MCU_SRCT

#define	CMCU_nRESET	GPIO_ID(C, 4)

#endif /* !BOARD_MK3_SMCU_H */
