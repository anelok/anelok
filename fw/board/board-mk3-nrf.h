/*
 * fw/board/board-mk3-nrf.h - GPIOs of 2014 Anelok board
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_MK3_NRF_H
#define	BOARD_MK3_NRF_H

/* ----- LED --------------------------------------------------------------- */

#define	LED		9

/* ----- cMCU -------------------------------------------------------------- */

#define	RF_CRRT	6
#define	RF_CTRR	7

#endif /* !BOARD_MK3_NRF_H */
