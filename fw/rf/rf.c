/*
 * fw/rf/rf.c - RF transceiver
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "gpio.h"
#include "cc.h"
#include "rf.h"

#include "misc.h" /* @@@ for mdelay */
#include "board.h"


/* ----- SPI --------------------------------------------------------------- */


static void spi_begin(void)
{
	gpio_clr(RF_nSEL);
}


static void spi_end(void)
{
	gpio_set(RF_nSEL);
}


static uint8_t spi_io(uint8_t v)
{
	uint8_t res = 0;
	uint8_t mask;

	for (mask = 0x80; mask; mask >>= 1) {
		if (v & mask)
			gpio_set(RF_MOSI);
		else
			gpio_clr(RF_MOSI);
		if (gpio_read(RF_MISO))
			res |= mask;
		gpio_set(RF_SCLK);
		gpio_clr(RF_SCLK);
	}
mdelay(1); /* @@@ remove after switching CC to DMA */
	return res;
}


/* ----- Handshake --------------------------------------------------------- */


static enum state {
	S_IDLE,
	S_WAIT_NOINT,
} state = S_IDLE;

static const void *tx_buf;
static uint8_t tx_cmd;


static uint8_t rf_io(void *buf, uint8_t size)
{
	const uint8_t *p = tx_buf;
	uint8_t *b = buf;
	uint8_t rx_len, len, i, tmp;
	uint8_t tx_len = tx_cmd & 0x7f;

	rx_len = spi_io(tx_cmd);
	len = rx_len > tx_len ? rx_len : tx_len;
	tx_cmd = 0;

	for (i = 0; i != len; i++) {
		tmp = i < tx_len ? *p++ : 0;
		tmp = spi_io(tmp);
		if (i < size)
			*b++ = tmp;
	}
	return rx_len;
}


uint8_t rf_poll(void *buf, uint8_t size)
{
	switch (state) {
	case S_IDLE:
		if (gpio_read(RF_nINT))
			break;
		gpio_set(RF_nREQ);
		spi_begin();
		state = S_WAIT_NOINT;
		return rf_io(buf, size);
	case S_WAIT_NOINT:
		if (!gpio_read(RF_nINT))
			break;
		spi_end();
		state = S_IDLE;
		break;
	}
	return 0;
}


bool rf_send(const void *buf, uint8_t cmd)
{
	if (tx_cmd)
		return 0;
	gpio_clr(RF_nREQ);
	tx_buf = buf;
	tx_cmd = cmd;
	return 1;
}


void rf_init(void)
{
	bool on;

	gpio_init_out(RF_nRESET, 0);

	on = cc_on();

	gpio_init_out(RF_MOSI, 0);
	gpio_init_in(RF_MISO, 0);
	gpio_init_out(RF_SCLK, 0);
	gpio_init_out(RF_nSEL, on);

	gpio_init_in(RF_nINT, on);
	gpio_init_out(RF_nREQ, on);

	gpio_begin(RF_MOSI);
	gpio_begin(RF_MISO);
	gpio_begin(RF_SCLK);
	gpio_begin(RF_nSEL);
	gpio_begin(RF_nINT);
	gpio_begin(RF_nREQ);

	if (!on)
		return;

	gpio_begin(RF_nRESET);
	gpio_set(RF_nRESET);
	gpio_end(RF_nRESET);
}
