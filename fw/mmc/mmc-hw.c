/*
 * fw/mmc-hw.c - MMC hardware interface
 *
 * Written 2012, 2013, 2015, 2017 by Werner Almesberger
 * Copyright 2012, 2013, 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "misc.h"
#include "board.h"
#include "power.h"
#include "mmc-hw.h"


/*
 * Pin assignment from
 * http://alumni.cs.ucr.edu/~amitra/sdcard/ProdManualSDCardv1.9.pdf
 *
 * Pin	SD mode	SPI mode
 * 1	DAT3	Chip Select (active low)
 * 2	CMD	MOSI
 * 3	VSS	VSS
 * 4	VDD	VDD
 * 5	CLK	Clock
 * 6	VSS	VSS
 * 7	DAT0	MISO
 */


#define	MMC_nCS		CARD_DAT3
#define	MMC_MOSI	CARD_CMD
#define	MMC_CLK		CARD_CLK
#define	MMC_MISO	CARD_DAT0


void mmc_select(void)
{
	gpio_clr(MMC_nCS);
}

void mmc_deselect(void)
{
	gpio_set(MMC_nCS);
}


uint8_t mmc_recv(void)
{
	uint8_t i, v = 0;

	gpio_set(MMC_MOSI);
	for (i = 0; i != 8; i++) {
		gpio_set(MMC_CLK);
		v = (v << 1) | gpio_read(MMC_MISO);
		gpio_clr(MMC_CLK);
	}
	return v;
}


void mmc_send(uint8_t v)
{
	uint8_t i;

	for (i = 0; i != 8; i++) {
		if (v & 0x80)
			gpio_set(MMC_MOSI);
		else
			gpio_clr(MMC_MOSI);
		gpio_set(MMC_CLK);
		gpio_clr(MMC_CLK);
		v <<= 1;
	}
}


void card_init(void)
{
	gpio_init_off(CARD_CMD);
	gpio_init_off(CARD_CLK);
	gpio_init_off(CARD_DAT0);
	gpio_init_off(CARD_DAT1);
	gpio_init_off(CARD_DAT2);
	gpio_init_off(CARD_DAT3);

	gpio_init_in(CARD_SW, 1);
}


bool card_present(void)
{
	bool res;

	gpio_begin(CARD_SW);
	/*
	 * @@@ card switch closes (to ground) in Amphenol 10100600 (Mk 2) and
	 * Molex 047309-3351 (Mk 3)  but opens in Amphenol 1010058159 and
	 * Wuerth 693071010811 (both Mk 2).
	 */
	res = !gpio_read(CARD_SW);
	gpio_end(CARD_SW);
	return res;
}


void mmc_activate(void)
{
	gpio_begin(MMC_nCS);
	gpio_begin(MMC_MOSI);
	gpio_begin(MMC_MISO);
	gpio_begin(MMC_CLK);

	gpio_init_out(MMC_nCS, 1);
	gpio_init_out(MMC_CLK, 1); /* for pre-charging */
	gpio_init_out(MMC_MOSI, 1);
	gpio_init_in(MMC_MISO, 1);

	mdelay(100);	/* allow card to pre-charge, to limit inrush current */

	power_card(1);
	gpio_clr(MMC_CLK);

	mdelay(10);
}


void mmc_deactivate(void)
{
	power_card(0);
	mdelay(10);
	card_init();

	gpio_end(MMC_nCS);
	gpio_end(MMC_MOSI);
	gpio_end(MMC_MISO);
	gpio_end(MMC_CLK);
}
