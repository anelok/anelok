/*
 * fw/db/crypto/tweetnacl.c - TweetNaCl wrapper
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * We use uNaCl's highly optimized Curve25519 implementation as follows:
 * - we make crypto_scalarmult_curve25519*_tweet defined by TweetNaCl "weak",
 *   so that any regular symbol can override it,
 * = we rename the now "weak" crypto_scalarmult_curve25519*_tweet to
 *   crypto_scalarmult_curve25519*, i.e., the name uNaCl uses
 *
 * The reason for doing this with all those pragmas instead of just changing
 * the function names in the code is that we want to preserve the original
 * code, so that any future updates at upstream can be tracked without having
 * to consider Anelok-specific modifications.
 */

#pragma GCC diagnostic ignored "-Wsign-compare"

#pragma weak crypto_scalarmult_curve25519_tweet
#pragma weak crypto_scalarmult_curve25519_base_tweet

#pragma redefine_extname \
    crypto_scalarmult_curve25519_base_tweet crypto_scalarmult_curve25519_base
#pragma redefine_extname \
    crypto_scalarmult_curve25519_tweet crypto_scalarmult_curve25519
    
#include "../../../crypter/tweetnacl.c"
