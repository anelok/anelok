/*
 * fw/crypto/account.c - Account database (WIP)
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "qa.h"
#include "misc.h"
#include "cursor.h"
#include "dbconf.h"
#include "account.h"

#include "tweetnacl.h"


/* ----- Data -------------------------------------------------------------- */

struct key {
	uint32_t pin;
	uint8_t sk[crypto_box_SECRETKEYBYTES];
};


static const struct key keys[] = {
#include "../keys.inc"
};


#ifdef SIMULATOR

static const uint8_t db[] = {
#include "../sim/db.inc"
};

#define	DB	db
#define	DB_END	ARRAY_END(db)

#else /* SIMULATOR */

#define	DB	((const uint8_t *) DB_START)
#define	DB_END	((const uint8_t *) DB_START + DB_SIZE)

#endif /* !SIMULATOR */


uint8_t my_pk[crypto_box_PUBLICKEYBYTES];


static struct cache {
	uint8_t pkw[crypto_box_PUBLICKEYBYTES];
	uint8_t shk[crypto_box_PUBLICKEYBYTES];
} cache;

static uint8_t rk[crypto_box_PUBLICKEYBYTES];

static const struct key *key = NULL;


/* ----- Key selection ----------------------------------------------------- */


bool account_select_key(uint32_t code)
{
	memset(&cache, 0, sizeof(cache));
	key = keys;
	while (key != ARRAY_END(keys)) {
		if (key->pin == code) {
			crypto_scalarmult_base(my_pk, key->sk);
			return 1;
		}
		key++;
	}
	key = NULL;
	return 0;
}


/* ----- Access ------------------------------------------------------------ */

/*
 * @@@ There are several things wrong here:
 * - we should not let decrypted information sit around after use.
 *   This requires an API change.
 * - we should only decrypt as much as we need
 */


static uint16_t record_bytes(const uint8_t *pos)
{
	uint16_t len;

	REQUIRE(pos + 2 <= DB_END);
	len = pos[0] << 8 | pos[1];
	REQUIRE(pos + len + 2 <= DB_END);
	return len;
}


static uint16_t field_bytes(const struct account_list *list, const uint8_t *pos)
{
	uint16_t len;

	REQUIRE(pos + 3 <= list->m_end);
	len = pos[1] << 8 | pos[2];
	REQUIRE(len <= MAX_FIELD_BYTES);
	REQUIRE(pos + 3 + len <= list->m_end);
	return len;
}


#define	WRITER_PK(pos)	((pos) + 2)
#define	NONCE(pos)	(WRITER_PK(pos) + crypto_box_PUBLICKEYBYTES)
#define	N_READERS(pos)	(NONCE(pos) + crypto_box_NONCEBYTES)
#define	READER(pos, n)	(N_READERS(pos) + 1 + (n) * \
			(crypto_box_PUBLICKEYBYTES + crypto_secretbox_KEYBYTES))
#define	READER_PK(pos, n) READER(pos, n)
#define	READER_EK(pos, n) (READER(pos, n) + crypto_box_PUBLICKEYBYTES)
#define	PAYLOAD(pos)	READER(pos, *N_READERS(pos))


static bool may_decrypt(const uint8_t *pos)
{
	uint16_t len = record_bytes(pos);
	uint8_t i;

	REQUIRE(len >= READER(pos, 0) - pos);
	REQUIRE(len >= PAYLOAD(pos) - pos);
	for (i = 0; i != *N_READERS(pos); i++)
		if (!memcmp(my_pk, READER_PK(pos, i),
		    crypto_box_PUBLICKEYBYTES))
			goto found;
	return 0;

found:
	if (memcmp(cache.pkw, WRITER_PK(pos), crypto_box_PUBLICKEYBYTES)) {
		memcpy(cache.pkw, WRITER_PK(pos), crypto_box_PUBLICKEYBYTES);
		REQUIRE(!crypto_box_beforenm(cache.shk, cache.pkw, key->sk));
	}
	crypto_stream_xor(rk, READER_EK(pos, i), crypto_box_PUBLICKEYBYTES,
	    NONCE(pos), cache.shk);
	return 1;
}


static bool decrypt(struct account_list *list)
{
	uint16_t len = record_bytes(list->pos);
	uint16_t dlen;
	uint16_t clen;

	if (!may_decrypt(list->pos))
		return 0;

	dlen = len - (PAYLOAD(list->pos) - list->pos) + 2;
	clen = dlen + crypto_box_BOXZEROBYTES;
	REQUIRE(dlen <= MAX_ACCOUNT_BYTES);
	memset(list->c, 0, crypto_secretbox_BOXZEROBYTES);
	memcpy(list->c + crypto_secretbox_BOXZEROBYTES,
	    PAYLOAD(list->pos), dlen);
	REQUIRE(!crypto_secretbox_open(list->m, list->c, clen,
	    NONCE(list->pos), rk));
	list->m_end =
	    list->m + dlen + crypto_secretbox_ZEROBYTES -
	    crypto_secretbox_BOXZEROBYTES;

	list->wp = list->m;

	list->decrypted = 0;

	return 1;
}


static inline void write_back(struct account_list *list)
{
	*list->wp = list->w;
}


static void zero(struct account_list *list, uint8_t *p)
{
	*list->wp = list->w;
	list->wp = p;
	list->w = *p;
	*p = 0;
}


/* ----- List callbacks --------------------------------------------------- */


static bool account_list_next(void *user)
{
	struct account_list *list = user;
	uint16_t len;

	list->decrypted = 0;
	while (1) {
		len = record_bytes(list->pos);
		if (list->pos + 2 + len == DB_END)
			return 0;
		if (record_bytes(list->pos + 2 + len) == 0)
			return 0;
		list->pos += 2 + len;

		if (may_decrypt(list->pos))
			return 1;
	}
}


static bool account_list_rewind(void *user)
{
	struct account_list *list = user;

	if (list->pos != DB)
		list->decrypted = 0;
	list->pos = DB;
	if (may_decrypt(list->pos))
		return 1;
	return account_list_next(user);
}


static const struct cursor_ops account_list_ops = {
	.rewind	= account_list_rewind,
	.next	= account_list_next,
};


/* ----- List operations -------------------------------------------------- */


bool account_list_open(struct account_list *list)
{
	list->decrypted = 0;
	if (!account_list_rewind(list))
		return 0;
	cursor_init(&list->cursor, &account_list_ops, list);
	return 1;
}


const char *account_name(struct account_list *list)
{
	uint8_t *pos = list->m + crypto_box_ZEROBYTES;
	uint16_t len;

	if (!list->decrypted)
		REQUIRE(decrypt(list));
	len = field_bytes(list, pos);
	zero(list, pos + 3 + len);

	return (const char *) (pos + 3);
}


/* ----- Record callbacks -------------------------------------------------- */


static bool account_record_rewind(void *user)
{
	struct account_record *rec = user;
	struct account_list *list = rec->list;

	rec->pos = list->m + crypto_box_ZEROBYTES;

	return 1;
}


static bool account_record_next(void *user)
{
	struct account_record *rec = user;
	struct account_list *list = rec->list;
	uint16_t len;

	write_back(list);

	len = field_bytes(list, rec->pos);
	if (rec->pos + 3 + len == list->m_end)
		return 0;
	rec->pos += 3 + len;

	return 1;
}


static const struct cursor_ops account_record_ops = {
	.rewind	= account_record_rewind,
	.next	= account_record_next,
};



/* ----- Record operations ------------------------------------------------- */


void account_record_open(struct account_list *list, struct account_record *rec)
{
	if (!list->decrypted)
		REQUIRE(decrypt(list));
	rec->list = list;
	account_record_rewind(rec);
	cursor_init(&rec->cursor, &account_record_ops, rec);
}


enum pw_flags account_flags(struct account_record *rec)
{
	return 0; /* @@@ not implemented */
}


enum field_type account_field_type(struct account_record *rec)
{
	REQUIRE(rec->pos != rec->list->m_end);
	return rec->pos[0];
}


const char *account_text(struct account_record *rec)
{
	struct account_list *list = rec->list;
	uint16_t len;

	len = field_bytes(list, rec->pos);
	zero(list, rec->pos + 3 + len);
	return (const char *) (rec->pos + 3);
}
