/*
 * fw/crypto/account_db.h - Account database (WIP)
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ACCOUNT_DB_H
#define	ACCOUNT_DB_H

#include <stdbool.h>
#include <stdint.h>

#include "tweetnacl.h"

#include "dbconf.h"


struct account_list {
	uint8_t c[crypto_box_BOXZEROBYTES + MAX_ACCOUNT_BYTES];
	uint8_t m[crypto_box_ZEROBYTES + MAX_ACCOUNT_BYTES + 1]; /* for NUL */
	const uint8_t *m_end;

	uint8_t *wp;	/* write-back pointer */
	uint8_t w;	/* write-back value */

	const uint8_t *pos;

	bool decrypted;

	struct cursor cursor;
};

struct account_record {
	struct account_list *list;
	uint8_t *pos;

	struct cursor cursor;
};


extern uint8_t my_pk[crypto_box_PUBLICKEYBYTES];

#endif /* !ACCOUNT_DB_H */
