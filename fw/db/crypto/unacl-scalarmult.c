/*
 * fw/db/crypto/unacl-scalarmult.c - Wrapper for uNaCl's scalarmult.c
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * For some reason scalarmult.c does not include api.h, causing a complaint
 * since we use -Wmissing-prototypes. We work around this by merging api.h
 * and scalarmult.c in this wrapper.
 *
 * This file should actually live in unacl/, but we keep it here, so that the
 * "wildcard" (i.e., covering the whole directory) copyright notice in COPYING
 * is still valid.
 */

#include "unacl/api.h"

#include "unacl/scalarmult.c"
