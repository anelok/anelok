/*
 * fw/db/account.h - Account database (dummy)
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ACCOUNT_H
#define	ACCOUNT_H

#include <stdbool.h>
#include <stdint.h>

#include "cursor.h"


enum field_type {
	field_name,
	field_url,
	field_login,
	field_pw,
	field_end	/* must be last */
};


/*
 * @@@ these flags are bogus - transmission methods would not be a function
 * of the account entry but of the device status.
 */

enum pw_flags {
	pw_hidden	= 1 << 0,
	pw_inaccessible	= 1 << 1,
	pw_type		= 1 << 2,
	pw_radio	= 1 << 3,
};


/*
 * Database-specific definitions
 */

#include "account_db.h"


bool account_select_key(uint32_t code);

bool account_list_open(struct account_list *list);
const char *account_name(struct account_list *list);
//void account_list_close(struct account_list *list);

enum pw_flags account_flags(struct account_record *rec);
enum field_type account_field_type(struct account_record *rec);
const char *account_text(struct account_record *rec);

void account_record_open(struct account_list *list, struct account_record *rec);
//void account_record_close(struct account_record *rec);

#endif /* !ACCOUNT_H */
