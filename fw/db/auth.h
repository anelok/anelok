/*
 * fw/db/auth.h - User authentication (dummy)
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef AUTH_H
#define	AUTH_H

#include <stdbool.h>
#include <stdint.h>


void auth_delta(uint16_t ms);
bool auth_valid(void);
void auth_zap(void);
bool auth_try(uint32_t code);

#endif /* !AUTH_H */
