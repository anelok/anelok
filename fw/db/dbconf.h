/*
 * fw/ui/dbconf.h - Account database configuration parameters
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef DBCONF_H
#define	DBCONF_H

/* Maximum binary account entry size */

#define	MAX_ACCOUNT_BYTES	256

/* Maximum field size, not counting type and length */

#define	MAX_FIELD_BYTES		255

/* Directory name limit */

#define	MAX_DIR_NAME	16

/* Directory separator */

#define	SEP_CHAR	'/'

/* Arrays and types in fw/ui/dir.h and dir.c */

#define	MAX_DIRS	254	/* MUST be < 255 */
#define	MAX_ACCOUNTS	1024	/* MUST be < 65536 */

/* Flash location of database */

#define	DB_START	(128 * 1024 - DB_SIZE)	/* 0x1c000 */
#define	DB_SIZE		(16 * 1024)

#endif /* !DBCONF_H */
