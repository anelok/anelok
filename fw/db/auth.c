/*
 * fw/db/auth.h - User authentication (dummy)
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "pm.h"
#include "account.h"
#include "auth.h"


#define	AUTH_TIMEOUT_MS	(5 * 60 * 1000)


static uint32_t ms_left = 0;


void auth_delta(uint16_t ms)
{
	if (ms_left && pm_state() == pm_active)
		ms_left = AUTH_TIMEOUT_MS;
	ms_left = ms_left >= ms ? ms_left - ms : 0;
}


bool auth_valid(void)
{
	return ms_left;
}


void auth_zap(void)
{
	ms_left = 0;
}


bool auth_try(uint32_t code)
{
	ms_left = account_select_key(code) ? AUTH_TIMEOUT_MS : 0;
	return ms_left;
}
