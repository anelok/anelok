/*
 * fw/backtrace-glibc.c - Stack trace for glibc
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stddef.h>
#include <execinfo.h>

#include "backtrace.h"


void backtrace_start(struct backtrace *ctx)
{
	ctx->n = backtrace(ctx->buf, MAX_BACKTRACE);
	ctx->pos = 0;
}


const void *backtrace_next(struct backtrace *ctx)
{
	if (ctx->pos == ctx->n)
		return NULL;
	return ctx->buf[ctx->pos++];
}
