/*
 * fw/backtrace-arm.c - Stack trace for ARM EABI
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * newlib does not have glibc's backtrace() function, so we need to improvise.
 *
 * Stack frames on ARM seem to be quite disorderly. An attempt to start at the
 * address obtained with __builtin_frame_address(0) and then following
 * variations of
 * http://class.ece.iastate.edu/arun/Cpre381_Sp06/lab/labw12a/eabi.htm
 * and
 * http://gperftools.googlecode.com/svn/trunk/src/stacktrace_arm-inl.h
 * did not yield reliable results beyond the very first stack frame.
 *
 * Using -mapcs-frame (*) and -fno-omit-frame-pointer did not significantly
 * improve the situatio.
 *
 * (*) deprecated, apparently without replacement:
 *     http://lists.infradead.org/pipermail/linux-arm-kernel/2015-April/336604.html
 *
 * So we keep things simple and just return all addresses on the stack that
 * _may_ be code. This also avoids the size and run-time overhead caused by
 * keeping track of frame pointers.
 */

#include <stddef.h>
#include <stdint.h>

#include "backtrace.h"


extern char __stack;
extern char __text_start;
extern char __text_end;


void backtrace_start(struct backtrace *ctx)
{
	ctx->sp = __builtin_frame_address(0);
}


const void *backtrace_next(struct backtrace *ctx)
{
	const void *v;

	while ((char *) ctx->sp < &__stack) {
		v = *ctx->sp++;

		/*
		 * Test:
		 * - lower bound,
		 * - upper bound,
		 * - is a valid return address (word-aligned, with bit 0 set
		 *   for Thumb)
		 */
		if ((void *) &__text_start <= v &&
		    v < (void *) &__text_end &&
		    ((uint32_t) v & 3) == 1)
			return v;
	}
	return NULL;
}
