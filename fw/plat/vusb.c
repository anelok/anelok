/*
 * fw/vusb.c - USB bus power handling
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "misc.h"
#include "regs.h"
#include "board.h"
#include "isr.h"
#include "irq.h"
#include "event.h"
#include "gpio.h"
#include "extint.h"
#include "vusb.h"


static struct extint vusb_extint;


static void vusb_isr(void)
{
	SIM_SOPT1CFG = SIM_SOPT1CFG_URWE_MASK;
	if (gpio_read(USB_nSENSE)) {
		SIM_SOPT1 = 0;
		event_clear_interrupt(ev_usb_plug);
		event_set_interrupt(ev_usb_unplug);
	} else {
		SIM_SOPT1 = SIM_SOPT1_USBREGEN_MASK;
		event_clear_interrupt(ev_usb_unplug);
		event_set_interrupt(ev_usb_plug);
	}
}


/*
 * We don't need gpio_begin / gpio_end here because the active exernal
 * interrupt keeps the port running already.
 */

bool vusb_sense(void)
{
	return !gpio_read(USB_nSENSE);
}


void vusb_init(void)
{
	unsigned long flags;

	gpio_init_in(USB_nSENSE, 1);
	extint_add(&vusb_extint, USB_nSENSE, vusb_isr, int_both);
	extint_enable(&vusb_extint);

	flags = irq_save();
	if (!vusb_sense()) {
		/* disable USB voltage regulator */

		SIM_SOPT1CFG = SIM_SOPT1CFG_URWE_MASK;
		SIM_SOPT1 = 0;
	}
	irq_restore(flags);
}
