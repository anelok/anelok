/*
 * fw/led.h - LED control
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef LED_H
#define	LED_H

#include <stdbool.h>


void led(bool on);
void led_toggle(void);
void led_init(void);

#endif /* !LED_H */
