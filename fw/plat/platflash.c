/*
 * fw/platflash.c - Board-specific flash functions (KL2x side)
 *
 * Written 2011, 2013-2016, 2017 by Werner Almesberger
 * Copyright 2011, 2013-2016, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "dfu.h"
#include "regs.h"
#include "flash.h"


#define	SECTOR_SIZE	1024

/* @@@ merge flash commands with similar list in ben.c */

enum {
	PGM4	= 0x06,
	ERSSCR	= 0x09,
};


static void *app_base = (void *) APP_BASE;
static uint32_t payload;


static void flash_start(void)
{
	payload = 0;
}


/*
 * @@@ Instead of adding general-purpose functions like flash_start_at to
 * platflash.c, flash operations should be generalized and platflash.c (for
 * DFU) should then use this general interface.
 */

void flash_start_at(uint32_t start)
{
	payload = start - APP_BASE;
}


static bool flash_can_write(uint16_t size)
{
	if (size & 3)
		return 0;
	return payload + size <= APP_SIZE;
}


static void flash_wait(void)
{
	while (!(FTFA_FSTAT & FTFA_FSTAT_CCIF_MASK));
}


static void flash_run(void)
{
	FTFA_FSTAT = FTFA_FSTAT_CCIF_MASK;
	flash_wait();
}


#define	FW(x)	(*((volatile uint32_t *) &(x)))

#define	FTFA_FCCOW0	FW(FTFA_FCCOB3)
#define	FTFA_FCCOW1	FW(FTFA_FCCOB7)


static void flash_write(const uint8_t *buf, uint16_t size)
{
	/* @@@ weird */
	if (!size)
		size = EP0_SIZE;

	while (size) {
		if (!(payload & (SECTOR_SIZE - 1))) {
			flash_wait();
			FTFA_FCCOW0 = (APP_BASE + payload) | ERSSCR << 24;
			flash_run();
		}
		flash_wait();
		FTFA_FCCOW0 = (APP_BASE + payload) | PGM4 << 24;
		FTFA_FCCOW1 =
		    buf[0] | buf[1] << 8 | buf[2] << 16 | buf[3] << 24;
		flash_run();
		buf += 4;
		payload += 4;
		size -= 4;
	}
}


static void flash_end_write(void)
{
	flash_wait();
}


static uint16_t flash_read(uint8_t *buf, uint16_t size)
{
	if (payload + size > APP_SIZE)
		size = APP_SIZE - payload;
	memcpy(buf, app_base + payload, size);
	payload += size;
	return size;
}


const struct dfu_flash_ops plat_flash_ops = {
	.start		= flash_start,
	.can_write	= flash_can_write,
	.write		= flash_write,
	.end_write	= flash_end_write,
	.read		= flash_read,
};
