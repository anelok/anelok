/*
 * fw/led.c - LED control
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "board.h"
#include "gpio.h"
#include "led.h"


static bool is_on = 0;


void led(bool on)
{
	gpio_begin(LED);
	if (on)
		gpio_set(LED);
	else
		gpio_clr(LED);
	gpio_end(LED);
	is_on = on;
}


void led_toggle(void)
{
	led(!is_on);
}


void led_init(void)
{
	gpio_init_out(LED, 0);
#if LED == GPIO_ID(B, 0)
	gpio_begin(LED);
	PORTB_PCR0 |= PORT_PCR_DSE_MASK; /* enable high drive strength */
	gpio_end(LED);
#endif
	is_on = 0;
}
