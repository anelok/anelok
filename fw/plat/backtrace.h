/*
 * fw/backtrace.h - Stack trace
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef BACKTRACE_H
#define	BACKTRACE_H

#ifdef SIMULATOR
#include "backtrace-glibc.h"
#else
#include "backtrace-arm.h"
#endif


void backtrace_start(struct backtrace *ctx);
const void *backtrace_next(struct backtrace *ctx);

#endif /* !BACKTRACE_H */
