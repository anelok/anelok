/*
 * fw/backtrace-arm.h - Stack trace for ARM EABI
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef BACKTRACE_ARM_H
#define	BACKTRACE_ARM_H

#include <stdint.h>


struct backtrace {
	const void **sp;
};

#endif /* !BACKTRACE_ARM_H */
