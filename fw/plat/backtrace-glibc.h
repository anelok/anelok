/*
 * fw/backtrace-glibc.h - Stack trace for glibc
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef BACKTRACE_GLIBC_H
#define	BACKTRACE_GLIBC_H

#define	MAX_BACKTRACE	10


struct backtrace {
	void *buf[MAX_BACKTRACE];
	int n, pos;
};

#endif /* !BACKTRACE_GLIBC_H */
