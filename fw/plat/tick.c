/*
 * fw/tick.c - System ticks for inexact timekeeping
 *
 * Written 2013-2015, 2017 by Werner Almesberger
 * Copyright 2013-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdint.h>

#include "regs.h"
#include "isr.h"
#include "sleep.h"
#include "tick.h"


#define	CSR_VALUE(enable) \
	( LPTMR_CSR_TCF_MASK		/* clear overflow interrupt */ \
	| LPTMR_CSR_TIE_MASK		/* enable timer interrupt */ \
	| LPTMR_CSR_TFC_MASK		/* reset on overflow */ \
	| ((enable) ? LPTMR_CSR_TEN_MASK : 0)) /* enable timer */


/* ----- Tick interrupt ---------------------------------------------------- */


/*
 * This interrupt handler does nothing useful. We'll need it to be able to
 * wait for interrupts also without using the LLWU.
 */

void tick_isr(void)
{
	LPTMR0_CSR = CSR_VALUE(1); /* reset overflow */
	NVIC_ClearPendingIRQ(LPTMR0_IRQn);
}


/* ----- Tick counter ------------------------------------------------------ */


uint16_t tick_now(void)
{
	LPTMR0_CNR = 0;	/* synchronize */
	return LPTMR0_CNR;
}


uint16_t tick_delta(uint16_t *t)
{
	uint16_t now = tick_now();
	uint16_t dt = now - *t;

	*t = now;
	return dt;
}


/* ----- Sleep ------------------------------------------------------------- */


void msleep(uint32_t ms)
{
	uint16_t t0, t1;

	if (!ms)
		return;

	t0 = tick_now();
	while (1) {
		LPTMR0_CSR = CSR_VALUE(1); /* reset overflow */
		LPTMR0_CMR = t0 + ms;
		t1 = tick_now();
		if (t0 == t1)
			break;
		t0 = t1;
	}

	enter_stop(1 << 0);	/* WUME0 == LPTM */
}


/* ----- Initialization ---------------------------------------------------- */


void tick_init(void)
{
	/* Enable access to LPTMR registers */
	SIM_SCGC5 |= SIM_SCGC5_LPTMR_MASK;

	LPTMR0_CSR = 0;
	LPTMR0_PSR =
	    LPTMR_PSR_PBYP_MASK			/* bypass prescaler */
	    | LPTMR_PSR_PCS(1);			/* use 1 kHz LPO */

	LPTMR0_CSR = CSR_VALUE(0);
	/* "While writing 1 to this field, CSR[5:1] must not be altered." */
	LPTMR0_CSR = CSR_VALUE(1);

	NVIC_ClearPendingIRQ(LPTMR0_IRQn);
	NVIC_EnableIRQ(LPTMR0_IRQn);
}
