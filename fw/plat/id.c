/*
 * fw/plat/id.c - Chip ID
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "regs.h"
#include "id.h"


bool id_eq(const struct id *id)
{
	return id->uidmh == SIM_UIDMH && id->uidml == SIM_UIDML &&
	    id->uidl == SIM_UIDL;
}


void id_get(struct id *id)
{
	id->uidmh = SIM_UIDMH;
	id->uidml = SIM_UIDML;
	id->uidl = SIM_UIDL;
}
