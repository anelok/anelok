/*
 * fw/plat/id.h - Chip ID
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef ID_H
#define	ID_H

#include <stdbool.h>
#include <stdint.h>


struct id {
	uint16_t uidmh;
	uint32_t uidml;
	uint32_t uidl;
};



bool id_eq(const struct id *id);
void id_get(struct id *id);

#endif /* !ID_H */
