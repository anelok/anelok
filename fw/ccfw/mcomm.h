/*
 * fw/ccfw/mcomm.h - CC254x to KL2x communication
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_MCOMM_H
#define	CC_MCOMM_H

#include <stdbool.h>
#include <stdint.h>


bool comm_idle(void);
uint8_t comm_poll(void *buf, uint8_t size);
bool comm_send(const void *buf, uint8_t len);
void comm_init(void);

#endif /* !CC_MCOMM_H */
