/*
 * fw/ccfw/board-2014.h - GPIOs of CC25xx in Anelok-2014 board
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_BOARD_2014_H
#define	CC_BOARD_2014_H

#include "gpio.h"


#define	RF_MISO		GPIO_ID(1, 3)
#define	RF_MOSI		GPIO_ID(1, 4)
#define	RF_SCLK		GPIO_ID(0, 3)
#define	RF_nSEL		GPIO_ID(2, 0)

#define	RF_nINT		GPIO_ID(2, 1)	/* RF_DD */
#define	RF_nREQ		GPIO_ID(2, 2)	/* RF_DC */


#define	UART0_PERCFG	2	/* alt 3 */

#endif /* !CC_BOARD_2014_H */
