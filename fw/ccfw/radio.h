/*
 * fw/cc/radio.h - CC25xx radio control
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_RADIO_H
#define	CC_RADIO_H

#include <stdbool.h>
#include <stdint.h>


#define	RADIO_F_MIN_MHz	2379
#define	RADIO_F_MAX_MHz	2495

#define	RF_ERROR	0xff
#define	RF_MAX_LEN	254


bool radio_has_rx(void);
bool radio_is_idle(void);

void radio_send(const void *buf, uint8_t len);
uint8_t radio_recv(void *buf, uint8_t size);
void radio_listen(void);

void radio_cw(void);
void radio_setup(unsigned f);
void radio_off(void);

#endif /* !CC_RADIO_H */
