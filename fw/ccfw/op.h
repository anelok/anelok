/*
 * fw/ccfw/op.h - CC254x operations
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CCFW_OP_H
#define	CCFW_OP_H

/*
 * The protocol version is just a counter. We expect all the firmware to be
 * updated at the same time, i.e., radio and MCU should run exactly matching
 * versions.
 */

#define	RADIO_PROTOCOL_VERSION_LOW	0
#define	RADIO_PROTOCOL_VERSION_HIGH	0


/*
 * Message format is:
 *
 * 0x80 | len, code, p1, p2, ...
 *
 * Response:
 *
 * len, r1, r2, ...
 *
 * The pi and ri lists can be empty.
 */

enum {
	OP_NULL,	/* reserved */
	OP_VERSION,	/* protocol version (r1 | r2 << 8) */
	OP_ID,		/* build information */
	OP_POKE,	/* xram[p1 | p2 << 8] = p3 */
	OP_PEEK,	/* r1 = xram[p1 | p2 << 8] */
	OP_LISTEN,	/* listen on frequency to (p1 | p2 << 8) MHz */
	OP_CW,		/* emit constant wave on (p1 | p2 << 8) MHz */
	OP_OFF,		/* turn the radio off */
	OP_TX,		/* send a packet */
	OP_RX,		/* receive a packet (len = 0 if none) */
};

#endif /* !CCFW_OP_H */
