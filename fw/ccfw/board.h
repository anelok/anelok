/*
 * fw/ccfw/board.h - GPIOs of CC25xx
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_BOARD_H
#define	CC_BOARD_H

#if defined(ANELOK_2014)
#include "board-2014.h"
#else
#error Please define board type
#endif

#endif /* !CC_BOARD_H */
