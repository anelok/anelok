/*
 * fw/ccfw/version.c - Build version
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "version.h"


const char __xdata build_date[] = BUILD_DATE;
const uint8_t __xdata build_date_length = sizeof(build_date) - 1;

