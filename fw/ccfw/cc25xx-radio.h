/*
 * fw/ccfw/cc25xx-radio.h - CC2543 radio register definitions
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_CC25XX_RADIO_H
#define	CC_CC25XX_RADIO_H

/* ----- Radio SFRs -------------------------------------------------------- */

enum {
	RFIRQF0_REG	= 0xe9,		/* RF interrupt flags */
		RFIRQF0_TXTHSHDN	= 1 << 0, /* TX thresh crossed down */
		RFIRQF0_RXTHSHDN	= 1 << 1, /* RX thresh crossed down */
		RFIRQF0_TXTHSHUP	= 1 << 2, /* TX thresh crossed up */
		RFIRQF0_RXTHSHUP	= 1 << 3, /* RX thresh crossed up */
	RFIRQF1_REG	= 0x91,		/* RF interrupt flags */
		RFIRQF1_RXOK		= 1 << 0, /* Received correctly */
		RFIRQF1_TXFLUSHED	= 1 << 1, /* TX ACK buffer flushed */
		RFIRQF1_RXNOK		= 1 << 2, /* RX with bad CRC */
		RFIRQF1_RXIGNORED	= 1 << 3, /* RX with bad sequence */
		RFIRQF1_RXEMPTY		= 1 << 4, /* RX empty packet */
		RFIRQF1_TXDONE		= 1 << 5, /* TX FIFO packet complete */
		RFIRQF1_TASKDONE	= 1 << 6, /* Task ended */
		RFIRQF1_PINGRSP		= 1 << 7, /* CMD_PING response */
	RFERRF_REG	= 0xbf,		/* RF error interrupt flags */
		RFERRF_TXUNDERF		= 1 << 0, /* TX FIFO underflow */
		RFERRF_RXUNDERF		= 1 << 1, /* RX FIFO underflow */
		RFERRF_TXOVERF		= 1 << 2, /* TX FIFO overflow */
		RFERRF_RXOVERF		= 1 << 3, /* RX FIFO overflow */
		RFERRF_RXTXABO		= 1 << 4, /* RX/TX aborted */
		RFERRF_LLEERR		= 1 << 5, /* LLE cmd/param error */
		RFERRF_RXFIFOFULL	= 1 << 6, /* No room for RX data */
	RFD_REG		= 0xd9,		/* RF data */
	RFST_REG	= 0xe1,		/* LLE and FIFO commands */
};

/* ----- Commands (RFST) --------------------------------------------------- */

enum {
	/* LLE */
	CMD_SHUTDOWN		= 0x01,	/* Stop operation immediately */
	CMD_DEMOD_TEST		= 0x02,	/* Start demod without sync search */
	CMD_RX_TEST		= 0x03,	/* Start demodulator and sync search */
	CMD_TX_TEST		= 0x04,	/* Start transmitter and send zeros */
	CMD_TX_FIFO_TEST	= 0x05,	/* Start transmitter, from TX FIFO */
	CMD_PING		= 0x06,	/* Respond with a PINGRSP interrupt */
	CMD_RX			= 0x08,	/* Start receive operation */
	CMD_TX			= 0x09,	/* Start transmit operation */
	CMD_TX_ON_CC		= 0x0a,	/* Start transmit on clear channel */
	CMD_STOP		= 0x0b,	/* Gracefully stop radio task */
	CMD_SEND_EVENT1		= 0x21,	/* Do the same action as on T2 ev 1 */
	CMD_SEND_EVENT2		= 0x22,	/* Do the same action as on T2 ev 2 */
	CMD_FLUSH_ACK0		= 0x30, /* Flush the ACK buffers for addr 0 */
	CMD_FLUSH_ACK1		= 0x31,	/* Flush the ACK buffers for addr 1 */
	CMD_FLUSH_ACK2		= 0x32,	/* Flush the ACK buffers for addr 2 */
	CMD_FLUSH_ACK3		= 0x33,	/* Flush the ACK buffers for addr 3 */
	CMD_FLUSH_ACK4		= 0x34,	/* Flush the ACK buffers for addr 4 */
	CMD_FLUSH_ACK5		= 0x35,	/* Flush the ACK buffers for addr 5 */
	CMD_FLUSH_ACK6		= 0x36,	/* Flush the ACK buffers for addr 6 */
	CMD_FLUSH_ACK7		= 0x37,	/* Flush the ACK buffers for addr 7 */

	/* FIFO */
	CMD_RXFIFO_RESET	= 0x81,	/* Reset (empty) RX FIFO */
	CMD_RXFIFO_DEALLOC	= 0x82,	/* Deallocate RX FIFO */
	CMD_RXFIFO_RETRY	= 0x83,	/* Retry RX FIFO */
	CMD_RXFIFO_DISCARD	= 0x84,	/* Discard RX FIFO */
	CMD_RXFIFO_COMMIT	= 0x85,	/* Commit RX FIFO */
	CMD_TXFIFO_RESET	= 0x91,	/* Reset (empty) TX FIFO */
	CMD_TXFIFO_DEALLOC	= 0x92,	/* Deallocate TX FIFO */
	CMD_TXFIFO_RETRY	= 0x93,	/* Retry TX FIFO */
	CMD_TXFIFO_DISCARD	= 0x94,	/* Discard TX FIFO */
	CMD_TXFIFO_COMMIT	= 0x95,	/* Commit TX FIFO */
	CMD_FIFO_RESET		= 0xf1,	/* Reset (empty) both FIFOs */
	CMD_FIFO_DEALLOC	= 0xf2,	/* Deallocate both FIFOs */
	CMD_FIFO_RETRY		= 0xf3,	/* Retry both FIFOs */
	CMD_FIFO_DISCARD	= 0xf4,	/* Discard both FIFOs */
	CMD_FIFO_COMMIT		= 0xf5,	/* Commit both FIFOs */
};

/* ----- End-of-task causes (PRF_ENDCAUSE) --------------------------------- */

enum {
	TASK_ENDOK	= 0,	/* Task ended normally */
	TASK_RXTIMEOUT	= 1,	/* T2 ev 2 or CMD_STOP while waiting for sync */
	TASK_NOSYNC	= 2,	/* No sync in the specified time */
	TASK_NOCC	= 3,	/* TX_ON_CC ended because channel not clear */
	TASK_MAXRT	= 4,	/* Maximum number of retransmissions reached */
	TASK_STOP	= 5,	/* T2 ev 2 or CMD_STOP while TX or RX */
	TASK_ABORT	= 6,	/* Task aborted by command */
	TASKERR_MODUNF	= 249,	/* Modulator underflow */
	TASKERR_RXFIF	= 250,	/* RX FIFO overflow */
	TASKERR_TXFIFO	= 251,	/* TX FIFO underflow */
	TASKERR_PAR	= 252,	/* Illegal parameter */
	TASKERR_SEM	= 253,	/* Unable to obtain semaphore */
	TASKERR_CMD	= 254,	/* Unknown command */
	TASKERR_INTERNAL = 255,	/* Internal program error */

};

/* ----- XREG registers ---------------------------------------------------- */

enum {
	FRMCTRL0_REG	= 0x6180,	/* Framing control */
		FRMCTRL0_ENDIANNESS	= 1 << 0, /* MSb first */
		FRMCTRL0_SW_CRC_MODE	= 1 << 1, /* Include sync in CRC */
		FRMCTRL0_FOC_MAGN_CONT_SHIFT = 5, /* Amplitude weight */
		FRMCTRL0_FOC_MAGN_CONT_MASK = 7 << FRMCTRL0_FOC_MAGN_CONT_SHIFT,
	RFIRQM0_REG	= 0x6181,	/* RF interrupt mask  (1: enable) */
		/* use values from RFIRQF0 */
	RFIRQM1_REG	= 0x6182,	/* RF interrupt mask  (1: enable) */
		/* use values from RFIRQF1 */
	RFERRM_REG	= 0x6183,	/* RF error mask  (1: enable) */
		/* use values from RFERRF */
	FREQCTRL_REG	= 0x6184,	/* Synth Frequency Control */
		FREQCTRL_FREQ_SHIFT	= 0, /* 2379 + n MHz */
		FREQCTRL_FREQ_MASK	= 0x7f << FREQCTRL_FREQ_SHIFT,
	FREQTUNE_REG	= 0x6185,	/* XOSC Frequency Tuning */
		FREQTUNE_XOSC32M_TUNE_SHIFT = 0, /* 0: slow, 0xf: fast */
		FREQTUNE_XOSC32M_TUNE_MASK = 0xf << FREQTUNE_XOSC32M_TUNE_SHIFT,
	TXPOWER_REG	= 0x6186,	/* Output Power Control */
		TXPOWER_PA_BIAS_SHIFT	= 0,  /* PA bias control */
		TXPOWER_PA_BIAS_MASK	= 0xf << TXPOWER_PA_BIAS_SHIFT,
		TXPOWER_PA_POWER_SHIFT	= 4, /* 0: max-15 dB, 0xF: max */
		TXPOWER_PA_POWER_MASK	= 0xf << TXPOWER_PA_BIAS_SHIFT,
	TXCTRL_REG	= 0x6187,	/* TX Settings */
		TXCTRL_TXMIX_CURRENT_SHIFT = 0, /* Transmit mixer current */
		TXCTRL_TXMIX_CURRENT_MASK = 3 << TXCTRL_TXMIX_CURRENT_SHIFT,
		TXCTRL_DAC_DC_SHIFT	= 2, /* Adjust TX mixer DC level */
		TXCTRL_DAC_DC_MASK	= 3 << TXCTRL_DAC_DC_SHIFT,
		TXCTRL_DAC_CURR_SHIFT	= 4, /* Change DAC step current */
		TXCTRL_DAC_CURR_MASK	= 3 << TXCTRL_DAC_CURR_SHIFT,
	LLESTAT_REG	= 0x6188,	/* LLE Status */
		LLESTAT_VCO_ON		= 1 << 0, /* VCO is powered on */
		LLESTAT_SYNC_SEARCH	= 1 << 1, /* Modem searching/rx'ing */
		LLESTAT_LLE_IDLE	= 1 << 2, /* LLE is idle */
		LLESTAT_WAIT_T2E1	= 1 << 3, /* Waiting for T2 ev 1 */
		LLESTAT_AGC_LOWGAIN	= 1 << 4, /* AGC reduced gain */
	/* no 0x6189 */
	SEMAPHORE0_REG	= 0x618a,
		SEMAPHORE0_SEMAPHORE	= 1 << 0, /* 0: locked */
	SEMAPHORE1_REG	= 0x618b,
		SEMAPHORE1_SEMAPHORE	= 1 << 0, /* 0: locked */
	SEMAPHORE2_REG	= 0x618c,
		SEMAPHORE2_SEMAPHORE	= 1 << 0, /* 0: locked */
	RFSTAT_REG	= 0x618d,
		RFSTAT_RX_ACTIVE	= 1 << 0, /* LLE in an RX state */
		RFSTAT_TX_ACTIVE	= 1 << 1, /* LLE in a TX state */
		RFSTAT_LOCK_STATUS	= 1 << 2, /* PLL locked */
		RFSTAT_CAL_RUNNING	= 1 << 3, /* Sync calibrating */
		RFSTAT_SFD		= 1 << 4, /* Sync word sent/received */
		RFSTAT_DEM_STATUS_SHIFT	= 5,	/* Demodulator status */
			RFSTAT_DEM_STATUS_IDLE	= 0,
			RFSTAT_DEM_STATUS_ACTIVE = 1,
			RFSTAT_DEM_STATUS_FINISH = 2, /* Finishing */
			RFSTAT_DEM_STATUS_ERROR	 = 3,
		RFSTAT_DEM_STATUS_MASK	= 3 << RFSTAT_DEM_STATUS_SHIFT,
		RFSTAT_MOD_UNDERFLOW	= 1 << 7, /* Modem underflowed */
	RSSI_REG	= 0x618e,	/* RSSI, signed, dB, 0x80 = invalid */
	RFPSRND_REG	= 0x618f,	/* PRNG read */
	MDMCTRL0_REG	= 0x6190,	/* Modem Configuration */
		MDMCTRL0_PHASE_INVERT	= 1 << 0, /* invert phase */
		MDMCTRL0_MODULATION_SHIFT = 1, /* Modulation scheme */
		MDMCTRL0_MODULATION_MASK = 0xf << MDMCTRL0_MODULATION_SHIFT,
			MDMCTRL0_MODULATION_GFSK_250kHz_1Mbps	= 2,
			MDMCTRL0_MODULATION_GFSK_500kHz_2Mbps	= 3,
			MDMCTRL0_MODULATION_GFSK_160kHz_250kbps	= 4,
			MDMCTRL0_MODULATION_GFSK_160kHz_1Mbps	= 6,
			MDMCTRL0_MODULATION_GFSK_320kHz_2Mbps	= 7,
			MDMCTRL0_MODULATION_MSK_250kbps		= 8,
			MDMCTRL0_MODULATION_MSK_500kbps		= 9,
		MDMCTRL0_TX_IF		= 1 << 5, /* tone set by RFR_TX_TONE */
		MDMCTRL0_FOC_DECAY_SHIFT = 6, /* FOC decay ratio */
			MDMCTRL0_FOC_DECAY_8	= 0,
			MDMCTRL0_FOC_DECAY_16	= 1,
			MDMCTRL0_FOC_DECAY_32	= 2,
			MDMCTRL0_FOC_DECAY_65	= 3,
		MDMCTRL0_FOC_DECAY_MASK	= 3 << MDMCTRL0_FOC_DECAY_SHIFT,
	MDMCTRL1_REG	= 0x6191,	/* Modem Configuration */
		MDMCTRL1_CORR_THR_SHIFT	= 0, /* Demod correlator threshold */
		MDMCTRL1_CORR_THR_MASK	= 0xf << MDMCTRL1_CORR_THR_SHIFT,
		MDMCTRL1_FOC_MODE_SHIFT	= 6, /* Frequency offset avg filter */
		MDMCTRL1_FOC_MODE_MASK	= 3 << MDMCTRL1_FOC_MODE_SHIFT,
			MDMCTRL1_FOC_MODE_NOCOMP = 0, /* No offs compensation */
			MDMCTRL1_FOC_MODE_FREEZE = 1,/* Freeze est after sync */
			MDMCTRL1_FOC_MODE_CONT	= 2, /* Continuously estimate */
			MDMCTRL1_FOC_MODE_DECAY	= 3, /* Freeze, double decay */
	MDMCTRL2_REG	= 0x6192,	/* Modem Configuration */
		MDMCTRL2_NUM_PREAM_BYTES_SHIFT = 0, /* n+1 preamble bytes */
		MDMCTRL2_NUM_PREAM_BYTES_MASK =
					0xf << MDMCTRL2_NUM_PREAM_BYTES_SHIFT,
		MDMCTRL2_PREAM_SEL_SHIFT = 4, /* Preamble pattern */
		MDMCTRL2_PREAM_SEL_MASK	= 3 << MDMCTRL2_PREAM_SEL_SHIFT,
			MDMCTRL2_PREAM_SEL_INV	= 0, /* invert sync */
			MDMCTRL2_PREAM_SEL_SAME	= 1, /* same as sync */
			MDMCTRL2_PREAM_SEL_01	= 2, /* 01... */
			MDMCTRL2_PREAM_SEL_10	= 3, /* 10... */
		MDMCTRL2_DEM_PREAM_MODE	= 1 << 6, /* polarity for offset est */
		MDMCTRL2_SW_BIT_ORDER	= 1 << 7, /* 0: Sync LSb first */
	MDMCTRL3_REG	= 0x6193,	/* Modem Configuration */
		MDMCTRL3_RSSI_MODE_SHIFT = 0, /* RSSI mode */
		MDMCTRL3_RSSI_MODE_MASK	= 3 << MDMCTRL3_RSSI_MODE_SHIFT,
			MDMCTRL3_RSSI_MODE_CONT	  = 0, /* continuous */
			MDMCTRL3_RSSI_MODE_FREEZE = 1, /* freeze at sync */
			MDMCTRL3_RSSI_MODE_PEAK	  = 2, /* peak */
			MDMCTRL3_RSSI_MODE_MIXED  = 3, /* cont, peak on sync */
		MDMCTRL3_RFC_SNIFF_CTRL_SHIFT = 3, /* Sniffer control */
		MDMCTRL3_RFC_SNIFF_CTRL_MASK =
					3 << MDMCTRL3_RFC_SNIFF_CTRL_SHIFT,
				MDMCTRL3_RFC_SNIFF_CTRL_OFF	= 0,
				MDMCTRL3_RFC_SNIFF_CTRL_BSP	= 1,
				MDMCTRL3_RFC_SNIFF_CTRL_DEMOD	= 2,
				MDMCTRL3_RFC_SNIFF_CTRL_MOD	= 3,
		MDMCTRL3_RAMP_AMP	= 1 << 5, /* ramp DAC amplitude */
		MDMCTRL3_SYNC_MODE_SHIFT = 6, /* Sync criteria */
		MDMCTRL3_SYNC_MODE_MODE	= 3 << MDMCTRL3_SYNC_MODE_SHIFT,
			MDMCTRL3_SYNC_MODE_CORR	= 0, /* correl > thresh */
			MDMCTRL3_SYNC_MODE_DECIS = 1, /* idem + data decision */
			MDMCTRL3_SYNC_MODE_1ERR	= 2, /* idem, accept 1 error */
	SW_CONF_REG	= 0x6194,	/* Sync Word Configuration */
		SW_CONF_SW_LEN_SHIFT	= 0, /* sync len, 0 = 32 */
		SW_CONF_SW_LEN_MASK	= 0x1f << SW_CONF_SW_LEN_SHIFT,
		SW_CONF_SW_RX		= 1 << 5, /* 0: 1st sync; 1: 2nd */
		SW_CONF_DUAL_RX		= 1 << 7, /* expect 1st and 2nd */
	SW0_REG		= 0x6195,	/* 1st sync[7:0] */
	SW1_REG		= 0x6196,	/* 1st sync[15:8] */
	SW2_REG		= 0x6197,	/* 1st sync[23:16] */
	SW3_REG		= 0x6198,	/* 1st sync[31:24] */
	FREQEST_REG	= 0x6199,	/* frequency offset estimate, signed */
	RXCTRL_REG	= 0x619a,	/* Receive Section Tuning */
		RXCTRL_MIX_CURRENT_SHIFT = 0, /* RX mixer current */
		RXCTRL_MIX_CURRENT_MASK	= 3 << RXCTRL_MIX_CURRENT_SHIFT,
		RXCTRL_GBIAS_LNA_REF_SHIFT = 2, /* LNA PTAT, M=3...6 */
		RXCTRL_GBIAS_LNA_REF_MASK = 3 << RXCTRL_GBIAS_LNA_REF_SHIFT,
		RXCTRL_GBIAS_LNA2_REF_SHIFT = 2, /* LNA2/mix PTAT, M=3...6 */
		RXCTRL_GBIAS_LNA2_REF_MASK = 3 << RXCTRL_GBIAS_LNA2_REF_SHIFT,
	FSCTRL_REG	= 0x619b,	/* Frequency Synthesizer Tuning */
		FSCTRL_LODIV_CURRENT_SHIFT = 0, /* Divider currents */
		FSCTRL_LODIV_CURRENT_MASK = 3 << FSCTRL_LODIV_CURRENT_SHIFT,
		FSCTRL_LODIV_BUF_CURRENT_RX_SHIFT = 2, /* RX mixer/PA current */
		FSCTRL_LODIV_BUF_CURRENT_RX_MASK =
					3 << FSCTRL_LODIV_BUF_CURRENT_RX_SHIFT,
		FSCTRL_LODIV_BUF_CURRENT_TX_SHIFT = 4, /* TX mixer/PA current */
		FSCTRL_LODIV_BUF_CURRENT_TX_MASK =
					3 << FSCTRL_LODIV_BUF_CURRENT_TX_SHIFT,
		FSCTRL_PRE_CURRENT_SHIFT = 6, /* Prescaler current */
		FSCTRL_PRE_CURRENT_MASK	= 3 << FSCTRL_PRE_CURRENT_SHIFT,
	/* no 0x619c - 0x619f */
	LNAGAIN_REG	= 0x61a0,	/* LNA Gain Setting */
		LNAGAIN_LNA3_CURRENT_SHIFT = 0, /* LNA3: n*3 dB */
		LNAGAIN_LNA3_CURRENT_MASK = 3 << LNAGAIN_LNA3_CURRENT_SHIFT,
		LNAGAIN_LNA2_CURRENT_SHIFT = 2, /* LNA2: n*3 dB */
		LNAGAIN_LNA2_CURRENT_MASK = 7 << LNAGAIN_LNA2_CURRENT_SHIFT,
		LNAGAIN_LNA1_CURRENT_SHIFT = 5, /* LNA1: 0/3/-/6 dB */
		LNAGAIN_LNA1_CURRENT_MASK = 3 << LNAGAIN_LNA1_CURRENT_SHIFT,
	AAFGAIN_REG	= 0x61a1,	/* AAF Gain Setting */
		AAFGAIN_AAF_GAIN_SHIFT	= 0, /* 9-3*n dB attenuation */
		AAFGAIN_AAF_GAIN_MASK	= 3 << AAFGAIN_AAF_GAIN_SHIFT,
	ADCTEST0_REG	= 0x61a2,	/* ADC Tuning */
	/* no 0x61a3 - 0x61a4 */
	MDMTEST0_REG	= 0x61a5,	/* Modem Configuration */
		MDMTEST0_DC_BLOCK_MODE_SHIFT = 0, /* Mode of operation */
		MDMTEST0_DC_BLOCK_MODE_MASK = 3 << MDMTEST0_DC_BLOCK_MODE_SHIFT,
			MDMTEST0_DC_BLOCK_MODE_MANUAL = 0, /* man override */
			MDMTEST0_DC_BLOCK_MODE_NORMAL = 1, /* enable */
			MDMTEST0_DC_BLOCK_MODE_FREEZE = 2, /* freeze on sync */
			MDMTEST0_DC_BLOCK_MODE_DELAY  = 3, /* delayed est */
		MDMTEST0_DC_BLOCK_LENGTH_SHIFT = 2, /* samples per dump */
		MDMTEST0_DC_BLOCK_LENGTH_MODE =
					3 << MDMTEST0_DC_BLOCK_LENGTH_SHIFT,
			MDMTEST0_DC_BLOCK_LENGTH_16	= 0,
			MDMTEST0_DC_BLOCK_LENGTH_32	= 1,
			MDMTEST0_DC_BLOCK_LENGTH_64	= 2,
			MDMTEST0_DC_BLOCK_LENGTH_128	= 3,
		MDMTEST0_RSSI_ACC_SHIFT	= 5,
		MDMTEST0_RSSI_ACC_MODE	= 7 << MDMTEST0_RSSI_ACC_SHIFT,
			MDMTEST0_RSSI_ACC_MODE_1x5us	= 0,
			MDMTEST0_RSSI_ACC_MODE_2x5us	= 1,
			MDMTEST0_RSSI_ACC_MODE_4x5us	= 3,
			MDMTEST0_RSSI_ACC_MODE_1x21us	= 4,
			MDMTEST0_RSSI_ACC_MODE_2x21us	= 5,
			MDMTEST0_RSSI_ACC_MODE_4x21us	= 7,
	MDMTEST1_REG	= 0x61a6,	/* Modem Configuration */
		MDMTEST1_TX_TONE_SHIFT	= 0, /* Baseband TX frequency */
		MDMTEST1_TX_TONE_MASK	= 0x1f << MDMTEST1_TX_TONE_SHIFT,
			MDMTEST1_TX_TONE_n8MHz	= 0, /* -8 MHz */
			MDMTEST1_TX_TONE_n6MHz	= 1,
			MDMTEST1_TX_TONE_n4MHz	= 2,
			MDMTEST1_TX_TONE_n3MHz	= 3,
			MDMTEST1_TX_TONE_n2MHz	= 4,
			MDMTEST1_TX_TONE_n1MHz	= 5,
			MDMTEST1_TX_TONE_n500kHz = 6,
			MDMTEST1_TX_TONE_n250kHz = 7,
			MDMTEST1_TX_TONE_n125Hz	= 8,
			MDMTEST1_TX_TONE_n4kHz	= 9,
			MDMTEST1_TX_TONE_0Hz	= 10, /* 0 Hz */
			MDMTEST1_TX_TONE_4kHz	= 11,
			MDMTEST1_TX_TONE_125kHz	= 12,
			MDMTEST1_TX_TONE_250kHz	= 13,
			MDMTEST1_TX_TONE_500kHz	= 14,
			MDMTEST1_TX_TONE_1MHz	= 15,
			MDMTEST1_TX_TONE_2MHz	= 16,
			MDMTEST1_TX_TONE_3MHz	= 17,
			MDMTEST1_TX_TONE_4MHz	= 18,
			MDMTEST1_TX_TONE_6MHz	= 19,
			MDMTEST1_TX_TONE_8MHz	= 20,
		MDMTEST1_RX_IF		= 1 << 5, /* Mixer: 0: +1, 1: -1 MHz */
		MDMTEST1_DC_DELAY_SHIFT	= 6, /* DC delay, n+5 delays */
		MDMTEST1_DC_DELAY_MASK	= 3 << MDMTEST1_DC_DELAY_SHIFT,
	/* no 0x61a7 - 0x61a8 */
	ATEST_REG	= 0x61a9,	/* Analog Test Control */
		ATEST_ATEST_CTRL_SHIFT	= 0, /* Analog test mode */
		ATEST_ATEST_CTRL_MASK	= 0x3f << ATEST_ATEST_CTRL_SHIFT,
			ATEST_ATEST_CTRL_OFF	= 0, /* disabled */
			ATEST_ATEST_CTRL_TEMP	= 1, /* temperature sensor */
	/* no 0x61aa - 0x61ad */
	RFC_OBS_CTRL0_REG = 0x61ae,	/* RF Observation Mux Control 0 */
		RFC_OBS_CTRL_MUX_SHIFT	= 0, /* rfc_obs_sig(n) selection */
			RFC_OBS_CTRL_MUX_DATA	= 0x07,	/* sniffer data */
			RFC_OBS_CTRL_MUX_CLOCK	= 0x08,	/* sniffer clock */
			RFC_OBS_CTRL_MUX_TX_ACT	= 0x09,	/* TX active */
			RFC_OBS_CTRL_MUX_RX_ACT	= 0x0a,	/* RX active */
			RFC_OBS_CTRL_MUX_VCO_ON	= 0x0b,	/* VCO ON */
			RFC_OBS_CTRL_MUX_SEARCH	= 0x0c,	/* Synch search */
			RFC_OBS_CTRL_MUX_IDLE	= 0x0d,	/* LLE idle */
			RFC_OBS_CTRL_MUX_WAIT_T	= 0x0e,	/* wait T2 ev 1 */
			RFC_OBS_CTRL_MUX_AGC_LOW = 0x0f, /* AGC reduced gain */
			RFC_OBS_CTRL_MUX_LOCK	= 0x13,	/* PLL lock */
			RFC_OBS_CTRL_MUX_PA_OFF	= 0x1b,	/* PA power down */
			RFC_OBS_CTRL_MUX_LNA_OFF = 0x1c, /* LNA power down */
			RFC_OBS_CTRL_MUX_SFOUND	= 0x30,	/* sync found */
			RFC_OBS_CTRL_MUX_SSENT	= 0x31,	/* sync sent */
		RFC_OBS_CTRL_MUX_MASK	= 0x3f << RFC_OBS_CTRL_MUX_SHIFT,
		RFC_OBS_CTRL_POL	= 1 << 6, /* XOR signal with */
	RFC_OBS_CTRL1_REG = 0x61af,	/* RF Observation Mux Control 1 */
	RFC_OBS_CTRL2_REG = 0x61b0,	/* RF Observation Mux Control 2 */
	LLECTRL_REG	= 0x61b1,	/* LLE Control */
		LLECTRL_LLE_EN		= 1 << 0, /* LLE enabled */
	/* no 0x61b2 - 0x61b5 */
	ACOMPQS_REG	= 0x61b6,	/* Quadrature skew, signed, -16...15 */
	/* no 0x61b7 - 0x61bb */
	TXFILTCFG_REG	= 0x61bc,	/* TX Filter Configuration */
		TXFILTCFG_FC_SHIFT	= 0, /* TX anti-aliasing filter bw */
		TXFILTCFG_FC_MASK	= 0xf << TXFILTCFG_FC_SHIFT,
	/* no 0x61bd - 0x61be */
	RFRND_REG	= 0x61bf,	/* Random Data */
	RFRAMCFG_REG	= 0x61c0,	/* Radio RAM Configuration */
		RFRAMCFG_PRE_SHIFT	= 0, /* RF RAM page selection */
		RFRAMCFG_PRE_MASK	= 7 << RFRAMCFG_PRE_SHIFT,
	/* no 0x61c1 - 0x61c2 */
	RFFDMA0_REG	= 0x61c3,	/* Radio DMA Trigger 0 (19) Control */
		RFFDMA_DMA_SHIFT	= 0,	/* DMA trigger 0 when ... */
		RFFDMA_DMA_MASK		= 0x1f << RFFDMA_DMA_SHIFT,
			RFFDMA_DMA_NEVER	= 0, /* never */
			RFFDMA_DMA_RX_DATA	= 1, /* RX not empty */
			RFFDMA_DMA_RX_SPACE	= 2, /* RX not full */
			RFFDMA_DMA_RX_EMPTY	= 3, /* RX is empty */
			RFFDMA_DMA_RX_FULL	= 4, /* RX is full */
			RFFDMA_DMA_RX_WTHRESH	= 5, /* RX at thresh after wr */
			RFFDMA_DMA_RX_RTHRESH	= 6, /* RX at thresh b4 read */
			RFFDMA_DMA_RX_RESET	= 7, /* RX FIFO is reset */
			RFFDMA_DMA_RX_DEALLOC	= 8, /* RX FIFO is deallocated*/
			RFFDMA_DMA_RX_RETRY	= 9, /* RX FIFO is retried */
			RFFDMA_DMA_RX_DISCARD	= 10, /* RX FIFO is discarded */
			RFFDMA_DMA_RX_COMMIT	= 11, /* RX FIFO is committed */
			RFFDMA_DMA_NEVER2	= 16, /* never */
			RFFDMA_DMA_TX_DATA	= 17, /* TX not empty */
			RFFDMA_DMA_TX_SPACE	= 18, /* TX not full */
			RFFDMA_DMA_TX_EMPTY	= 19, /* TX is empty */
			RFFDMA_DMA_TX_FULL	= 20, /* TX is full */
			RFFDMA_DMA_TX_WTHRESH	= 21, /* TX at thresh after wr*/
			RFFDMA_DMA_TX_RTHRESH	= 22, /* TX at thresh b4 read*/
			RFFDMA_DMA_TX_RESET	= 23, /* TX FIFO is reset */
			RFFDMA_DMA_TX_DEALLOC	= 24, /* TX FIFO is dealloc'ed*/
			RFFDMA_DMA_TX_RETRY	= 25, /* TX FIFO is retried */
			RFFDMA_DMA_TX_DISCARD	= 26, /* TX FIFO is discarded */
			RFFDMA_DMA_TX_COMMIT	= 27, /* TX FIFO is committed */
	RFFDMA1_REG	= 0x61c4,	/* Radio DMA Trigger 1 (11) Control */
	RFFSTATUS_REG	= 0x61c5,
		RFFSTATUS_RXFFULL	= 1 << 0, /* RX FIFO is full */
		RFFSTATUS_RXDTHEX	= 1 << 1, /* RX FIFO >= RFRXFTHRS */
		RFFSTATUS_RXFEMPTY	= 1 << 2, /* RX FIFO is empty */
		RFFSTATUS_RXAVAIL	= 1 << 3, /* RX FIFO has readable data*/
		RFFSTATUS_TXFFULL	= 1 << 4, /* TX FIFO full */
		RFFSTATUS_TXDTHEX	= 1 << 5, /* TX FIFO >= RFTXFTHRS */
		RFFSTATUS_TXFEMPTY	= 1 << 6, /* TX FIFO empty */
		RFFSTATUS_TXAVAIL	= 1 << 7, /* TX FIFO has readable data*/
	RFFCFG_REG	= 0x61c6,
		RFFCFG_RXFAUTODEALLOC	= 1 << 0, /* RX: always set SRP = RP */
		RFFCFG_RXAUTOCOMMIT	= 1 << 1, /* RX: always set SWP = WP */
		RFFCFG_TXFAUTODEALLOC	= 1 << 4, /* TX: always set SRP = RP */
		RFFCFG_TXAUTOCOMMIT	= 1 << 5, /* TX: always set SWP = WP */
	/* no 0x61c7 */
	RFRXFLEN_REG	= 0x61c8,	/* RX FIFO Length (bytes) */
	RFRXFTHRS_REG	= 0x61c9,	/* RX FIFO Threshold, <= 127 */
	RFRXFWR_REG	= 0x61ca,	/* RX FIFO Write Register */
	RFRXFRD_REG	= 0x61cb,	/* RX FIFO Read Register */
	RFRXFWP_REG	= 0x61cc,	/* RX FIFO Write Pointer, <= 127 */
	RFRXFRP_REG	= 0x61cd,	/* RX FIFO Read Pointer, <= 127 */
	RFRXFSWP_REG	= 0x61ce,	/* RX FIFO SOF Write Pointer, <= 127 */
	RFRXFSRP_REG	= 0x61cf,	/* RX FIFO SOF Read Pointer, <= 127 */

	RFTXFLEN_REG	= 0x61d0,	/* TX FIFO Length (bytes) */
	RFTXFTHRS_REG	= 0x61d1,	/* TX FIFO Threshold, <= 127 */
	RFTXFWR_REG	= 0x61d2,	/* TX FIFO Write Register */
	RFTXFRD_REG	= 0x61d3,	/* TX FIFO Read Register */
	RFTXFWP_REG	= 0x61d4,	/* TX FIFO Write Pointer, <= 127 */
	RFTXFRP_REG	= 0x61d5,	/* TX FIFO Read Pointer, <= 127 */
	RFTXFSWP_REG	= 0x61d6,	/* TX FIFO SOF Write Pointer, <= 127 */
	RFTXFSRP_REG	= 0x61d7,	/* TX FIFO SOF Read Pointer, <= 127 */
	/* no 0x61d7 - 0x61df */
	BSP_P0_REG	= 0x61e0,	/* CRC p[7:0] */
	BSP_P1_REG	= 0x61e1,	/* CRC p[15:8] */
	BSP_P2_REG	= 0x61e2,	/* CRC p[23:16] */
	BSP_P3_REG	= 0x61e3,	/* CRC p[31:24] */
	BSP_D0_REG	= 0x61e4,	/* CRC d[7:0] */
	BSP_D1_REG	= 0x61e5,	/* CRC d[15:8] */
	BSP_D2_REG	= 0x61e6,	/* CRC d[23:16] */
	BSP_D3_REG	= 0x61e7,	/* CRC d[31:24] */
	BSP_W_REG	= 0x61e8,	/* Whitener Value */
		BSP_W_W_SHIFT		= 0, /* whitener w reg (flipped) */
		BSP_W_W_MASK		= 0x7f << BSP_W_W_SHIFT,
		BSP_W_W_PN9_RESET	= 1 << 7, /* reset PN9 whitener */
	BSP_MODE_REG	= 0x61e9,	/* Bit Stream Processor Configuration */
		BSP_MODE_W_PN7_EN	= 1 << 0, /* enable PN7 whitener */
		BSP_MODE_W_PN9_EN	= 1 << 1, /* enable PN9 whitener */
		BSP_MODE_CP_MODE_SHIFT	= 2, /* Co-processor mode */
		BSP_MODE_CP_MODE_MASK	= 3 << BSP_MODE_CP_MODE_SHIFT,
			BSP_MODE_CP_MODE_OFF	= 0, /* disabled */
			BSP_MODE_CP_MODE_RX	= 1, /* receive mode */
			BSP_MODE_CP_MODE_TXF	= 3, /* transmit mode */
		BSP_MODE_CP_END		= 1 << 4, /* endianness, 1: MSb first */
		BSP_MODE_CP_READOUT	= 1 << 5, /* Co-processor mode readout*/
		BSP_MODE_CP_BUSY	= 1 << 6, /* Co-processor mode busy */
	BSP_DATA_REG	= 0x61ea,	/* Bit Stream Proc Co-Processor Data */
	/* no 0x61eb - 0x61f7 */
	SW4_REG		= 0x61f8,	/* 2nd sync[7:0] */
	SW5_REG		= 0x61f9,	/* 2nd sync[15:8] */
	SW6_REG		= 0x61fa,	/* 2nd sync[23:16] */
	SW7_REG		= 0x61fb,	/* 2nd sync[31:24] */
	DC_I_L_REG	= 0x61fc,	/* In-Phase DC Offset Est Low Byte */
	DC_I_H_REG	= 0x61fd,	/* In-Phase DC Offset Est High Byte */
	DC_Q_L_REG	= 0x61fe,	/* Quadrature-Phase DC Offs Est Low */
	DC_Q_H_REG	= 0x61ff,	/* Quadrature-Phase DC Offs Est High */
};

/* ----- Radio RAM-based registers (Page 0) -------------------------------- */

enum {
	PRF_CHAN_REG	= 0x6000,	/* Frequency to use */
		PRF_CHAN_FREQ_SHIFT		= 0,	/* 2379+n MHz */
		PRF_CHAN_FREQ_MASK		= 0x7f << PRF_CHAN_FREQ_SHIFT,
		PRF_CHAN_FREQ_SYNTH_ON		= 1 << 7, /* Leave synth on */
	PRF_TASK_CONF_REG = 0x6001,	/* Configuration of task control */
		PRF_TASK_CONF_MODE_SHIFT	= 0,	/* Operation mode */
		PRF_TASK_CONF_MODE_MASK		= 3 << PRF_TASK_CONF_MODE_SHIFT,
			PRF_TASK_CONF_MODE_BASIC_FIXED	= 0,
			PRF_TASK_CONF_MODE_BASIC_VAR	= 1,
			PRF_TASK_CONF_MODE_AUTO_HDR9	= 2,
			PRF_TASK_CONF_MODE_AUTO_HDR10	= 3,
		PRF_TASK_CONF_REPEAT		= 1 << 2, /* Repeated */
		PRF_TASK_CONF_START_CONF	= 1 << 3, /* Start on T2 ev 1 */
		PRF_TASK_CONF_STOP_CONF_SHIFT	= 4,	/* Stop config */
		PRF_TASK_CONF_STOP_CONF_MASK	= 3,
			PRF_TASK_CONF_STOP_CONF_NON	= 0,
			PRF_TASK_CONF_STOP_CONF_DONE_T2	= 1,
			PRF_TASK_CONF_STOP_CONF_T2	= 2,
			PRF_TASK_CONF_STOP_CONF_SYNC_T2	= 3,
		PRF_TASK_CONF_TX_ON_CC_CONF	= 1 << 6,
			/* 0: wait for RSSI to drop; 1: end if RSSI high */
		PRF_TASK_CONF_REPEAT_CONF	= 1 << 7,
	PRF_FIFO_CONF_REG = 0x6002,	/* Configure FIFO use */
		PRF_FIFO_CONF_AUTOFLUSH_IGN	= 1 << 0, /* flush bad seq */
		PRF_FIFO_CONF_AUTOFLUSH_CRC	= 1 << 1, /* flush bad CRC */
		PRF_FIFO_CONF_AUTOFLUSH_EMPTY	= 1 << 2, /* flush zero-len */
		PRF_FIFO_CONF_RX_STATUS_CONF	= 1 << 3, /* append RSSI+RES */
		PRF_FIFO_CONF_RX_ADDR_ADDR	= 1 << 4, /* include address */
		PRF_FIFO_CONF_RX_ADDR_CONFIG	= 1 << 5, /* include config */
		PRF_FIFO_CONF_TX_ADDR_ADDR	= 1 << 6, /* have address */
		PRF_FIFO_CONF_TX_ADDR_CONFIG	= 1 << 7, /* have config */
	PRF_PKT_CONF_REG = 0x6003,	/* Packet configuration */
		PRF_PKT_CONF_ADDR_LEN		= 1 << 0, /* addr bytes 0/1 */
		PRF_PKT_CONF_AGC_EN		= 1 << 1, /* use AGC */
		PRF_PKT_CONF_START_TONE		= 1 << 2, /* override preamble*/
	PRF_CRC_LEN_REG	= 0x6004,	/* CRC bytes (0-4) */
	PRF_RSSI_LIMIT_REG = 0x6005,	/* Clear channel threshold */
	PRF_RSSI_COUNT_REG = 0x6006,	/* Measurements before clear */
	PRF_CRC_INIT_REG = 0x6008,	/* Initial CRC value */
	PRF_W_INIT_REG	= 0x600c,	/* Whitener init */
	PRF_RETRANS_CNT_REG = 0x600d,	/* Retransmission limit */
	PRF_TX_DELAY_REG = 0x600e,	/* Inter-packet delay, x 62.5 ns */
	PRF_RETRANS_DELAY_REG = 0x6010,	/* Retransmission delay, x 62.5 ns */
	PRF_SEARCH_TIME_REG = 0x6012,	/* Search time; 0/>= 256, x 31.25 ns */
	PRF_RX_TX_TIME_REG = 0x6014,	/* ACK turnaround; x 31.25 ns */
	PRF_TX_RX_TIME_REG = 0x6016,	/* RX-RX turnaround; x 31.25 ns */
	PRF_ADDR_ENTRY0_REG = 0x6018,	/* Address #0 */
	PRF_ADDR_ENTRY1_REG = 0x6024,	/* Address #1 */
	PRF_ADDR_ENTRY2_REG = 0x6030,	/* Address #2 */
	PRF_ADDR_ENTRY3_REG = 0x603c,	/* Address #3 */
	PRF_ADDR_ENTRY4_REG = 0x6048,	/* Address #4 */
	PRF_ADDR_ENTRY5_REG = 0x6054,	/* Address #5 */
	PRF_ADDR_ENTRY6_REG = 0x6060,	/* Address #6 */
	PRF_ADDR_ENTRY7_REG = 0x606c,	/* Address #7 */
	PRF_N_TX_REG	= 0x6078,	/* Total number of packets tx'ed */
	PRF_LAST_RSSI_REG = 0x6079,	/* RSSI of last received packet */
	PRF_LAST_DCOFF_REG = 0x607a,	/* DC offset of last received packet */
	PRF_RADIO_CONF_REG = 0x607e,	/* Configure radio hardware */
		PRF_RADIO_CONF_RXCAP_SHIFT = 0,	/* RX capture */
		PRF_RADIO_CONF_RXCAP_MASK  = 3 << PRF_RADIO_CONF_RXCAP_SHIFT,
			PRF_RADIO_CONF_RXCAP_NO		= 0, /* never */
			PRF_RADIO_CONF_RXCAP_START	= 1, /* every start */
			PRF_RADIO_CONF_RXCAP_END	= 2, /* every end */
			PRF_RADIO_CONF_RXCAP_FIRST	= 3, /* first start */
		PRF_RADIO_CONF_TXCAP_SHIFT = 2,	/* TX capture */
		PRF_RADIO_CONF_TXCAP_MASK  = 3 << PRF_RADIO_CONF_TXCAP_SHIFT,
			PRF_RADIO_CONF_TXCAP_NO		= 0, /* never */
			PRF_RADIO_CONF_TXCAP_START	= 1, /* every start */
			PRF_RADIO_CONF_TXCAP_END	= 2, /* every end */
			PRF_RADIO_CONF_TXCAP_FIRST	= 3, /* first start */
		PRF_RADIO_CONF_TXIF_SHIFT = 4,	/* TX IF for 2 Mbps (MHz) */
		PRF_RADIO_CONF_TXIF_MASK  = 3 << PRF_RADIO_CONF_TXIF_SHIFT,
		PRF_RADIO_CONF_DCOFF	= 1 << 6,	/* Special DC offset */
		PRF_RADIO_CONF_DCWB	= 1 << 7,	/* Write back DC off */
	PRF_ENDCAUSE_REG	= 0x607f,	/* Reason why LLE ended task */
};

/* ----- Radio RAM-based registers (Page 5) -------------------------------- */

enum {
	PRFX_LAST_FREQEST_REG	= 0x6006,	/* Last frequency offset est */
	PRFX_RSSI_LIM_LOWER_REG	= 0x6008,	/* Lower RSSI limit for AGC */
	PRFX_RSSI_LIM_UPPER_REG	= 0x6009,	/* Upper RSSI limit for AGC */
	PRFX_RSSI_DIFF_REG	= 0x600a,	/* High/low RSSI gain diff */
	PRFX_LNAGAIN_SAT_REG	= 0x600b,	/* LNAGAIN near saturation */
	PRFX_TONE_DURATION_REG	= 0x600c,	/* Start tone, x 31.25 ns */
	PRFX_TONE_OFFSET_REG	= 0x600e,	/* TX cal reduction, 31.25 ns */
};

#endif /* !CC_CC25XX_RADIO_H */
