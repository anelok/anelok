/*
 * fw/cc/misc.h - Helper functions
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_MISC_H
#define	CC_MISC_H

void delay(void);

#endif /* !CC_MISC_H */
