/*
 * fw/ccfw/mcomm.c - CC254x to KL2x communication
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "sfr.h"
#include "board.h"
#include "mcomm.h"


/* ----- SPI interface ----------------------------------------------------- */


static bool nreq_read(void)
{
	return gpio_read(RF_nREQ);
}


static void nint_set(bool on)
{
	if (on) {
		gpio_set(RF_nINT);
	} else {
		gpio_clr(RF_nINT);
	}
}


static void spi_init(void)
{
	PERCFG |= UART0_PERCFG << PERCFG_U0CFG_SHIFT;

	gpio_fn(RF_SCLK);
	gpio_fn(RF_MISO);
	gpio_fn(RF_MOSI);
	gpio_fn(RF_nSEL);

	U0GCR = U0GCR_ORDER;	/* SPI is MSb first */
	U0UCR = U0UCR_FLUSH;	/* flush USART */
	U0CSR =
	    U0CSR_SLAVE		/* SPI slave */
	    | U0CSR_RE;		/* enable receiver */

	/* disable pull-ups */
	gpio_set_reg(INP, RF_SCLK);
	gpio_set_reg(INP, RF_MOSI);
}


/* ----- Transfer protocol ------------------------------------------------- */


static enum state {
	S_IDLE,
} state = S_IDLE;

static const void *tx_buf;
static uint8_t tx_len;


bool comm_idle(void)
{
	return !tx_len && nreq_read();
}


uint8_t comm_poll(void *buf, uint8_t size)
{
	const uint8_t *tx = tx_buf;
	uint8_t *b = buf;
	uint8_t cmd, len;
	uint8_t i;

	if (comm_idle())
		return 0;

	(void) U0DBUF; /* make sure the buffer is empty */
	U0DBUF = tx_len;

	nint_set(0);
	while (!(U0CSR & U0CSR_RX_BYTE));
	nint_set(1);

	cmd = U0DBUF;
	len = cmd & 0x7f;
	if (tx_len > len)
		len = tx_len;

	for (i = 0; i != len; i++) {
		if (i < tx_len)
			U0DBUF = *tx++;
		else
			U0DBUF = 0;
		while (!(U0CSR & U0CSR_RX_BYTE));
		if (i < size)
			*b++ = U0DBUF;
		else
			(void) U0DBUF;
	}

	while (U0CSR & U0CSR_ACTIVE);
	tx_len = 0;

	return cmd;
}


bool comm_send(const void *buf, uint8_t len)
{
	if (tx_len)
		return 0;
	tx_buf = buf;
	tx_len = len;
	return 1;
}


void comm_isr(void) __interrupt(INT_P2INT)
{
	/* clear interrupt */
	gpio_clr_reg(IFG, RF_nREQ);
	IRCON2 &= ~IRCON2_P2IF;
}


void comm_init(void)
{
	spi_init();

	/* make DD/nINT an output */

	gpio_output(RF_nINT);

	/* DC/nREQ on P2_2 */

	/* clear interrupt */
	gpio_clr_reg(IFG, RF_nREQ);
	IRCON2 &= ~IRCON2_P2IF;

	PICTL = PICTL_P2ICONL;	/* interrupt on falling edge */
	/* enable interrupts */
	gpio_set_reg(IEN, RF_nREQ);
}
