/*
 * fw/cc/radio.h - CC25xx radio control
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "cc2543.h"
#include "sfr.h"
#include "cc25xx-radio.h"
#include "cc25xx-radio-regs.h"
#include "misc.h"
#include "radio.h"


static volatile enum radio_state {
	RADIO_OFF,	/* radio is powered down */
	RADIO_IDLE,	/* radio is not executing any command */
	RADIO_LISTEN,	/* radio is listening */
	RADIO_TX,	/* radio is transmitting */
	RADIO_RX,	/* radio is receiving */
} state = RADIO_OFF;


bool radio_has_rx(void)
{
	return state == RADIO_RX;
}


bool radio_is_idle(void)
{
	return state == RADIO_IDLE;
}


void radio_send(const void *buf, uint8_t len)
{
	uint8_t i;

#if 0 /* don't want no trouble with EVELYN */
	switch (state) {
	case RADIO_IDLE:
		break;
	default:
		// @@@ complain ?
	}
#endif

	/*
	 * Put the packet into the FIFO.
	 * Auto-commit (RFFCFG.TXAUTOCOMMIT) is enabled by default.
	 */
	RFST = CMD_TXFIFO_RESET;
	RFD = len;
	for (i = 0; i != len; i++)
		RFD = ((const uint8_t *) buf)[i];

	RFST = CMD_TX;
	state = RADIO_TX;

	/* int: RXEMPTY RXOK RXNOK */
	/* empty fifo */
}


uint8_t radio_recv(void *buf, uint8_t size)
{
	uint8_t len, i;

	if (state != RADIO_RX) {
		// @@@ complain ?
	}

	len = RFD;
	if (len > size)
		return RF_ERROR;

	// @@@ check for FIFO underrun
	for (i = 0; i != len; i++)
		((uint8_t *) buf)[i] = RFD;

	state = RADIO_IDLE;
	return len;
}


void radio_isr(void) __interrupt(INT_RF)
{
	uint8_t irq;

	irq = RFIRQF1;
	if (irq & RFIRQF1_TASKDONE) {
		switch (state) {
		case RADIO_LISTEN:
			// @@@ not entirely correct. We should go to RX while
			// data is on the air (to delay sending), then signal
			// packet availability.
			// @@@ also, ignore bad packets
			state = RADIO_RX;
			// @@@ rx done event
			break;
		case RADIO_TX:
			state = RADIO_IDLE;
			// @@@ tx done event
			break;
		default:
			// @@@ error event
			break;
		}
	}

	/* clear interrupt */
	S1CON = 0;
	RFIRQF1 = 0;	/* we only care about TASKDONE */
}	


void radio_listen(void)
{
	switch (state) {
	case RADIO_LISTEN:
		return;
	case RADIO_IDLE:
		break;
	default:
		// @@@ complain ?
	}
	RFST = CMD_RXFIFO_RESET;

	/*
	 * Length is first byte of payload; no repeat, timers.
	 */
	PRF_TASK_CONF =
	     PRF_TASK_CONF_MODE_BASIC_VAR << PRF_TASK_CONF_MODE_SHIFT;
	RFST = CMD_RX;

	state = RADIO_LISTEN;
}


void radio_cw(void)
{
	MDMTEST1 =
	    MDMTEST1_TX_TONE_0Hz	/* TX IF + 0 MHz */
	    | 0 * MDMTEST1_RX_IF;	/* RX IF + 1 MHz */

	RFST = CMD_TX_TEST;
}


void radio_setup(unsigned f)
{
	radio_off();

	if (!f)
		return;

	LLECTRL = LLECTRL_LLE_EN;
	/* @@@ data sheet specifies no delay here. Wonder if that's correct. */
delay();

	/* frequency is set by hw regs, keep synthesizer on at task end */

	PRF_CHAN = 127 | PRF_CHAN_FREQ_SYNTH_ON;

	FREQCTRL = f - RADIO_F_MIN_MHz;

	MDMCTRL0 =
	    0 * MDMCTRL0_PHASE_INVERT
	    | MDMCTRL0_MODULATION_GFSK_250kHz_1Mbps
	    | MDMCTRL0_TX_IF;	/* for continuous wave (CW) */

#if 0
	MDMTEST1 =
	    MDMTEST1_TX_TONE_0Hz	/* TX IF + 0 MHz */
	    | 0 * MDMTEST1_RX_IF;	/* RX IF + 1 MHz */
#endif

	/* initialize RAM-based registers */

	PRF_CHAN = 127;
	PRF_TASK_CONF = 0;
	PRF_FIFO_CONF = 0;
	PRF_PKT_CONF = 0;
	PRF_CRC_LEN = 0;
	//PRF_RSSI_LIMIT = 0;
	//PRF_RSSI_COUNT = 0;
	//PRF_CRC_INIT
	// PRF_W_INIT
	PRF_RETRANS_CNT = 1;
	PRF_TX_DELAY = 0;
	PRF_RETRANS_DELAY = 0;
	PRF_SEARCH_TIME = 0; /* forever */
	PRF_RX_TX_TIME = 0;
	PRF_TX_RX_TIME = 0;
	PRF_RADIO_CONF = 0;

	/* enable whitening */

	BSP_MODE = BSP_MODE_W_PN7_EN | BSP_MODE_W_PN9_EN;

	/* section 23.12.2 updates */

	FRMCTRL0 = 0x43;
	TXPOWER = 0xe5;
	MDMCTRL1 = 0x48;
	MDMCTRL2 = 0xc0;
	MDMCTRL3 = 0x63;
	RXCTRL = 0x33;
	FSCTRL = 0x55;
	LNAGAIN = 0x3a;
	ACOMPQS = 0x16;
	TXFILTCFG = 0x07;

	/* configure for Basic Mode */

	/*
	 * Basic mode. Variable length (length is first byte of payload).
	 * No repetition or timers.
	 * Wait for RSSI to drop before transmitting.
	 */
	PRF_TASK_CONF =
	     PRF_TASK_CONF_MODE_BASIC_VAR << PRF_TASK_CONF_MODE_SHIFT;

	PRF_PKT_CONF =
	    0 * PRF_PKT_CONF_ADDR_LEN;	/* no address */
	PRF_CRC_LEN = 0;		/* no CRC */

	/* Neither address nor config byte in payload. */
	PRF_FIFO_CONF = 0;

	/* See Table 23-6 "Address Structure for Basic Mode" */

	PRF_ADDR[0].CONF	= 0;	/* nothing fancy */
	PRF_ADDR[0].ADDRESS	= 0;	/* dummy */
	PRF_ADDR[0].RXLENGTH	= RF_MAX_LEN;

	IEN2 |= IEN2_RFIE;
	RFIRQM1 |= RFIRQF1_TASKDONE;
}


void radio_off(void)
{
	/* cancel any previous command */
	RFST = CMD_SHUTDOWN;

delay();

	/* reset LLE */
	LLECTRL = 0;
}
