/*
 * fw/ccfw/cc.c - CC254x initialization and main loop
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "sfr.h"
#include "mcomm.h"
#include "radio.h"
#include "op.h"
#include "version.h"


static bool radio_idle = 1;


/* ----- Interrupt vectors ------------------------------------------------- */


void comm_isr(void) __interrupt(INT_P2INT);
void radio_isr(void) __interrupt(INT_RF);


/* ----- Clock output ------------------------------------------------------ */


#if 0

/*
 * Enable 32 MHz crystal oscillator and set T3 to output 2 MHz on CH1 aka P1_3.
 */

static void clock_output(void)
{
	CLKCONCMD = CLKCONCMD_OSC32K;
	P1SEL |= 1 << 3;
	T3CTL = T3CTL_CLR;
	T3CCTL1 =
	    T3CCTL1_MODE
	    | T3CCTL1_CMP_TOG_CMP;
	T3CC0 = 0;
	T3CTL =
	    T3CTL_MODE_MOD
	    | T3CTL_CLR
	    | T3CTL_START
	    | T3CTL_DIV_8;
}

#endif


/* ----- Interrupt control ------------------------------------------------- */


static void irq_enable(void)
{
	IEN0 |= IEN0_EA;
}


static void irq_disable(void)
{
	IEN0 &= ~IEN0_EA;
}


/* ----- Idle states ------------------------------------------------------- */


static void pcon_idle(void)
{
	__asm__(
		"\t.bndry	2\n"
		"\tmov		0x87, #1\n");	/* PCON_IDLE = 1 */
}


static void idle(void)
{
	SLEEPCMD = SLEEPCMD_MODE_ACTIVE << SLEEPCMD_MODE_SHIFT;
	pcon_idle();
}


static void power_down(void)
{
	SLEEPCMD = SLEEPCMD_MODE_PM3 << SLEEPCMD_MODE_SHIFT;
	pcon_idle();
}


/* ----- Main loop --------------------------------------------------------- */


static void command(const uint8_t *buf, uint8_t len)
{
	static uint8_t res_buf[20];
	uint8_t res_len;
	volatile __xdata uint8_t *xptr;

	if (!len)
		return;
	switch (buf[0]) {
	case OP_VERSION:
		res_buf[0] = RADIO_PROTOCOL_VERSION_LOW;
		res_buf[1] = RADIO_PROTOCOL_VERSION_HIGH;
		comm_send(res_buf, 2);
		break;
	case OP_ID:
		comm_send(build_date, build_date_length);
		break;
	case OP_POKE:
		if (len < 4)
			return;
		xptr = (volatile __xdata uint8_t *) (buf[1] | buf[2] << 8);
		*xptr = buf[3];
		break;
	case OP_PEEK:
		if (len < 3)
			return;
		xptr = (volatile __xdata uint8_t *) (buf[1] | buf[2] << 8);
		res_buf[0] = *xptr;
		comm_send(res_buf, 1);
		break;
	case OP_CW:
		if (len < 3)
			return;
		radio_setup(buf[1] | buf[2] << 8);
		radio_cw();
		break;
	case OP_LISTEN:
		if (len < 3)
			return;
		radio_setup(buf[1] | buf[2] << 8);
		radio_listen();
		break;
	case OP_OFF:
		radio_off();
		break;
	case OP_TX:
		radio_send(buf + 1, len - 1);
		break;
	case OP_RX:
		if (!radio_has_rx)
			break;
		res_len = radio_recv(res_buf, sizeof(res_buf));
		comm_send(res_buf, res_len);
		break;
	default:
		break;
	}
}


static void data(const uint8_t *buf, uint8_t len)
{
	(void) buf;
	(void) len;
}


static void main_loop(void)
{
	static __xdata uint8_t rx_buf[128];
	uint8_t cmd;

	while (1) {
		irq_disable();
		if (comm_idle()) {
			if (radio_idle) {
				power_down();
			} else {
				idle();
			}
		}
		irq_enable();

		cmd = comm_poll(rx_buf, sizeof(rx_buf));
		if (cmd & 0x80) {
			command(rx_buf, cmd & 0x7f);
		} else {
			data(rx_buf, cmd);
		}
	}
}


void main(void)
{
	/* P1_0, P1_1, P1_2 have no pull-up. Drive them low. */
	P1 = ~7;
	P1DIR = 7;

	comm_init();

	/* enable port 2 interrupts */
	IEN2 = IEN2_P2IE;

	irq_enable();

	main_loop();
}
