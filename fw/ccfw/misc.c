/*
 * fw/cc/misc.c - Helper functions
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>


void delay(void)
{
	uint16_t i;

	for (i = 0; i != 0xffff; i++);
}
