/*
 * fw/ccfw/cc25xx-radio-regs.h - CC25xx radio registers
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_CC25XX_RADIO_REGS_H
#define	CC_CC25XX_RADIO_REGS_H

#include <stdint.h>

#include "cc25xx-radio.h"


/* ----- Address Structure ------------------------------------------------- */

/* For PRF_ADDR[] in Page 0 */

enum {
	ADDR_CONF_ENA0		= 1 << 0,	/* Enable entry for 1st sync */
	ADDR_CONF_ENA1		= 1 << 1,	/* Enable entry for 2nd sync */
	ADDR_CONF_REUSE		= 1 << 2,	/* Reuse TX packet */
	ADDR_CONF_AA		= 1 << 3,	/* Auto ACK / retransmit */
	ADDR_CONF_VARLEN	= 1 << 4,	/* Variable len <= RXLENGTH */
	ADDR_CONF_FIXEDSEQ	= 1 << 5,	/* 0: SEQSTAT.SEQ; 1: FIFO */
	ADDR_CONF_TXLEN		= 1 << 6,	/* Transmit fixed length */
};

enum {
	ADDR_SEQSTAT_VALID	= 1 << 0,	/* Accept only "new" packets */
	ADDR_SEQSTAT_SEQ_SHIFT	= 1,		/* Last (RX) / next (TX) seq */
	ADDR_SEQSTAT_SEQ_MASK	= 3 << ADDR_SEQSTAT_SEQ_SHIFT,
	ADDR_SEQSTAT_ACKSEQ_SHIFT = 3,		/* ACK seq (for RX) */
	ADDR_SEQSTAT_ACKSEQ_MASK = 3 << ADDR_SEQSTAT_ACKSEQ_SHIFT,
	ADDR_SEQSTAT_ACK_PAYLOAD_SENT = 1 << 5,	/* Last ACK had payload */
	ADDR_SEQSTAT_NEXTACK	= 1 << 6,	/* ACK buffer index (RX) */
};

struct cc_addr_struct {
	uint8_t	CONF;		/* Address/length configuration */
	uint8_t RXLENGTH;	/* Maximum length of received packet (0­127) */
	uint8_t ADDRESS;	/* Address of packet */

	/* ----- Auto Mode Only ----- */
	uint8_t	SEQSTAT;	/* Sequence numbers */
	uint8_t ACKLENGTH0;	/* Buffer 0 length */
	uint8_t ACKLENGTH1;	/* Buffer 1 length */
	uint16_t CRCVAL;	/* CRC value of last received packet */
	uint8_t N_TXDONE;	/* Number of (new) packets transmitted */
	uint8_t N_RXIGNORED;	/* Retransmission received with CRC OK */

	/* ----- Basic and Auto Mode ----- */
	uint8_t N_RXOK;		/* Number of packets received with CRC OK */
	uint8_t N_RXNOK;	/* Number of packets received with CRC error */
};

/* ----- SFRs -------------------------------------------------------------- */

__sfr __at (RFIRQF0_REG)	RFIRQF0;
__sfr __at (RFIRQF1_REG)	RFIRQF1;
__sfr __at (RFERRF_REG)		RFERRF;
__sfr __at (RFD_REG)		RFD;
__sfr __at (RFST_REG)		RFST;

/* ----- XREG registers ---------------------------------------------------- */

volatile __xdata uint8_t	__at (FRMCTRL0_REG)	FRMCTRL0;
volatile __xdata uint8_t	__at (RFIRQM0_REG)	RFIRQM0;
volatile __xdata uint8_t	__at (RFIRQM1_REG)	RFIRQM1;
volatile __xdata uint8_t	__at (RFERRM_REG)	RFERRM;
volatile __xdata uint8_t	__at (FREQCTRL_REG)	FREQCTRL;
volatile __xdata uint8_t	__at (FREQTUNE_REG)	FREQTUNE;
volatile __xdata uint8_t	__at (TXPOWER_REG)	TXPOWER;
volatile __xdata uint8_t	__at (TXCTRL_REG)	TXCTRL;
volatile __xdata uint8_t	__at (LLESTAT_REG)	LLESTAT;
volatile __xdata uint8_t	__at (SEMAPHORE0_REG)	SEMAPHORE0;
volatile __xdata uint8_t	__at (SEMAPHORE1_REG)	SEMAPHORE1;
volatile __xdata uint8_t	__at (SEMAPHORE2_REG)	SEMAPHORE2;
volatile __xdata uint8_t	__at (RFSTAT_REG)	RFSTAT;
volatile __xdata int8_t		__at (RSSI_REG)		RSSI;
volatile __xdata uint8_t	__at (RFPSRND_REG)	RFPSRND;
volatile __xdata uint8_t	__at (MDMCTRL0_REG)	MDMCTRL0;
volatile __xdata uint8_t	__at (MDMCTRL1_REG)	MDMCTRL1;
volatile __xdata uint8_t	__at (MDMCTRL2_REG)	MDMCTRL2;
volatile __xdata uint8_t	__at (MDMCTRL3_REG)	MDMCTRL3;
volatile __xdata uint8_t	__at (SW_CONF_REG)	SW_CONF;
volatile __xdata uint8_t	__at (SW0_REG)		SW0;
volatile __xdata uint8_t	__at (SW1_REG)		SW1;
volatile __xdata uint8_t	__at (SW2_REG)		SW2;
volatile __xdata uint8_t	__at (SW3_REG)		SW3;
volatile __xdata int8_t		__at (FREQEST_REG)	FREQEST;
volatile __xdata uint8_t	__at (RXCTRL_REG)	RXCTRL;
volatile __xdata uint8_t	__at (FSCTRL_REG)	FSCTRL;
volatile __xdata uint8_t	__at (LNAGAIN_REG)	LNAGAIN;
volatile __xdata uint8_t	__at (AAFGAIN_REG)	AAFGAIN;
volatile __xdata uint8_t	__at (ADCTEST0_REG)	ADCTEST0;
volatile __xdata uint8_t	__at (MDMTEST0_REG)	MDMTEST0;
volatile __xdata uint8_t	__at (MDMTEST1_REG)	MDMTEST1;
volatile __xdata uint8_t	__at (ATEST_REG)	ATEST;
volatile __xdata uint8_t	__at (RFC_OBS_CTRL0_REG) RFC_OBS_CTRL0;
volatile __xdata uint8_t	__at (RFC_OBS_CTRL1_REG) RFC_OBS_CTRL1;
volatile __xdata uint8_t	__at (RFC_OBS_CTRL2_REG) RFC_OBS_CTRL2;
volatile __xdata uint8_t	__at (LLECTRL_REG)	LLECTRL;
volatile __xdata int8_t		__at (ACOMPQS_REG)	ACOMPQS;
volatile __xdata uint8_t	__at (TXFILTCFG_REG)	TXFILTCFG;
volatile __xdata uint8_t	__at (RFRND_REG)	RFRND;
volatile __xdata uint8_t	__at (RFRAMCFG_REG)	RFRAMCFG;
volatile __xdata uint8_t	__at (RFFDMA0_REG)	RFFDMA0;
volatile __xdata uint8_t	__at (RFFDMA1_REG)	RFFDMA1;
volatile __xdata uint8_t	__at (RFFSTATUS_REG)	RFFSTATUS;
volatile __xdata uint8_t	__at (RFFCFG_REG)	RFFCFG;
volatile __xdata uint8_t	__at (RFRXFLEN_REG)	RFRXFLEN;
volatile __xdata uint8_t	__at (RFRXFTHRS_REG)	RFRXFTHRS;
volatile __xdata uint8_t	__at (RFRXFWR_REG)	RFRXFWR;
volatile __xdata uint8_t	__at (RFRXFRD_REG)	RFRXFRD;
volatile __xdata uint8_t	__at (RFRXFWP_REG)	RFRXFWP;
volatile __xdata uint8_t	__at (RFRXFRP_REG)	RFRXFRP;
volatile __xdata uint8_t	__at (RFRXFSWP_REG)	RFRXFSWP;
volatile __xdata uint8_t	__at (RFRXFSRP_REG)	RFRXFSRP;
volatile __xdata uint8_t	__at (RFTXFLEN_REG)	RFTXFLEN;
volatile __xdata uint8_t	__at (RFTXFTHRS_REG)	RFTXFTHRS;
volatile __xdata uint8_t	__at (RFTXFWR_REG)	RFTXFWR;
volatile __xdata uint8_t	__at (RFTXFRD_REG)	RFTXFRD;
volatile __xdata uint8_t	__at (RFTXFWP_REG)	RFTXFWP;
volatile __xdata uint8_t	__at (RFTXFRP_REG)	RFTXFRP;
volatile __xdata uint8_t	__at (RFTXFSWP_REG)	RFTXFSWP;
volatile __xdata uint8_t	__at (RFTXFSRP_REG)	RFTXFSRP;
volatile __xdata uint8_t	__at (BSP_P0_REG)	BSP_P0;
volatile __xdata uint8_t	__at (BSP_P1_REG)	BSP_P1;
volatile __xdata uint8_t	__at (BSP_P2_REG)	BSP_P2;
volatile __xdata uint8_t	__at (BSP_P3_REG)	BSP_P3;
volatile __xdata uint8_t	__at (BSP_D0_REG)	BSP_D0;
volatile __xdata uint8_t	__at (BSP_D1_REG)	BSP_D1;
volatile __xdata uint8_t	__at (BSP_D2_REG)	BSP_D2;
volatile __xdata uint8_t	__at (BSP_D3_REG)	BSP_D3;
volatile __xdata uint8_t	__at (BSP_W_REG)	BSP_W;
volatile __xdata uint8_t	__at (BSP_MODE_REG)	BSP_MODE;
volatile __xdata uint8_t	__at (BSP_DATA_REG)	BSP_DATA;
volatile __xdata uint8_t	__at (SW4_REG)		SW4;
volatile __xdata uint8_t	__at (SW5_REG)		SW5;
volatile __xdata uint8_t	__at (SW6_REG)		SW6;
volatile __xdata uint8_t	__at (SW7_REG)		SW7;
volatile __xdata uint8_t	__at (DC_I_L_REG)	DC_I_L;
volatile __xdata uint8_t	__at (DC_I_H_REG)	DC_I_H;
volatile __xdata uint8_t	__at (DC_Q_L_REG)	DC_Q_L;
volatile __xdata uint8_t	__at (DC_Q_H_REG)	DC_Q_H;

/* ----- RAM registers (Page 0) -------------------------------------------- */

volatile __xdata uint8_t	__at (PRF_CHAN_REG)		PRF_CHAN;
volatile __xdata uint8_t	__at (PRF_TASK_CONF_REG)	PRF_TASK_CONF;
volatile __xdata uint8_t	__at (PRF_FIFO_CONF_REG)	PRF_FIFO_CONF;
volatile __xdata uint8_t	__at (PRF_PKT_CONF_REG)		PRF_PKT_CONF;
volatile __xdata uint8_t	__at (PRF_CRC_LEN_REG)		PRF_CRC_LEN;
volatile __xdata uint8_t	__at (PRF_RSSI_LIMIT_REG)	PRF_RSSI_LIMIT;
volatile __xdata uint16_t	__at (PRF_RSSI_COUNT_REG)	PRF_RSSI_COUNT;
volatile __xdata uint32_t	__at (PRF_CRC_INIT_REG)		PRF_CRC_INIT;
volatile __xdata uint8_t	__at (PRF_W_INIT_REG)		PRF_W_INIT;
volatile __xdata uint8_t	__at (PRF_RETRANS_CNT_REG)	PRF_RETRANS_CNT;
volatile __xdata uint16_t	__at (PRF_TX_DELAY_REG)		PRF_TX_DELAY;
volatile __xdata uint16_t	__at (PRF_RETRANS_DELAY_REG)  PRF_RETRANS_DELAY;
volatile __xdata uint16_t	__at (PRF_SEARCH_TIME_REG)	PRF_SEARCH_TIME;
volatile __xdata uint16_t	__at (PRF_RX_TX_TIME_REG)	PRF_RX_TX_TIME;
volatile __xdata uint16_t	__at (PRF_TX_RX_TIME_REG)	PRF_TX_RX_TIME;

volatile __xdata struct cc_addr_struct __at (PRF_ADDR_ENTRY0_REG) PRF_ADDR[8];

volatile __xdata uint8_t	__at (PRF_N_TX_REG)		PRF_N_TX;
volatile __xdata int8_t		__at (PRF_LAST_RSSI_REG)	PRF_LAST_RSSI;
volatile __xdata uint32_t	__at (PRF_LAST_DCOFF_REG)	PRF_LAST_DCOFF;
volatile __xdata uint8_t	__at (PRF_RADIO_CONF_REG)	PRF_RADIO_CONF;
volatile __xdata uint8_t	__at (PRF_ENDCAUSE_REG)		PRF_ENDCAUSE;

/* ----- RAM registers (Page 5) -------------------------------------------- */

volatile __xdata uint8_t  __at (PRFX_LAST_FREQEST_REG)	PRFX_LAST_FREQEST;
volatile __xdata uint8_t  __at (PRFX_RSSI_LIM_LOWER_REG) PRFX_RSSI_LIM_LOWER;
volatile __xdata uint8_t  __at (PRFX_RSSI_LIM_UPPER_REG) PRFX_RSSI_LIM_UPPER;
volatile __xdata uint8_t  __at (PRFX_RSSI_DIFF_REG)	PRFX_RSSI_DIFF;
volatile __xdata uint8_t  __at (PRFX_LNAGAIN_SAT_REG)	PRFX_LNAGAIN_SAT;
volatile __xdata uint16_t __at (PRFX_TONE_DURATION_REG)	PRFX_TONE_DURATION;
volatile __xdata uint16_t __at (PRFX_TONE_OFFSET_REG)	PRFX_TONE_OFFSET;

#endif /* !CC_CC25XX_RADIO_REGS_H */
