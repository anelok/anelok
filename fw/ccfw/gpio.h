/*
 * fw/ccfw/gpio.h - GPIO abstraction (CC25xx)
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_GPIO_H
#define	CC_GPIO_H

#define	GPIO_ID(port, bit)	port, bit

#define	__gpio_set_reg(reg, port, bit)	P##port##reg |= 1 << (bit)
#define	__gpio_clr_reg(reg, port, bit)	P##port##reg &= ~(1 << (bit))
#define	__gpio_test_reg(reg, port, bit)	((P##port##reg >> (bit)) & 1)

#define	gpio_set_reg(reg, id)	__gpio_set_reg(reg, id)
#define	gpio_clr_reg(reg, id)	__gpio_clr_reg(reg, id)
#define	gpio_test_reg(reg, id)	__gpio_test_reg(reg, id)

#define	gpio_output(id)	__gpio_set_reg(DIR, id)
#define	gpio_input(id)	__gpio_clr_reg(DIR, id)

#define	gpio_fn(id)	__gpio_set_reg(SEL, id)
#define	gpio_gpio(id)	__gpio_clr_reg(SEL, id)

#define	gpio_set(id)	__gpio_set_reg(, id)
#define	gpio_clr(id)	__gpio_clr_reg(, id)
#define	gpio_read(id)	__gpio_test_reg(, id)

#endif /* !CC_GPIO_H */
