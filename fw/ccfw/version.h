/*
 * fw/ccfw/version.h - Build version
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CCFW_VERSION_H
#define CCFW_VERSION_H

#include <stdint.h>


extern const char __xdata build_date[];
extern const uint8_t __xdata build_date_length;

#endif /* !CCFW_VERSION_H */
