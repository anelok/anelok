/*
 * fw/cc/sfr.h - CC2543 SFR definitions
 *
 * Written 2014-215 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CC_SFR_H
#define	CC_SFR_H

#include "cc2543.h"


/* ----- Clock control ----------------------------------------------------- */

__sfr __at (CLKCONCMD_REG) CLKCONCMD;
__sfr __at (CLKCONSTA_REG) CLKCONSTA;

/* ----- I/O Ports --------------------------------------------------------- */

__sfr __at (P0_REG) P0;
__sfr __at (P1_REG) P1;
__sfr __at (P2_REG) P2;
__sfr __at (PERCFG_REG) PERCFG;
__sfr __at (APCFG_REG) APCFG;
__sfr __at (P0SEL_REG) P0SEL;
__sfr __at (P1SEL_REG) P1SEL;
__sfr __at (P2SEL_REG) P2SEL;
__sfr __at (PPRI_REG) PPRI;
__sfr __at (P0DIR_REG) P0DIR;
__sfr __at (P1DIR_REG) P1DIR;
__sfr __at (P2DIR_REG) P2DIR;
__sfr __at (P0INP_REG) P0INP;
__sfr __at (P1INP_REG) P1INP;
__sfr __at (P2INP_REG) P2INP;
__sfr __at (PPULL_REG) PPULL;
__sfr __at (P0IFG_REG) P0IFG;
__sfr __at (P1IFG_REG) P1IFG;
__sfr __at (P2IFG_REG) P2IFG;
__sfr __at (PICTL_REG) PICTL;
__sfr __at (P0IEN_REG) P0IEN;
__sfr __at (P1IEN_REG) P1IEN;
__sfr __at (P2IEN_REG) P2IEN;
__sfr __at (PMUX_REG) PMUX;

/* ----- Timer 3 ----------------------------------------------------------- */

__sfr __at (T3CNT_REG) T3CNT;
__sfr __at (T3CTL_REG) T3CTL;
__sfr __at (T3CCTL0_REG) T3CCTL0;
__sfr __at (T3CC0_REG) T3CC0;
__sfr __at (T3CCTL1_REG) T3CCTL1;
__sfr __at (T3CC1_REG) T3CC1;

/* ----- USART 0 ----------------------------------------------------------- */

__sfr __at (U0CSR_REG) U0CSR;
__sfr __at (U0UCR_REG) U0UCR;
__sfr __at (U0GCR_REG) U0GCR;
__sfr __at (U0DBUF_REG) U0DBUF;
__sfr __at (U0BAUD_REG) U0BAUD;

/* ----- Power management -------------------------------------------------- */

__sfr __at (PCON_REG) PCON;
__sfr __at (SLEEPCMD_REG) SLEEPCMD;

/* ----- Interrupts -------------------------------------------------------- */

__sfr __at (IEN0_REG) IEN0;
__sfr __at (IEN1_REG) IEN1;
__sfr __at (IEN2_REG) IEN2;
__sfr __at (TCON_REG) TCON;
__sfr __at (S0CON_REG) S0CON;
__sfr __at (S1CON_REG) S1CON;
__sfr __at (IRCON_REG) IRCON;
__sfr __at (IRCON2_REG) IRCON2;
__sfr __at (IP0_REG) IP0;
__sfr __at (IP1_REG) IP1;

#endif /* !CC_SFR_H */
