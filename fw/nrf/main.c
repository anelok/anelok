#include <stdbool.h>
#include <stdint.h>

#include "board/board.h"
#include "regs.h"


#define	IN(n)	do NRF_GPIO->DIRCLR = 1 << (n); while (0)
#define	OUT(n)	do NRF_GPIO->DIRSET = 1 << (n); while (0)
#define	SET(n)	do NRF_GPIO->OUTSET = 1 << (n); while (0)
#define	CLR(n)	do NRF_GPIO->OUTCLR = 1 << (n); while (0)
#define	PORT(n)	((NRF_GPIO->PIN >> (n)) & 1)


int main(void)
{
	uint32_t i;
	bool on;

	CLR(LED);
	OUT(LED);

	on = 0;

	while (1) {
		if (on)
			SET(LED);
		else
			CLR(LED);
		on = !on;
		for (i = 0; i != 1000000; i++)
			asm("");
	}
	return 0;
}
