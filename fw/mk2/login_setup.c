/*
 * fw/mk2/ui_login_setup.c - administrative tasks on the Mk 2 login screen
 *
 * Written 2015-2017 by Werner Almesberger
 * Copyright 2015-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "misc.h"
#include "display.h"
#include "console.h"
#include "input.h"
#include "icon.h"
#include "textsel.h"
#include "ui.h"


static bool rotated = 0;


/* ----- Task: left/right -------------------------------------------------- */


static bool toggle_hand(void)
{
	rotated = !rotated;
	display_rotate(!rotated);
	input_rotate(rotated);
	icon_rotate(rotated);
	return 0;
}


/* ----- Task: version ----------------------------------------------------- */


#include "version.h"


static void do_version(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("#%d\n%s\n%x%s\n", build_number, build_date,
	    (unsigned) build_hash, build_dirty ? "+" : "");
	console_update();
}


static bool version(void)
{
	ui_show(do_version, NULL, login_setup_proceed);
	return 1;
}



/* ----- Task: identify ---------------------------------------------------- */


#include "devcfg.h"
#include "id.h"


static void do_identify(void *user)
{
	struct id id;

	id_get(&id);
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("\"%s\"\n",
	    devcfg->name ? devcfg->name : "unknown");
	console_printf("ID: %04X\n%08X\n%08X\n",
	    id.uidmh, (unsigned) id.uidml, (unsigned) id.uidl);
	console_update();
}


static bool identify(void)
{
	ui_show(do_identify, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: perfmon ----------------------------------------------------- */


#include "perfmon.h"


/*
 * There is no explicit visual feedback here because the idea is to place
 * perfmon calls into std.c, so toggling it would have an immediate visual
 * effect.
 */

static bool perfmon(void)
{
	perfmon_enable(!perfmon_enabled);
	perfmon_start(); /* clear the counter so that we don't show garbage */
	return 0;
}


/* ----- Task: Show calibration -------------------------------------------- */


#include "touch.h"


static void do_show_cal(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("A %u\nB %u\n\nStandby\nA %u\nB %u",
	    (unsigned) cal_a.avg, (unsigned) cal_b.avg,
	    (unsigned) standby_cal_a.avg, (unsigned) standby_cal_b.avg);
	console_update();
}


static bool show_cal(void)
{
	ui_show(do_show_cal, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: LED --------------------------------------------------------- */


#include "led.h"
#include "tick.h"


static bool toggle_led(void)
{
	led_toggle();
	return 0;
}


static bool blink_led(void)
{
	uint8_t i;

	for (i = 0; i != 10; i++) {
		msleep(200);
		led_toggle();
	}
	return 0;
}


/* ----- Task: USB --------------------------------------------------------- */


#include "vusb.h"
#include "usb-board.h"


static void do_usb(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("USB: %s\n",
	    vusb_sense() ? "powered" : "no power");
	console_printf("%s role", usb_a() ? "host" : "device");
}


static bool usb(void)
{
	ui_show(do_usb, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: type on HID ------------------------------------------------- */


#include "hid.h"
#include "hid-type.h"


static bool type(void)
{
	static struct hid_type_ctx ctx;

	hid_type(&ctx, "Hello, world !", NULL, NULL);
	return 0;
}


/* ----- Task: RF ---------------------------------------------------------- */


#include "ccdbg.h"
#include "cc.h"


static void do_rf(void *user)
{
	uint16_t id;

	console_window(main_x0, main_x1);
	console_clear();
	console_printf("RF: ");
	if (cc_on()) {
		if (!cc_acquire()) {
			console_printf("unresponsive\n");
		} else {
			id = ccdbg_get_chip_id(&ccdbg);
			cc_release();
			console_printf("CC25%02x\n    version 0x%02x\n",
			    id >> 8, id & 0xff);
		}
	} else {
		console_printf("off\n");
	}
	console_update();
}


static bool rf(void)
{
	ui_show(do_rf, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: RF ID ------------------------------------------------------- */


#include "op.h"
#include "rf.h"


static void do_rf_id(void *user)
{
	char buf[100];
	uint8_t len;

	rf_init();
	rf_send((uint8_t []) { OP_ID }, 0x81);
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("RF comm:\n");
	console_update();
	do {
		len = rf_poll(buf, sizeof(buf));
	} while (!len);
	buf[len] = 0;
	console_printf("%s\n", buf);
	console_update();
	while (rf_poll(buf, sizeof(buf)));
	console_printf("done\n");
	console_update();
}


static bool rf_id(void)
{
	ui_show(do_rf_id, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: RF TX ------------------------------------------------------- */


#include "op.h"
#include "rf.h"


static void do_rf_tx(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	console_printf("done\n");
	console_update();
}


static bool rf_tx(void)
{
	rf_send((uint8_t []) { OP_LISTEN,  2400 & 0xff, 2400 >> 8 }, 0x83);
	rf_send((uint8_t []) { OP_TX, 'h', 'e', 'l', 'l', 'o' }, 0x86);
	ui_show(do_rf_tx, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: RF RX ------------------------------------------------------- */


#include "op.h"
#include "rf.h"


static void do_rf_rx(void *user)
{
	char buf[100];
	uint8_t len, i;

	console_window(main_x0, main_x1);
	console_clear();
	console_printf("RF RX:\n");
	console_update();
	while (1) {
		rf_send((uint8_t []) { OP_RX }, 0x81);
		for (i = 0; i != 100; i++) {
			len = rf_poll(buf, sizeof(buf));
			if (len)
				goto out;
		}
	}
out:
	buf[len] = 0;
	console_printf("%s\n", buf);
	console_update();
	while (rf_poll(buf, sizeof(buf)));
	console_printf("done\n");
	console_update();
}


static bool rf_rx(void)
{
	rf_send((uint8_t []) { OP_LISTEN,  2400 & 0xff, 2400 >> 8 }, 0x83);
	ui_show(do_rf_rx, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: detect memory card ------------------------------------------ */


#include "fmt.h"
#include "mmc-hw.h"
#include "mmc.h"


static char clean_ascii(uint8_t c)
{
	return c >= ' ' && c < 127 ? c : '?';
}


/*
 * CID:
 * 0     Manufacturer ID (MID), apparently not publicly documented [1]
 * 1-2   OEM/Application ID (OID), two ASCII characters
 * 3-8   Product Name (PNM), 5 ASCII characters
 * 9     Product Revision (PRV), two BCD digits
 *
 * [1] http://www.bunniestudios.com/blog/?p=2297
 */

/*
 * Decoded format:
 * "OI PNM__ P.R\0"
 */

static void decode_cid(const uint8_t cid[16], char res[13])
{
	char *s = res;
	uint8_t i;

	for (i = 1; i != 3; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' ';
	for (i = 3; i != 8; i++)
		*s++ = clean_ascii(cid[i]);
	*s++ = ' ';
	print_number(s++, cid[9] >> 4, 1, 16);
	*s++ = '.';
	print_number(s++, cid[9] & 15, 1, 16);
	*s = 0;
}


static void id_memcard(void)
{
	uint8_t cid[16];
	char buf[13];


	if (!card_present()) {
		console_printf("No card");
		return;
	}

	if (!mmc_init()) {
		console_printf("Card failed");
		return;
	}
	if (!mmc_read_cid(cid)) {
		console_printf("Card failed");
		return;
	}
	decode_cid(cid, buf);
	console_printf("%s", buf);
}


static void do_memcard(void *user)
{
	console_window(main_x0, main_x1);
	console_clear();
	id_memcard();
	console_update();
}


static bool memcard(void)
{
	ui_show(do_memcard, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: dump memory card -------------------------------------------- */

/*
 * @@@ For design verification only. Note that the "Memory card" function has
 * to be invoked before trying to dump.
 */


static bool dump_once;


static void do_dump_card(void *user)
{
	uint8_t buf[512];
	uint32_t block;
	uint16_t i;
	uint8_t c;

	if (!dump_once)
		return;
	dump_once = 0;
	for (block = 0; block != 100; block++) {
		console_clear();
		console_printf("%u", (unsigned) block);
		console_update();
		msleep(100);
		if (!mmc_begin_read(block << 9)) {
			console_printf("mmc_begin_read failed");
			break;
		}
		for (i = 0; i != MMC_BLOCK; i++)
			buf[i] = mmc_read();
		mmc_end_read();
		msleep(100);
		for (i = 0; i != MMC_BLOCK; i++) {
			c = buf[i];
			if (!c || c == 0xff)
				break;
			console_char(c < ' ' || c >= 0x7f ? '?' : c);
		}
		console_update();
		msleep(200);
	}
	console_update();
}


static bool dump_card(void)
{
	dump_once = 1;
	ui_show(do_dump_card, NULL, login_setup_proceed);
	return 1;
}


/* ----- Task: leave UI ---------------------------------------------------- */


static bool do_leave_ui(void)
{
	leave_ui = 1;
	return 1;
}


/* ----- Task: panic ------------------------------------------------------- */


static bool do_panic(void)
{
	oops();
	return 1;
}


/* ----- Tasks ------------------------------------------------------------- */


const struct textsel_entry login_setup_tasks[] = {
	{ "Right/Left",		toggle_hand	},
	{ "Version",		version		},
	{ "Identify",		identify	},
	{ "Perfmon on/off",	perfmon		},
	{ "Show calibration",	show_cal	},
	{ "USB",		usb		},
	{ "Memory card",	memcard		},
	{ "Dump card",		dump_card	},
	{ "RF",			rf		},
	{ "RF ID",		rf_id		},
	{ "RF TX",		rf_tx		},
	{ "RF RX",		rf_rx		},
	{ "Type on HID",	type		},
	{ "LED on/off",		toggle_led	},
	{ "LED blink",		blink_led	},
	{ "Leave UI",		do_leave_ui	},
	{ "Panic",		do_panic	},
	{ "Reboot",		reset_cpu	},
	{ NULL,			NULL		},
};
