/*
 * fw/mk2/boot.c - DFU boot loader firmware for Anelok Mk 2 design
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb-board.h"
#include "flash.h"
#include "usb.h"
#include "dfu.h"
#include "dfu-wait.h"

#include "regs.h"
#include "misc.h"
#include "board.h"
#include "gpio.h"
#include "led.h"
#include "cc.h"


static void ports_off(void)
{
	/* power distribution */

	gpio_init_out(PWR_EN, 0);
	gpio_init_out(DISP_ON, 0);
	gpio_init_out(CARD_ON, 0);

	/* display */

	gpio_init_off(DISP_SCLK);
	gpio_init_off(DISP_SDIN);
	gpio_init_off(DISP_DnC);
	gpio_init_out(DISP_nRES, 0);
	gpio_init_off(DISP_nCS);

	/* capacitative sensor */

	gpio_init_off(CAP_A);
	gpio_init_off(CAP_B);

	/* USB */

	gpio_init_off(USB_ID);
	gpio_init_off(USB_nSENSE);

	/* memory card */

	gpio_init_off(CARD_DAT2);
	gpio_init_off(CARD_DAT3);
	gpio_init_off(CARD_CMD);
	gpio_init_off(CARD_CLK);
	gpio_init_off(CARD_DAT0);
	gpio_init_off(CARD_DAT1);

	gpio_init_off(CARD_SW);

	/* RF */

	gpio_init_off(RF_MOSI);
	gpio_init_off(RF_MISO);
	gpio_init_off(RF_SCLK);
	gpio_init_off(RF_nSEL);
	gpio_init_off(RF_DD);
	gpio_init_off(RF_DC);
	gpio_init_off(RF_nRESET);
	gpio_init_off(VRF);
	gpio_init_off(RF_nINT);
	gpio_init_off(RF_nREQ);

	/* system pins */

	gpio_init_off(GPIO_ID(A, 0));	/* SWD_CLK */
	gpio_init_off(GPIO_ID(A, 3));	/* SWD_DIO */
	gpio_init_off(GPIO_ID(A, 4));	/* NMI_b */

	/* unused pins */

	gpio_init_off(GPIO_ID(E, 20));	/* hardwired to ground */
	gpio_init_off(GPIO_ID(D, 1));	/* unconnected */
}


static void (**app_vec)(void) = (void (**)(void)) (APP_BASE | 4);


int main(void)
{
	bool cc;

	/*
	 * @@@ we seem to reset a few times before the system stabilized.
	 * find out why.
	 */

	ports_off();
	led_init();

	cc_init();
	cc = cc_on();
	if (cc)
		cc_acquire();

	usb_init();
	usb_begin_device();
	dfu_init();
	flash_init(); /* must follow dfu_init */
	while (dfu_wait());
	usb_end_device();

	if (cc)
		cc_release();

	(*app_vec)();

	reset_cpu();
}
