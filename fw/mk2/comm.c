/*
 * fw/mk2/comm.c - Communication handler for Mk 2
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>

#include "event.h"
#include "input.h"
#include "account.h"
#include "hid-type.h"
#include "comm.h"


enum comm comm = comm_none;


static volatile bool comm_busy = 0;


static struct hid_type_ctx type;


static void clear_busy(void *user)
{
	comm_busy = 0;
	event_set_interrupt(ev_refresh);
}


/*
 * @@@ We have a race condition here: account_text(rec) is consumed
 * asynchronously after comm_send returns. This means that, if the record is
 * changed rapidly (or if sending takes unusually long), we may send garbage.
 *
 * We mitigate the race in hid_type by immediately calculating the length, so
 * that we don't depend on the terminating NUL, which the current
 * implementation inserts (and removes) ad hoc when iterating over the account
 * record.
 *
 * A correct solution may use one of the following approaches:
 * - buffer the string buffer,
 * - "lock" the account record to ensure data remains valid until used,
 * - block the user interface until the * string is sent,
 * - use the callback to reopen the acccount record when more data is needed,
 * etc.
 */


bool comm_send(struct account_record *rec)
{
	if (account_field_type(rec) != field_pw)
		return 0;
	switch (comm) {
	case comm_hid:
		comm_busy = 1;
		hid_type(&type, account_text(rec), clear_busy, NULL);
		return 1;
	default:
		return 0;
	}
}


enum comm comm_mode(struct account_record *rec)
{
	if (comm_busy)
		return comm_none;
	return comm;
}


void comm_update(enum comm new)
{
	comm = new;
	input_refresh();
}
