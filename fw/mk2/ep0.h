/*
 * fw/mk2/ep0.h - EP0 extension protocol
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EP0_H
#define	EP0_H

#include <stdint.h>


#ifndef	USB_TYPE_VENDOR
#define	USB_TYPE_VENDOR	0x40
#endif

#ifndef	USB_DIR_IN
#define	USB_DIR_IN	0x80
#endif

#ifndef	USB_DIR_OUT
#define	USB_DIR_OUT	0x00
#endif


#define	ANELOK_TOUCH	(USB_TYPE_VENDOR | USB_DIR_IN  | 0 << 8)
#define	ANELOK_MODE	(USB_TYPE_VENDOR | USB_DIR_IN  | 1 << 8)
#define	ANELOK_SET_ADDR	(USB_TYPE_VENDOR | USB_DIR_OUT | 2 << 8)
#define	ANELOK_GET_ADDR	(USB_TYPE_VENDOR | USB_DIR_IN  | 2 << 8)
#define	ANELOK_PEEK	(USB_TYPE_VENDOR | USB_DIR_IN  | 3 << 8)
#define	ANELOK_POKE	(USB_TYPE_VENDOR | USB_DIR_OUT | 3 << 8)
#define	ANELOK_READ_DB	(USB_TYPE_VENDOR | USB_DIR_IN  | 4 << 8)
#define	ANELOK_WRITE_DB	(USB_TYPE_VENDOR | USB_DIR_OUT | 4 << 8)
#define	ANELOK_CANCEL	(USB_TYPE_VENDOR | USB_DIR_IN  | 5 << 8)

extern uint8_t anelok_mode; /* for development */


void ep0_init(void);

#endif /* !EP0_H */
