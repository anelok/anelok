/*
 * fw/mk2/tact.c - Tactile switches
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "gpio.h"
#include "display.h"	/* for FB_Y */
#include "touch.h"	/* for prototypes */
#include "qa.h"

#include "tact.h"


/*
 * Button   A B C
 * ------   -----
 * Top	    0 1 1
 * Bottom   1 1 0
 * Middle   1 0 1
 * Multiple 0 0 0
 *
 * "A" and "B" are the respective channels when both are inputs with pull-up.
 * "C" is CAP_B with CAP_A driven to GND.
 */

static enum tact_button do_poll(void)
{
	static bool toggle = 0;
	bool a, c;

	if (!toggle) {
		a = gpio_read(CAP_A);

		if (!gpio_read(CAP_B))
			return a ? tact_bottom : tact_multi;
		if (!a)
			return tact_top;

		/* Try Middle */

		gpio_output(CAP_A);
		gpio_clr(CAP_A);

		toggle = 1;

		return tact_delay;
	}


	/*
	 * We have a race here: if Bottom is pressed after reading CAP_B for
	 * the first time, it will be interpreted as Middle. We fix this in
	 * tact_poll (below) by requiring consecutive results to be identical.
	 */

	/*
	 * We need to wait a while after configuration changes before we can
	 * read the new value. (Experiments showed a delay of 100 us to work
	 * well, but a delay of 20 us to be highly unreliable.)
	 *
	 * We alternate modes, such that there will be a few ms between polls,
	 * thus implicitly providing the necessary delay.
	 */

	c = gpio_read(CAP_B);
	gpio_input(CAP_A);

	toggle = 0;

	return c ? tact_none : tact_middle;
}


enum tact_button tact_poll(void)
{
	static enum tact_button last = tact_none;
	enum tact_button res;

	gpio_begin(CAP_A);
	gpio_begin(CAP_B);
	res = do_poll();
	gpio_end(CAP_A);
	gpio_end(CAP_B);

	if (res == tact_delay)
		return last;

	/* filter out race condition described above */
	if (res == last)
		return res;
	last = res;
	return tact_none;
}


void tact_init(void)
{
	gpio_init_in(CAP_A, 1);
	gpio_init_in(CAP_B, 1);
	gpio_clr(CAP_A);
}


/* Touch API compatibility (just temporarily) */


bool tact_touch(uint8_t *pos)
{
	enum tact_button tact;

	tact = tact_poll();
	switch (tact) {
	case tact_none:
	case tact_multi:
		return 0;
	case tact_top:
		*pos = FB_Y - 1;
		return 1;
	case tact_middle:
		*pos = FB_Y >> 1;
		return 1;
	case tact_bottom:
		*pos = 0;
		return 1;
	default:
		panic();
	}
	panic();
}
