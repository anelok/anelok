/*
 * fw/mk2/touch.h - Touch sensing
 *
 * Written 2014-2015 by Werner Almesberger
 * Copyright 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TOUCH_H
#define	TOUCH_H

#include <stdbool.h>
#include <stdint.h>


struct cal {
	uint32_t avg;
	uint32_t sum;
	uint32_t n;
};


extern struct cal cal_a, cal_b;
extern struct cal standby_cal_a, standby_cal_b;


void touch_read(uint16_t res[4]);
bool touch_filter(uint8_t *pos);
void touch_calm(bool power_drop);
void touch_init(void);

#endif /* !TOUCH_H */
