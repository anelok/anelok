/*
 * fw/mk2/power.c - Power management (3.3 V rail)
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "gpio.h"
#include "power.h"


/* ----- Display power ----------------------------------------------------- */


void power_disp(bool on)
{
	static uint8_t count;

	gpio_begin(DISP_ON);
	if (on) {
		if (!count++) {
			power_3v3(1);
			gpio_set(DISP_ON);
		}
	} else {
		if (!--count) {
			gpio_clr(DISP_ON);
			power_3v3(0);
		}
	}
	gpio_end(DISP_ON);
}


void power_disp_force_on(void)
{
	power_3v3_force_on();
	gpio_init_out(DISP_ON, 1);
}


/* ----- Memory card power ------------------------------------------------- */


void power_card(bool on)
{
	static uint8_t count;

	gpio_begin(CARD_ON);
	if (on) {
		if (!count++) {
			power_3v3(1);
			gpio_set(CARD_ON);
		}
	} else {
		if (!--count) {
			gpio_clr(CARD_ON);
			power_3v3(0);
		}
	}
	gpio_end(CARD_ON);
}


/* ----- Initialize power distribution ------------------------------------- */


void power_init(void)
{
	power_boost_init();

	/* disable display power  */
	gpio_init_out(DISP_ON, 0);

	/* disable memory card power  */
	gpio_init_out(CARD_ON, 0);
}
