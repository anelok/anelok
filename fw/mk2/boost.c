/*
 * fw/mk2/boost.c - Boost converter control
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "board.h"
#include "tick.h"
#include "gpio.h"
#include "power.h"


static uint8_t count;


void power_3v3(bool on)
{

	gpio_begin(PWR_EN);
	if (on) {
		if (!count++)
			gpio_set(PWR_EN);
		msleep(1);
	} else {
		REQUIRE(count);
		if (!--count)
			gpio_clr(PWR_EN);
	}
	gpio_end(PWR_EN);
}


void power_3v3_force_on(void)
{
	gpio_init_out(PWR_EN, 1);
}


void power_boost_init(void)
{
	gpio_init_out(PWR_EN, 0);
	count = 0;
}
