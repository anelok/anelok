/*
 * fw/mk2/main.c - Experimental firmware for Anelok Mk 2 design
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb.h"
#include "usb-board.h"

#include "regs.h"
#include "misc.h"
#include "sleep.h"
#include "irq.h"
#include "event.h"
#include "ep0.h"
#include "touch.h"
#include "input.h"
#include "board.h"
#include "devcfg.h"
#include "gpio.h"
#include "clock.h"
#include "power.h"
#include "tick.h"
#include "led.h"
#include "display.h"
#include "console.h"
#include "vusb.h"
#include "hid.h"
#include "comm.h"
#include "pm.h"
#include "ui.h"
#include "cc.h"
#include "rf.h"


/* ----- Low-current test -------------------------------------------------- */


static void __attribute__((noreturn)) low_current(void)
{
	/* disable USB voltage regulator */

	SIM_SOPT1CFG = SIM_SOPT1CFG_URWE_MASK;
	SIM_SOPT1 = 0;

	gpio_init_off(CAP_A);
	gpio_init_off(CAP_B);

//	ports_off();

//	clock_xtal_32k_fll();
	clock_xtal_32k();
//	clkout_osc();
//	clkout_bus();

	/*
	 * Isys about 2.06 uA
	 * added R10 (flux not cleaned): about 3.3 uA
	 * added Q2 (not cleaned): about 3.3 uA
	 * added Q3:
	 *   on Vbat (high load):
	 *	Ibat = 9.92 mA, Vbat = 2.951 V, Vsys = 2.943 V, Vgs = 0.141 V
	 *	-> Q3.R(on) = 0.8 Ohm
	 *   on Vbat (low current):
	 *	Ibat = 3.3 uA
	 *   on Vusb, measuring current into Vbat:
	 *	Vbat	Ibat
	 *	 0  V	-282    mA  (shorted)
	 *	3.0 V	- 50    nA
	 *	2.9 V	-400	nA (varies between 260 and 560 nA)
	 *	2.8 V	-  7	uA (varies between 5 and 8 uA)
	 *	2.7 V	- 93	uA
	 *	2.6 V	-  2.22 mA
	 *	2.5 V   -  2.68 mA  (reverse current limited by power supply)
	 *	2.0 V   -  2.68 mA  (reverse current limited by power supply)
	 * added U2 (flux not cleaned):
	 *	U2 disabled:	Isys = 5.8 uA
	 *	U2 enabled:	Isys = 385 uA, Vout = 3.40 V
	 *	Note: idle current when disabled is higher than expected.
	 *	U2 should only add about 1 uA. Maximum current is 500 uA when
	 * 	enabled, which is consistent with the measurements.
	 * added Q3, Q4, R11, Q5, Q6, R18 (R11, R18 as 680 kOhm, not cleaned)
	 *	U2		display		card		Isys
	 *	(state)		state	I@100R	state	I@100R	(unloaded)
	 *	-------		-----	-------	-----	-------	----------
	 *	disabled	off		off		  5.9 uA
	 *	enabled		off	 15 uA	off	 15 uA	385   uA
	 *	enabled		on	 31 mA	off	 14 uA	391   uA
	 *	enabled		off	 16 uA	on	 31 mA	392   uA
	 *	enabled		on	 30 mA	on	 30 mA	398   uA
	 *
	 *	The 6 uA difference per activation corresponds roughly to the
	 *	current through the pull-up resistor: 3.0 V / 680 kOhm = 4.4 uA
	 *
	 *	The comparably large off-currents (15 uA vs. a nominal maximum
	 *	Idss of 100 nA) seem to be measurement errors: loading DISP_VDD
	 *	and CARD_VDD does not perceptably increase Isys (measured with
	 *	a very accurate meter) despite the high indicated leakage
	 *	current.
	 *
	 * fixed SW1 connection, cleaned board, low-power test:
	 *	with rfkill:	5.5 uA
	 *	without rfkill:	3.1 mA	(CC2543 idling on RCOSC)
	 *	without rfkill:	5.8 uA	(CC2543 in PM3, all ports pull down)
	 */

	gpio_init_out(LED, 0);
	gpio_begin(LED);
	PORTB_PCR0 |= PORT_PCR_DSE_MASK; /* enable high drive strength */

	while (1) {
		gpio_set(LED);
		msleep(1000);
		gpio_clr(LED);
		msleep(29000);
	}
}


/* ----- Run the UI -------------------------------------------------------- */


static void run_ui(void)
{
	bool usb_on = 0;
	uint8_t count_down = 0;

	power_init();
	cc_init();	/* @@@ also fix boot loader */

	vusb_init();
	if (vusb_sense())
		count_down = 5;

	msleep(10);
	power_3v3(1);
	msleep(10);

	power_disp(1);
	display_init();
	display_rotate(1);
	console_init();
	touch_init();
	rf_init();

	ui_off();
	ui_init();
	while (ui()) {
		if (event_consume(ev_usb_plug)) {
			count_down = 5;
			continue;
		}
		if (count_down && !--count_down) {
			allow_stop = 0;
			usb_init();
			usb_begin_device();
			ep0_init();
			usb_device_interrupts(1);
			usb_on = 1;
		}
		if (event_consume(ev_usb_unplug)) {
			/* @@@ should also abort on-going transfers */
			if (usb_on) {
				usb_device_interrupts(0);
				usb_end_device();
			}
			allow_stop = 1;
			count_down = 0;
			usb_on = 0;
			comm_update(comm_none);
		}

		if (event_consume(ev_usb_hid_setup)) {
			comm_update(comm_hid);
		}
		if (event_consume(ev_console)) {
			pm_busy();
			console_update();
		}
	}
}


/* ----- Operating without USB --------------------------------------------- */


#define	EWMA_SHIFT	8
#define	HOLD		20


static void standalone(void)
{
	uint32_t avg[4] = { 0, };
	uint16_t res[4];
	int32_t diff[4];
	int32_t max[4];
	uint8_t hold[4];
	uint8_t i;
	uint16_t first = 1000 * 4;
	uint16_t loops = 2000; // about 30 s

	power_init();
	power_disp(1);
	display_init();
	display_rotate(1);
	console_init();
	touch_init();

	for (i = 0; i != 4; i++) {
		max[i] = 0;
		hold[i] = 0;
	}
	while (loops--) {
		touch_read(res);
		console_clear();
		for (i = 0; i != 4; i++) {
			if (first) {
				avg[i] = (uint32_t) res[i] << EWMA_SHIFT;
				first--;
			}
			avg[i] = avg[i] - (avg[i] >> EWMA_SHIFT) + res[i];
			diff[i] = (int32_t) res[i] -
			    (uint32_t) (avg[i] >> EWMA_SHIFT);
			if (max[i] < diff[i]) {
				max[i] = diff[i];
				hold[i] = 0;
			} else {
				if (hold[i] < HOLD)
					hold[i]++;
				else
					max[i] = diff[i];
			}
			console_printf("%u\t%u\t%d\n", (unsigned) res[i],
			    (unsigned) avg[i] >> EWMA_SHIFT, (int) max[i]);
		}
		console_update();
		first = 0;
	}
	reset_cpu();
}


/* ----- Main loop --------------------------------------------------------- */


int main(void)
{
	devcfg_init();

	/* do these two before using msleep */
	tick_init();
	irq_enable();

	msleep(1);	/* let host notice we've detached */

	gpio_init_in(USB_nSENSE, 1);

	run_ui();

	if (gpio_read(USB_nSENSE))
		standalone();

	low_current();
}
