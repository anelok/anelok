/*
 * fw/mk2/ep0.c - EP0 extension protocol
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb.h"
#include "dfu.h"

#include "board.h"
#include "misc.h"
#include "event.h"
#include "touch.h"
#include "hid.h"
#include "dbconf.h"
#include "flash.h"
#include "ep0.h"


uint8_t anelok_mode = 0;	/* 0 = keep looping */


/* ----- ANELOK_PEEK / ANELOK_POKE ----------------------------------------- */


static uint32_t *addr;
static uint32_t data;


static void poke(void *user)
{
	*addr = data;
}


/* ----- ANELOK_READ_DB / ANELOK_WRITE_DB ---------------------------------- */


static uint8_t buf[EP0_SIZE];
static uint16_t flash_db_size;


static bool check_area(uint16_t start, uint16_t len)
{
	if ((start | len) & 3)
		return 0;
	if (len > DB_SIZE || start > DB_SIZE - len)
		return 0;
	if (len > sizeof(buf))
		return 0;
	return 1;
}


static bool prepare_read(uint16_t start, uint16_t len)
{
	if (!check_area(start, len))
		return 0;
	memcpy(buf, (void *) ((uint32_t) start + DB_START), len);
	return 1;
}


static bool prepare_write(uint16_t start, uint16_t len)
{
	if (!check_area(start, len))
		return 0;
	flash_start_at((uint32_t) start + DB_START);
	flash_db_size = len;
	return 1;
}


static void write_db(void *user)
{
	plat_flash_ops.write(buf, flash_db_size);
	plat_flash_ops.end_write();
}


/* ----- Anelok-specific control transfers --------------------------------- */


static bool anelok_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;
	static uint16_t touch_res[4];

	switch (req) {
	case ANELOK_TOUCH:
		touch_read(touch_res);
		usb_send(&eps[0], touch_res, 8, NULL, NULL);
		return 1;
	case ANELOK_MODE:
		anelok_mode = setup->wValue;
		return 1;
	case ANELOK_SET_ADDR:
		usb_recv(&eps[0], (void *) &addr, sizeof(addr), NULL, NULL);
		return 1;
	case ANELOK_GET_ADDR:
		usb_send(&eps[0], &addr, sizeof(addr), NULL, NULL);
		return 1;
	case ANELOK_PEEK:
		data = *addr;
		usb_send(&eps[0], &data, sizeof(data), NULL, NULL);
		return 1;
	case ANELOK_POKE:
		usb_recv(&eps[0], (void *) &data, sizeof(data), poke, NULL);
		return 1;
	case ANELOK_READ_DB:
		if (!prepare_read(setup->wValue, setup->wLength))
			return 0;
		usb_send(&eps[0], buf, setup->wLength, NULL, NULL);
		return 1;
	case ANELOK_WRITE_DB:
		if (!prepare_write(setup->wValue, setup->wLength))
			return 0;
		usb_recv(&eps[0], buf, setup->wLength, write_db, NULL);
		return 1;
	case ANELOK_CANCEL:
		/* @@@ cancel or end session */
		return 1;
	default:
		return 0;
	}
}


/* ----- Descriptor / Control transfer dispatching ------------------------- */


static bool dfu_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;

	switch (req) {
	case DFU_TO_DEV(DFU_DETACH):
		/* @@@ should use wTimeout */
		dfu.state = appDETACH;
		return 1;
	default:
		return dfu_setup_common(setup);
	}
}


static bool my_setup(const struct setup_request *setup)
{
	switch (setup->wIndex) {
	case 0:
		return anelok_setup(setup);
	case 1:
		if (hid_setup(setup)) {
			event_set_interrupt(ev_usb_hid_setup);
			return 1;
		}
		return 0;
	case 2:
		return dfu_setup(setup);
	default:
		return 0;
	}
}


static bool my_get_descriptor(uint8_t type, uint8_t index,
    const uint8_t **reply, uint8_t *size)
{
	if (hid_get_descriptor(type, index, reply, size))
		return 1;
	return dfu_my_descr(type, index, reply, size);
}


static void my_reset(void)
{
	if (dfu.state == appDETACH)
		reset_cpu();
}


void ep0_init(void)
{
	dfu.state = appIDLE;
	user_setup = my_setup;
	user_get_descriptor = my_get_descriptor;
	user_reset = my_reset;
}
