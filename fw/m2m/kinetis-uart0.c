/*
 * fw/m2m/kinetis-uart0.h - Kinetis UART0 low-level driver
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "regs.h"
#include "gpio.h"
#include "uart.h"
#include "kinetis-uart0.h"


static bool uart0_may_tx(void *ctx)
{
	return UART0_S1 & UART0_S1_TDRE_MASK;
}


static void uart0_tx(void *ctx, uint8_t data)
{
	UART0_D = data;
}


static void uart0_send_break(void *ctx)
{
	UART0_C2 |= UART0_C2_SBK_MASK;
	UART0_C2 &= ~UART0_C2_SBK_MASK;
}


#define	UART0_S1_ERRORS	\
    (UART0_S1_OR_MASK | UART0_S1_NF_MASK | UART0_S1_FE_MASK)


static bool uart0_poll_rx(void *ctx, uint8_t *v)
{
	uint8_t status = UART0_S1;

	if (!(UART0_S1 & UART0_S1_RDRF_MASK))
		return 0;
	if (status & UART0_S1_ERRORS)
		return 0;
	*v = UART0_D;
	return 1;
}


static bool uart0_poll_error(void *ctx, bool *is_break)
{
	uint8_t errors = UART0_S1 & UART0_S1_ERRORS;
	uint8_t got;

	if (!errors)
		return 0;
	UART0_S1 = errors;
	got = UART0_D;	/* discard garbage */
	if ((errors & UART0_S1_FE_MASK) && !got && is_break)
		*is_break = 1;
	return 1;
}


struct uart_ops kinetis_uart0_ops = {
	.poll_rx	= uart0_poll_rx,
	.poll_error	= uart0_poll_error,
	.may_tx		= uart0_may_tx,
	.tx		= uart0_tx,
	.send_break	= uart0_send_break,
};


void kinetis_uart0_init(void)
{
	/*
	 * Baud rate is clock / SBR / (OSR + 1)
	 *
	 * clock = 48 MHz (MCGFLLCLK clock or MCGPLLCLK/2, 48 MHz)
	 * UART0_C4[OSR] reset default is 15
	 * Thus, rate = 3 MHz / SBR
	 *
	 * Try rate = 100 kHz, SBR = 30
	 * (Note: after reset, it's just 43.7 kHz)
	 */
	SIM_SOPT2 |= SIM_SOPT2_UART0SRC(1);	// FLL or PLL/2
	SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;

	UART0_BDH = 0;
	UART0_BDL = 30;

	UART0_C2 =
	    UART_C2_TE_MASK	// enable transmitter
	    | UART_C2_RE_MASK;	// enable receiver

	gpio_init_off(GPIO_ID(A, 0));
	gpio_init_off(GPIO_ID(A, 3));

	gpio_init_out(MCU_SxCx_TX, 1);
	gpio_begin(MCU_SxCx_TX);
	gpio_fn(MCU_SxCx_TX, 2);
	gpio_end(MCU_SxCx_TX);

	gpio_init_in(MCU_SxCx_RX, 1);
	gpio_begin(MCU_SxCx_RX);
	gpio_fn(MCU_SxCx_RX, 2);
	gpio_end(MCU_SxCx_RX);
}
