/*
 * fw/m2m/uart.h - Common UART operations
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef UART_H
#define	UART_H

#include <stdbool.h>
#include <stdint.h>


struct uart_ops {
	bool (*poll_rx)(void *ctx, uint8_t *data);
	bool (*poll_error)(void *ctx, bool *is_break);
	bool (*may_tx)(void *ctx);
	void (*tx)(void *ctx, uint8_t data);
	void (*send_break)(void *ctx);
};

#endif /* UART_H */
