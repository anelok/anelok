/*
 * fw/m2m/m2m.c - Machine-to-machine communication
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * We make heavy use of callbacks, which may cause re-entrancy. To avoid
 * trouble, changes to state or other protocol variables should be performed
 * before calling anything that may call back.
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "uart.h"
#include "m2m.h"


enum {
	ACK	= 0x80,
	NAK	= 0x40,
	TYPE	= 0x3f
};

/*
 * Timeout must be longer than maximum wakeup time, or any latency between
 * polls.
 */

#define	TIMEOUT_MS	1000


/* ----- Tracing ----------------------------------------------------------- */


#ifdef TRACING

#include "tracing.h"


#define TX_STATE(s) \
	do { trace_tx_state(m2m, #s, __LINE__); m2m->tx_state = M2M_##s; } \
	    while (0)
#define RX_STATE(s) \
	do { trace_rx_state(m2m, #s, __LINE__); m2m->rx_state = M2M_##s; } \
	    while (0)
#define LINK_STATE(s)\
	 do { trace_link_state(m2m, #s, __LINE__); \
	    m2m->link_state = M2M_##s; } while (0)

#define	TRACE_M2M	trace_m2m

#else /* TRACING */

#define	TX_STATE(s)	m2m->tx_state = M2M_##s
#define	RX_STATE(s)	m2m->rx_state = M2M_##s
#define	LINK_STATE(s)	m2m->link_state = M2M_##s

#define	TRACE_M2M(m2m, fmt, ...)

#endif /* !TRACING */


/* ----- Timer ------------------------------------------------------------- */


static void start_timer(const struct m2m *m2m)
{
	m2m->timer_ops->start(m2m->timer_ctx, TIMEOUT_MS);
}


static bool poll_timer(const struct m2m *m2m)
{
	return m2m->timer_ops->poll(m2m->timer_ctx);
}


/* ----- UART helper functions --------------------------------------------- */


static void uart_tx(struct m2m *m2m, uint8_t data)
{
	m2m->uart_ops->tx(m2m->uart_ctx, data);
}


static bool uart_may_tx(struct m2m *m2m)
{
	return m2m->uart_ops->may_tx(m2m->uart_ctx);
}


static void send_header(struct m2m *m2m)
{
	uint8_t data =
	    (m2m->out_ack ? 0 : ACK) | (m2m->out_nak ? 0 : NAK) | m2m->type;

	m2m->out_ack = m2m->out_nak = 0;
	m2m->type = 0;
	uart_tx(m2m, data);
}


static void send_break(struct m2m *m2m)
{
	m2m->uart_ops->send_break(m2m->uart_ctx);
}


/* ----- Link -------------------------------------------------------------- */


static void link_reset(struct m2m *m2m)
{
	enum m2m_tx_state state;

	TRACE_M2M(m2m, "link_reset");

	switch (m2m->rx_state) {
	case M2M_RX_BEGIN:
		RX_STATE(RX_IDLE);
		break;
	case M2M_RX:
		m2m->out_ack = m2m->out_nak = 0;
		RX_STATE(RX_IDLE);
		m2m->rx_ops->fail(m2m->rx_ctx);
		break;
	default:
		break;
	}

	state = m2m->tx_state;
	TX_STATE(TX_IDLE);
	switch (state) {
	case M2M_TX_IDLE:
		break;
	case M2M_TX_WAKE:
	case M2M_TX_WOKEN:
		if (!m2m->type)
			break;
		/* fall through */
	case M2M_TXING:
	case M2M_ACK_WAIT:
	case M2M_TX_FLUSH:
		m2m->tx_ops->fail(m2m->tx_ctx);
		break;
	default:
		panic();
	}
}


static void link_awake(struct m2m *m2m)
{
	TRACE_M2M(m2m, "link_awake");

	switch (m2m->link_state) {
	case M2M_STANDBY:
		start_timer(m2m);
		LINK_STATE(READY);
		send_break(m2m);
		break;
	case M2M_WAKE:
		LINK_STATE(READY);
		break;
	case M2M_READY:
		break;
	default:
		panic();
	}

	switch (m2m->tx_state) {
	case M2M_TX_IDLE:
	case M2M_TX_WAKE:
		if (!m2m->out_ack && !m2m->out_nak && !m2m->type) {
			TX_STATE(TX_IDLE);
			break;
		}
		if (uart_may_tx(m2m)) {
			TX_STATE(TXING);
			send_header(m2m);
		} else {
			TX_STATE(TX_WOKEN);
		}
		break;
	default:
		break;
	}
}


static void break_received(struct m2m *m2m)
{
	TRACE_M2M(m2m, "break_received");

	link_awake(m2m);

	switch (m2m->rx_state) {
	case M2M_RX_IDLE:
	case M2M_RX_BEGIN:
		RX_STATE(RX_BEGIN);
		break;
	case M2M_RX:
		RX_STATE(RX_IDLE);
		m2m->rx_ops->fail(m2m->rx_ctx);
		break;
	default:
		break;
	}
}


static void link_wake(struct m2m *m2m)
{
	TRACE_M2M(m2m, "link_wake");

	switch (m2m->link_state) {
	case M2M_STANDBY:
		start_timer(m2m);
		LINK_STATE(WAKE);
		send_break(m2m);
		break;
	case M2M_READY:
		send_break(m2m);
		link_awake(m2m);
		break;
	default:
		panic();
	}
}


/* ----- TX ---------------------------------------------------------------- */


static bool tx_tx(struct m2m *m2m)
{
	uint8_t data;

	switch (m2m->tx_state) {
	case M2M_TX_WOKEN:
		if (m2m->type)
			TX_STATE(TXING);
		else
			TX_STATE(TX_IDLE);
		send_header(m2m);
		break;	
	case M2M_TXING:
		data = m2m->tx_ops->tx(m2m->tx_ctx);
		if (m2m->tx_state == M2M_TXING)
			uart_tx(m2m, data);
		break;
	case M2M_TX_FLUSH:
		REQUIRE(!m2m->type);
		TX_STATE(ACK_WAIT);
		send_header(m2m);
		break;
	default:
		return 0;
	}
	return 1;
}


void m2m_tx_setup(struct m2m *m2m, const struct m2m_tx_ops *ops, void *ctx)
{
	m2m->tx_ops = ops;
	m2m->tx_ctx = ctx;
}


void m2m_send(struct m2m *m2m, uint8_t type)
{
	REQUIRE(type);
	switch (m2m->tx_state) {
	case M2M_TX_IDLE:
		TX_STATE(TX_WAKE);
		m2m->in_ack = m2m->in_nak = 0;
		m2m->type = type;
		link_wake(m2m);
		break;
	case M2M_TX_WOKEN:
		REQUIRE(!m2m->type);
		m2m->in_ack = m2m->in_nak = 0;
		m2m->type = type;
		break;
	default:
		panic();
	}
}


static void kick_tx_out(struct m2m *m2m)
{
	TRACE_M2M(m2m, "kick_tx_out");

	REQUIRE(m2m->link_state == M2M_READY);
	REQUIRE(m2m->tx_state == M2M_TX_IDLE);
	link_wake(m2m);
}


/* call if we may have in_ack or in_nak */

static void kick_tx_in(struct m2m *m2m)
{
	TRACE_M2M(m2m, "kick_tx_in");

	switch (m2m->tx_state) {
	case M2M_TXING:
		if (m2m->in_ack || m2m->in_nak) {
			TX_STATE(TX_IDLE);
			m2m->tx_ops->fail(m2m->tx_ctx);
		}
		break;
	case M2M_ACK_WAIT:
		if (m2m->in_ack) {
			TX_STATE(TX_IDLE);
			m2m->tx_ops->done(m2m->tx_ctx);
		} else if (m2m->in_nak) {
			TX_STATE(TX_IDLE);
			m2m->tx_ops->fail(m2m->tx_ctx);
		}
		break;
	case M2M_TX_FLUSH:
		if (m2m->in_ack) {
			TX_STATE(TX_WOKEN);
			m2m->tx_ops->done(m2m->tx_ctx);
		} else if (m2m->in_nak) {
			TX_STATE(TX_WOKEN);
			m2m->tx_ops->fail(m2m->tx_ctx);
		}
		break;
	default:
		break;
	}
}


void m2m_tx_end(struct m2m *m2m)
{
	switch (m2m->tx_state) {
	case M2M_TXING:
		TX_STATE(ACK_WAIT);
		kick_tx_in(m2m);
		break;
	default:
		panic();
	}
}


void m2m_tx_cancel(struct m2m *m2m)
{
	switch (m2m->tx_state) {
	case M2M_TX_WAKE:
	case M2M_TX_WOKEN:
		m2m->type = 0;
		break;
	case M2M_TXING:
	case M2M_ACK_WAIT:
		TX_STATE(TX_IDLE);
		break;
	case M2M_TX_FLUSH:
		TX_STATE(TX_WOKEN);
		break;
	default:
		panic();
	}
}


/* ----- RX ---------------------------------------------------------------- */


static void rx_rx(struct m2m *m2m, uint8_t data)
{
	uint8_t type = data & TYPE;

	switch (m2m->rx_state) {
	case M2M_RX_IDLE:
		break;
	case M2M_RX_BEGIN:
		if (type)
			RX_STATE(RX);
		m2m->in_ack = !(data & ACK);
		m2m->in_nak = !(data & NAK);
		kick_tx_in(m2m);
		if (type)
			m2m->rx_ops->begin(m2m->rx_ctx, data & type);
		else
			RX_STATE(RX_IDLE);
		break;
	case M2M_RX:
		m2m->rx_ops->rx(m2m->rx_ctx, data);
		break;
	default:
		panic();
	}
}


void m2m_rx_setup(struct m2m *m2m, const struct m2m_rx_ops *ops, void *ctx)
{
	m2m->rx_ops = ops;
	m2m->rx_ctx = ctx;
}


void m2m_rx_end(struct m2m *m2m)
{
	switch (m2m->rx_state) {
	case M2M_RX:
		RX_STATE(RX_IDLE);
		m2m->out_ack = 1;
//		kick_tx_out(m2m);
		break;
	default:
		panic();
	}
}


void m2m_rx_cancel(struct m2m *m2m)
{
	switch (m2m->rx_state) {
	case M2M_RX:
		RX_STATE(RX_IDLE);
		m2m->out_nak = 1;
//		kick_tx_out(m2m);
		break;
	default:
		panic();
	}
}


/* ----- Polling ----------------------------------------------------------- */


static bool try_flush_out(struct m2m *m2m)
{
	switch (m2m->tx_state) {
	case M2M_TX_IDLE:
		switch (m2m->link_state) {
		case M2M_STANDBY:
			link_wake(m2m);
			break;
		case M2M_WAKE:
			break;
		case M2M_READY:
			kick_tx_out(m2m);
			break;
		default:
			panic();
		}
		return 1;
	case M2M_ACK_WAIT:
		TX_STATE(TX_FLUSH);
		send_break(m2m);
		return 1;
	default:
		return 0;
	}
}


bool m2m_poll(struct m2m *m2m)
{
	bool change = 0;
	uint8_t data;
	bool is_break;

	if (m2m->uart_ops->poll_rx(m2m->uart_ctx, &data)) {
		change = 1;
		switch (m2m->link_state) {
		case M2M_STANDBY:
		case M2M_WAKE:
			break;
		case M2M_READY:
			start_timer(m2m);
			rx_rx(m2m, data);
			break;
		default:
			panic();
		}
	}
	if (m2m->uart_ops->poll_error(m2m->uart_ctx, &is_break)) {
		change = 1;
		if (is_break) {
			break_received(m2m);
		} else {
			if (m2m->link_state == M2M_READY) {
				start_timer(m2m);
				LINK_STATE(STANDBY);
				link_reset(m2m);
			}
		}
	}
	if (m2m->uart_ops->may_tx(m2m->uart_ctx)) {
		change |= tx_tx(m2m);
		if (change)
			start_timer(m2m);
	}
	if (poll_timer(m2m)) {
		switch (m2m->link_state) {
		case M2M_WAKE:
		case M2M_READY:
			change = 1;
			LINK_STATE(STANDBY);
			link_reset(m2m);
			break;
		default:
			break;
		}
	}
	if (m2m->out_ack || m2m->out_nak)
		change = try_flush_out(m2m);
	return change;
}


/* ----- Setup ------------------------------------------------------------- */


void m2m_init(struct m2m *m2m, const struct uart_ops *uart_ops, void *uart_ctx,
    const struct m2m_timer_ops *timer_ops, void *timer_ctx)
{
	m2m->tx_ops = NULL;
	m2m->rx_ops = NULL;
	m2m->uart_ops = uart_ops;
	m2m->uart_ctx = uart_ctx;
	m2m->timer_ops = timer_ops;
	m2m->timer_ctx = timer_ctx;
	m2m->link_state = M2M_STANDBY;
	m2m->rx_state = M2M_RX_IDLE;
	m2m->tx_state = M2M_TX_IDLE;
	m2m->type = 0;
	m2m->in_ack = m2m->in_nak = m2m->out_ack = m2m->out_nak = 0;
}
