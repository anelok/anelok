/*
 * fw/m2m/m2m.h - Machine-to-machine communication
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef M2M_H
#define M2M_H

#include <stdbool.h>
#include <stdint.h>

#include "uart.h"


enum m2m_link_state {
	M2M_STANDBY,
	M2M_WAKE,
	M2M_READY,
};

enum m2m_tx_state {
	M2M_TX_IDLE,
	M2M_TX_WAKE,	/* wait for link to wake up */
	M2M_TX_WOKEN,	/* wait for TX opportunity after wakeup */
	M2M_TXING,	/* sending data */
	M2M_ACK_WAIT,	/* waiting for ACK */
	M2M_TX_FLUSH,	/* send an ACK/NAK-only packet while waiting for ACK */
};

struct m2m_timer_ops {
	void (*start)(void *ctx, uint16_t ms);
	bool (*poll)(void *ctx);
};

struct m2m_tx_ops {
	uint8_t (*tx)(void *ctx);
	void (*done)(void *ctx);
	void (*fail)(void *ctx);
};

enum m2m_rx_state {
	M2M_RX_IDLE,
	M2M_RX_BEGIN,
	M2M_RX,
};

struct m2m_rx_ops {
	void (*begin)(void *ctx, uint8_t type);
	void (*rx)(void *ctx, uint8_t data);
	void (*fail)(void *ctx);
};

struct m2m {
	enum m2m_link_state link_state;

	enum m2m_tx_state tx_state;
	const struct m2m_tx_ops *tx_ops;
	void *tx_ctx;

	enum m2m_rx_state rx_state;
	const struct m2m_rx_ops *rx_ops;
	void *rx_ctx;

	const struct uart_ops *uart_ops;
	void *uart_ctx;

	const struct m2m_timer_ops *timer_ops;
	void *timer_ctx;

	uint8_t type;
	bool in_ack, in_nak;
	bool out_ack, out_nak;
};


/* ----- TX ---------------------------------------------------------------- */

void m2m_tx_setup(struct m2m *m2m, const struct m2m_tx_ops *ops, void *ctx);
void m2m_send(struct m2m *m2m, uint8_t type);
void m2m_tx_end(struct m2m *m2m);
void m2m_tx_cancel(struct m2m *m2m);

/* ----- RX ---------------------------------------------------------------- */

void m2m_rx_setup(struct m2m *m2m, const struct m2m_rx_ops *ops, void *ctx);
void m2m_rx_end(struct m2m *m2m);
void m2m_rx_cancel(struct m2m *m2m);

/* ----- Setup and polling ------------------------------------------------- */

bool m2m_poll(struct m2m *m2m);
void m2m_init(struct m2m *m2m, const struct uart_ops *uart_ops, void *uart_ctx,
    const struct m2m_timer_ops *timer_ops, void *timer_ctx);

#endif /* M2M_H */
