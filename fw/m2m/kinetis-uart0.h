/*
 * fw/m2m/kinetis-uart0.h - Kinetis UART0 low-level driver
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef KINETIS_UART0_H
#define	KINETIS_UART0_H

#include "uart.h"


extern struct uart_ops kinetis_uart0_ops;


void kinetis_uart0_init(void);

#endif /* KINETIS_UART0_H */
