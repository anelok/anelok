/*
 * fw/mk3/boot-cmcu.c - DFU boot loader firmware for Anelok Mk 3 cMCU
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb-board.h"
#include "flash.h"
#include "usb.h"
#include "dfu.h"
#include "dfu-wait.h"

#include "regs.h"
#include "misc.h"
#include "board.h"
#include "gpio.h"
#include "led.h"


static void ports_off(void)
{
	/* Power */

	gpio_init_off(VBAT_SENSE);

	/* USB */

	gpio_init_off(USB_ID);
	gpio_init_off(USB_nSENSE);

	/* RF */

	gpio_init_off(RF_CTRR);
	gpio_init_off(RF_CRRT);
	gpio_init_off(RF_SWDIO);
	gpio_init_off(RF_SWDCLK);
	gpio_init_off(VRF_OFF);
	gpio_init_off(VRF);

	/* system pins */

	gpio_init_off(GPIO_ID(A, 0));	/* SWD_CLK */
	gpio_init_off(GPIO_ID(A, 3));	/* SWD_DIO */
	gpio_init_off(GPIO_ID(A, 4));	/* NMI_b */

	/* sMCU */

	gpio_init_off(MCU_STCR);
	gpio_init_off(MCU_SRCT);

	/* unconnected pins */

	gpio_init_off(GPIO_ID(D, 7));
	gpio_init_off(GPIO_ID(D, 5));
	gpio_init_off(GPIO_ID(D, 4));
	gpio_init_off(GPIO_ID(C, 7));
	gpio_init_off(GPIO_ID(C, 6));
}


static void (**app_vec)(void) = (void (**)(void)) (APP_BASE | 4);


int main(void)
{
	/*
	 * @@@ we seem to reset a few times before the system stabilizes.
	 * find out why.
	 */

	ports_off();
	led_init();

	usb_init();
	usb_begin_device();
	dfu_init();
	flash_init(); /* must follow dfu_init */
	while (dfu_wait());
	usb_end_device();

	(*app_vec)();

	reset_cpu();
}
