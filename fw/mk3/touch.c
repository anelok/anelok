/*
 * fw/mk3/touch.c - Wrapper to provide touch API
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "display.h"	/* for FB_Y */
#include "qa.h"
#include "tact.h"
#include "touch.h"


void touch_calm(bool power_drop)
{
}


bool touch_filter(uint8_t *pos)
{
	enum tact_button tact;

	tact = tact_poll();
	switch (tact) {
	case tact_none:
	case tact_multi:
		return 0;
	case tact_top:
		*pos = FB_Y - 1;
		return 1;
	case tact_middle:
		*pos = FB_Y >> 1;
		return 1;
	case tact_bottom:
		*pos = 0;
		return 1;
	default:
		panic();
	}
	panic();
}


void touch_init(void)
{
	tact_init();
}
