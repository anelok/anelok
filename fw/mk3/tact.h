/*
 * fw/mk3/tact.h - Tactile switches
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef	TACT_H
#define	TACT_H

#include <stdbool.h>
#include <stdint.h>


enum tact_button {
	tact_none	= 0,
	tact_top	= 1 << 0,
	tact_middle	= 1 << 1,
	tact_bottom	= 1 << 2,
	tact_multi	= 7,
};


enum tact_button tact_poll(void);
void tact_init(void);

/* fake touch API */
bool tact_touch(uint8_t *pos);

#endif /* !TACT_H */
