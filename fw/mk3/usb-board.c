/*
 * fw/mk3/usb-board.c - Board-specific USB functions for Mk 3 cMCU
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "regs.h"
#include "clock.h"
#include "usb-board.h"


void usb_board_begin(void)
{
/*
 * @@@ Use the RC oscillator for now, until crystals are verified.
 */
	clock_internal();

	/*
	 * PLLFLLSEL = 0: FLL clock (48 MHz) (already set)
	 * USBSRC = 1: use FLL clock
	 */
	SIM_SOPT2 |= SIM_SOPT2_USBSRC_MASK;

	SIM_SCGC4 |= SIM_SCGC4_USBOTG_MASK;	/* enable USB clock */
}


void usb_board_end(void)
{
	SIM_SCGC4 &= ~SIM_SCGC4_USBOTG_MASK;	/* disable USB clock */

	clock_internal();
}
