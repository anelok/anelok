/*
 * fw/mk3/tact.c - Tactile switches
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "gpio.h"
#include "tact.h"


static enum tact_button do_poll(void)
{
	bool a, b, c;

	a = !gpio_read(CAP_A);
	b = !gpio_read(CAP_B);
	c = !gpio_read(CAP_C);
	if (a + b + c > 1)
		return tact_multi;
	if (a)
		return tact_top;
	if (b)
		return tact_middle;
	if (c)
		return tact_bottom;
	return tact_none;
}


enum tact_button tact_poll(void)
{
	enum tact_button res;

	gpio_begin(CAP_A);
	gpio_begin(CAP_B);
	gpio_begin(CAP_C);
	res = do_poll();
	gpio_end(CAP_A);
	gpio_end(CAP_B);
	gpio_end(CAP_C);

	return res;
}


void tact_init(void)
{
	gpio_init_in(CAP_A, 1);
	gpio_init_in(CAP_B, 1);
	gpio_init_in(CAP_C, 1);
}
