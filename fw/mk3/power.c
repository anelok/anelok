/*
 * Mk 2 and Mk 3 happen to be identical when it comes to power distribution,
 * except that the boost converter in Mk 3 is never completely turned off
 * under CPU control, only its voltage may change.
 */
#include "../mk2/power.c"
