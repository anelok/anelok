/*
 * fw/mk3/comm.c - Communication handler for Mk 3
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "input.h"
#include "account.h"
#include "comm.h"


enum comm comm = comm_none;


static volatile bool comm_busy = 0;


bool comm_send(struct account_record *rec)
{
	if (account_field_type(rec) != field_pw)
		return 0;
	switch (comm) {
	default:
		return 0;
	}
}


enum comm comm_mode(struct account_record *rec)
{
	if (comm_busy)
		return comm_none;
	return comm;
}


void comm_update(enum comm new)
{
	comm = new;
	input_refresh();
}
