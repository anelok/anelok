/*
 * fw/mk3/smcu.c - Experimental firmware for Anelok Mk 3 design
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "board.h"
#include "devcfg.h"
#include "gpio.h"
#include "irq.h"
#include "clock.h"
#include "tick.h"
#include "event.h"
#include "led.h"
#include "power.h"
#include "display.h"
#include "console.h"
#include "pm.h"
#include "touch.h"
#include "ui.h"


/* ----- Main loop --------------------------------------------------------- */


static void __attribute__((noreturn)) low_current(void)
{
	/* @@@ signal cMCU to shut down */
	/* @@@ signal nRF to shu down */

	clock_xtal_32k();

	gpio_init_off(CAP_A);
	gpio_init_off(CAP_B);
	gpio_init_off(CAP_C);

	gpio_init_out(LED, 0);
	gpio_begin(LED);
	while (1) {
		gpio_set(LED);
		msleep(1000);
		gpio_clr(LED);
		msleep(29000);
	}
}


static void run_ui(void)
{
	power_init();

	msleep(10);
	power_3v3(1);
	msleep(10);

	power_disp(1);
	display_init();
	display_rotate(1);
	console_init();
	touch_init();
//	cmcu_init();
//	rf_init();

	ui_off();
	ui_init();
	while (ui()) {
		/* @@@ handle cMCU and RF events */
                if (event_consume(ev_console)) {
                        pm_busy();
                        console_update();
                }
        }
}


int main(void)
{
	devcfg_init();

	/* boot should do this for us, but we may run standalone */
	led_init();

	/* do these two before using msleep */
	tick_init();
	irq_enable();

	msleep(1);      /* let host notice we've detached */

	run_ui();

        low_current();
}
