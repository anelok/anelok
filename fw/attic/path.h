/*
 * fw/ui/path.h - Path operations
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PATH_H
#define	PATH_H

#include <stdbool.h>


/*
 * The path classifier can detect the following conditions:
 *
 * - an entry has the current path as prefix, e.g.,
 *   foo (if the path is empty) or bar/foo (if the path is bar)
 *   The return value is path_inside in this case.
 *
 * - an entry is in a directory below the current path. If we have seen this
 *   subdirectory immediately before, it becomes the "current" subdirectory
 *   and we get path_sub.
 *   Otherwise, we get path_skip.
 *
 * - an entry is somewhere else. This also yields path_skip.
 *
 * Please note that calling path_classify a second time after it has returned
 * path_sub will yield path_skip since path_classify does not know whether the
 * cursor has moved between calls.
 *
 * @@@ Known bug: if entries with the same subdirectory are not contiguous,
 * the subdirectory appears multiple times. E.g.,
 *
 * foo/one		foo
 * bar		->	bar
 * foo/two		foo
 *
 * versus
 *
 * bar			bar
 * foo/one	->	foo
 * foo/two
 */


#define	MAX_PATH	64
#define	MAX_DIR_NAME	16

/* Directory separator */

#define	SEP_CHAR	'/'
#define	SEP_STRING	"/"


enum path_type {
	path_skip,	/* outside or repetition of subdirectory */
	path_inside,	/* inside current directory and not a directory */
	path_sub,	/* subdirectory of current directory */
};


struct path {
	char path[MAX_PATH + 1];
	char sub[MAX_DIR_NAME + 1];
};


enum path_type path_classify(struct path *ctx, const char *name);
void path_reset_classifier(struct path *ctx);

bool path_at_root(const struct path *ctx);
const char *path_current_sub(const struct path *ctx);

void path_enter(struct path *ctx);
void path_leave(struct path *ctx);

void path_init(struct path *ctx);

#endif /* !PATH_H */
