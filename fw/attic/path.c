/*
 * fw/ui/path.c - Path operations
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "qa.h"
#include "path.h"


/* ----- Path classifier --------------------------------------------------- */


enum path_type path_classify(struct path *ctx, const char *name)
{
	const char *next, *sep;
	uint8_t len, sub_len;

	len = strlen(ctx->path);
	if (strncmp(name, ctx->path, len))
		return path_skip;

	next = name + len;
	if (*ctx->path) {
		if (*next != SEP_CHAR)
			return path_skip;
		next++;
	}

	sep = strchr(next, SEP_CHAR);
	if (!sep)
		return path_inside;
	if (sep == next)		/* foo//bar */
		return path_skip;

	sub_len = sep - next;
	REQUIRE(sub_len <= MAX_DIR_NAME);
	if (strlen(ctx->sub) == sub_len && !strncmp(next, ctx->sub, sub_len))
		return path_skip;

	memcpy(ctx->sub, next, sub_len);
	ctx->sub[sub_len] = 0;

	return path_sub;
}


void path_reset_classifier(struct path *ctx)
{
	*ctx->sub = 0;
}


bool path_at_root(const struct path *ctx)
{
	return !*ctx->path;
}


const char *path_current_sub(const struct path *ctx)
{
	return ctx->sub;
}


void path_enter(struct path *ctx)
{
	REQUIRE(strlen(ctx->path) + strlen(ctx->sub) + 1 <= MAX_PATH);

	if (*ctx->path)
		strcat(ctx->path, SEP_STRING);
	strcat(ctx->path, ctx->sub);
}


void path_leave(struct path *ctx)
{
	char *sep;

	sep = strrchr(ctx->path, SEP_CHAR);
	if (sep)
		*sep = 0;
	else
		*ctx->path = 0;
}


void path_init(struct path *ctx)
{
	*ctx->path = 0;
	*ctx->sub = 0;
}
