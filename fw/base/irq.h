/*
 * fw/irq.h - Interrupt handling
 *
 * Written 2013 by Werner Almesberger
 * Copyright 2013 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Loosely based on Linux arch/arm/include/asm/irqflags.h
 */


#ifndef IRQ_H
#define	IRQ_H

static inline void irq_enable(void)
{
	asm volatile ("\tcpsie i");
}


static inline void irq_disable(void)
{
	asm volatile ("\tcpsid i");
}


static inline unsigned long irq_save(void)
{
	unsigned long flags;

	asm volatile (
	    "\tmrs %0, primask\n"
	    "\tcpsid i"
	    : "=r" (flags));
	return flags;
}


static inline void irq_restore(unsigned long flags)
{
	asm volatile ("\tmsr primask, %0" : : "r" (flags));
}

#endif /* !IRQ_H */
