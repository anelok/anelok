/*
 * fw/clock.c - Clock management
 *
 * Written 2013-2015, 2017 by Werner Almesberger
 * Copyright 2013-2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * External 2 MHz clock source must be enabled before calling cpu_clock_kHz and
 * clock_external.
 */


#include <stdbool.h>
#include <stdint.h>

#include "regs.h"
#include "board.h"
#include "gpio.h"
#include "clock.h"


/* ----- System clock measurement ------------------------------------------ */


static inline bool syst_wrapped(void)
{
	return SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk;
}


#define	MEAS_MS	20


/*
 * Reset configuration:
 *
 * 32.768 kHz IRC -> MCG_C1.IREFS (1, slow) -> MCG_C4.DRS+DMX32 (00+0, x640) ->
 * MCG_C6.PLLS (0, FLL) -> MCG_C1.CLKS (0, FLL/PLL) -> MCGOUTCLK
 *
 * MCGOUTCLK = 32.768 kHz * 640 = 20.971520 MHz (nominal)
 *
 * MCGOUTCLK -> OUTDIV1 (FTFA_FOPT.LPBOOT = 11 -> SIM_CLKDIV1.OUTDIV1 = 0, /1)
 * -> core clock
 *
 * core clock -> OUTDIV4 (SIM_CLKDIV1.OUTDIV4 = 1 (/2) -> bus clock
 *
 * Thus we get (if in reset configuration):
 * CPU/core clock = ~21 MHz
 * Bus clock = ~10.5 MHz
 */


/* ----- OSC0_CR ----------------------------------------------------------- */

/*
 * Seems that one can't read OSC0_CR
 */

static void osc0_cr(uint32_t change, uint32_t set)
{
	static uint8_t shadow = 0;

	shadow = (shadow & ~change) | set;
	OSC0_CR = shadow;
}



/* ----- Clock output ------------------------------------------------------ */


#define	CLKOUT	GPIO_ID(C, 3)


static void enable_clkout(void)
{
	gpio_init_out(CLKOUT, 1);
	gpio_begin(CLKOUT);
	gpio_fn(CLKOUT, 5);
	gpio_end(CLKOUT);
}


void clkout_bus(void)
{
	/* Output bus clock on PTC3/CLKOUT */
	SIM_SOPT2 |= SIM_SOPT2_CLKOUTSEL(2);

	enable_clkout();
}


void clkout_osc(void)
{
	/* Output OSCCLK clock on OSCERCLK (internal) */
	osc0_cr(OSC_CR_ERCLKEN_MASK, OSC_CR_ERCLKEN_MASK);

	/* Output OSCERCLK clock on PTC3/CLKOUT */
	SIM_SOPT2 |= SIM_SOPT2_CLKOUTSEL(6);

	enable_clkout();
}


void clkout_off(void)
{
	gpio_init_off(CLKOUT);

	/* Clear ERCLKEN */
	osc0_cr(OSC_CR_ERCLKEN_MASK, 0);

	/* Disable clock output */
	SIM_SOPT2 &= ~SIM_SOPT2_CLKOUTSEL(7);
}


/* ----- Clock selection --------------------------------------------------- */

/*
 * The reference manual describes an absurdly complicated clock switching
 * procedure. We avoid taking the scenic route and do most of the work running
 * from the internal (slow) oscillator.
 */

#define	VDIV0	(48-24)		/* PLL x48 */


static void change_clks(uint8_t clks, uint8_t clkst)
{
	MCG_C1 = MCG_C1_CLKS(clks) | MCG_C1_IREFS_MASK;
	while (((MCG_S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != clkst);
}


static void begin_select_fll(void)
{
	/* select internal reference clock */
	change_clks(1, 1);

	/*
	 * OUTDIV1 = 0: CPU clock is MCGOUTCLK (FLL)
	 * OUTDIV4 = 1: bus clock is CPU clock / 2
	 */
	SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV4(1);

	/*
	 * This should yield a 48 MHz clock.
	 *
	 * DRST_DRS = 1: mid range
	 * DMX32 = 1: fine-tune for maximum frequency (48 MHz)
	 */
	MCG_C4 |= MCG_C4_DMX32_MASK | MCG_C4_DRST_DRS(1);
	while (((MCG_C4 & MCG_C4_DRST_DRS_MASK) >> MCG_C4_DRST_DRS_SHIFT) != 1);

	/*
	 * PLLS = 0: select FLL
	 * VDIV0 = 24: x48 clock multiplier (for PLL, not used here)
	 */
	MCG_C6 = VDIV0;

	/*
	 * Disable the PLL.
	 */
	MCG_C5 = 0;

	/* wait for FLL to be selected */
	while (MCG_S & MCG_S_PLLST_MASK);

	/* select FLL */
	change_clks(0, 0);
}


static void end_select_fll(void)
{
	/* also switch peripherals to FLL */
	SIM_SOPT2 &= ~SIM_SOPT2_PLLFLLSEL_MASK;
}


/*
 * FLL Engaged Internal (FEI)
 */

void clock_internal(void)
{
	begin_select_fll();
	end_select_fll();
}


/*
 * FLL Engaged External (FEE)
 */


static void xtal_load(uint8_t pF)
{
	osc0_cr(
	    OSC_CR_SC2P_MASK | OSC_CR_SC4P_MASK | OSC_CR_SC8P_MASK |
	    OSC_CR_SC16P_MASK,
	    (pF & 2 ? OSC_CR_SC2P_MASK : 0) |
	    (pF & 4 ? OSC_CR_SC4P_MASK : 0) |
	    (pF & 8 ? OSC_CR_SC8P_MASK : 0) |
	    (pF & 16 ? OSC_CR_SC16P_MASK : 0));
}


void clock_xtal_32k_fll(void)
{
	begin_select_fll();

	/*
	 * Crystal is 12.5 pF. If we assume Cstray = 2 pF, we should load it
	 * with 2*(12.5-2) pF = 21 pF
	 *
	 * @@@ Even with nominally 0 pF, the oscilloscope measures a clock
	 * that is about 330 ppm too slow. The crystal has a nominal tolerance
	 * of 20 ppm.
	 *
	 * Observations:
	 * - measurement of the 32.768 kHz oscillator show that it actually
	 *   runs a bit too fast !
	 * - the FLL works as specified, i.e., it multiplies the oscillator
	 *   clock by 1464 with good accuracy
	 * - the exact factor of 1464 should be 1464.84, so 1464 is a bit too
	 *   low, introducing a systematic error of -558 ppm.
	 *
	 * USB as the following clock tolerances:
	 * Full-speed host:	  500 ppm
	 * Full-speed device:	 2500 ppm
	 * Low-speed device:	15000 ppm (1.5%)
	 *
	 * Measuring the 32.768 clock directly:
	 *
	 * Cload	32 kHz clock		*1464/2		Bus clock
	 * (pF)
	 * 		CLKOUT	Error		Nominal		CLKOUT	Error
	 * 		(kHz)	(ppm)		(MHz)		(MHz)	(ppm)
	 *  0		32.7776	+293		23.9932		23.9921	-329
	 * 10		32.7720	+122
	 * 16		32.7702	+ 67		23.9878		23.9881	-496
	 * 18		32.7699	+ 58
	 * 20		32.7696	+ 49
	 * 28		32.7688	+ 24
	 * 30		32.7686	+ 18		23.9866		23.9870	-542
	 */
	xtal_load(0);	// -330 ppm

	/*
	 * LOCRE0 = 0: disable loss of clock reset
	 * EREFS0 = 1: use crystal oscillator (for OSC)
	 */

	MCG_C2 = MCG_C2_EREFS0_MASK;

	/* select "external" clock source (OSC module) for FLL */
	MCG_C1 &= ~MCG_C1_IREFS_MASK;

/*
 * @@@ EEEW ! we switch the FLL clock source here while the FLL is "live".
 * The manual says FEI -> FEE is okay, but this doesn't sounds nice.
 */

	/* wait for oscillator to spin up */
	while (!(MCG_S & MCG_S_OSCINIT0_MASK));

	end_select_fll();
}


/*
 * Bypassed Low Power External (BLPE)
 *
 * For clock_xtal_32k we assume that the MCU is already in FEE.
 */

void clock_xtal_32k(void)
{
	/* go to FBE */
	change_clks(2, 2);

	/* and now to BLPE */
	MCG_C2 |= MCG_C2_LP_MASK;
}


/*
 * Bypassed Low Power Internal (BLPI)
 *
 * For clock_low we assume that the MCU is already in FEI.
 */

void clock_low(void)
{
	/*
	 * Reset default FCRDIV = 1 -> output clock is 4 MHz / 2 = 2 MHz
	 */

	/*
         * OUTDIV1 = 1: CPU clock is MCGIRCLK (2 MHz) / 2
         * OUTDIV4 = 1: bus clock is CPU clock / 2
	 *
	 * We need to make sure the Flash clock is not faster than 800 kHz
	 * (5.5.2).
         */
	SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(1) | SIM_CLKDIV1_OUTDIV4(1);

	/* select 4 MHz IRC */
	MCG_C2 |= MCG_C2_IRCS_MASK;

	/* go to FBI */
	change_clks(1, 1);

	/*
	 * OUTDIV1 = 0: CPU clock is MCGIRCLK
	 * OUTDIV4 = 4: bus clock is CPU clock / 5
	 * MCG_SC[FCRDIV] = 0: MCGIRCLK is 4 MHz
	 */

	SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV4(4);
	MCG_SC = 0;

	/* disable the PLL, just in case */
	MCG_C5 = 0;

	/* and now to BLPI */
	MCG_C2 |= MCG_C2_LP_MASK;
}


/*
 * PLL Engaged External (PEE)
 *
 * FEI -> PEE transition is according to section 24.5.3.1
 */

void clock_external(void)
{
	/* select internal reference clock */
	change_clks(1, 1);

	/*
	 * LOCRE0 = 1: enable loss of clock reset
	 * EREFS0 = 0: external reference clock (for OSC)
	 */
	MCG_C2 = MCG_C2_LOCRE0_MASK;

	/*
	 * Enable the PLL (the manual says setting PLLS does that as well, but
	 * it doesn't seem to work.)
	 */
	MCG_C5 = MCG_C5_PLLCLKEN0_MASK;

	/*
	 * PLLS = 1: select PLL
	 * VDIV0 = 24: x48 clock multiplier
	 */
	MCG_C6 = MCG_C6_PLLS_MASK | VDIV0;

	/* wait for PLL to be selected */
	while (!(MCG_S & MCG_S_PLLST_MASK));

	/* wait for PLL to lock */
	while (!(MCG_S & MCG_S_LOCK0_MASK));

	/*
	 * OUTDIV1 = 1: CPU clock is MCGOUTCLK (PLL) / 2
	 * OUTDIV4 = 1: bus clock is CPU clock / 2
	 */
	SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(1) | SIM_CLKDIV1_OUTDIV4(1);

	/* select PLL */
	change_clks(0, 3);

	/* also switch peripherals to PLL */
	SIM_SOPT2 |= SIM_SOPT2_PLLFLLSEL_MASK;
}
