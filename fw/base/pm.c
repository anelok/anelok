/*
 * fw/base/pm.c - Power management
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "qa.h"
#include "devcfg.h"
#include "power.h"
#include "display.h"
#include "input.h"
#include "touch.h"
#include "pm.h"


#define	POLICY_S(name, a, b, c, d) \
    struct pm_policy pm_policy_##name = \
      { { (a) * 1000, (b) * 1000, (c) * 1000, (d) * 1000, 0 } }

		     /* Active Dark1	Dark2	Ready */
POLICY_S(login,	 	 20,     0,	  0,	  0);
POLICY_S(default, 	 20,    30,	 31,	 91);
POLICY_S(peruse,	 40,    50,	 51,	111);


static const struct pm_policy *curr_pol = &pm_policy_default;
static enum pm_state state = pm_active;


void pm_usb_power(bool have)
{
}


enum pm_state pm_state(void)
{
	return state;
}


void pm_busy(void)
{
	switch (state) {
	case pm_standby:
	case pm_ready:
		power_3v3(1);
		touch_calm(0);
		/* fall through */
	case pm_dark2:
		power_disp(1);
		display_on(0);
		/* fall through */
	case pm_dark1:
		input_resume();
		/* fall through */
	case pm_active:
		break;
	default:
		oops();
	}
	state = pm_active;
}


void pm_sleep(enum pm_state s)
{
	static bool once;

	if (devcfg->stay_on && s > pm_dark2)
		s = pm_dark2;
	switch (state) {
	case pm_active:
		once = 1;
		input_suspend();
		/* suspend handler probably called ui_off */
		if (!once)
			// coverity[dead_error_line]
			return;
		display_clear();
		if (s == pm_dark1)
			break;
		/* fall through */
	case pm_dark1:
		display_off();
		power_disp(0);
		if (s == pm_dark2)
			break;
		/* fall through */
	case pm_dark2:
		power_3v3(0);
		touch_calm(1);
		if (s == pm_ready)
			break;
		/* fall through */
	case pm_ready:
	default:
		if (s == pm_standby)
			break;
		oops();
	}
	state = s;
	once = 0;
}


void pm_idle(uint32_t ms)
{
	enum pm_state s;

	for (s = state; s != pm_n; s++) {
		if (!curr_pol->t[s])
			break;
		if (curr_pol->t[s] > ms)
			break;
	}
	if (s != state)
		pm_sleep(s);
}


void pm_set_policy(const struct pm_policy *policy)
{
	curr_pol = policy;
}
