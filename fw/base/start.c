/*
 * fw/start.c - CPU bringup
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Based on Andrew Payne's _startup.c
 */


#include <stdint.h>
#include <string.h>

#include "regs.h"
#include "isr.h"


extern void _start(void);                   // newlib C lib initialization
extern char __stack;
extern char __data_start__;
extern char __data_end__;
extern char __etext;

void Reset_Handler(void)    __attribute__((naked, aligned(8)));
void _exit(int);


void default_handler(void);

void default_handler(void)
{
}


#define	VECS_DEFAULT
#include "vecs.inc"


void (* const vectors[])() __attribute__((section(".isr_vector"))) = {
	(void (*)(void)) &__stack,
	Reset_Handler,
	0,	/* NMI */
	0,	/* Hard Fault */
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,	/* SVC */
	0,
	0,
	0,	/* PendSV */
	0,	/* SysTick */
#define	VECS_ASSIGN
#include "vecs.inc"
};


void Reset_Handler(void)
{
	SIM_COPC = 0;	/* kill the watchdog */
	SCB->VTOR = (uint32_t) vectors;

	/* initialize data segment */
	memcpy(&__data_start__, &__etext, &__data_end__ - &__data_start__);

	_start();
}


void _exit(int status)
{
	while (1);
}
