/*
 * fw/misc.c - Miscellanea
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "misc.h"


#ifndef SIMULATOR

#include "regs.h"


void reset_cpu(void)
{
	NVIC_SystemReset();
	while (1);
}

#endif /* !SIMULATOR */
