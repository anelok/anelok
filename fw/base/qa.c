/*
 * fw/base/qa.c - Quality assurance tools
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * To decode a stack trace, use
 * arm-none-eabi-addr2line -fpe anelok-2014.elf 5711 7dc1 7ab9 ...
 */


#include "power.h"
#include "display.h"
#include "console.h"
#include "backtrace.h"
#include "qa.h"


#ifndef MAX_BACKTRACE
#define	MAX_BACKTRACE	10
#endif


void __oops(const char *file, unsigned line)
{
	struct backtrace trace;
	const void *pc;
	uint8_t i;

	power_disp_force_on();
	display_init();
	console_init();
	console_printf("%s:%d\n", file, line);

	backtrace_start(&trace);
	for (i = 0; i != MAX_BACKTRACE; i++) {
		pc = backtrace_next(&trace);
		if (!pc)
			break;
		console_printf("%p%c", pc, i & 1 ? '\n' : ' ');
	}

	console_update();
	panic();
}
