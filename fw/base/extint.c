/*
 * fw/extint.h - External interrupts (GPIOs)
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Note: pin changed ocurring during deep-sleep seem to create only a
 * low-leakage wakeup (and interrupt), not the corresponding PORT interrupt.
 * Therefore, we have to handle both, even if we wouldn't need to wake up for
 * this.
 *
 * https://community.freescale.com/thread/307967
 */

/*
 * @@@ Known bug: not every LLWU pin is interrupt-capable and not every
 * interrupt-capable pin is connected to the LLWU. We currently only support
 * pins that have both capabilities.
 */


#include <stddef.h>
#include <stdint.h>

#include "qa.h"
#include "irq.h"
#include "regs.h"
#include "gpio.h"
#include "isr.h"
#include "extint.h"


static struct extint *extints = NULL;


/* ----- LLWU configuration ------------------------------------------------ */


static const gpio_id llwu2id[16] = {
	[5]	= GPIO_ID(B, 0),
	[6]	= GPIO_ID(C, 1),
	[7]	= GPIO_ID(C, 3),
	[8]	= GPIO_ID(C, 4),
	[9]	= GPIO_ID(C, 5),
	[10]	= GPIO_ID(C, 6),
	[14]	= GPIO_ID(D, 4),
	[15]	= GPIO_ID(D, 6),
};


static uint8_t gpio_id_to_llwu(gpio_id id)
{
	uint8_t i;

	for (i = 0; i != 16; i++)
		if (llwu2id[i] == id)
			return i;
	oops();
}


static void llwu_set(gpio_id id, enum int_type type)
{
	uint8_t pn, shift, enable;
	volatile uint8_t *reg;

	pn = gpio_id_to_llwu(id);
	shift = 2 * (pn & 3);

	switch (type) {
	case int_disabled:
		enable = 0;
		break;
	case int_rising:
		enable = 1;
		break;
	case int_falling:
		enable = 2;
		break;
	case int_both:
		enable = 3;
		break;
	default:
		oops();
	}

	switch (pn >> 2) {
	case 0:
		reg = &LLWU_PE1;
		break;
	case 1:
		reg = &LLWU_PE2;
		break;
	case 2:
		reg = &LLWU_PE3;
		break;
	case 3:
		reg = &LLWU_PE4;
		break;
	default:
		oops();
	}

	*reg = (*reg & ~(3 << shift)) | enable << shift;
}


/* ----- ISRs for LLWU and PORT -------------------------------------------- */


static void service_pin(gpio_id id)
{
	const struct extint *extint;

	for (extint = extints; extint; extint = extint->next)
		if (extint->id == id) {
			extint->fn();
			return;
		}
}


void extint_llwu(uint16_t flags)
{
	uint8_t first;

	while (1) {
		first = __builtin_ffs(flags);
		if (!first)
			return;
		flags &= ~(1 << (first - 1));
		service_pin(llwu2id[first - 1]);
	}
}


static void service_port(uint8_t port, volatile uint32_t *isfr, uint8_t irq_n)
{
	uint32_t flags;
	uint8_t first;

	while (1) {
		flags = *isfr;
		first = __builtin_ffs(flags);
		if (!first)
			return;
		service_pin(gpio_mkid(port, first - 1));
		*isfr = 1 << (first - 1);
	}
	NVIC_ClearPendingIRQ(irq_n);
}


/* ----- Service port interrupts ------------------------------------------- */


void port_a_isr(void)
{
	service_port(GPIO_PORT_A, &PORTA_ISFR, PORTA_IRQn);
}


void port_cd_isr(void)
{
	service_port(GPIO_PORT_C, &PORTC_ISFR, PORTC_PORTD_IRQn);
	service_port(GPIO_PORT_D, &PORTD_ISFR, PORTC_PORTD_IRQn);
}


/* ----- Fast enable/disable ----------------------------------------------- */


void extint_disable(const struct extint *extint)
{
	llwu_set(extint->id, int_disabled);
	gpio_int(extint->id, int_disabled);
	gpio_end(extint->id);
}


void extint_enable(const struct extint *extint)
{
	gpio_begin(extint->id);
	gpio_int(extint->id, extint->type);
	llwu_set(extint->id, extint->type);
}


/* ----- Configuration ----------------------------------------------------- */


static const uint8_t port2irqn[] = {
	PORTA_IRQn,		/* A */
	0,			/* B */
	PORTC_PORTD_IRQn,	/* C */
	PORTC_PORTD_IRQn,	/* D */
	0			/* E */
};

static uint8_t use[4];	/* only up to D */


static void get_irq(gpio_id id)
{
	uint8_t port, irqn;

	port = gpio_port(id);
	irqn = port2irqn[port];
	if (!irqn)
		panic();
	if (!use[port]++) {
		NVIC_ClearPendingIRQ(irqn);
		NVIC_EnableIRQ(irqn);
	}
}


static void put_irq(gpio_id id)
{
	uint8_t port, irqn;

	port = gpio_port(id);
	irqn = port2irqn[port];
	if (!irqn)
		panic();
	if (!use[port])
		panic();
	if (!--use[port])
		NVIC_DisableIRQ(irqn);
}


void extint_add(struct extint *extint, gpio_id id,
    void (*fn)(void), enum int_type type)
{
	unsigned long flags;

	gpio_begin(id);

	gpio_int(id, int_disabled);

	extint->id = id;
	extint->type = type;
	extint->fn = fn;

	flags = irq_save();
	extint->next = extints;
	extints = extint;
	irq_restore(flags);

	switch (gpio_port(id)) {
	case GPIO_PORT_A:
		PORTA_ISFR = gpio_mask(id);
		break;
	case GPIO_PORT_C:
		PORTC_ISFR = gpio_mask(id);
		break;
	case GPIO_PORT_D:
		PORTD_ISFR = gpio_mask(id);
		break;
	default:
		oops();
	}

	gpio_end(id);

	get_irq(id);
}


void extint_del(struct extint *extint)
{
	unsigned long flags;
	struct extint **walk;

	put_irq(extint->id);

	flags = irq_save();
	for (walk = &extints; *walk && *walk != extint; walk = &(*walk)->next);
	if (!*walk)
		panic();
	*walk = (*walk)->next;
	irq_restore(flags);
}
