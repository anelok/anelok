/*
 * fw/base/base32.c - Base32 encoder
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>
#include <stdbool.h>

#include "base32.h"


static char encode_char(uint8_t v)
{
	if (v < 26)
		return 'A' + v;
	return "346789"[v - 26];
}


void base32_encode(bool (*in)(void *user, uint8_t *byte),
    void (*out)(void *user, char ch), void *user)
{
	uint8_t bits = 0;
	uint16_t v = 0;
	uint8_t byte;

	while (in(user, &byte)) {
		v |= byte << bits;
		bits += 8;
		while (bits > 5) {
			out(user, encode_char(v & 0x1f));
			v >>= 5;
			bits -= 5;
		}
	}
	if (bits)
		out(user, encode_char(v));
}
