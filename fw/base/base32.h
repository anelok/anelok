/*
 * fw/base/base32.h - Base32 encoder
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BASE32_H
#define	BASE32_H

#include <stdbool.h>
#include <stdint.h>


void base32_encode(bool (*in)(void *user, uint8_t *byte),
    void (*out)(void *user, char ch), void *user);

#endif /* !BASE32_H */
