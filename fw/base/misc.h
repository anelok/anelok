/*
 * fw/misc.h - Miscellanea
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef MISC_H
#define	MISC_H

#include <stdint.h>


#define	ARRAY_ELEMENTS(a)	(sizeof(a) / sizeof(a[0]))
#define	ARRAY_END(a)		((a) + ARRAY_ELEMENTS(a))

#define	MDELAY_LOOP		9599
#define	UDELAY_LOOP		10


static inline void mdelay(uint32_t ms)
{
	uint32_t i;
	uint16_t j;

	for (i = 0; i != ms; i++)
		for (j = 0; j != MDELAY_LOOP; j++)
			asm("");
}


static inline void udelay(uint32_t us)
{
	uint32_t i;
	uint16_t j;

	for (i = 0; i != us; i++)
		for (j = 0; j != UDELAY_LOOP; j++)
			asm("");
}


void reset_cpu(void) __attribute__((noreturn));

#endif /* !MISC_H */
