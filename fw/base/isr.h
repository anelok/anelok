/*
 * fw/isr.h - Interrupt service routines
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Loosely based on Linux arch/arm/include/asm/irqflags.h
 */


#ifndef ISR_H
#define	ISR_H

#define	VECS_DECLARE
#include "vecs.inc"

#endif /* !ISR_H */
