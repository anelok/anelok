/*
 * fw/base/qa.h - Quality assurance tools
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef QA_H
#define	QA_H

/*
 * Note: REQUIRE(cond) is like assert(cond) or BUG_ON(!(cond)) except that
 * it is guaranteed to always execute and that side-effects are expressly
 * allowed.
 */

#ifdef SIMULATOR

#include <assert.h>

#ifdef NDEBUG
#error	Do not use NDEBUG
#else
#define	REQUIRE(cond)	assert(cond)
#endif /* !NDEBUG */

#else /* SIMULATOR */

#include "board.h"


#define	REQUIRE(cond)	do { if (!(cond)) oops(); } while (0)

#endif /* !SIMULATOR */


void panic(void) __attribute__((noreturn));
void __oops(const char *file, unsigned line) __attribute__((noreturn));

#ifndef oops
#define	oops(void) __oops(__FILE__, __LINE__)
#endif

#endif /* !QA_H */
