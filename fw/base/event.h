/*
 * fw/base/event.h - Asynchronous events
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EVENT_H
#define	EVENT_H

#include <stdbool.h>
#include <stdint.h>


enum event {
	ev_usb_plug		= 0,	/* USB (power) was plugged in */
	ev_usb_unplug		= 1,	/* USB (power) was plugged out */
	ev_usb_hid_setup	= 2,	/* we have processed a HID SETUP */
	ev_refresh		= 3,	/* display may need refreshing */
	ev_console		= 4,	/* asynchronous console output */
};


extern uint32_t events;


static inline void event_set_interrupt(enum event n)
{
	events |= 1 << n;
}


static inline void event_clear_interrupt(enum event n)
{
	events &= ~(1 << n);
}


static inline bool event_test(enum event n)
{
	return (events >> n) & 1;
}


bool __event_consume(enum event n);


static inline bool event_consume(enum event n)
{
	return events ? __event_consume(n) : 0;
}

#endif /* !EVENT_H */
