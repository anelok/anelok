#
# Makefile - Makefile of the Anelok firmware
#
# Written 2013-2017 by Werner Almesberger
# Copyright 2013-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Based on Andrew Payne's Makefile from bare-metal-arm
#


# ----- Device configuration database -----------------------------------------

ANELOK_DEVCFG ?= DEVCFG

# ----- Target selection ------------------------------------------------------

#
# Supported targets:
#
# ANELOK_MK2	Anelok Mk 2 (2014) design, with capacitive sensor or buttons
# BOOT_MK2	DFU boot loader for Anelok Mk 2
# YBOX		Y-Box
# BOOT_YBOX	DFU boot loader for Y-Box
# ANELOK_MK3_SMCU  sMCU of Anelok Mk 3 (2017) design, under development
# BOOT_MK3_CMCU	boot loader for Anelok Mk 3 cMCU
#

TARGETS = ANELOK_MK2 BOOT_MK2 YBOX BOOT_YBOX \
	  ANELOK_MK3_SMCU BOOT_MK3_CMCU

TARGET ?= ANELOK_MK2

NAME_ANELOK_MK2		= anelok-mk2
NAME_BOOT_MK2		= boot-mk2
NAME_YBOX		= ybox
NAME_BOOT_YBOX		= boot-ybox
NAME_ANELOK_MK3_SMCU	= anelok-mk3-smcu
NAME_BOOT_MK3_CMCU	= boot-mk3-cmcu
NAME = $(NAME_$(TARGET))

BOARD_DIR_ANELOK_MK2	= mk2
BOARD_DIR_BOOT_MK2	= mk2
BOARD_DIR_YBOX		= ybox
BOARD_DIR_BOOT_YBOX	= ybox
BOARD_DIR_ANELOK_MK3_SMCU = mk3
BOARD_DIR_BOOT_MK3_CMCU	= mk3
BOARD_DIR = $(BOARD_DIR_$(TARGET))

PART_BASE_ANELOK_MK2	= $(APP_BASE)
PART_BASE_BOOT_MK2	= $(BOOT_BASE)
PART_BASE_YBOX		= $(APP_BASE)
PART_BASE_BOOT_YBOX	= $(BOOT_BASE)
PART_BASE_ANELOK_MK3_SMCU = $(BOOT_BASE)
PART_BASE_BOOT_MK3_CMCU	= $(BOOT_BASE)
PART_BASE = $(PART_BASE_$(TARGET))

USB_ID_ANELOK_MK2	= 20b7:ae70
USB_ID_BOOT_MK2		= 20b7:ae70
USB_ID_YBOX		= 20b7:ae71
USB_ID_BOOT_YBOX	= 20b7:ae71
USB_ID_BOOT_MK3_CMCU	= 20b7:ae70

USB_ID = $(USB_ID_$(TARGET))

# ----- Memory layout ---------------------------------------------------------

BOOT_BASE	= 0x00410
APP_BASE	= 0x04000

BOOT_SIZE	= ($(APP_BASE) - $(BOOT_BASE))
APP_SIZE	= (0x20000 - $(APP_BASE))

# ----- Target-specific object directory --------------------------------------

OBJDIR = obj-$(NAME)/

all::		| $(OBJDIR:%/=%)

$(OBJDIR:%/=%):
		mkdir -p $@

# ----- General build definitions ---------------------------------------------

PREFIX = arm-none-eabi-

CC = $(PREFIX)gcc
AS = $(PREFIX)as
AR = $(PREFIX)ar
OBJCOPY = $(PREFIX)objcopy
SIZE = $(PREFIX)size

SWDLIB = ../swdlib

LD_SCRIPT_BARE		= sdk/MKL46Z128xxx4_flash.ld
LD_SCRIPT_ANELOK_MK2	= $(NAME).lds
LD_SCRIPT_BOOT_MK2	= $(LD_SCRIPT_BARE)
LD_SCRIPT_YBOX		= $(NAME).lds
LD_SCRIPT_BOOT_YBOX	= $(LD_SCRIPT_BARE)
LD_SCRIPT_ANELOK_MK3_SMCU = $(NAME).lds
LD_SCRIPT_BOOT_MK3_CMCU	= $(LD_SCRIPT_BARE)
LD_SCRIPT = $(LD_SCRIPT_$(TARGET))

USB_OBJS = kinetis-dev.o kinetis-host.o kinetis-common.o \
	   usb-board.o usb-board-common.o usb.o host.o
USB_STACK = ../../ben-wpan/atusb/fw/usb

USBWAIT = ../../ben-wpan/tools/usbwait/usbwait

CFLAGS_WARN = -Wall -Wextra -Wshadow -Wmissing-prototypes \
	      -Wmissing-declarations -Wno-format-zero-length \
	      -Wno-unused-parameter

#
# @@@ to do: switch to newlib-nano:
# https://devzone.nordicsemi.com/question/4740/how-to-get-rid-of-impure_data/
# https://plus.google.com/+AndreyYurovsky/posts/XUr9VBPFDn7
#
# Note that -flto seems to move things from TEXT to BSS, oblivious to the
# fact that BSS space is about eight times as valuable as TEXT space.
#

include ../crypter/Makefile.pin

ASFLAGS = -mcpu=cortex-m0 -mthumb -mfloat-abi=soft
CFLAGS = -g -Os $(CFLAGS_WARN) $(ASFLAGS) \
	 -fdata-sections -ffunction-sections -Wl,--gc-sections \
	 -I$(shell pwd) -Iboard -I$(USB_STACK) -I$(SWDLIB) \
	 -I../crypter \
	 $(SUBDIRS:%=-I$(shell pwd)/%) \
	 -DAPP_BASE=$(APP_BASE) -DAPP_SIZE="$(APP_SIZE)" \
	 -D$(TARGET) -DDEFAULT_PIN=$(PIN)

UNACL_OBJS = unacl-scalarmult.o cortex_m0_mpy121666.o cortex_m0_reduce25519.o \
	     sqr.o mul.o

SUBDIRS = usb disp mmc base ui common plat rf cc board sdk fatfs m2m \
	  db db/crypto db/crypto/unacl $(BOARD_DIR)
COMMON_OBJS = gpio.o extint.o board.o misc.o clock.o led.o
APP_OBJS = $(COMMON_OBJS) start.o main.o display.o text.o boost.o \
	   power.o mmc-hw.o mmc.o adc.o console.o fmt.o \
	   devcfg.o sleep.o tick.o ff.o diskio.o \
	   backtrace-arm.o platflash.o
UI_OBJS = ui.o ui_off.o ui_login.o ui_login_setup.o login_setup.o \
    ui_select.o ui_select_setup.o \
    ui_account.o \
    ui_show.o ui_sorry.o \
    input.o cursor.o sel.o textsel.o std.o alert.o icon.o dir.o perfmon.o \
    event.o base32.o id.o \
    record.o chill.o \
    account.o tweetnacl.o $(UNACL_OBJS) auth.o
BOOT_OBJS = $(COMMON_OBJS) start.o flashcfg.o \
	    dfu.o dfu_common.o dfu-wait.o \
	    flash.o platflash.o
OBJS_ANELOK_MK2 = $(APP_OBJS) $(USB_OBJS) $(UI_OBJS) \
    descr.o dfu_common.o ep0.o touch.o tact.o \
    hid.o hid-type.o comm.o \
    cc.o ccdbg.o \
    qa.o pm.o vusb.o \
    rf.o \
    al_radio.o
OBJS_BOOT_MK2 = $(BOOT_OBJS) $(USB_OBJS) boot.o ccflash.o cc.o ccdbg.o
OBJS_YBOX = $(COMMON_OBJS) $(USB_OBJS) ybox.o rf.o descr.o ep0.o touch.o
OBJS_BOOT_YBOX = $(BOOT_OBJS) $(USB_OBJS) boot.o ccflash.o cc.o ccdbg.o
OBJS_ANELOK_MK3_SMCU = $(COMMON_OBJS) $(UI_OBJS) \
    start.o flashcfg.o smcu.o \
    display.o tick.o sleep.o power.o boost.o text.o console.o \
    qa.o fmt.o pm.o devcfg.o touch.o tact.o mmc.o mmc-hw.o comm.o \
    kinetis-uart0.o \
    backtrace-arm.o
OBJS_BOOT_MK3_CMCU = $(BOOT_OBJS) $(USB_OBJS) boot-cmcu.o kinetis-uart0.o
OBJS = $(OBJS_$(TARGET))
FONTS = 10x20

vpath usb.c $(USB_STACK)
vpath dfu.c $(USB_STACK)
vpath dfu_common.c $(USB_STACK)
vpath ccdbg.c $(SWDLIB)
vpath %.c $(SUBDIRS)
vpath %.s $(SUBDIRS)

# Include Makefile.c-common after setting CC, etc.

include ../common/Makefile.c-common

VERSION_C = board/version.c
include ../common/Makefile.version

# -----------------------------------------------------------------------------

.PHONY:	all $(TARGETS:%=all-%)
.PHONY:	fonts frdm flash cc dfu update ccdfu targets
.PHONY:	clean clean-all spotless
.PHONY:	mk2 mk3 mk2-clean mk3-clean


all::		$(NAME).bin all-$(TARGET)

all-ANELOK_MK2 all-BOOT_MK2 all-YBOX all-BOOT_YBOX: cc

all-ANELOK_MK3_SMCU:

all-ANELOK_MK3_CMCU:

mk2:
		$(MAKE) TARGET=ANELOK_MK2

mk3:
		$(MAKE) TARGET=ANELOK_MK3_SMCU
		$(MAKE) TARGET=BOOT_MK3_CMCU

mk2-clean:
		$(MAKE) TARGET=ANELOK_MK2 clean

mk3-clean:
		$(MAKE) TARGET=ANELOK_MK3_SMCU clean
		$(MAKE) TARGET=BOOT_MK3_CMCU clean

# ---- Generated C source -----------------------------------------------------

$(OBJDIR)devcfg.o: devcfg.inc

devcfg.inc:	$(ANELOK_DEVCFG) mkdevcfg
		./mkdevcfg $< >$@ || { rm -f $@; exit 1; }

generated_headers:: vecs.inc

vecs.inc:	genvecs.pl
		./genvecs.pl >vecs.inc \
		    DMA0=dma0 DMA1=dma1 DMA2=dma2 DMA3=dma3 FTFA=ftfa \
		    LVD_LVW=lvd_lvw LLW=llwu I2C0=i2c0 I2C1=i2c1 \
		    SPI0=spi0 SPI1=spi1 UART0=uart0 UART1=uart1 UART2=uart2 \
		    ADC0=adc CMP0=cmp \
		    TPM0=tpm0 TPM1=tpm1 TPM2=tpm2 RTC=rtc RTC_Seconds=seconds \
		    PIT=pit \
		    I2S0=i2s USB0=usb DAC0=dac TSI0=touch MCG=mcg \
		    LPTMR0=tick PORTA=port_a PORTC_PORTD=port_cd || \
		    { rm -f $@; exit 1; }

$(OBJDIR)account.o: keys.inc

keys.inc:	../crypter/anelok.keys
		$(MAKE) -C ../crypter keycode OUTFILE=`pwd`/$@ || \
		  { rm -f $@; exit 1; }

../crypter/anelok.keys:
		$(MAKE) -C ../crypter keygen

# ---- Fonts ------------------------------------------------------------------

fonts:		$(FONTS:%=font_%.h)

disp/text.c:	$(FONTS:%=font_%.h)

FONTDIR = /usr/share/fonts/X11/misc
FONT_10x20 = $(FONTDIR)/10x20.pcf.gz

font_10x20.h:	$(FONT_10x20) fontify.pl
		pcf2bdf $< | ./fontify.pl font_10x20 >$@ || \
		    { rm -f $@; exit 1; }

# ----- Executable ------------------------------------------------------------

%.srec:		%.elf
		$(BUILD) $(OBJCOPY) -O srec $< $@

%.bin:		%.elf
		$(BUILD) $(OBJCOPY) -O binary $< $@
		@echo -n "build #$(BUILD_NUMBER) $(BUILD_DATE) "
		@echo "$(BUILD_HASH)$(if $(BUILD_DIRTY:0=),+)"

$(NAME).elf:	$(OBJS_IN_OBJDIR) $(LD_SCRIPT)
		$(MAKE) $(OBJDIR)version.o
		$(CC) $(CFLAGS) -T $(LD_SCRIPT) -o $@ $(OBJS_IN_OBJDIR) \
		    $(OBJDIR)version.o
		$(SIZE) $@

$(NAME).lds:	$(LD_SCRIPT_BARE) mklds
		$(BUILD) ./mklds $(PART_BASE) $< >$@ || { rm -f $@; exit 1; }

# ----- Flashing (FRDM-KL25Z) -------------------------------------------------

frdm:		$(NAME).srec
		mount -L FRDM-KL25Z /mnt/tmp
		cp $(NAME).srec /mnt/tmp
		sync
		cat /mnt/tmp/LASTSTAT.TXT
		@echo
		umount /mnt/tmp

# ----- Flashing (Ben and libswd) ---------------------------------------------

flash:		$(NAME).bin
		scp $^ ben:
		ssh ben ./swd $^

# ----- CC2543 firmware -------------------------------------------------------

cc:
		$(MAKE) -C ccfw

# ----- DFU -------------------------------------------------------------------

dfu:		$(NAME).bin
		dfu-util -d $(USB_ID) -a KL26 -D $^

update:		$(NAME).bin
		$(USBWAIT) -r -i 0.01 $(USB_ID)
		$(MAKE) dfu

ccdfu:
		$(MAKE) -C ccfw USB_ID=$(USB_ID) dfu

# ----- Test-build all targets ------------------------------------------------

targets:
		for n in $(TARGETS); do make TARGET=$$n clean; done
		for n in $(TARGETS); do echo "# ----- $$n -----" 1>&2; \
		    make TARGET=$$n || exit; \
		    make TARGET=$$n clean || exit; done

# ----- Tags ------------------------------------------------------------------

etags:
		git ls-files | etags -

# ----- Cleanup ---------------------------------------------------------------

clean::
		rm -f devcfg.inc vecs.inc
		rm -f keys.inc
		rm -f $(NAME).elf $(NAME).srec $(NAME).bin
		rm -f $(NAME).lds
		$(MAKE) -C ccfw clean

clean-all:
		for n in $(TARGETS); do make TARGET=$$n clean; done

spotless:	clean
		rm -f $(FONTS:%=font_%.h)
#		rm -f $(FRDM_KL26Z_SC_EXE)
		rm -f .version
		$(MAKE) -C ccfw spotless
