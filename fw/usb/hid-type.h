/*
 * fw/usb/hid-type.h - Type strings on HID
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef HID_TYPE
#define	HID_TYPE

#include <stdint.h>

#include "hid.h"


/*
 * According to page 56 (66) of
 * http://www.usb.org/developers/hidpage/HID1_11.pdf
 */

enum hid_modifier_code {
	HID_MOD_LEFT_CTRL	= 0,
	HID_MOD_LEFT_SHIFT,
	HID_MOD_LEFT_ALT,
	HID_MOD_LEFT_GUI,
	HID_MOD_RIGHT_CTRL,
	HID_MOD_RIGHT_SHIFT,
	HID_MOD_RIGHT_ALT,
	HID_MOD_RIGHT_GUI,
};


/*
 * According to pages 53-59 of
 * http://www.usb.org/developers/hidpage/Hut1_12v2.pdf
 */

enum hid_key_code {
	HID_KEY_UNDEFINED	= 3,
	HID_KEY_A		= 4,
	HID_KEY_B,
	HID_KEY_C,
	HID_KEY_D,
	HID_KEY_E,
	HID_KEY_F,
	HID_KEY_G,
	HID_KEY_H,
	HID_KEY_I,
	HID_KEY_J,
	HID_KEY_K,
	HID_KEY_L,
	HID_KEY_M,
	HID_KEY_N,
	HID_KEY_O,
	HID_KEY_P,
	HID_KEY_Q,
	HID_KEY_R,
	HID_KEY_S,
	HID_KEY_T,
	HID_KEY_U,
	HID_KEY_V,
	HID_KEY_W,
	HID_KEY_X,
	HID_KEY_Y,
	HID_KEY_Z,
	HID_KEY_1		= 30,
	HID_KEY_2,
	HID_KEY_3,
	HID_KEY_4,
	HID_KEY_5,
	HID_KEY_6,
	HID_KEY_7,
	HID_KEY_8,
	HID_KEY_9,
	HID_KEY_0,
	HID_KEY_ENTER		= 40,
	HID_KEY_ESCAPE		= 41,
	HID_KEY_DELETE		= 42,
	HID_KEY_TAB		= 43,
	HID_KEY_SPACE		= 44,
	HID_KEY_MINUS		= 45,
	HID_KEY_EQUAL		= 46,
	HID_KEY_BRACKET_OPEN	= 47,
	HID_KEY_BRACKET_CLOSE	= 48,
	HID_KEY_BACKSLASH	= 49,
	HID_KEY_GER_HASH	= 50,
	HID_KEY_SEMICOLON	= 51,
	HID_KEY_APOSTROPHE	= 52,
	HID_KEY_GRAVE		= 53,
	HID_KEY_COMMA		= 54,
	HID_KEY_PERIOD		= 55,
	HID_KEY_SLASH		= 56,

	HID_KEY_HOME		= 74,
	HID_KEY_PAGE_UP		= 75,
	HID_KEY_END		= 77,
	HID_KEY_PAGE_DOWN	= 78,
	HID_KEY_RIGHT		= 79,
	HID_KEY_LEFT		= 80,
	HID_KEY_DOWN		= 81,
	HID_KEY_UP		= 82,

	HID_KEY_INTERNATIONAL1	= 135,
};


struct hid_type_ctx {
	void (*fn)(void *user);
	void *user;
	const char *s;
	uint16_t n;

	struct hid_req req;
};

void hid_type_n(struct hid_type_ctx *ctx, const char *s, uint16_t len,
    void (*fn)(void *user), void *user);
void hid_type(struct hid_type_ctx *ctx, const char *s,
    void (*fn)(void *user), void *user);

#endif /* HID_TYPE */
