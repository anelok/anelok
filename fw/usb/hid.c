/*
 * fw/usb/hid.c - HID definitions
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "qa.h"
#include "usb.h"
#include "kinetis-common.h"
#include "hid.h"


/*
 * Based on section E.6 on page 69 (79) of
 * http://www.usb.org/developers/hidpage/HID1_11.pdf
 *
 * Note that there is a similar table in
 * https://github.com/bbenchoff/UnhappyHacking/blob/master/Firmware/main.c
 */

static const uint8_t hid_report_descriptor[HID_REPORT_DESC_LEN] = {
	0x05, 0x01,	// Usage Page (Generic Desktop),
	0x09, 0x06,	// Usage (Keyboard),
	0xa1, 0x01,	// Collection (Application),
	0x05, 0x07,	//   Usage Page (Key Codes);
	0x19, 0xe0,	//   Usage Minimum (224),
	0x29, 0xe7,	//   Usage Maximum (231),
	0x15, 0x00,	//   Logical Minimum (0),
	0x25, 0x01, 	//   Logical Maximum (1),
	0x75, 0x01,	//   Report Size (1),
	0x95, 0x08,	//   Report Count (8),
        0x81, 0x02,	//   Input (Data, Variable, Absolute),	;Modifier byte
	0x95, 0x01,	//   Report Count (1),
	0x75, 0x08,	//   Report Size (8),
        0x81, 0x01,	//   Input (Constant),		;Reserved byte
	0x95, 0x05,	//   Report Count (5),
	0x75, 0x01,	//   Report Size (1),
	0x05, 0x08,	//   Usage Page (Page# for LEDs),
	0x19, 0x01,	//   Usage Minimum (1),
	0x29, 0x05,	//   Usage Maximum (5),
	0x91, 0x02,	//   Output (Data, Variable, Absolute),	;LED report
	0x95, 0x01,	//   Report Count (1),
	0x75, 0x03,	//   Report Size (3),
   	0x91, 0x01,	//   Output (Constant),		;LED report padding
	0x95, 0x06,	//   Report Count (6),
	0x75, 0x08,	//   Report Size (8),
	0x15, 0x00,	//   Logical Minimum (0),
	0x25, 0x65,	//   Logical Maximum(101),
	0x05, 0x07,	//   Usage Page (Key Codes),
	0x19, 0x00,	//   Usage Minimum (0),
	0x29, 0x65,	//   Usage Maximum (101),
	0x81, 0x00,	//   Input (Data, Array),	;Key arrays (6 bytes)
	0xc0		// End Collection
};


static uint8_t report_out[8];
static uint8_t report_in[8];
static uint16_t report_in_type;
static uint8_t report_in_len;


bool hid_get_descriptor(uint8_t type, uint8_t index, const uint8_t **reply,
    uint8_t *size)
{
	switch (type) {
	case USB_DT_HID_DEVICE:
		*reply = device_descriptor + 18;
		*size = 9;
		break;
	case USB_DT_HID_REPORT:
		*reply = hid_report_descriptor;
		*size = HID_REPORT_DESC_LEN;
		break;
	default:
		return 0;
	}
	return 1;
}


bool hid_setup(const struct setup_request *setup)
{
	switch (setup->bmRequestType | setup->bRequest << 8) {
	case HID_FROM_DEV(HID_GET_REPORT):
		usb_send(&eps[0], report_out, 8, NULL, NULL);
		return 1;
	case HID_TO_DEV(HID_SET_REPORT):
		if (setup->wLength > sizeof(report_in))
			return 0;
		report_in_type = setup->wValue;
		report_in_len = setup->wLength;
		usb_recv(&eps[0], report_in, setup->wLength, NULL, NULL);
		return 1;
	case HID_FROM_DEV(HID_GET_IDLE):
		usb_send(&eps[0], "", 1, NULL, NULL);
		return 1;
	case HID_FROM_DEV(HID_SET_IDLE):
		/* ignore */
		return 1;
	case HID_FROM_DEV(HID_GET_PROTOCOL):
		usb_send(&eps[0], "", 1, NULL, NULL);
		return 1;
	case HID_TO_DEV(HID_SET_PROTOCOL):
		/* ignore */
		return 1;
	default:
		return 0;
	}
}


static void hid_send_release(void *user)
{
	const struct hid_req *req = user;

	report_out[0] = report_out[2] = 0;
	usb_send(&eps[1], report_out, 8, req->done, req->user);
}


static void hid_send_key(void *user)
{
	const struct hid_req *req = user;

	report_out[2] = req->key;
	usb_send(&eps[1], report_out, 8, hid_send_release, (void *) req);
}


void hid_press_key(const struct hid_req *req)
{
	if (eps[1].state != EP_IDLE)
		oops();

	report_out[0] = req->modifiers;
	report_out[2] = 0;	/* @@@ not really necessary */

	if (req->modifiers) {
		usb_send(&eps[1], report_out, 8, hid_send_key, (void *) req);
	} else {
		hid_send_key((void *) req);
	}
}
