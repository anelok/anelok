/*
 * fw/usb-board.c - Board-specific USB functions
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "misc.h"
#include "regs.h"
#include "board.h"
#include "usb-board.h"


bool usb_sof(void)
{
	uint8_t i;

	USB0_ISTAT = 0xff;	/* clear all interrupts */

	/* reset takes >= 50 ms, so we loop for 200 ms */
	for (i = 0; i != 100; i++) {
		mdelay(2);
		if (USB0_ISTAT & USB_ISTAT_SOFTOK_MASK)
			return 1;
	}
	return 0;
}


#ifdef USB_ID

bool usb_a(void)
{
	bool is_open;

	gpio_init_in(USB_ID, 1);
	mdelay(1);
	is_open = gpio_read(USB_ID);
	gpio_init_off(USB_ID);
	return !is_open;
}

#endif /* USB_ID */


void usb_board_init(void)
{
#ifdef USB_ID
	gpio_init_off(USB_ID);
#endif /* USB_ID */
}
