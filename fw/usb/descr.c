/*
 * fw/descr.c - USB descriptors
 *
 * Written 2008-2011, 2014-2015 by Werner Almesberger
 * Copyright 2008-2011, 2014-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>

#include "usb.h"
#include "dfu.h"
#include "hid.h"
#include "board.h"


#define LE(x) ((uint16_t) (x) & 0xff), ((uint16_t) (x) >> 8)

/*
 * Device descriptor
 */

const uint8_t device_descriptor[18] = {
	18,			/* bLength */
	USB_DT_DEVICE,		/* bDescriptorType */
	LE(0x110),		/* bcdUSB */
	USB_CLASS_PER_INTERFACE,/* bDeviceClass */
	0x00,			/* bDeviceSubClass */
	0x00,			/* bDeviceProtocol */
	EP0_SIZE,		/* bMaxPacketSize */
	LE(USB_VENDOR),		/* idVendor */
	LE(USB_PRODUCT),	/* idProduct */
	LE(0x0001),		/* bcdDevice */
	0,			/* iManufacturer */
	0,			/* iProduct */
#ifdef HAS_BOARD_SERNUM
	1,			/* iSerialNumber */
#else
	0,			/* iSerialNumber */
#endif
	1			/* bNumConfigurations */
};


/*
 * Our configuration
 *
 * We're always bus-powered.
 */

const uint8_t config_descriptor[] = {
	9,			/* bLength */
	USB_DT_CONFIG,		/* bDescriptorType */
	LE(9 + 9 + 9 + 9 + 7 + 9),
				/* wTotalLength */
	3,			/* bNumInterfaces */
	1,			/* bConfigurationValue (> 0 !) */
	0,			/* iConfiguration */
	USB_ATTR_BUS_POWERED,	/* bmAttributes */
	((BOARD_MAX_mA) + 1) / 2, /* bMaxPower */

	/* Interface #0 */

	9,			/* bLength */
	USB_DT_INTERFACE,	/* bDescriptorType */
	0,			/* bInterfaceNumber */
	0,			/* bAlternateSetting */
	0,			/* bNumEndpoints */
	USB_CLASS_VENDOR_SPEC,	/* bInterfaceClass */
	0,			/* bInterfaceSubClass */
	0,			/* bInterfaceProtocol */
	0,			/* iInterface */

	/* Interface #1 */

	9,			/* bLength */
	USB_DT_INTERFACE,	/* bDescriptorType */
	1,			/* bInterfaceNumber */
	0,			/* bAlternateSetting */
	1,			/* bNumEndpoints */
	USB_CLASS_HID,		/* bInterfaceClass */
	USB_SUBCLASS_HID_BOOT,	/* bInterfaceSubClass */
	HID_PROTO_KEYBOARD,	/* bInterfaceProtocol */
	0,			/* iInterface */

	/* HID Device */

	9,			/* bLength */
	USB_DT_HID_DEVICE,	/* bDescriptorType */
	LE(0x111),		/* bcdHID */
	HID_COUNTRY_NONE,	/* bCountryCode */
	1,			/* bNumDescriptors */
	USB_DT_HID_REPORT,	/* bDescriptorType */
	LE(HID_REPORT_DESC_LEN),/* wDescriptorLength */

#if 1
	/* EP IN */

	7,			/* bLength */
	USB_DT_ENDPOINT,	/* bDescriptorType */
	0x81,			/* bEndPointAddress */
	USB_ENDPOINT_TYPE_INTERRUPT,
				/* bmAttributes (bulk) */
	LE(EP1_SIZE),		/* wMaxPacketSize */
	14,			/* bInterval */
#endif

	/* Interface #1 */

	DFU_ITF_DESCR(2, 0, dfu_proto_runtime, 0)
};
