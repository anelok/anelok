/*
 * fw/usb/host.c - USB host
 *
 * Written 2013, 2015 by Werner Almesberger
 * Copyright 2013, 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>

#include "usb.h"

#include "host.h"
#include "kinetis-common.h" // @@@



static struct host_ep ep0;
static struct urb setup_urb;
static struct setup_request setup;
static uint8_t dev_descr[18];


static void dev_descr_all(struct urb *urb, bool ok)
{
	if (!ok) {
		DEBUGF("urb %p failed\n");
		return;
	}

	DEBUGF("\nVID:PID %04x:%04x\n",
	    dev_descr[9] << 8 | dev_descr[8],
	    dev_descr[11] << 8 | dev_descr[10]);
}


static void dev_descr_8(struct urb *urb, bool ok)
{
	if (!ok) {
		DEBUGF("urb %p failed\n");
		return;
	}

	ep0.size = dev_descr[7];
	DEBUGF("URB %d OK (%d)\n", urb->done, ep0.size);
	usb_fill_setup(&setup, FROM_DEVICE(0), GET_DESCRIPTOR,
	    USB_DT_DEVICE << 8, 0, sizeof(dev_descr));
	usb_setup_urb(urb, &ep0, &setup, dev_descr, sizeof(dev_descr),
	    dev_descr_all);
	usb_submit_urb(urb);
}


void usb_attach(void)
{
	ep0.addr = 0;
	ep0.n = 0;
	ep0.size = 8;
	usb_fill_setup(&setup, FROM_DEVICE(0), GET_DESCRIPTOR,
	    USB_DT_DEVICE << 8, 0, 8);
	usb_setup_urb(&setup_urb, &ep0, &setup, dev_descr, 8, dev_descr_8);
	usb_submit_urb(&setup_urb);
}


void usb_detach(void)
{
}
