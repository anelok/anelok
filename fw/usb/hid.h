/*
 * fw/usb/hid.h - HID definitions
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef USB_HID_H
#define	USB_HID_H

#include <stdbool.h>
#include <stdint.h>

#include "usb.h"


/* Subclass */

#define	USB_SUBCLASS_HID_BOOT	1

/* Protocol */

#define	HID_PROTO_KEYBOARD	1
#define	HID_PROTO_MOUSE		2

/* HID descriptor types */

#define	USB_DT_HID_DEVICE	33
#define	USB_DT_HID_REPORT	34

/* HID country codes */

#define	HID_COUNTRY_NONE	0

#define	HID_TO_DEV(req)		(0x21 | (req) << 8)
#define	HID_FROM_DEV(req)	(0xa1 | (req) << 8)

enum hid_request {
	HID_GET_REPORT		= 1,
	HID_GET_IDLE		= 2,
	HID_GET_PROTOCOL	= 3,
	HID_SET_REPORT		= 9,
	HID_SET_IDLE		= 10,
	HID_SET_PROTOCOL	= 11
};

enum his_report_type {
	HID_REPORT_INPUT	= 1,
	HID_REPORT_OUTPUT	= 2,
	HID_REPORT_FEATURE	= 3,
};

#define	HID_REPORT_DESC_LEN	63


struct hid_req {
	uint8_t modifiers;
	uint8_t key;
	void (*done)(void *user);
	void *user;
};


struct setup_request; /* @@@ simulator's usb.h is not what we expect here */


bool hid_get_descriptor(uint8_t type, uint8_t index, const uint8_t **reply,
    uint8_t *size);
bool hid_setup(const struct setup_request *setup);

void hid_press_key(const struct hid_req *req);

#endif /* !USB_HID_H */
