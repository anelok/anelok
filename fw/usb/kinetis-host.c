/*
 * fw/usb/kinetis.c - Kinetis USB device
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Reference manual erratum: section 35.6 "Host Mode Operation Examples"
 * suggests that host mode always uses the TX side of EP0's BDT entry.
 * This is not true: only SETUP and OUT use TX while IN uses RX.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb.h"

#include "qa.h"
#include "console.h"
#include "misc.h"
#include "regs.h"
#include "usb-board.h"
#include "host.h"
#include "kinetis-common.h"


static bool attached = 0;
static bool low_speed;
static struct urb *curr_urb = NULL;
static struct urb **last_urb = &curr_urb;
static bool ignore_urb = 0;


/* ----- Reset and initialization ------------------------------------------ */


static void host_reset(void)
{
	USB0_CTL |= USB_CTL_RESET_MASK;
	mdelay(15);
	USB0_CTL &= ~USB_CTL_RESET_MASK;
}


/* ----- URB processing ---------------------------------------------------- */


static void start_urb(struct urb *urb)
{
	volatile struct BD *bd;
	struct host_ep *ep = urb->ep;
	uint16_t size;

	bd = curr_bd(0, urb->pid != PID_IN);
//DEBUGF("S%p:%x+%d,", bd, urb->pid, urb->len);
	/*
	 * All lovingly described in section 35.6 of the refernce manual.
	 */
	USB0_ENDPT0 = (low_speed ? USB_ENDPT_HOSTWOHUB_MASK : 0) |
	    USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPRXEN_MASK |
	    USB_ENDPT_EPHSHK_MASK |
	    (urb->pid == PID_SETUP ? 0 : USB_ENDPT_EPCTLDIS_MASK);

	if (urb->pid == PID_SETUP) {
		size = 8;
		bd->buf = (void *) urb->setup;	/* ignore the "const" */
	} else if (urb->len == 0) {
		size = 0;
	} else {
		size = urb->len - urb->done;
		if (size > ep->size)
			size = ep->size;
		bd->buf = urb->buf + urb->done;
	}
	DEBUGF("S(%x+%d)", urb->pid, size);
	bd->flags = size << BD_BC_SHIFT | BD_OWN | (ep->toggle ? BD_DATA : 0);
//DEBUGF("<%x>", bd->flags);

	USB0_ADDR = (low_speed ? USB_ADDR_LSEN_MASK : 0) | ep->addr;
//DEBUGF("%x", USB0_ADDR);
	USB0_TOKEN = urb->pid << USB_TOKEN_TOKENPID_SHIFT | ep->n;
}


static void token_done(void)
{
	volatile struct BD *bd;
	struct urb *urb = curr_urb;
	struct urb *next;
	struct host_ep *ep;
	uint8_t pid, len;
	bool tx;

	//DEBUGF("T%p", urb);
	console_update();
	if (!urb)
		oops();
	tx = urb->pid != PID_IN;
	bd = curr_bd(0, tx);
	ep = urb->ep;
	flip_odd(0, tx);

	if (ignore_urb) {
		ignore_urb = 0;
		goto next;
	}

	pid = (bd->flags & BD_PID_MASK) >> BD_PID_SHIFT;
		len = (bd->flags & BD_BC_MASK) >> BD_BC_SHIFT;
DEBUGF("(%x+%d/%d)", pid, len, urb->len);
console_update();
	switch (pid) {
	case PID_ACK:
	case PID_DATA0: /* @@@ can we ever get this ? */
	case PID_DATA1: /* @@@ can we ever get this ? */
		if (urb->setup) {
			uint8_t type = *(const uint8_t *) urb->setup;
			bool from = type & FROM_DEVICE(0);
			uint8_t data = from ? PID_IN : PID_OUT;

			if (urb->pid == PID_SETUP) {
				if (urb->len)
					urb->pid = data;
				else
					urb->pid = PID_IN ^ PID_OUT ^ data;
				ep->toggle = 1;
				urb->done = 0;
				start_urb(urb);
				return;
			}
			if (urb->pid != data)
				goto complete;
		}
		len = (bd->flags & BD_BC_MASK) >> BD_BC_SHIFT;
		urb->done += len;
		ep->toggle = !ep->toggle;
#if 1
		if (urb->setup && urb->len == urb->done)
			goto status;
#endif
		if (len == ep->size) {
			start_urb(urb);
			return;
		}
		if (urb->setup) {
			if (len || urb->len) {
status:
				urb->pid = PID_IN ^ PID_OUT ^ urb->pid;
				urb->len = 0;
				ep->toggle = 1;
				start_urb(urb);
				return;
			}
		}

complete:
		next = urb->next;
		curr_urb = next;
		if (!curr_urb)
			last_urb = &curr_urb;
		if (urb->fn)
			urb->fn(urb, 1);
		if (next && curr_urb == next)
			start_urb(next);
		return;
	case PID_timeout:
	case PID_error:
		if (urb->fn)
			urb->fn(urb, 0);
		break;
	case PID_STALL:
		DEBUGF("STALL");
		console_update();
		/* fall through */
	default:
		oops();
	}

next:
	curr_urb = urb->next;
	if (!curr_urb)
		last_urb = &curr_urb;
	if (curr_urb)
		start_urb(curr_urb);
}


void usb_submit_urb(struct urb *urb)
{
	/* @@@ will require locking when going to interrupts */

	urb->next = NULL;
	*last_urb = urb;
	last_urb = &urb->next;
	if (curr_urb == urb)
		start_urb(urb);
}


bool usb_cancel_urb(struct urb *urb)
{
	struct urb **anchor;

	if (curr_urb == urb) {
		ignore_urb = 1;
		return 1;
	}
	for (anchor = &curr_urb; *anchor; anchor = &(*anchor)->next)
		if (*anchor == urb) {
			*anchor = urb->next;
			return 1;
		}
	return 0;
}


/* ----- Interrupt dispatcher (host) --------------------------------------- */


static void attach(void)
{
	uint16_t n = 0;

	DEBUGF("ATTACH");
	console_update();

	attached = 1;
	while (USB0_CTL & USB_CTL_SE0_MASK)
		if (!++n) {
			DEBUGF(".");
			console_update();
		}
	console_update();
	low_speed = !(USB0_CTL & USB_CTL_JSTATE_MASK);
	DEBUGF(" %cS\n", low_speed ? 'L' : 'F');
	console_update();

	USB0_ENDPT0 = low_speed ? USB_ENDPT_HOSTWOHUB_MASK : 0;

	host_reset();

	USB0_CTL |= USB_CTL_USBENSOFEN_MASK; /* start sending SOF */
	USB0_ENDPT0 |= USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPRXEN_MASK |
	    USB_ENDPT_EPHSHK_MASK;

	mdelay(5);
	usb_attach();
}


static void detach(void)
{
	DEBUGF("detach");

	console_update();

	usb_detach();

	/* @@@ controller reset; clear ignore_urb and reset curr_urb  */

	USB0_CTL &= ~USB_CTL_USBENSOFEN_MASK;
	USB0_ENDPT0 = 0;
	attached = 0;
}


void usb_poll_host(void)
{
	uint8_t istat;

{
static uint16_t n = 0;
if (!n++) { DEBUGF("."); }

}
	while (1) {
		istat = USB0_ISTAT;
		if (!istat)
			break;
		USB0_ISTAT = istat;
		if (istat & USB_ISTAT_ATTACH_MASK) {
			if (!attached)
				attach();
		}
		if (istat & USB_ISTAT_TOKDNE_MASK) {
			DEBUGF("T");

			token_done();
		}
		if (istat & USB_ISTAT_USBRST_MASK) {
			DEBUGF("R");

			if (attached)
				detach();
			console_update();
		}
		if (istat & USB_ISTAT_ERROR_MASK) {
			DEBUGF("E%02x", USB0_ERRSTAT);

			/* @@@ count errors ? */
			USB0_ERRSTAT = USB0_ERRSTAT;
		}
//		USB0_ISTAT = istat;
	}
}


/* ----- Reset and initialization ------------------------------------------ */


void usb_begin_host(void)
{
	usb_board_begin();

	kinetis_reset_module();
	kinetis_setup_module();

	USB0_CTL = USB_CTL_HOSTMODEEN_MASK;	/* enable module */
	USB0_CONTROL = 0;			/* no pull-up */
	USB0_USBCTRL = USB_USBCTRL_PDE_MASK;
		/* disable suspend, enable pull-downs */
	USB0_SOFTHLD = 74;
		/* SOF timing for <= 64 byte packets */
}


void usb_end_host(void)
{
	kinetis_end();
}
