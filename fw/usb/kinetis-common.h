/*
 * fw/usb/kinetis-common.h - Kinetis USB, shared device and host code
 *
 * Written 2013-2015 by Werner Almesberger
 * Copyright 2013-2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef KINETIS_COMMON_H
#define	KINETIS_COMMON_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "board.h"


#ifdef DEBUG
#include "console.h"

#define	DEBUGF(...)		console_printf(__VA_ARGS__)
#define	console_update()	console_update()
#else
#define	DEBUGF(...)
#define	console_update()
#endif


#define	PID_timeout	0x0	/* host: bus timeout */
#define	PID_error	0xf	/* host: data error */


/* ----- Buffer descriptor table ------------------------------------------- */


#define	BD_BC_MASK	0x03ff0000	/* byte count */
#define	BD_BC_SHIFT	16
#define	BD_OWN		0x00000080	/* controller owns the BD */
#define	BD_DATA		0x00000040	/* send/receivd DATA0/1 */
#define	BD_KEEP		0x00000020	/* keep BD */
#define	BD_NINC		0x00000010	/* no increment */
#define	BD_DTS		0x00000008	/* data toggle synchronization */
#define	BD_STALL	0x00000004	/* send stall */

#define	BD_PID_MASK	0x0000003c	/* PID received */
#define	BD_PID_SHIFT	2


struct BD {
	uint32_t flags;
	void *buf;
};


/*
 * Indices of BDT:
 * - EP number
 * - TX (0 for receive)
 * - ODD (ping-pong)
 */

extern volatile struct BD BDT[NUM_EPS][2][2];
extern bool bd_odd[NUM_EPS][2];
extern bool bd_data[NUM_EPS];


static inline void flip_odd(uint8_t ep, bool tx)
{
	bd_odd[ep][tx] = !bd_odd[ep][tx];
}


static inline volatile struct BD *curr_bd(uint8_t ep, bool tx)
{
	return &BDT[ep][tx][bd_odd[ep][tx]];
}



void kinetis_reset_module(void);
void kinetis_setup_module(void);
void kinetis_end(void);

#endif /* !KINETIS_COMMON_H */
