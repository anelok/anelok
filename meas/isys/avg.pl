#!/usr/bin/perl
#
# isys/avg.pl - Average current in measurement intervals
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

@xrange = (0.5, 0.8);

while (<>) {
	chop;
	if (/^# 20/) {
		$t = "20$'";
		next;
	}
	next if /^#/;
	if (/^\s*$/) {
		print "$t ", $s/$n, " $s $n\n" if defined $t;
		($s, $n) = 0;
		undef $t;
		next;
	}
	die unless /^([0-1](\.\d*))\s+/;
	next if $1 < $xrange[0] || $1 > $xrange[1];
	$s += $';
	$n++;
}
