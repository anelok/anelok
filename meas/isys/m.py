#!/usr/bin/python
#
# isys/m.py - Run current measurements
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Each measurement serie is deliminated by a high-current event (LED being
# lit). This program collects samples and each time it finds a sequence
# delimited by high-current events, it dumps it in gnuplot format. The x axis
# is the normalized position in the interval, i.e., [0, 1)
#

import sys, datetime
import tmc.meter


def print_vec(v):
	print "# "+str(datetime.datetime.today())
	for i in range(len(v)):
		print float(i)/len(v), v[i]
	print
	print


# Measure in the 100 uA range by default
r = 100e-6

if len(sys.argv) > 1:
	if len(sys.argv) > 2:
		r = float(sys.argv[2])
	limit = int(sys.argv[1])
else:
	limit = None

# @@@ FIXME: can't disable initial debug output on stdout, do we "comment" it
print "# ",
m = tmc.meter.fluke_8845a("fluke")
m.debug = False

m.i(r)

first = True
v = []
errors = m.errors()
i = 0
while limit is not None and i < limit:
	s = m.sample()
	if s < r:
		if not first:
			v.append(s)
	else:
		if first:
			first = False
		elif len(v) != 0:
			if errors == m.errors():
				print_vec(v)
				sys.stdout.flush()
				sys.stderr.write(".")
				i += 1
			else:
				sys.stderr.write("E")
				errors = m.errors()
			sys.stderr.flush()

			v = []
