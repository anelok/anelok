#!/usr/bin/python
#
# crypter/keyfile.py - Key file
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import sys
import re
import nacl
from nacl.secret import SecretBox
from nacl.public import PublicKey, PrivateKey
from nacl.bindings import crypto_scalarmult_base

import base32


encrypted_magic = b'EAK0\n'
name_tag = "Name"
pin_tag = "PIN"
flags_tag = "Flags"
seckey_tag = "Secret key"
pubkey_tag = "Public key"


# Scrypt looks like a good choice for key derivation. However, it's been added
# only very recently to PyNaCl, so we temporarily use something simpler and
# weaker.
#
# https://pynacl.readthedocs.io/en/latest/password_hashing/#key-derivation
#
# Scrypt-using code below is completely untested.
#

#from nacl.pwhash import kdf_scryptsalsa208sha256


def notyet_decrypt(s, password):
	salt = s[0:pwhash.SCRYPT_SALTBYTES]
	key = kdf_scryptsalsa208sha256(SecretBox.KEY_SIZE, password, salt)
	box = SecretBox(key)
	return box.decrypt(s[pwhash.SCRYPT_SALTBYTES:])


def notyet_encrypt(s, password):
	salt = nacl.utils.random(pwhash.SCRYPT_SALTBYTES)
	key = kdf_scryptsalsa208sha256(SecretBox.KEY_SIZE, password, salt)
	nonce = nacl.utils.random(SecretBox.NONCE_SIZE)
	box = SecretBox(key)
	return salt + box.encrypt(s, nonce)

#
# Homebrewn key derivation, intentionally fairly primitive. Don't trust this
# for anything serious.
#
# Why we need key stretching in the first place:
# https://www.schneier.com/academic/paperfiles/paper-low-entropy.pdf
#

from nacl.hash import sha256


class Hash:

	def decrypt(self, s):
		if self.salt != s[0:SecretBox.KEY_SIZE]:
			raise Exception("Salt mismatch")
		box = SecretBox(self.hash)
		return box.decrypt(s[SecretBox.KEY_SIZE:])

	def encrypt(self, s):
		nonce = nacl.utils.random(SecretBox.NONCE_SIZE)
		box = SecretBox(self.hash)
		return self.salt + box.encrypt(s, nonce)

	def __init__(self, password, s = None):
		if s is None:
			self.salt = nacl.utils.random(SecretBox.KEY_SIZE)
		else:
			mlen = len(encrypted_magic)
			if s[0:mlen] != encrypted_magic:
				self.hash = None
				return
			self.salt = s[mlen:SecretBox.KEY_SIZE + mlen]
		self.hash = sha256(self.salt + password,
		    encoder = nacl.encoding.RawEncoder)


class Key:

	@staticmethod
	def name_is_valid(s):
		return re.match("^\S+$", s)

	def decode(self, s):
		m = re.search(name_tag + ":\s+(\S*)\s*$", s, re.MULTILINE)
		if m:
			self.name = m.group(1)
		else:
			raise Exception("Name is required")

		m = re.search(pin_tag + ":\s+0x(.*)\s*$", s, re.MULTILINE)
		if m:
			self.pin = int(m.group(1), 16)
		else:
			self.pin = None

		m = re.search(seckey_tag + ":\s+(.*)\s*$", s, re.MULTILINE)
		if m:
			seckey = base32.decode(m.group(1))[0:32]
			if len(seckey) == PrivateKey.SIZE:
				self.seckey = seckey
			else:
				raise Exception("Invalid secret key")
		else:
			self.seckey = None

		m = re.search(pubkey_tag + ":\s+(.*)\s*$", s, re.MULTILINE)
		if m:
			self.pubkey = base32.decode(m.group(1))[0:32]
		else:
			if self.seckey is None:
				self.pubkey = None
			else:
				self.pubkey = \
				    crypto_scalarmult_base(self.seckey)

		m = re.search(flags_tag + ":\s*([RW]*)\s*$", s, re.MULTILINE)
		if m:
			flags = m.group(1).upper()
			self.is_reader = "R" in flags
			self.is_writer = "W" in flags
		else:
			self.is_reader = False
			self.is_writer = False
		self.update()

	def dump(self):
		assert not self.deleted

		s = ""

		if self.name:
			s += name_tag + ": " + self.name + "\n"

		if self.pin:
			s += pin_tag + ": " + "0x%x\n" % self.pin

		s += flags_tag + ": "
		if self.is_reader:
			s += "R"
		if self.is_writer:
			s += "W"
		s += "\n"

		s += seckey_tag + ": "
		s += base32.encode(self.seckey, pretty = 1)
		s += "\n"
		return s

	def set_reader(self, set = True):
		if set and self.pubkey is None:
			raise Exception("Need pubkey to be reader")
		self.is_reader = set
			
	def set_writer(self, set = True):
		if set and self.seckey is None:
			raise Exception("Need seckey to be writer")
		self.is_writer = set

	def delete(self, undelete = False):
		self.deleted = not undelete

	def update(self):
		self.orig_name = self.name
		self.orig_seckey = self.seckey
		self.orig_pubkey = self.pubkey

	def has_changed(self):
		return self.name != self.orig_name or \
		    self.seckey != self.orig_seckey or \
		    self.pubkey != self.orig_pubkey

	def revert(self):
		self.name = self.orig_name 
		self.seckey = self.orig_seckey
		self.pubkey = self.orig_pubkey

	def generate(self, name, pin = None):
		key = PrivateKey.generate()
		self.name = name
		self.seckey = bytes(key)
		self.pubkey = bytes(key.public_key)
		self.pin = pin

	def __init__(self, s = None, seckey = None, pubkey = None):
		self.name = None
		self.pin = None
		self.is_reader = False
		self.is_writer = False
		self.deleted = False
		if seckey:
			PrivateKey(seckey)
		if pubkey:
			PublicKey(pubkey)
		self.seckey = seckey
		self.pubkey = pubkey
		if s:
			self.decode(s)
		if self.seckey and self.pubkey is None:
			self.pubkey = crypto_scalarmult_base(self.seckey)
		self.update()


class Keyfile(list):

	def find(self, name):
		for key in self:
			if key.name == name:
				return key
		return None

	def parse_key(self, s):
		if s == "":
			return
		key = Key(s)
		if self.find(key.name):
			raise Exception("Duplicate key name '" + key.name + "'")
		self.append(key)
		
	def parse(self, keys, hash):
		if keys[0:len(encrypted_magic)] == encrypted_magic:
			if hash is None:
				raise Exception("Encrypted")
			keys = hash.decrypt(keys[len(encrypted_magic):])

		self.new()
		s = ""
		for line in str.split(keys, "\n"):
			line = re.sub("#.*$", "", line)
			if re.search("^\s*$", line):
				continue
			if re.match("^" + name_tag + ":", line):
				self.parse_key(s)
				s = ""
			s += line + "\n"
		self.parse_key(s)

	def load(self, file):
		self.new()	# make sure we're in a clean state
		if file:
			with open(file) as f:
				s = f.read()
		else:
			s = sys.stdin.read()
		return s

	def save_file(self, file, hash):
		s = ""
		nl = False
		for key in self:
			if key.deleted:
				continue
			if nl:
				s += "\n"
			s += key.dump()
			nl = True
		if hash is not None:
			s = encrypted_magic + hash.encrypt(s)
		file.write(s)
		
	def save(self, file, hash):
		if file:
			with open(file, "w") as f:
				self.save_file(f, hash)
		else:
			self.save_file(sys.stdout, hash)

	def find_pk(self, pubkey):
		for key in self:
			if key.pubkey == pubkey:
				return key
		return None

	def readers(self, names = None):
		res = []
		if names is None:
			for key in self:
				if key.is_reader:
					res.append(key)
		else:
			for name in names:
				for key in self:
					if key.pubkey and key.name == name:
						res.append(key)
						break
				else:
					try:
						key = Key(pubkey =
						    base32.decode(name)[0:32])
						res.append(key)
					except (Exception, ValueError):
						raise Exception("Key for '" +
						    name + "' not found")
		return res

	def writer(self, name = None):
		first = None
		for key in self:
			if key.seckey:
				if key.name == name:
					return key
				if first is None or (first is not None and
				    not first.is_writer and key.is_writer):
					first = key
		if name:
			try:
				return Key(seckey = base32.decode(name)[0:32])
			except (Exception, ValueError):
				raise Exception("Key for '" + name +
				    "' not found")
		return first

	def generate(self, name, pin = None):
		key = Key()
		key.generate(name, pin)
		if self.find(name):
			raise Exception("Duplicate key name '" + name + "'")
		self.append(key)
		return key

	def new(self):
		list.__init__(self)

	def __init__(self):
		self.new()
