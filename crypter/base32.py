#!/usr/bin/perl
#
# crypter/base32.py - Base32 encoding
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# The base32 alphabet consists of the following characters:
#
# ABCDEFGHIJKLMNOPQRSTUVWXYZ		Values 0-25
#               0   5      2		Equivalent symbols
#
# 346789				Values 26-31
#
# Rationale for not making "1" equivalent to I or L:
# If using upper case, "1" could be confused with "I". If using lower case,
# "1" could be confused with "l". Only if the case is unknown, "I" and "L"
# could be confused with each other.
#
# On input, spaces, tabs, dots, commas, underlines, and dashes are ignored.
#


digits = "346789"
separators = "-_., \t"


def encode(blob, pretty = False):

	def char(n):
		if n < 26:
			return chr(ord("A") + n)
		else:
			return digits[n - 26]

	s = ""
	shift = 0
	v = 0
	chars = 0
	for b in blob:
		v |= ord(b) << shift
		shift += 8
		while shift >= 5:
			if pretty and chars and (chars & 3) == 0:
				s += "-" if chars % 12 else "."
			s += char(v & 0x1f)
			v >>= 5
			shift -= 5
			chars += 1
	if shift:
		if pretty and chars and (chars & 3) == 0:
			s += "-" if chars % 12 else "."
		s += char(v)
	return s


def decode(s):

	def value( c):
		if c == "0":
			c = "O"
		elif c == "5":
			c = "S"
		elif c == "2":
			c = "Z"
		n = ord(c.upper()) - ord("A")
		if n >= 0 and n < 26:
			return n
		pos = digits.find(c)
		if pos == -1:
			raise Exception("Invalid base32 digit")
		return pos + 26

	blob = ""
	shift = 0
	v = 0
	for c in s:
		if separators.find(c) != -1:
			continue
		v |= value(c) << shift
		shift += 5
		if shift >= 8:
			blob += chr(v & 0xff)
			v >>= 8
			shift -= 8
	if shift:
		blob += chr(v)
	return blob
