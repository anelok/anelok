#!/usr/bin/python
#
# crypter/dump.pl - Dump an account database (for debugging)
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


from struct import *
from nacl.secret import SecretBox
from nacl.public import PublicKey, PrivateKey, Box

import base32
from csx import crypto_stream_xor


def shared_key(sk, pk):
	box = Box(PrivateKey(sk), PublicKey(pk))
	return bytes(box)


def hexdump(prefix, s):
	while len(s):
		t = " ".join("%02X" % ord(c) for c in s[0:16])
		t = prefix + "%-47s  " % t
		t += "".join("." if ord(c) < 32 or ord(c) > 126 else c
		    for c in s[0:16])
		print t
		s = s[16:]


def dump_field(s, hex_dump = False):
	if hex_dump:
		t = ""
		while len(s):
			t += "".join("%02X" % ord(c) for c in s[0:4])
			s = s[4:]
			if len(s):
				t += " "
		print t
	else:
		print base32.encode(s, pretty = True)


def dump(s, keys = None, hex_dump = False):
	while len(s):
		length = unpack_from(">H", s)[0]
		s = s[2:]
		print "Length:", length, "bytes"

		t = s[0:length]
		s = s[length:]

		pkw = t[0:PublicKey.SIZE]
		t = t[PublicKey.SIZE:]
		print "  PKw:",
		dump_field(pkw, hex_dump)

		nonce = t[0:Box.NONCE_SIZE]
		t = t[Box.NONCE_SIZE:]
		print "  Nonce:",
		dump_field(nonce, hex_dump)

		readers = ord(t[0])
		t = t[1:]
		print "  Readers:", readers

		rk = None
		for i in range(0, readers):
			pkr = t[0:PublicKey.SIZE]
			t = t[PublicKey.SIZE:]
			print "    PKr%2u:" % i,
			dump_field(pkr, hex_dump)

			shk = None
			if keys:
				key = keys.find_pk(pkr)
				if key and key.seckey:
					shk = shared_key(key.seckey, pkw)
					print "      ShK:",
					dump_field(shk, hex_dump)

			ekr = t[0:SecretBox.KEY_SIZE]
			t = t[SecretBox.KEY_SIZE:]
			print "      EKr:",
			dump_field(ekr, hex_dump)

			if shk:
				rk = crypto_stream_xor(ekr, nonce, shk)
				print "      RK:",
				dump_field(rk, hex_dump)

		c = t

		print "  Payload:", len(t), "bytes"
		hexdump("    ", t)

		if rk is None:
			continue

		box = SecretBox(rk)
		try:
			m = box.decrypt(t, nonce)
		except ValueError:
			print "  Decryption failed"
			continue

		print "  Decrypted:"
		hexdump("    ", m)
