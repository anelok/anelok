#!/usr/bin/perl
#
# crypter/pin.pl - PIN calculator
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

%v = (
    t => 1, m => 2, b => 3,	# Top, Middle, Bottom
    1 => 1, 2 => 2, 3 => 3,	# or just numbers
);


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-q] code ...

where code is a sequence of the letters t(op), m(iddle), and b(ottom),
or the digits 1, 2, and 3.
EOF
	exit(1);
}


if ($ARGV[0] eq "-q") {
	$quiet = 1;
	shift @ARGV;
}
&usage if $ARGV[0] =~ /^-/;

$n = 0;
for (@ARGV) {
	for (split("", $_)) {
		die "unrecognized position \"$_\"" unless defined $v{$_};
		$pin += $v{$_} << ($n * 2);
		$n++;
	}
}
die "no code" unless $n;
printf("0x%x\n", $pin);
print STDERR sprintf("3^$n = 10^%.1f\n", log(3) / log(10) * $n)
    unless $quiet;
