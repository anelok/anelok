#!/usr/bin/python
#
# crypter/crypter.pl - Key generation and account database encryption/decryption
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import sys
from sys import argv
from getopt import getopt, GetoptError
import re

import base32
from keyfile import Keyfile, Hash
from account_db import Account_db, Fields
import dump


def load_keys(key_file, pw):
	keys = Keyfile()
	s = keys.load(key_file)
	if pw is None:
		hash = None
	else:
		hash = Hash(pw, s)
	keys.parse(s, hash)
	return keys


def list_db(key_file = None, pw = None, db_file = None, hex_dump = False):
	if key_file:
		keys = load_keys(key_file, pw)
	else:
		keys = None

	if db_file:
		with open(db_file) as f:
			s = f.read()
	else:
		s = sys.stdin.read()

	dump.dump(s, keys, hex_dump)


def generate_key(name = None, pw = None, pin = None):
	k = Keyfile()
	key = k.generate(name, pin)
	key.set_writer(True)
	key.set_reader(True)
	if pw is None:
		hash = None
	else:
		hash = Hash(pw)
	k.save(None, hash)


def encrypt(key_file, pw, writer = None, readers = None):
	keys = load_keys(key_file, pw)
	db = Account_db()

	s = sys.stdin.read()
	s = re.sub("^\s*#.*?$", "", s, flags = re.MULTILINE)
	s = re.sub("^\s*", "", s)
	s = re.sub("\s*$", "", s)

	for r in re.split("\s*\n\s*\n", s, flags = re.MULTILINE):
		first = True
		f = Fields()
		for field in re.split("[ \t]*\n", r):
			m = re.search("^(.*?):\s+(.*)", field)
			if not m:
				raise Exception("Bad field syntax")
			f.set(m.group(1), m.group(2))
		rec = db.new_record(f.name)
		rec.update(f)

	db.save(keys.writer(writer), keys.readers(readers))
	

def decrypt(key_file, pw):
	keys = load_keys(key_file, pw)
	db = Account_db()
	db.load(keys)
	first = True
	for r in db.db:
		if first:
			first = False
		else:
			print
		f = r.decode()
		if f.name:
			print "%-9s %s" % ("Name:", f.name)
		if f.url:
			print "%-9s %s" % ("URL:", f.url)
		if f.login:
			print "%-9s %s" % ("Login:", f.login)
		if f.pw:
			print "%-9s %s" % ("PW:", f.pw)


def key_code_by_name(keys, name = None):
	for key in keys:
		if key.pin is None or key.seckey is None:
			continue
		if name is not None and key.name != name:
			continue

		print "{"
		print ".pin = " + ("0x%x" % key.pin) + ","
		print ".sk = { " + \
		    ", ".join(map((lambda x: "0x%02x" % ord(x)), key.seckey)) \
		    + " }"
		print "},"

		if name is not None:
			return

	if name is not None:
		raise Exception("No key found for '" + name + "'")


def key_code(key_file, pw, names = None):
	keys = load_keys(key_file, pw)
	if names is not None:
		for name in names:
			key_code_by_name(keys, name)
	else:
		key_code_by_name(keys, None)


def pubkey_by_name(keys, name = None):
	for key in keys:
		if name is None:
			if key.seckey is None:
				continue
		else:
			if key.name != name:
				continue
		print base32.encode(key.pubkey, pretty = True)
		if name is not None:
			return

	if name is not None:
		raise Exception("No key found for '" + name + "'")


def show_pubkey(key_file, pw, names = None):
	keys = load_keys(key_file, pw)
	if names is None:
		pubkey_by_name(keys, None)
	else:
		for name in names:
			pubkey_by_name(keys, name)


def list_keys_by_name(keys, name = None):
	for key in keys:
		if key.name == name:
			sys.stdout.write(key.dump())
			return
	raise Exception("No key found for '" + name + "'")


def list_keys(key_file, pw, names = None):
	keys = load_keys(key_file, pw)
	if names is None:
		keys.save(None, None)
	else:
		first = True
		for name in names:
			if not first:
				print
			first = False
			list_keys_by_name(keys, name)


# Learned this from
# http://stackoverflow.com/questions/10660435/pythonic-way-to-create-a-long-multi-line-string/32980897#32980897


def usage():
	print >>sys.stderr, """
usage: {name} [-k keyfile] [-p password] [-x] [pw_db.bin]
       {name} -g [-p password] [-n pin] name
       {name} -e keyfile [-w writer] [-p password] [reader ...]
       {name} -d keyfile [-p password]
       {name} -c keyfile [-p password] [name ...]
       {name} -P keyfile [-p password] [name ...]
       {name} -K keyfile [-p password] [name ...]
""".format(name = argv[0])
	exit(1)


def main():
	try:
		opts, args = getopt(argv[1:], "c:d:e:gk:n:p:w:xK:P:")
	except GetoptError as err:
		print >>sys.stderr, str(err)
		usage()

	mode = None
	pin = None
	pw = None
	hex_dump = False
	key_file = None
	writer = None
	for opt, arg in opts:
		if opt == "-c":
			if mode:
				usage()
			mode = 'c'
			key_file = arg
		elif opt == "-d":
			if mode:
				usage()
			mode = 'd'
			key_file = arg
		elif opt == "-e":
			if mode:
				usage()
			mode = 'e'
			key_file = arg
		elif opt == "-g":
			if mode:
				usage()
			mode = 'g'
		elif opt == "-k":
			if key_file:
				usage()
			key_file = arg
		elif opt == "-n":
			if pin:
				usage()
			pin = int(arg, 16)
		elif opt == "-p":
			if pw:
				usage()
			pw = arg
		elif opt == "-w":
			if writer:
				usage()
			writer = arg
		elif opt == "-x":
			hex_dump = True
		elif opt == "-P":
			if mode:
				usage()
			mode = 'P'
			key_file = arg
		elif opt == "-K":
			if mode:
				usage()
			mode = 'K'
			key_file = arg
		else:
			usage()

	n = len(args)
	if mode is None:
		if n == 0:
			list_db(key_file, pw, None, hex_dump)
		elif n == 1:
			list_db(key_file, pw, key_file, hex_dump)
		else:
			usage()
	elif mode == 'g':
		if n == 1:
			generate_key(args[0], pw, pin)
		else:
			usage()
	elif mode == 'e':
		if n == 0:
			encrypt(key_file, pw, writer)
		else:
			encrypt(key_file, pw, writer, readers = args)
	elif mode == 'd':
		if n:
			usage
		decrypt(key_file, pw)
	elif mode == 'c':
		if n == 0:
			key_code(key_file, pw)
		else:
			key_code(key_file, pw, args)
	elif mode == 'P':
		if n == 0:
			show_pubkey(key_file, pw)
		else:
			show_pubkey(key_file, pw, args)
	elif mode == 'K':
		if n == 0:
			list_keys(key_file, pw)
		else:
			list_keys(key_file, pw, args)
	else:
		usage()


if __name__ == "__main__":
	main()
