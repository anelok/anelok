/*
 * crypt/base32.c - Base32 encoding
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * @@@ NEEDS PROPER REGRESSION TESTING !
 */

/*
 * The base32 alphabet consists of the following characters:
 *
 * ABCDEFGHIJKLMNOPQRSTUVWXYZ		Values 0-25
 *               0   5      2		Equivalent symbols
 *
 * 346789				Values 26-31
 *
 * Rationale for not making "1" equivalent to I or L:
 * If using upper case, "1" could be confused with "I". If using lower case,
 * "1" could be confused with "l". Only if the case is unknown, "I" and "L"
 * could be confused with each other.
 *
 * On input, spaces, tabs, dots, commas, underlines, and dashes are ignored.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "base32.h"


#define	ABC_UPPER	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define	ABC_LOWER	"abcdefghijklmnopqrstuvwxyz"
#define	ABC_VALUES	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, \
			13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25
#define	DIGITS		"346789"
#define	DIGIT_VALUES	26, 27, 28, 29, 30, 31
#define	EQUIV		"052"
#define	EQUIV_VALUES	14, 18, 25

#define	SEPARATORS	"-_., \t"


static const char in_map[] = ABC_UPPER ABC_LOWER DIGITS EQUIV;
static const uint8_t out_map[] =
    { ABC_VALUES, ABC_VALUES, DIGIT_VALUES, EQUIV_VALUES };


/*
 * @@@ consider omitting trailing zeroes. That way, decode(encode(X)) will not
 * grow larger than X.
 */

uint16_t base32_encode(char *out, uint16_t out_size,
    const uint8_t *in, uint16_t in_bits)
{
	uint8_t shift = 0;
	char *p = out;
	uint8_t v;

	if (!out_size)
		return 0;
	if ((out_size - 1) * 5 < in_bits)
		return 0;
	for (p = out; in_bits; in_bits -= 5) {
		v = *in >> shift;
		if (shift <= 3) {
			shift += 5;
		} else {
			if (in_bits > 8 - shift)
				v |= *++in << (8 - shift);
			shift -= 3;	/* shift = (shift + 5) % 8 */
		}
		v &= 0x1f;
		// coverity[data_index]
		*p++ = v < 26 ? 'A' + v : DIGITS[v - 26];
		if (in_bits < 5)
			break;
	}
	*p = 0;
	return p - out;
}


bool base32_decode(uint8_t *out, uint16_t out_bits, const char *in)
{
	uint8_t shift = 0;
	const uint8_t *end = out + ((out_bits + 7) >> 3);
	uint8_t *fill;
	const char *p;
	char ch;
	uint8_t v;

	while (*in) {
		ch = *in++;
		if (strchr(SEPARATORS, ch))
			continue;
		p = strchr(in_map, ch);
		if (!p)
			return 0;
		v = out_map[p - in_map];
		if (!shift)
			*out = 0;
		*out |= v << shift;
		if (shift <= 3) {
			shift += 5;
		} else {
			out++;
			v >>= 8 - shift;
			if (out == end)
				return !v && !*in;
			*out = v;
			shift -= 3;	/* shift = (shift + 5) % 8 */
		}
	}
	for (fill = out + 1; fill != end; fill++)
		*fill = 0;
	/*
	 * @@@ add proper overrun detection. we have an overrun if:
	 * - there are more characters in the input string than needed to
	 *   produce out_bits + 4 bits
	 * - if strlen(out) * 5 > out_bits, any of the extra bits is not zero
	 */
	return 1;
}
