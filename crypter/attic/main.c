/*
 * crypt/main.c - Anelok crypter
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "base32.h"
#include "tweetnacl.h"


/* ----- Helper functions -------------------------------------------------- */


static void *alloc(size_t bytes)
{
	void *p = malloc(bytes);

	if (p)
		return p;
	perror("malloc");
	exit(1);
}


/* ----- Random number source ---------------------------------------------- */


#define	DEV_URANDOM	"/dev/urandom"


static void get_random(const char *name, uint8_t *bytes, uint64_t len)
{
	FILE *file;
	size_t got;

	file = fopen(name, "r");
	if (!file) {
		perror(name);
		exit(1);
	}
	got = fread(bytes, 1, len, file);
	if (ferror(file)) {
		perror(name);
		exit(1);
	}
	if (got != len) {
		fprintf(stderr, "%s: short read\n", name);
		exit(1);
	}
	fclose(file);
}


void randombytes(uint8_t *bytes, uint64_t len);

void randombytes(uint8_t *bytes, uint64_t len)
{
	get_random(DEV_URANDOM, bytes, len);
}


/* ----- Base32 pretty-printing -------------------------------------------- */


static void print_base32(const char *name, const uint8_t *data,
    uint16_t bytes)
{
	char buf[bytes * 8 / 5 + 2];
	int i;

	if (!base32_encode(buf, sizeof(buf), data, bytes * 8)) {
		fprintf(stderr, "could not base32-encode\n");
		exit(1);
	}
	if (name)
		printf("%s: ", name);
	for (i = 0; buf[i]; i++) {
		if (i) {
			if (!(i % 12)) {
				putchar('.');
			} else if (!(i & 3)) {
				putchar('-');
			}
		}
		putchar(buf[i]);
	}
	putchar('\n');
}


/* ----- Key and nonce generation ------------------------------------------ */


static void do_generate_keys(void)
{
	uint8_t sk[crypto_box_SECRETKEYBYTES];
	uint8_t pk[crypto_box_PUBLICKEYBYTES];

	crypto_box_keypair(pk, sk);
	print_base32("Secret key", sk, crypto_box_SECRETKEYBYTES);
}


static void do_generate_nonce(void)
{
	uint8_t n[crypto_box_NONCEBYTES];

	randombytes(n, crypto_box_NONCEBYTES);
	print_base32("Nonce", n, crypto_box_NONCEBYTES);
}


/* ----- File IO ----------------------------------------------------------- */


#define	GROW	10000


static uint32_t read_msg(uint8_t **buf, uint16_t zero_bytes)
{
	uint32_t len = zero_bytes;
	size_t got;

	*buf = calloc(zero_bytes, 1);
	if (!*buf) {
		perror("calloc");
		exit(1);
	}
	while (!feof(stdin)) {
		*buf = realloc(*buf, len + GROW);
		if (!*buf) {
			perror("realloc");
			exit(1);
		}
		got = fread(*buf + len, 1, GROW, stdin);
		len += got;
		if (got < GROW)
			break;
	}
	return len;
}


static void write_msg(const uint8_t *msg, uint32_t bytes)
{
	size_t wrote;

	wrote = fwrite(msg, 1, bytes, stdout);
	if (wrote != bytes) {
		perror("stdout");
		exit(1);
	}
}


/* ----- Decrypting and encrypting ----------------------------------------- */


static void get_key(const char *key, uint8_t *res, uint8_t bytes)
{
	if (base32_decode(res, bytes * 8, key))
		return;
	fprintf(stderr, "cannot base32-decode key \"%s\"\n", key);
	exit(1);
}


static void decode_base32_add(const char *s, uint8_t *res, uint16_t bits)
{
	char tmp[strlen(s) + 1];
	char *p, *end;
	uint16_t bytes;
	unsigned long add = 0;
	uint8_t *q;

	strcpy(tmp, s);
	p = strchr(tmp, '+');
	if (p) {
		*p = 0;
		add = strtoul(p + 1, &end, 0);
		if (*end) {
			fprintf(stderr, "invalid increment \"+%s\"\n", p + 1);
			exit(1);
		}
	}

	if (!base32_decode(res, bits, tmp)) {
		fprintf(stderr, "cannot base32-decode \"%s\"\n", s);
		exit(1);
	}

	bytes = (bits + 7) >> 3;
	for (q = res; add && q != res + bytes; q++) {
		add += *q;
		*q = add;
		add >>= 8;
	}
}


static void get_nonce(const char *s, uint8_t *res)
{
	decode_base32_add(s, res, crypto_box_NONCEBYTES * 8);
}


static void do_decrypt(const char *secret, const char *public,
    const char *nonce)
{
	uint8_t sk[crypto_box_SECRETKEYBYTES];
	uint8_t pk[crypto_box_PUBLICKEYBYTES];
	uint8_t n[crypto_box_NONCEBYTES];
	uint8_t *c, *m;
	uint32_t clen;

	get_key(secret, sk, crypto_box_SECRETKEYBYTES);
	get_key(public, pk, crypto_box_PUBLICKEYBYTES);
	get_nonce(nonce, n);

	clen = read_msg(&c, crypto_box_BOXZEROBYTES);
	m = alloc(clen);
	if (crypto_box_open(m, c, clen, n, pk, sk)) {
		fprintf(stderr, "crypto_box_open failed\n");
		exit(1);
	}
	write_msg(m + crypto_box_ZEROBYTES, clen - crypto_box_ZEROBYTES);
	free(c);
	free(m);
}


static void do_encrypt(const char *secret, const char *public,
    const char *nonce)
{
	uint8_t sk[crypto_box_SECRETKEYBYTES];
	uint8_t pk[crypto_box_PUBLICKEYBYTES];
	uint8_t n[crypto_box_NONCEBYTES];
	uint8_t *m, *c;
	uint32_t mlen;

	get_key(secret, sk, crypto_box_SECRETKEYBYTES);
	get_key(public, pk, crypto_box_PUBLICKEYBYTES);
	get_nonce(nonce, n);

	mlen = read_msg(&m, crypto_box_ZEROBYTES);
	c = alloc(mlen);
	crypto_box(c, m, mlen, n, pk, sk);
	write_msg(c + crypto_box_BOXZEROBYTES, mlen - crypto_box_BOXZEROBYTES);
	free(c);
	free(m);
}


/* ----- Base32 operations ------------------------------------------------- */


static void do_decode_base32(const char *s, const char *opt_len, bool binary)
{
	uint8_t code[32];
	uint16_t len, bytes, i;

	len = opt_len ? atoi(opt_len) : sizeof(code) * 8;
	decode_base32_add(s, code, len);
	bytes = (len + 7) >> 3;
	if (binary) {
		write_msg(code, bytes);
		return;
	}
	for (i = 0; i != bytes; i++) {
		if (i)
			putchar(' ');
		printf("%02x", code[i]);
	}
	putchar('\n');
}


static void do_encode_base32(int bytes, char **s)
{
	uint8_t code[33];
	uint8_t i;

	for (i = 0; i != bytes; i++)
		code[i] = strtoul(s[i], NULL, 16);
	print_base32(NULL, code, bytes);
}


static void do_encode_base32_binary(void)
{
	uint8_t *buf;
	uint16_t bytes;

	bytes = read_msg(&buf, 0);
	print_base32(NULL, buf, bytes);
	free(buf);
}


/* ----- Calculate public key ---------------------------------------------- */


static void do_calculate_public(const char *secret)
{
	uint8_t sk[crypto_box_SECRETKEYBYTES];
	uint8_t pk[crypto_box_PUBLICKEYBYTES];

	get_key(secret, sk, crypto_box_SECRETKEYBYTES);
	if (crypto_scalarmult_base(pk, sk)) {
		fprintf(stderr, "crypto_scalarmult_base failed\n");
		exit(1);
	}
	print_base32(NULL, pk, crypto_box_PUBLICKEYBYTES);
}


/* ----- Command-line processing ------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s -g\n"
"       %s -n\n"
"       %s -e my-secret their-public nonce[+n]\n"
"       %s -d my-secret their-public nonce[+n]\n"
"       %s -P secret\n"
"       %s -E hex-byte ...\n"
"       %s -E -b\n"
"       %s -D [-b] base32[+n] [num_bits]\n\n"
"-b  use binary data\n"
"-D  base32-decode\n"
"-d  crypto_box_open a message (read from standard input)\n"
"-P  calculate public key from secret key\n"
"-E  base32-encode binary data\n"
"-e  crypto_box a message (read from standard input if not on command line)\n"
"-g  generate a secret-public key pair\n"
"-n  generate a random nonce\n"
    , name, name, name, name, name, name, name, name);
	exit(1);
}


int main(int argc, char **argv)
{
	enum {
		undefined,
		generate_keys,
		generate_nonce,
		decrypt,
		encrypt,
		decode_base32,
		encode_base32,
		calculate_public,
	} mode = undefined;
	bool binary = 0;
	int c;

	while ((c = getopt(argc, argv, "bDdEegnP")) != EOF)
		switch (c) {
		case 'b':
			binary = 1;
			break;
		case 'd':
			mode = decrypt;
			break;
		case 'D':
			mode = decode_base32;
			break;
		case 'e':
			mode = encrypt;
			break;
		case 'E':
			mode = encode_base32;
			break;
		case 'g':
			mode = generate_keys;
			break;
		case 'n':
			mode = generate_nonce;
			break;
		case 'P':
			mode = calculate_public;
			break;
		default:
			usage(*argv);
		}

	switch (mode) {
	case generate_keys:
		if (optind != argc)
			usage(*argv);
		do_generate_keys();
		break;
	case generate_nonce:
		if (optind != argc)
			usage(*argv);
		do_generate_nonce();
		break;
	case decrypt:
		if (argc - optind != 3)
			usage(*argv);
		do_decrypt(argv[optind], argv[optind + 1], argv[optind + 2]);
		break;
	case encrypt:
		if (argc - optind != 3)
			usage(*argv);
		do_encrypt(argv[optind], argv[optind + 1], argv[optind + 2]);
		break;
	case decode_base32:
		switch (argc - optind) {
		case 1:
		case 2:
			do_decode_base32(argv[optind], argv[optind + 1],
			    binary);
			break;
		default:
			usage(*argv);
		}
		break;
	case encode_base32:
		if (binary) {
			if (argc != optind)
				usage(*argv);
			do_encode_base32_binary();
		} else {
			do_encode_base32(argc - optind, argv + optind);
		}
		break;
	case calculate_public:
		if (argc - optind != 1)
			usage(*argv);
		do_calculate_public(argv[optind]);
		break;
	default:
		usage(*argv);
	}

	return 0;
}
