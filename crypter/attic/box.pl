#!/usr/bin/perl
#
# crypter/box.pl - Account database boxer/unboxer
#
# Written 2015-2016 by Werner Almesberger
# Copyright 2015-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Note: this is just the format we use for experimenting. The final format
# will be different.
#
# Database format:
# - Zero or more records:
#   - Length (2 bytes, big-endian)
#   - Public key (32 bytes)
#   - Nonce (24 bytes)
#   - Crypto-Box
# - EOF
#


$KEY_LEN = 32;
$NONCE_LEN = 24;
$BOX_OVERHEAD = 16;


sub box
{
	local ($seckey, $pubkey, $nonce) = @_;

	my $s = join("", <STDIN>);
	my $sk = `anelok-crypter -D -b $seckey`;
	my $pk = `anelok-crypter -D -b $pubkey`;

	die "incorrect secret key length" unless length $sk == $KEY_LEN;
	die "incorrect public key length" unless length $pk == $KEY_LEN;

	my $add = 0;
	while (length $s) {
		my $len;

		die "truncated record" unless length $s > 1;
		($len, $s) = unpack("na*", $s);
		die "truncated record" unless length $s >= $len;

		print pack("n", $len + $KEY_LEN + $NONCE_LEN + $BOX_OVERHEAD);

		print $pk;

		my $n = `anelok-crypter -D -b $nonce+$add 192`;
		die "incorrect nonce length" unless length $n == $NONCE_LEN;
		print $n;

		open(PIPE, "|anelok-crypter -e $seckey $pubkey $nonce+$add") ||
		    die "pipe: $!";
		print PIPE substr($s, 0, $len);
		close PIPE;
		$s = substr($s, $len);
		$add++;
	}
}


sub split
{
	local ($s) = @_;

	die "record too short" unless length $s >= $KEY_LEN + $NONCE_LEN;

	my $pk = substr($s, 0, $KEY_LEN);
	my $nonce = substr($s, $KEY_LEN, $NONCE_LEN);
	my $tail = substr($s, $KEY_LEN + $NONCE_LEN);

	my $tmp = join(" ", unpack("(H2)*", $pk));
	$pk = `anelok-crypter -E $tmp`;
	chop($pk);

	my $tmp = join(" ", unpack("(H2)*", $nonce));
	$nonce = `anelok-crypter -E $tmp`;
	chop($nonce);

	return ($pk, $nonce, $tail);
}


sub unbox
{
	local ($seckey) = @_;

	my $my_pubkey = `anelok-crypter -P $seckey`;
	chop($my_pubkey);

	my $s = join("", <STDIN>);

	while (length $s) {
		my $len;

		die "truncated record" unless length $s > 1;
		($len, $s) = unpack("na*", $s);
		die "truncated record" unless length $s >= $len;

		my ($pubkey, $nonce, $data) = &split($s);
		my $clen = $len - $KEY_LEN - $NONCE_LEN;
		print pack("n", $clen - $BOX_OVERHEAD);

		die "destination key does not match ours"
		    if $pubkey ne $my_pubkey;

		open(PIPE, "|anelok-crypter -d $seckey $pubkey $nonce") ||
		    die "pipe: $!";
		print PIPE substr($data, $0, $clen);
		close PIPE;
		$s = substr($s, $len);
	}
}


sub list
{
	local ($file) = @_;

	open(PIPE, defined $file ? "cat '$file' |" : "cat |") ||
	    die "pipe: $!";
	my $s = join("", <PIPE>);
	close PIPE;

	while (length $s) {
		my $len;

		die "truncated record" unless length $s > 1;
		($len, $s) = unpack("na*", $s);

		my ($pubkey, $nonce, $data) = &split($s);
		my $clen = $len - $KEY_LEN - $NONCE_LEN;

		print sprintf("Record: %d + %d + %d bytes\n",
		    $clen, $NONCE_LEN, $BOX_OVERHEAD);

		print "\tPublic: $pubkey\n";
		print "\tNonce:  $nonce\n";

		die "truncated record" unless length $s >= $len;
		$s = substr($s, $len);
	}
}


sub usage
{
	print STDERR "usage: $0 -e seckey pubkey nonce\n";
	print STDERR "       $0 -d seckey\n";
	print STDERR "       $0 [container-file]\n";
	exit(1);
}


$mode = undef;
while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-e") {
		&usage if defined $mode;
		$mode = "e";
	} elsif ($ARGV[0] eq "-d") {
		&usage if defined $mode;
		$mode = "d";
	} else {
		&usage;
	}
	shift @ARGV;
}

$| = 1;
if (!defined $mode && $#ARGV == -1) {
	&list;
} elsif (!defined $mode && $#ARGV == 0) {
	&list($ARGV[0]);
} elsif ($mode == "d" && $#ARGV == 0) {
	&unbox(@ARGV);
} elsif ($mode == "e" && $#ARGV == 2) {
	&box(@ARGV);
} else {
	&usage;
}
