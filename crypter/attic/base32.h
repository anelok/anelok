/*
 * crypt/base32.c - Base32 encoding
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef BASE32_H
#define	BASE32_H

#include <stdbool.h>
#include <stdint.h>


uint16_t base32_encode(char *out, uint16_t out_size,
    const uint8_t *in, uint16_t in_bits);

bool base32_decode(uint8_t *out, uint16_t out_bits, const char *in);

#endif /* !BASE32_H */
