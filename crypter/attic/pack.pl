#!/usr/bin/perl
#
# crypter/pack.pl - Account record packer/unpacker
#
# Written 2015-2016 by Werner Almesberger
# Copyright 2015-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Known issues:
# - does not support account flags yet. They're still poorly defined and it
#   may make sense to turn them into per-field flags.
# - allows repeated fields
# - does not order fields (besides enforcing that Name comes first)
#

#
# Record format:
# - Length (2 bytes, big-endian)
# - Zero or more fields:
#   - Type (1 byte; 0 = Name, 1 = URL, etc. See @fields, below.)
#   - Length of content (2 bytes, big-endian)
#   - Zero or more bytes of content
#
# Note: when encrypting, the length is moved outside the encrypted part and
# increased to include the data needed for encryption.
#

@fields = ("Name", "URL", "Login", "PW");


sub usage
{
	print STDERR "usage: $0 [-c dbconf.h] [file ...]\n";
	exit(1);
}


sub code
{
	local ($f) = @_;

	for (my $i = 0; $i <= $#fields; $i++) {
		return ($i, $') if $f =~ /^$fields[$i]:\s+/i;
	}
	die "unknown field name in \"$f\"\n";
}


sub count_dirs
{
	local (@a) = @_;

	my $path = "";
	for (@a) {
		$path .= $cfg{"SEP_CHAR"} . $_;
		if (!defined $have_dir{$path}) {
			$have_dir{$path} = 1;
			$n_dirs++;
		}
	}
	die "too many directories ($n_dirs > " . $cfg{"MAX_DIRS"}
	    if $n_dirs > $cfg{"MAX_DIRS"};
}


sub pack
{
	local ($s) = @_;

	$s =~ s/^\s*#[^\n]*$//mg;
	$s =~ s/^\s*//s;
	$s =~ s/\s*$//s;
	for $rec (split(/\s*\n\s*\n/, $s)) {
		my $first = 1;
		my $out = "";
		for $field (split(/\s*\n/, $rec)) {
			my ($c, $t) = &code($field);
			die "first field must be Name, not $fields[$c]\n"
			    if $first && $c;
			if ($first) {
				my @a = split($cfg{"SEP_CHAR"}, $t);

				pop(@a);
				for (@a) {
					die "directory name \"$_\" too long " .
					    "(maximum " . $cfg{"MAX_DIR_NAME"}
					    . ")"
					    if length $_ > $cfg{"MAX_DIR_NAME"};
				}
				&count_dirs(@a);
			}
			die "field too long"
			    if length $t > $cfg{"MAX_FIELD_BYTES"};
			$out .= pack("Cna*", $c, length $t, $t);
			$first = 0;
		}
		die "record too long (" . length($out) . " > " .
		    $cfg{"MAX_ACCOUNT_BYTES"}
		    if length $out > $cfg{"MAX_ACCOUNT_BYTES"};
		print pack("n", length $out), $out;
		$n_accounts++;
	}
	die "too many accounts ($n_accounts > ". $cfg{"MAX_ACCOUNTS"}
	    if $n_accounts > $cfg{"MAX_ACCOUNTS"};
}


sub unpack
{
	local ($s) = @_;

	my $first = 1;
	while (length $s) {
		die "incomplete field header" unless length $s > 1;
		my $rec_len;

		($rec_len, $s) = unpack("na*", $s);
		my $rec = substr($s, 0, $rec_len);
		die "truncated record" if $rec_len != length $rec;
		$s = substr($s, $rec_len);

		while (length $rec) {
			my ($c, $len);
			($c, $len, $rec) = unpack("Cna*", $rec);
			die "fields exceeds message" if $len > length $rec;
			die "unknown field code" unless $c <= $#fields;
			print "\n" if !$c && !$first;
			print sprintf("%-9s %s\n",
			    $fields[$c].":", substr($rec, 0, $len));
			$rec = substr($rec, $len);
		}
		$first = 0;
	}
}


# Defaults based on value ranges

$cfg{"MAX_ACCOUNT_BYTES"} = 65535;
$cfg{"MAX_FIELD_BYTES"} = 255;
$cfg{"MAX_DIR_NAME"} = 255;
$cfg{"SEP_CHAR"} = "/";
$cfg{"MAX_DIRS"} = 65535;	# no real limit here
$cfg{"MAX_ACCOUNTS"} = 65535;	# no real limit here

if ($ARGV[0] eq "-c") {
	shift @ARGV;
	&usage unless defined $ARGV[0];
	open(CONFIG, $ARGV[0]) || die "$ARGV[0]: $!";
	while (<CONFIG>) {
		chop;
		s|\s*//.*||;
		s|\s*/\*.*?\*/\s*||g;
		next unless /^\s*#define\s+(\S+)\s+/;
		$cfg{$1} = $';
		$cfg{$1} =~ s/^'(\S+)'$/\1/;
	}
	close CONFIG;
	shift @ARGV;
}
&usage if $ARGV[0] =~ /^-/;
#print STDERR map("$_ = \"$cfg{$_}\"\n", keys %cfg);

$s = join("", <>);

if (unpack("C", $s) >= 32) {
	&pack($s);
} else {
	&unpack($s);
}
