/*
 * crypter/randombytes.c - Dummy randombytes function
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdint.h>
#include <stdlib.h>


void randombytes(uint8_t *r, uint64_t n);

void randombytes(uint8_t *r, uint64_t n)
{
	abort();
}
