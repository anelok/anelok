#!/usr/bin/python
#
# crypter/account_db.py - Account database
#
# Written 2016-2017 by Werner Almesberger
# Copyright 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import sys
import re
from struct import *
import nacl
from nacl.secret import SecretBox
from nacl.public import PublicKey, PrivateKey, Box

from csx import crypto_stream_xor


def shared_key(sk, pk):
	box = Box(PrivateKey(sk), PublicKey(pk))
	return bytes(box)

	
class Node:
	def __init__(self, name, path, directory, fields = None, rec = None):
		self.name = name
		self.path = path
		self.is_directory = directory
		self.fields = fields
		self.rec = rec


class Fields:

	def set_name(self, name):
		if self.name:
			raise Exception("Duplicate name")
		self.name = name

	def set_url(self, url):
		if self.url:
			raise Exception("Duplicate URL")
		self.url = url

	def set_login(self, login):
		if self.login:
			raise Exception("Duplicate login")
		self.login = login

	def set_pw(self, pw):
		if self.pw:
			raise Exception("Duplicate password")
		self.pw = pw

	def set(self, type, value):
		if type == "Name":
			self.set_name(value)
		elif type == "URL":
			self.set_url(value)
		elif type == "Login":
			self.set_login(value)
		elif type == "PW":
			self.set_pw(value)
		else:
			raise Exception("Unrecognized type")

	def __init__(self):
		self.name = None
		self.url = None
		self.login = None
		self.pw = None
		pass


class Record:

	@staticmethod
	def name_is_valid(s):
		if s == "" or re.search("^\s", s) or re.search("\s$", s) \
		    or re.search("\s\s", s) or re.search("/", s):
			return False
		try:
			s.decode("ascii")
		except UnicodeDecodeError:
			return False
		return True

	@staticmethod
	def path_is_valid(s):
		for name in s.split("/"):
			if not Record.name_is_valid(name):
				return False
		return True

	def decode(self):
		d = self.data
		f = Fields()
		while len(d):
			if len(d) < 2:
				raise Exception("Field too short")
			(type, n) = unpack_from(">BH", d)
			d = d[3:]
			if len(d) < n:
				raise Exception("Record truncated")
			if type == 0:
				f.set_name(d[:n])
			elif type == 1:
				f.set_url(d[:n])
			elif type == 2:
				f.set_login(d[:n])
			elif type == 3:
				f.set_pw(d[:n])
			else:
				raise Exception("Unknown field type")
			d = d[n:]
		return f

	def update(self, fields):
		d = pack(">BH", 0, len(fields.name)) + fields.name
		if fields.url:
			d += pack(">BH", 1, len(fields.url)) + fields.url
		if fields.login:
			d += pack(">BH", 2, len(fields.login)) + fields.login
		if fields.pw:
			d += pack(">BH", 3, len(fields.pw)) + fields.pw
		self.data = d

	def has_changed(self):
		return self.data != self.orig_data

	def revert(self):
		# @@@ name changes may not be revertable if a conflicting name
		# @@@ has been added since
		self.data = self.orig_data

	def encrypt(self, writer, readers = None):
		if readers is None:
			readers = [ writer ]

		rk = nacl.utils.random(SecretBox.KEY_SIZE)
		nonce = nacl.utils.random(Box.NONCE_SIZE)

		blob = writer.pubkey + nonce
		blob += pack("B", len(readers))

		for rd in readers:
			shared = shared_key(writer.seckey, rd.pubkey)
			ek = crypto_stream_xor(rk, nonce, shared)
			blob += rd.pubkey + ek

		box = SecretBox(rk)
		blob += box.encrypt(self.data, nonce)[Box.NONCE_SIZE:]

		return pack(">H", len(blob)) + blob
		
	def __init__(self, data):
		self.data = data
		self.orig_data = data


class Record_decrypt(Record):

	# @@@ should cache boxes

	def __init__(self, keys, blob):
		pubkey = blob[0:PublicKey.SIZE]
		blob = blob[PublicKey.SIZE:]

		nonce = blob[0:Box.NONCE_SIZE]
		blob = blob[Box.NONCE_SIZE:]

		n = ord(blob[0])
		blob = blob[1:]

		my_ek = None
		self.readers = []
		for i in range(n):
			pk = blob[0:PublicKey.SIZE]
			blob = blob[PublicKey.SIZE:]
			ek = blob[0:SecretBox.KEY_SIZE]
			blob = blob[SecretBox.KEY_SIZE:]
			key = keys.find_pk(pk)
			if key and key.seckey:
				shared = shared_key(key.seckey, pubkey)
				my_ek = ek
			self.readers.append(pk)
		if my_ek is None:
			raise Exception("No matching key")

		rk = crypto_stream_xor(my_ek, nonce, shared)

		box = SecretBox(rk)
		data = box.decrypt(blob, nonce)
		Record.__init__(self, data)


class Record_new(Record):

	def __init__(self, name):
		data = pack(">BH", 0, len(name)) + name
		Record.__init__(self, data)


class Account_db:

	def list(self, base = None):
		res = []
		if base is None:
			prefix = ""
		else:
			prefix = base.path + "/"
		last_dir = None
		for rec in self.db:
			f = rec.decode()
			if not f:
				res.append(None)
				continue
			if f.name[0:len(prefix)] != prefix:
				continue
			tail = f.name[len(prefix):]
			pos = tail.find("/")
			if pos == -1:
				res.append(Node(tail, f.name, False, f, rec))
				last_dir = None
				continue
			name = tail[0:pos]
			if name == last_dir:
				continue
			res.append(Node(name, f.name[0:len(prefix) + pos],
			    True))
			last_dir = name
		return res

	# after = before = None: append at end

	def new_record(self, path, after = None, before = None):
		r = Record_new(path)
		if after:
			self.db.insert(self.db.index(after.rec) + 1, r)
		elif before:
			self.db.insert(self.db.index(before.rec), r)
		else:
			self.db.append(r)
		return r

	def parse(self, s, keys):
		self.db = []
		while len(s):
			if len(s) < 2 + 32 + 24:
				raise Exception("Record too short")
			n = unpack_from(">H", s)[0]
			s = s[2:]
			if len(s) < n:
				raise Exception("Record truncated")
			r = Record_decrypt(keys, s[:n])
			self.db.append(r)
			s = s[n:]

	def load(self, keys, file = None):
		if file:
			with open(file) as f:
				s = f.read()
		else:
			s = sys.stdin.read()
		self.parse(s, keys)

	def pack(self, writer, readers = None):
		s = ""
		for r in self.db:
			s += r.encrypt(writer, readers)
			r.orig_data = r.data
		return s
		
	def save(self, writer, readers = None, file = None):
		if file:
			with open(file, "w") as f:
				f.write(self.pack(writer, readers))
		else:
			sys.stdout.write(self.pack(writer, readers))

	def new(self):
		self.db = []

	def __init__(self):
		self.new()
