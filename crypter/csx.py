#!/usr/bin/python
#
# crypter/csx.py - crypto_stream_xor
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# PyNaCl doesn't provide crypto_stream_xor, so we take it from TweetNaCl.
#


from ctypes import *
import sys


def crypto_stream_xor(m, nonce, key):
	path = sys.path[0]
	if path == "":
		path = "."
	nacl = CDLL(path + "/tweetnacl.so")
	c = create_string_buffer('\000' * len(m))
	nacl.crypto_stream_xsalsa20_tweet_xor(c, m, len(m), nonce, key)
	return c.raw[0:len(m)]
