/*
 * tool.c - Anelok control tool (for development)
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <usb.h>

#include "usbopen.h"

#include "../fw/mk2/ep0.h"		/* for ANELOK_TOUCH */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae70


static usb_dev_handle *dev;


static void touch(void)
{
	uint16_t buf[4];
	int res;

	res = usb_control_msg(dev, ANELOK_TOUCH & 0xff, ANELOK_TOUCH >> 8,
	    0, 0, (char *) buf, 8, 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_TOUCH: %d\n", res);
		exit(1);
	}
	if (res)
		printf("%u %u %u %u\n", buf[0], buf[1], buf[2], buf[3]);
	exit(0);
}


static void mode(int value)
{
	int res;

	res = usb_control_msg(dev, ANELOK_MODE & 0xff, ANELOK_MODE >> 8,
	    value, 0, NULL, 0, 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_MODE: %d\n", res);
		exit(1);
	}
}


int main(int argc, char **argv)
{
	int res;

	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}

	res = usb_claim_interface(dev, 0);
	if (res) {
		fprintf(stderr, "usb_claim_interface: %d\n", res);
		return 1;
	}

	switch (argc) {
	case 2:
		mode(atoi(argv[1]));
		break;
	default:
		touch();
	}

	return 0;
}
