/*
 * demo.c - Anelok touch sensor demo (for development)
 *
 * Written 2014, 2017 by Werner Almesberger
 * Copyright 2014, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include "usbopen.h"

#include "../fw/mk2/ep0.h"	/* for ANELOK_TOUCH */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae70

#define	XRES	128
#define	YRES	64

static usb_dev_handle *dev;



/* ----- Touch sensor sampling --------------------------------------------- */


struct sample {
	uint32_t a, b;
};


static struct sample sample(void)
{
	static uint16_t buf[4];
	struct sample s;
	int res;

	do {
		res = usb_control_msg(dev,
		    ANELOK_TOUCH & 0xff, ANELOK_TOUCH >> 8,
		    0, 0, (char *) buf, 8, 1000);
		if (res < 0) {
			fprintf(stderr, "ANELOK_TOUCH: %d\n", res);
			exit(1);
		}
	} while (!res);

	s.a = buf[0]+buf[1];
	s.b = buf[2]+buf[3];

	return s;
}


/* ----- Sample -> Position and up/down ------------------------------------ */


#define	POSITIONS	128

/*
 * This threshold is quite high, requiring a firm push (index finger) or the
 * use of a large finger (thumb).
 */

#define	THRESH	500

#define	CAL_RUNS	2000
#define	EXCURSION	25


struct cal
{
	uint32_t avg;
	uint32_t sum;
	uint32_t n;
};


static int32_t normalize(struct cal *cal, uint32_t v)
{
	if ((v - EXCURSION) * cal->n > cal->sum ||
	    (v + EXCURSION) * cal->n < cal->sum) {
		cal->sum = 0;
		cal->n = 0;
	}
	cal->sum += v;
	cal->n++;
	if (cal->n == CAL_RUNS) {
//fprintf(stderr, "locked\n");
		cal->avg = cal->sum / CAL_RUNS;
		cal->sum = 0;
		cal->n = 0;
	}
	return v - cal->avg;
}


static bool touch(uint8_t *pos)
{
	static struct cal cal_a, cal_b;
	struct sample s;
	int32_t a, b;
	int tmp;

	s = sample();
	a = normalize(&cal_a, s.a);
	b = normalize(&cal_b, s.b);

	if (cal_a.avg == 0 || cal_b.avg == 0)
		return 0;

//printf("%d %d\n", a, b);
	if (a+b < THRESH)
		return 0;

	tmp = (b-a)/3+POSITIONS/2;
	if (tmp < 0)
		tmp = 0;
	if (tmp >= POSITIONS)
		tmp = POSITIONS-1;
	*pos = tmp;
	return 1;
}


/* ----- GUI --------------------------------------------------------------- */


static void gui(void)
{
	SDL_Surface *surf;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf = SDL_SetVideoMode(XRES, YRES, 0, SDL_SWSURFACE);

	while (1) {
		uint8_t pos;
		bool down;

		down = touch(&pos);

		SDL_LockSurface(surf);
		SDL_FillRect(surf, NULL, SDL_MapRGB(surf->format, 0, 0, 0));
		
		if (down)
			vlineColor(surf, pos, 0, YRES-1, 0xffffffff);

		SDL_UnlockSurface(surf);
		SDL_UpdateRect(surf, 0, 0, 0, 0);
	}
}


/* ----- Main -------------------------------------------------------------- */


int main(int argc, char **argv)
{
	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}
	gui();
	return 0;
}

