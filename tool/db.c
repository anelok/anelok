/*
 * db.c - Upload/download Anelok password database
 *
 * Written 2016-2017 by Werner Almesberger
 * Copyright 2016-2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <usb.h>

#include "usbopen.h"

#include "../fw/mk2/ep0.h"		/* for ANELOK_* */
#include "../fw/db/dbconf.h"		/* for DB_SIZE */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae70

#define	XFER_SIZE	64


static usb_dev_handle *dev;


#define	ARRAY_ELEMENTS(a)	(sizeof(a) / sizeof(a[0]))
#define	ARRAY_END(a)		((a) + ARRAY_ELEMENTS(a))


static uint8_t db[DB_SIZE];


static void reset_session(void)
{
	int res;

	res = usb_control_msg(dev, ANELOK_CANCEL & 0xff, ANELOK_CANCEL >> 8,
	    0, 0, NULL, 0, 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_CANCEL: %d\n", res);
		exit(1);
	}
}


static void read_db(const char *name)
{
	FILE *file = stdout;
	uint16_t size, len;
	size_t wrote;
	uint8_t *p;
	int res;

	p = db;
	for (size = sizeof(db); size; size -= len) {
		len = size > XFER_SIZE ? XFER_SIZE : size;
			
		res = usb_control_msg(dev, ANELOK_READ_DB & 0xff,
		    ANELOK_READ_DB >> 8, p - db, 0,
		    (char *) p, len, 1000);
		if (res < 0) {
			fprintf(stderr, "ANELOK_READ_DB: %d\n", res);
			exit(1);
		}
		p += len;
	}

	if (name) {
		file = fopen(name, "w");
		if (!file) {
			perror(name);
			exit(1);
		}
	}

	p = db;
	while (1) {
		if (p + 1 >= ARRAY_END(db))
			goto fail;
		len = p[0] << 8 | p[1];
		if (!len)
			break;
		p += 2;
		if (p + len > ARRAY_END(db))
			goto fail;
		p += len;
	}
	size = p - db;

	wrote = fwrite(db, 1, size, file);
	if (wrote < size) {
		perror("write");
		exit(1);
	}
	if (fclose(file) < 0) {
		perror("write");
		exit(1);
	}
	return;

fail:
	fprintf(stderr, "bad database\n");
	exit(1);
}


static void write_db(const char *name)
{
	FILE *file = stdin;
	size_t size, len;
	uint8_t *p;
	int res;

	if (name) {
		file = fopen(name, "r");
		if (!file) {
			perror(name);
			exit(1);
		}
	}

	memset(db, 0, sizeof(db)); /* zero padding and end bytes */
	size = fread(db, 1, sizeof(db) - 1, file);
	if (ferror(file)) {
		perror(name);
		exit(1);
	}

	if (size == sizeof(db) - 1) {
		fprintf(stderr, "database too big\n");
		exit(1);
	}
	size = (size + 2 + 3) & ~3;
	for (p = db; size; p += len) {
		len = size > XFER_SIZE ? XFER_SIZE : size;
			
		res = usb_control_msg(dev, ANELOK_WRITE_DB & 0xff,
		    ANELOK_WRITE_DB >> 8, p - db, 0,
		    (char *) p, len, 1000);
		if (res < 0) {
			fprintf(stderr, "ANELOK_WRITE_DB: %d\n", res);
			exit(1);
		}
		size -= len;
	}
}


/* ----- Command-line processing ------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [file]\n"
"       %s -w [file]\n"
    , name, name);
	exit(1);
}


int main(int argc, char **argv)
{
	const char *file;
	bool do_read = 1;
	int res;
	char c;

	while ((c = getopt(argc, argv, "w")) != EOF)
		switch (c) {
		case 'w':
			do_read = 0;
			break;
		default:
			usage(*argv);
		}
	
	switch (argc - optind) {
	case 0:
		file = NULL;
		break;
	case 1:
		file = argv[optind];
		break;
	default:
		usage(*argv);
	}

	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}

	res = usb_claim_interface(dev, 0);
	if (res) {
		fprintf(stderr, "usb_claim_interface: %d\n", res);
		return 1;
	}

	reset_session();
	atexit(reset_session);
	if (do_read)
		read_db(file);
	else
		write_db(file);

	return 0;
}
