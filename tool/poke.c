/*
 * poke.c - Peek/poke for Anelok
 *
 * Written 2015, 2017 by Werner Almesberger
 * Copyright 2015, 2017 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <usb.h>

#include "usbopen.h"

#include "../fw/mk2/ep0.h"		/* for ANELOK_* */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae70


static usb_dev_handle *dev;


/* ----- USB operations ---------------------------------------------------- */


static uint32_t peek(void)
{
	uint32_t data;
	int res;

	res = usb_control_msg(dev, ANELOK_PEEK & 0xff, ANELOK_PEEK >> 8,
	    0, 0, (char *) &data, sizeof(data), 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_PEEK: %d\n", res);
		exit(1);
	}
	return data;
}


static void poke(uint32_t data)
{
	int res;

	res = usb_control_msg(dev, ANELOK_POKE & 0xff, ANELOK_POKE >> 8,
	    0, 0, (char *) &data, sizeof(data), 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_POKE: %d\n", res);
		exit(1);
	}
}


static void set_addr(uint32_t addr)
{
	int res;
	uint32_t check;

	res = usb_control_msg(dev, ANELOK_SET_ADDR & 0xff, ANELOK_SET_ADDR >> 8,
	    0, 0, (char *) &addr, sizeof(addr), 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_SET_ADDR: %d\n", res);
		exit(1);
	}

	res = usb_control_msg(dev, ANELOK_GET_ADDR & 0xff, ANELOK_GET_ADDR >> 8,
	    0, 0, (char *) &check, sizeof(check), 1000);
	if (res < 0) {
		fprintf(stderr, "ANELOK_GET_ADDR: %d\n", res);
		exit(1);
	}

	if (addr != check) {
		fprintf(stderr, "set address 0x%08lx, got 0x%08lx\n",
		    (unsigned long) addr, (unsigned long) check);
		exit(1);
	}
}


/* ----- GPIO low-level operations ----------------------------------------- */


#define	SIM_SCGC5	 	0x40048038
#define	SIM_SCGC5_PORT(n)	(1 << ((n) + 9))
#define	GPIOn_PSOR(n)		(0x400FF004 + 0x40 * (n))
#define	GPIOn_PCOR(n)		(0x400FF008 + 0x40 * (n))
#define	GPIOn_PDIR(n)		(0x400FF010 + 0x40 * (n))


static void gpio_activate(uint8_t id)
{
	uint32_t v;

	set_addr(SIM_SCGC5);
	v = peek();
	v |= SIM_SCGC5_PORT(id >> 5);
	poke(v);
}


static void gpio_set(uint8_t id, bool on)
{
	gpio_activate(id);
	if (on) {
		set_addr(GPIOn_PSOR(id >> 5));
	} else {
		set_addr(GPIOn_PCOR(id >> 5));
	}
	poke(1 << (id & 31));
}


static bool gpio_get(uint8_t id)
{
	uint32_t v;

	gpio_activate(id);
	set_addr(GPIOn_PDIR(id >> 5));
	v = peek();
	return (v >> (id & 31)) & 1;
}


/* ----- Helper functions -------------------------------------------------- */


static bool get_u32(const char *s, uint32_t *res)
{
	char *end;

	*res = strtoul(s, &end, 0);
	return !*end;
}


/* ----- Usage ------------------------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s address [value]\n"
"       %s pin[=value] ...\n"
"       %s -l\n", name, name, name);
	exit(1);
}


/* ----- Peek/poke high-level operations ----------------------------------- */


static void peek_poke(int argc, char **argv)
{
	uint32_t addr, data;

	switch (argc) {
	case 3:
		if (!get_u32(argv[2], &data))
			usage(*argv);
		/* fall through */
	case 2:
		if (!get_u32(argv[1], &addr))
			usage(*argv);
		break;
	default:
		usage(*argv);
	}

	set_addr(addr);

	switch (argc) {
	case 2:
		data = peek();
		printf("0x%08lx\n", (unsigned long) data);
		break;
	case 3:
		poke(data);
		break;
	default:
		abort();
	}
}


/* ----- GPIO high-level operations ---------------------------------------- */


#define	PORT_A_ID	0
#define	PORT_B_ID	1
#define	PORT_C_ID	2
#define	PORT_D_ID	3
#define	PORT_E_ID	4

#define	GPIO_ID(port, pin)	(PORT_##port##_ID << 5 | (pin))


static struct name {
	uint8_t id;		/* undefined at end */
	const char *name;	/* NULL at end */
} names[] = {
#include "names.inc"
	{ 0, 	NULL }
};


static bool gpio_op(char *op)
{
	const char *eq;
	int len;
	const struct name *name;
	char *end;
	uint8_t id;

	eq = strchr(op, '=');
	if (!isalpha(*op))
		return 0;

	len = eq ? eq - op : strlen(op);
	for (name = names; name->name; name++)
		if (!strncasecmp(op, name->name, len) && !name->name[len])
			break;

	if (name->name) {
		id = name->id;
	} else {
		id = strtoul(op + 1, &end, 10);
		if (*end) {
			if (end != eq)
				return 0;
			if (strcmp(eq + 1, "0") && strcmp(eq + 1, "1"))
				return 0;
		}
		id += (toupper(*op) - 'A') << 5;
	}
	if (eq) {
		gpio_set(id, eq[1] == '1');
	} else {
		printf("%d\n", gpio_get(id));
	}
	return 1;
}


static void gpios(int argc, char **argv)
{
	int i;

	for (i = 1; i != argc; i++)
		if (!gpio_op(argv[i]))
			usage(*argv);
}


static void list_gpios(void)
{
	const struct name *name;

	for (name = names; name->name; name++)
		printf("%c%d\t%s\n", 'A' + (name->id >> 5), name->id & 31,
		    name->name);
}


/* ----- Command-line processing ------------------------------------------- */


int main(int argc, char **argv)
{
	int res;

	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}

	if (argc < 2)
		usage(*argv);

	res = usb_claim_interface(dev, 0);
	if (res) {
		fprintf(stderr, "usb_claim_interface: %d\n", res);
		return 1;
	}

	if (!strcmp(argv[1], "-l")) {
		list_gpios();
	} else if (isdigit(*argv[1])) {
		peek_poke(argc, argv);
	} else {
		gpios(argc, argv);
	}
	return 0;
}
