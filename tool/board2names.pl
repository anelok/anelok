#!/usr/bin/perl
while (<>) {
	next unless /^#define\s+(\S+)\s+(GPIO_ID.*)/;
	$s{uc $1} = "\t{ $2,\t\"$1\" },\n";
}
for (sort keys %s) {
	print $s{$_};
}
