#!/usr/bin/perl

sub usage
{
	print STDERR "usage: $0 [-c] map.in file.sch ...\n";
	exit(1);
}


if ($ARGV[0] eq "-c") {
	shift @ARGV;
	$cache = 1;
}

open(MAP, $ARGV[0]) || die "$ARGV[0]: $!";
while (<MAP>) {
	chop;
	if (/^\s*$/) {
		undef $key;
		next;
	}
	next if /^\s*#/;
	if (!defined $key) {
		$key = $_;
		$key =~ s/([.{}()+[\]])/\\\1/g;
		$key =~ s/\*/.*/g;
		$key =~ s/\?/./g;
		next;
	}
	die "bad document line" unless /^("[^"]*"|\S+)\s+/;
	push(@keys, $key);
	push(@tags, $1);
	push(@s, $');
	next unless $cache;
	next if $1 eq "FP" || $1 eq "PKG";
	system("eeshow-viewer", "-c", $') && die;
}
close MAP;

shift @ARGV;


sub ith
{
	local ($i) = @_;

	$tag = $tags[$i];
	if ($tag eq "DS") {
		print "Data sheet\n";
	} elsif ($tag eq "TRM") {
		print "TRM\n";
	} elsif ($tag =~ /^"([^"]*)"$/) {
		print "$1\n";
	} elsif ($tag eq "FP" || $tag eq "PKG") {
		if ($tag eq "FP") {
			print "Footprint\n";
		} else {
			print "Package\n";
		}
		if ($s[$i] =~ /^(\d+)\s*$/) {
			print "$s[$i - 1]#$1\n\n";
			return;
		}
	} else {
		die "unknown tag \"$tag\"";
	}
	print "$s[$i]\n\n";
}


while (<>) {
	if (/^\$Comp/) {
		undef @f;
		undef $unit;
		next;
	}
	if (/^U\s+(\d+)/) {
		$unit = $1;
	}
	if (/^F\s+(\d+)\s+"([^"]*)"/) {
		$f[$1] = $2;
		next;
	}
	if (/^\$EndComp/) {
#print STDERR "|$f[0]_$f[1]| $unit\n";
		for ($i = 0; $i <= $#keys; $i++) {
#print STDERR "    |$keys[$i]|\n";
			next unless "$f[0]_$f[1]" =~ /^$keys[$i]$/;
			$sfx = 'A';
			for ($u = $unit; $u > 1; $u--) {
				$sfx++;
			}
			# @@@ hack: we should check in the library if the part
			# has multiple units, and generate the component
			# reference accordingly.
			if ($unit < 2)  {
				print "$f[0]\n";
				&ith($i);
			}
			print "$f[0]$sfx\n";
			&ith($i);
		}
	}
}
