#!/usr/bin/python
#
# me3/case.py - Put everything together
#
# Written 2015-2016 by Werner Almesberger
# Copyright 2015-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import FreeCAD, Part
from FreeCAD import Base
from math import sqrt, hypot, sin, cos, tan, atan2, pi


#
# Vertical stacking:
#
# Thick	Cumulat	Description
# -----	-------	---------------
# 	0	top of case
# 0.1	0.1	recess/tolerance/glue for glass
# 1.0	1.1	glass (polycarbonate)
# 0.05	1.15	air gap
# 1.45	2.6	OLED panel
# 0.6	3.2	top clearance (under OLED)
# 1.0	4.2	top components
#
# ...	1.1
# 1.6	2.7	sub-PCB (under glass)
# 1.5	4.2	board-to-board connector
#
# 	0
# 1.1	1.1	window (where glass is placed)
# 1.6	2.7	frame for OLED panel
# 0.5	3.2	top clearance (under frame)
# ...
#


# ===== Common definition =====================================================

#
# Naming:
#
# *_w = width, dimension on long axis (typically X)
# *_h = height, dimension on short axis (typically Y)
# *_t = thickness, dimension on Z axis
#
# *_r<qual> = radius (optional qualifier: i = inner, o = outer)
# *_d<qual> = diameter
#
# *_a = angle
#

# ----- Overall case parameters -----------------------------------------------

width = 56.0	# total width
top_ro = 8.5 / 2.0	# outside radius at top (narrow) end
wall = 1.5	# general wall thickness
base = 34.0	# distance between circle centers

side_wall_overlap = 0.5
top_bottom_gap = 0.1

side_wall = wall + side_wall_overlap

# ----- Components ------------------------------------------------------------

# PCBs

pcb_xy_clear = 0.2			# clearance on each side
pcb_z_clear = 0.1
pcb_overlap = 0.5			# case-PCB overlap

pcb_h = 27.0
pcb_w = width - 2 * (side_wall + pcb_xy_clear)
pcb_t = 0.8

# OLED

oled_w = 34.5				# module size
oled_h = 23.0
oled_t = 1.45

oled_off = (oled_h - 19.2) / 2.0	# visible area offset from center

oled_vis_w = 31.42			# visible area
oled_vis_h = 16.7

oled_vis_epsilon = 0.05			# pseudo-thickness of visual area

fpc_narrow_w = 12			# FPC width (at panel)
fpc_narrow_h = 11.0 - 6.887		# length of "neck"
fpc_wide_w = 22.0			# width at PCB
fpc_widen_h = 6.887 - 4.0		# distance over which the FPC widens
fpc_wide_h = 4.0
fpc_t = 0.15				# FPC thickness
fpc_min_hor = 1.5			# horizontal segment at panel
					# (0.8 mm, but we add some slack)

fpc_clear_vert = 0.5			# headroom above FPC
fpc_clear_side = 1.0			# clearance left/right of FPC
fpc_clear_front = 0.5			# room for the bend to move

# OLED position (relative to the top of the PCB, Z+ is above the PCB)
# (x, y) are the offset of the center of the visual area from the PCB's lower
# left corner

oled_x = oled_w / 2.0 + 4.0
oled_y = 11.2
oled_z = 1.6				# height of panel bottom above PCB

# Battery, based on Energizer EN92 data sheet

bat_cyl_d = 10		# 10.0 +/- 0.5 mm
bat_tot_h = 43.9	# 43.9 +/- 0.6 mm
bat_but_d = 3.8		# 3.8 mm max
bat_but_h = 0.8		# 0.8 mm min
bat_bot_d = 4.3		# 4.3 mm min
bat_bot_fake_h = 0.1	# fake height of bottom

# LED

led_base_w = 0.8
led_base_h = 1.6
#led_base_t = 0.28
led_base_t = 0.18

led_lens_w = led_base_w
#led_lens_h = 1.1
#led_lens_t = 0.8 - led_base_t
led_lens_h = 1.15
led_lens_t = 0.35 - led_base_t

# Micro USB receptacle (with funnel-shaped entry)

usb_rcpt_w = 7.5	# nominal
usb_rcpt_h = 5		# 5.00 +/- 0.15 mm
usb_rcpt_t = 2.5

usb_entry_w = 8.06
usb_entry_h = 0.6
usb_entry_t = 3.07

usb_poke_h = 0.7 - 0.05	# main body of Micro USB (without the entry "funnel"
			# pokes out over the PCB's edge by 0.7 mm. In our
			# case, we make it stick out 50 um less, due to grid
			# alignment.

usb_y = 5.0

usb_cutout_gap_xy = 0.3
usb_cutout_gap_z = 0.3

# Memory card

card_holder_w = 14
card_holder_h = 15.2
card_holder_t = 1.93

card_w = 11.0
card_t = 0.8
card_inserted_h = 1.7	# measured

card_off_x = 2.5
card_off_t = 1.0

# @@@ tentative: card holder is centered, note that card thus is off-center
card_holder_x = (pcb_w - card_holder_w) / 2.0
card_holder_y = 0.5

card_cutout_hor_gap = 1.0
card_cutout_vert_gap = 0.5
card_cutout_ext = 5.0		# extent from holder

# Tactile switch

tact_w = 4.5
tact_h = 4.5
tact_t = 0.55

# The button array

buttons_x = pcb_w - 5.7		# align cut-out with case wall
buttons_y = pcb_h / 2.0 - 1.0
buttons_step_y = 8.0		# center-to-center distance

# Keys

key_gap = 0.3		# gap between keys and top case
keycap_w = 9		# key cap width
keycap_h = buttons_step_y - key_gap
			# key cap height
keycap_r = 1.6 - key_gap
			# corner radius - determined by tool size (1/8 in)
#keycap_t = glass_t + glass_z_gap + wall + key_gap
keycap_t = wall + key_gap
keybase_w = 2 * key_gap + 0.5
			# protrusion of the key base
keybase_t = 1.2
key_act_t = 0.6		# length of actuator (gap is 0.75 mm)
key_act_r = 1.0		# radius of actuator

# Cut-out for button array

buttons_w = 8.8
buttons_h = 10.0 + 2 * buttons_step_y
buttons_r = 1.6         # just use tool radius + epsilon

# Slide switch

sw_body_w = 6.7
sw_body_h = 2.6
sw_body_t = 1.4

sw_knob_w = 1.3
sw_knob_h = 1.5
sw_knob_t = 1.1

sw_knob_travel = 1.5
sw_knob_z = 0.1

sw_boss_w = 3.0			# center-to-center distance
sw_boss_t = 0.5
sw_boss_r = 0.8 / 2.0
sw_boss_hole_r = 0.9 / 2.0

sw_x = pcb_w - wall - 1.3	# front edge
sw_y = 14.5 - 0.05		# (lower edge)

# Knob on switch

knob_hole_w = sw_knob_w + 2 * 0.1
knob_hole_h = sw_knob_h - 0.2
knob_t = sw_knob_t + 0.1

knob_body_h = wall + 0.4
knob_body_w = knob_hole_w + 2 * wall
knob_slider_h = side_wall + 0.9
knob_slider_w = knob_body_w + 2 * sw_knob_travel + 2 * 1.0

knob_x = (sw_body_w - sw_knob_travel) / 2.0
knob_y = -(knob_slider_h + 0.4)
knob_z = 0.1

knob_cutout_gap = 0.2
knob_slide_cutout_gap = 0.5	# gap in direction of slide

# Antenna

antenna_w = 16.8
antenna_h = 5.3
antenna_t = 0.05

antenna_y = 5.0

# Mechanism that hold top and bottom together

lug_w = 3.0			# lug width
lug_h = 0.5			# stright part of the lug
lug_base_h = 4.0		# rectangular base, on bottom half
lug_gap = top_bottom_gap	# clearance to top half
lug_offset = 18.0		# offset from center
lug_t = 1.7			# lug thickness (make it fit just under PCB)


# ----- Structures ------------------------------------------------------------

# Battery tube

contact = 2.0
tube_h = 44.5 + 0.5 + contact
				# 43.9 +/- 0.6 mm, tolerance + 0.5 mm
tube_ri = (10.5 + 0.5) / 2.0	# 10.0 +/- 0.5 mm, tolerance + 0.5 mm
tube_ro = tube_ri + wall

# Catches to hold the PCB in place

catch_w = 3.0
catch_r = 0.2
catch_ramp_h = 1.2
catch_flat_h = 0.5
catch_t = catch_r + 0.2 + pcb_xy_clear

catch_offset_upper = 13.0
catch_offset_lower = 21.0

# ----- PCB position (depends on other calculations) --------------------------

# Origins of main PCB, relative to case top assembly origin

pcb_x = side_wall + pcb_xy_clear - width / 2.0
pcb_y = tube_ro + pcb_xy_clear
pcb_z = -4.2

# ----- Flip ------------------------------------------------------------------

#
# Flip coordinates between top and bottom
#
# @@@ currently unused, but you never know ...
#

def flip(x, y):
	# base: (0, tube_ro) + n * (face_h, -d_r)
	d_r = tube_ro - top_ro

	# A, B vectors
	ax = x
	ay = y - tube_ro
	bx = face_h
	by = -d_r

	# A * B = |A| * |B| * cos \theta
	scalar = ax * bx + ay * by

	# length (squared)
	b2 = bx * bx + by * by

	# mirror point
	mx = bx * scalar / b2
	my = tube_ro + by * scalar / b2

	# flip (f = m + (m - x) = 2m - x
	dx = 2 * mx - x
	dy = 2 * my - y

	return dx, dy


# ----- Geometry calculations -------------------------------------------------

top_ri = top_ro - wall
height = tube_ro + top_ro + base
thickness = tube_ro * 2.0
tmp = tube_ro - top_ro

# height of top/bottom face (flat surface)

face_h = sqrt(base * base - tmp * tmp)

# angle between base and face

base_a = atan2(tmp, face_h) * 180.0 / pi

print width, "x", height, "x", thickness, "=", width * height * thickness
print "face =", face_h, "base_angle =", base_a

# ----- More structures -------------------------------------------------------

batt_cut_a = -base_a	# battery cut plane, angle with respect to base

# Lanyard cut-out

lanyard_r = 1.0
lanyard_w = 2 * lanyard_r
lanyard_h = lanyard_r + 2 * lanyard_r

lanyard_x = 2.0
lanyard_y = face_h + top_ro - lanyard_h - 3.0

# Lanyard cut-out in the bottom (and vertical extension of top)

lanyard_bottom_cut_h = 4.0
lanyard_bottom_cut_w = 4.0
lanyard_bottom_cut_r = lanyard_r + 1.0

# ----- Screws ----------------------------------------------------------------

# Screw specification example:
# M1.6*0.641*3mm Pan Head T5 Torx drive Thread Forming Screw for Plastic

# Confirmation that T5+M1.6 is a common combination
# https://www.semblex.com/files/Torx-Semblex.pdf

# We extract the following parameters:
# Head: T5, assume 3 mm diameter, unknown thickness (guess 1.0 mm ?)
# Thread: M1.6, 1.6 mm diameter
# http://www.fairburyfastener.com/torx_sizing_guide.htm

# Good source of information:
# http://www.apexfasteners.com/fasteners/threaded-products/screws/thread-forming-screws/thread-forming-for-plastic/pt-screws/delta-pt-screws/torx-plus-pan-head
# http://www.ejot-avdel.se/sites/default/files/product/files/Brochure_EJOT_DEL
# Dimensions = "16": T5, M1.6
# Head: T5, 3.0 mm diameter, 1.2 mm height
# Thread: M1.6, Dmaj = 1.6 mm, core = 1.07 mm

#
# More details (catalog, with lengths):
# http://www.ejot-avdel.se/sites/default/files/product/files/Brochure_EJOT_DELTA_PT_10.10_en.pdf
#

# Terminology (Dmaj) according to
# https://en.wikipedia.org/wiki/ISO_metric_screw_thread

# EJOT DELTA PT WN 5452 20 x 7 (Torx T6 drive) or
# EJOT DELTA PT WN 5412 H or Z 20 x 7 (Phillips-like drive)

screw_head_r = 3.5 / 2.0
screw_head_t = 1.6
screw_dmaj_r = 2.0 / 2.0	# M20
screw_len = 7.0			# EJOT have M2.0 at 4.0-20.0 mm

# Experimental: try off-the-shelf 4 x 1/4 (2.8 x 6.3 mm) metal screw

screw_head_r = 5.5 / 2.0
screw_head_t = 2.0
screw_len = 6.3
screw_dmaj_r = 2.9 / 2.0	# M20

# Holes for mounting screws (top part)

screw_hole_head_r = screw_head_r + 0.1
screw_hole_pass_r = screw_dmaj_r + 0.05
screw_hole_head_t = screw_head_t + 0.1
screw_hole_wall = 1.5		# wall thickness around (any) screw opening

# Length of unthreaded opening

screw_hole_pass_t = tube_ro * (1 - sin(2 * base_a / 180.0 * pi)) - \
    screw_hole_head_t
	# this lets the thread of a 7.0 mm screw go 2.5 +/- 0.45 mm into the
	# top part, with an extra 0.9 +/- 0.45 mm of headroom

# Holes for mounting screws (bottom part)

screw_hole_thread_r = screw_dmaj_r * 0.8	# EJOT catalog, page 11
screw_mount_gap = 0.2		# air gap between top/bottom cylinders
screw_mount_wall = 1.2				# EJOT: screw_dmaj_r / 2
screw_mount_r = screw_hole_thread_r + screw_mount_wall
screw_mount_extra = 0.9			# overshoot at the end, measured
screw_mount_len = \
    screw_len - screw_hole_pass_t - screw_mount_gap + screw_mount_extra

# Screw placement

screw_offsets = (-16, 13.3)	# from X center
screw_offsets = (-16, 12.1)	# from X center
screw_y = max(tube_ro + screw_hole_pass_r, tube_ri + screw_mount_r)
screw_z = max(wall, screw_hole_head_t)
			# height above bottom

# ===== Utility functions =====================================================


# ----- Common primitives -----------------------------------------------------


def v(x, y, z):
	return Base.Vector(x, y, z)


def extrude_shape(shape, z):
	wire = Part.Wire(shape.Edges)
	face = Part.Face(wire)

	return face.extrude(v(0, 0, z))


def corner(x, y, a, b):
	va = v(x + a[0], y + a[1], 0)
	f = 1 - 1 / sqrt(2)
	vb = v(x + (a[0] + b[0]) * f, y + (a[1] + b[1]) * f, 0)
	vc = v(x + b[0], y + b[1], 0)
	return Part.Arc(va, vb, vc)


def rrect(x, y, z, r):
	bottom = Part.Line(v(r, 0, 0), v(x - r, 0, 0))
	top = Part.Line(v(r, y, 0), v(x - r, y, 0))
	left = Part.Line(v(0, r, 0), v(0, y - r, 0))
	right = Part.Line(v(x, r, 0), v(x, y - r, 0))

	ll = corner(0, 0, (r, 0), (0, r))
	ul = corner(0, y, (0, -r), (r, 0))
	ur = corner(x, y, (-r, 0), (0, -r))
	lr = corner(x, 0, (0, r), (-r, 0))

	s = Part.Shape([ bottom, top, left, right, ll, ul, ur, lr ])
	return extrude_shape(s, z)


def rrect_center_xy(x, y, z, r):
	r = rrect(x, y, z, r)
	move(r, -x / 2.0, -y / 2.0, 0)
	return r


def rect(x, y, z):
	bottom = Part.Line(v(0, 0, 0), v(x, 0, 0))
	top = Part.Line(v(0, y, 0), v(x, y, 0))
	left = Part.Line(v(0, 0, 0), v(0, y, 0))
	right = Part.Line(v(x, 0, 0), v(x, y, 0))

	s = Part.Shape([ bottom, top, left, right ])
	return extrude_shape(s, z)


def rect_center_xy(x, y, z):
	r = rect(x, y, z)
	move(r, -x / 2.0, -y / 2.0, 0)
	return r


def cylinder(r, z):
	return Part.makeCylinder(r, z)


def move(obj, x, y, z):
	obj.translate(v(x, y, z))


# ----- Visualization helper --------------------------------------------------


def visualize(doc, shape, name, color, trans):
	obj = doc.addObject("Part::Feature", name)
	obj.Shape = shape
	obj.ViewObject.ShapeColor = color
	obj.ViewObject.Transparency = trans


# ----- Partial constructs: arc cylinders and tubes ---------------------------

#
# x axis is cylinder's center axis
#
# cylinder's center is at X = 0
#
# Angles:
# 0 = Y+
# 90 = Z+
#

def arc_cylinder(r, h, a0, a1):
	a0rad = (a0 + 90) / 180.0 * pi
	a1rad = (a1 + 90) / 180.0 * pi
	amrad = (a0rad + a1rad) / 2.0

	vz = v(0, 0, 0)
	v0 = v(r * cos(a0rad), r * sin(a0rad), 0)
	v1 = v(r * cos(a1rad), r * sin(a1rad), 0)
	vm = v(r * cos(amrad), r * sin(amrad), 0)

	s0 = Part.Line(vz, v0)
	s1 = Part.Line(v1, vz)
	arc = Part.Arc(v0, vm, v1)

	s = Part.Shape([ s0, arc, s1 ])
	cyl = extrude_shape(s, h)

	cyl.rotate(v(0, 0, 0), v(0, 1, 0), 90)
	move(cyl, -h / 2.0, 0, 0)

	return cyl


def arc_tube(ro, ri, h, a0, a1):
	outer = arc_cylinder(ro, h, a0, a1)
	inner = arc_cylinder(ri, h, a0, a1)
	return outer.cut(inner)


def half_tube(ro, ri, h, tilt):
	outer = cylinder(ro, h)
	keep = rect(2 * ro, ro, h)
	move(keep, -ro, -ro, 0)
	outer = outer.common(keep)

	inner = cylinder(ri, h)
	tube = outer.cut(inner)

	move(tube, 0, 0, -h / 2.0)
	tube.rotate(v(0, 0, 0), v(0, 1, 0), 90)
	tube.rotate(v(0, 0, 0), v(1, 0, 0), 90 + tilt)

	move(tube, 0, 0, ro)

	return tube


# ----- Partial constructs: side walls ----------------------------------------

#
# Lower face is on X+
#
# Face / thick end curvature boundary is at (X, y) = 0
#
# Part is extruded from Z = 0 upwards
#

def side(thick):
	r1 = tube_ro
	r2 = top_ro

	a = 2 * base_a / 180.0 * pi

	t1x = r1 * sin(a)
	t1y = r1 * (1 + cos(a))

	t2x = face_h + r2 * sin(a)
	t2y = r2 * (1 + cos(a))

	h1 = Part.Line(v(0, 0, 0), v(face_h, 0, 0))
	a2 = Part.Arc(v(face_h, 0, 0), v(face_h + r2, r2, 0), v(t2x, t2y, 0))
	h2 = Part.Line(v(t2x, t2y, 0), v(t1x, t1y, 0))
	a1 = Part.Arc(v(t1x, t1y, 0), v(-r1, r1, 0), v(0, 0, 0))

	s = Part.Shape([ h1, a2, h2, a1 ])
	return extrude_shape(s, thick)


# ----- Partial constructs: screw head area -----------------------------------

#
# The wall lies parallel to the XY plane.
# The screw center is at z0, the thread points in the Z+ direction.
#

def screw_head_area(x, y, z0):
	head_cyl_t = screw_hole_head_t + screw_hole_wall
	head_cyl = cylinder(screw_hole_head_r + screw_hole_wall, head_cyl_t)
	move(head_cyl, 0, 0, -screw_hole_head_t)

	thread_cyl = cylinder(screw_hole_pass_r + screw_hole_wall,
	    screw_hole_pass_t)
	head_cyl = head_cyl.fuse(thread_cyl)

	head_cut = cylinder(screw_hole_head_r, screw_hole_head_t)
	move(head_cut, 0, 0, -screw_hole_head_t)
	thread_cut = cylinder(screw_hole_pass_r, screw_hole_pass_t)
	cut = head_cut.fuse(thread_cut)

	move(head_cyl, x, y, z0)
	move(cut, x, y, z0)

	return head_cyl, cut


# ----- Partial constructs: extruded variations of the PCB outline ------------

#
# origin = top side lower left corner of PCB
#
# "offset" is an offset into the PCB
#

pcb_lanyard_corner_w = 4.0	# measured
pcb_lanyard_corner_h = 4.6	# measured
pcb_lanyard_corner_r = 1.5	# measured
pcb_mount_y = \
    tube_ro + screw_dmaj_r + screw_mount_r - pcb_y + pcb_xy_clear + 0.15
				# 0.15 to compensate for inclination of barrel
pcb_mount_w = 2.5		# measured: barrel is 1.3 mm into PCB
pcb_mount_h = 5.0		# measured
pcb_mount_corner = 1.0


def polygon_2d(points):
	poly = []
	first = None
	for xy in points:
		vec = v(xy[0], xy[1], 0)
		if first is None:
			first = vec
		else:
			poly.append(Part.Line(last, vec))
		last = vec
	poly.append(Part.Line(last, first))
	return Part.Shape(poly)


def pcb_main_outline_fn(offset, fn):
	diag = offset * sqrt(2 * (1 - sqrt(0.5)) ** 2)

	p = [(offset, offset)]
	for off in screw_offsets:
		mount_off = pcb_w / 2.0 + off	# offset of center from PCB edge
		x0 = mount_off - screw_mount_r - pcb_xy_clear
		x1 = mount_off + screw_mount_r + pcb_xy_clear
		p.append((x0 - offset, offset))
		p.append((x0 - offset, pcb_mount_y - pcb_mount_corner + diag))
		p.append((x0 + pcb_mount_corner - diag, pcb_mount_y + offset))
		p.append((x1 - pcb_mount_corner + diag, pcb_mount_y + offset))
		p.append((x1 + offset, pcb_mount_y - pcb_mount_corner + diag))
		p.append((x1 + offset, offset))
	p.append((pcb_w - offset, offset))
	p.append((pcb_w - offset, pcb_mount_h + offset))
	p.append((pcb_w - offset, pcb_h - offset))
	p.append((pcb_lanyard_corner_w + offset, pcb_h - offset))
	p.append((pcb_lanyard_corner_w + offset,
	      pcb_h - pcb_lanyard_corner_h + pcb_lanyard_corner_r - diag))
	p.append((pcb_lanyard_corner_w - pcb_lanyard_corner_r + diag,
	      pcb_h - pcb_lanyard_corner_h - offset))
	p.append((offset, pcb_h - pcb_lanyard_corner_h - offset))

	return fn(p)

	
def pcb_main_outline(offset, t):
	s = pcb_main_outline_fn(offset, polygon_2d)
	return extrude_shape(s, t)


def print_2d(points):
	first = None
	for xy in points:
		print xy[0], " ", xy[1]
		if first is None:
			first = xy 
	if first is not None:
		print first[0], " ", first[1]
	

def pcb_main_gp(offset):
	pcb_main_outline_fn(offset, print_2d)


# ----- Components ------------------------------------------------------------

# ----- Battery -----

def make_aaa():
	cyl = cylinder(bat_cyl_d / 2.0, bat_tot_h - bat_but_h - bat_bot_fake_h)
	but = cylinder(bat_but_d / 2.0, bat_but_h)
	bot = cylinder(bat_bot_d / 2.0, bat_bot_fake_h)

	move(cyl, 0, 0, bat_bot_fake_h)
	move(but, 0, 0, bat_tot_h - bat_but_h)

	bat = cyl.fuse(bot)
	bat = bat.fuse(but)

	return bat

# ----- Screw(s) -----

#
# head is in Z-, thread in Z+
# Z axis is center axis
#

def make_screw():
	h = cylinder(screw_head_r, screw_head_t)
	move(h, 0, 0, -screw_head_t)
	t = cylinder(screw_dmaj_r, screw_len)
	return h.fuse(t)

# ----- OLED -----

#
# bottom of panel lies on XY
# X = 0, Y = 0 center is visual center
#


def make_oled_panel():
	r = rect(oled_w, oled_h, oled_t - oled_vis_epsilon)
	move(r, -oled_w / 2.0, -oled_h / 2.0 + oled_off, 0)
	return r


def make_oled_visual():
	r = rect(oled_vis_w, oled_vis_h, oled_vis_epsilon)
	move(r, -oled_vis_w / 2.0, -oled_vis_h / 2.0, oled_t - oled_vis_epsilon)
	return r


#
# right-hand half of the part of the FPC that lies on the PCB
#
# xy is the PCB top
# x = 0 is the center of the FPC
# y = 0 is where the FPC bend touches the PCB, FPC continues in Y+
#
# "left" is how much is left of the FPC's "neck" when we reach the PCB
#

def half_flat_fpc(left):
	s = polygon_2d((
	    (0, 0),
	    (fpc_narrow_w / 2.0, 0),
	    (fpc_narrow_w / 2.0, left),
	    (fpc_wide_w / 2.0, left + fpc_widen_h),
	    (fpc_wide_w / 2.0, left + fpc_widen_h + fpc_wide_h),
	    (0, left + fpc_widen_h + fpc_wide_h)))
	return extrude_shape(s, fpc_t)


#
# xy is the PCB top
# x = 0 is the center of the FPC
# y = 0 is the point where the FPC attaches to the panel
#

def make_fpc(z):
	# horizontal part
	fpc = rect(fpc_narrow_w, -fpc_min_hor, fpc_t)
	move(fpc, -fpc_narrow_w / 2.0, 0, z)

	# bend
	bend = half_tube((z + fpc_t) / 2.0, (z - fpc_t) / 2.0, fpc_narrow_w,
	    -90)
	move(bend, 0, -fpc_min_hor, 0)
	fpc = fpc.fuse(bend)

	# we have this much FPC left when reaching the PCB
	neck_left = fpc_narrow_h - fpc_min_hor - pi * z / 2.0

	# part flat on PCB, composed of two symmetrical halves
	right = half_flat_fpc(neck_left)
	move(right, 0, -fpc_min_hor, 0)

	left = half_flat_fpc(neck_left)
	left.rotate(v(0, 0, fpc_t / 2.0), v(0, 1, 0), 180)
	move(left, 0, -fpc_min_hor, 0)

	fpc = fpc.fuse(right)
	fpc = fpc.fuse(left)

	fpc.rotate(v(0, 0, 0), v(0, 0, 1), 180)
	move(fpc, 0, oled_h / 2.0 + oled_off, -z)
		# -z since we'll use place_oled
	return fpc


def make_fpc_cutout(z):
	r = rect(fpc_narrow_w + 2 * fpc_clear_side,
	    -(fpc_min_hor + z / 2.0 + fpc_clear_front),
	    z + fpc_t + fpc_clear_vert)
	move(r, -fpc_narrow_w / 2.0 - fpc_clear_side, 0, 0)

	# @@@ cut-out for the protective tape, WIP
	r2 = rect(fpc_narrow_w + 2 * fpc_clear_side,
	    2.0, 10.0)
	move(r2, -fpc_narrow_w / 2.0 - fpc_clear_side, -1.1, z - 5.0)
	r = r.fuse(r2)

	r.rotate(v(0, 0, 0), v(0, 0, 1), 180)
	move(r, 0, oled_h / 2.0 + oled_off, -z)
		# -z since we'll use place_oled
	return r

# ----- USB -----

def make_usb():
	r1 = rect(usb_rcpt_w, usb_rcpt_h, usb_rcpt_t)
	r2 = rect(usb_entry_w, usb_entry_h, usb_entry_t)

	move(r2, (usb_rcpt_w - usb_entry_w) / 2.0, -usb_entry_h,
	    (usb_rcpt_t - usb_entry_t) / 2.0)

	usb = r1.fuse(r2)

	move(usb, 0, -usb_poke_h, 0)

	return usb


def make_usb_cutout():
	r = rect(usb_entry_w + 2 * usb_cutout_gap_xy, -usb_poke_h - side_wall,
	    usb_entry_t + 2 * usb_cutout_gap_z)
	move(r, (usb_rcpt_w - usb_entry_w) / 2.0 - usb_cutout_gap_xy, 0,
	    (usb_rcpt_t - usb_entry_t) / 2.0 - usb_cutout_gap_z)
	return r

# ----- Memory card -----

def make_memcard():
	holder = rect(card_holder_w, card_holder_h, card_holder_t)
	card = rect(card_w, card_inserted_h, card_t)
	move(card, card_off_x, card_holder_h, card_off_t)
	return holder.fuse(card)


def make_memcard_cutout():
	r = rect(card_w + 2 * card_cutout_hor_gap, card_cutout_ext,
	    card_t + 2 * card_cutout_vert_gap)
	move(r, card_off_x - card_cutout_hor_gap, card_holder_h,
	    card_off_t - card_cutout_vert_gap)
	return r

# ----- Buttons -----

def row(fn, z):
	r = None
	for dy in (-1, 0, 1):
		t = fn(dy)
		move(t, buttons_x, buttons_y + buttons_step_y * dy, z)
		if r is None:
			r = t
		else:
			r = r.fuse(t)
	return r


def tact(dy):
	return rect_center_xy(tact_w, tact_h, tact_t)


def tact_row():
        return row(tact, 0)


def rect_rrect_u(x, y, z, r):
	dy = -1 if y < 0 else 1

	bottom = Part.Line(v(r, 0, 0), v(x - r, 0, 0))
	top = Part.Line(v(0, y, 0), v(x, y, 0))
	left = Part.Line(v(0, dy * r, 0), v(0, y, 0))
	right = Part.Line(v(x, dy * r, 0), v(x, y, 0))

	ll = corner(0, 0, (r, 0), (0, dy * r))
	lr = corner(x, 0, (0, dy * r), (-r, 0))

	s = Part.Shape([ bottom, top, left, right, ll, lr ])
	return extrude_shape(s, z)


def rect_rrect_inverted_u(x, y, z, r):
	r = rect_rrect_u(x, -y, z, r)
	move(r, 0, y, 0)
	return r


def key_cap(dy):
	if dy == 1:
		cap = rect_rrect_inverted_u(keycap_w, keycap_h, keycap_t,
		    keycap_r)
	elif dy == 0:
		cap = rect(keycap_w, keycap_h, keycap_t)
	elif dy == -1:
		cap = rect_rrect_u(keycap_w, keycap_h, keycap_t, keycap_r)
	else:
		assert(False)

	move(cap, -keycap_w / 2.0, -keycap_h / 2.0, -keycap_t)

	act = cylinder(key_act_r, key_act_t + keybase_t)
	move(act, 0, 0, -key_act_t - keybase_t - keycap_t)
	return cap.fuse(act)


def key_base(dy):
	w = keycap_w + 2 * keybase_w
	h = keycap_h + keybase_w
	z = keycap_t + keybase_t
	r = keycap_r + keybase_w
	if dy == 1:
		cap = rect_rrect_inverted_u(w, h, keybase_t, r)
		move(cap, -w / 2.0, -keycap_h / 2.0, -z)
	elif dy == 0:
		cap = rect(w, keycap_h, keybase_t)
		move(cap, -w / 2.0, -keycap_h / 2.0, -z)
	elif dy == -1:
		cap = rect_rrect_u(w, keycap_h, keybase_t, r)
		move(cap, -w / 2.0, -keycap_h / 2.0, -z)
	else:
		assert(False)
	return cap


def button_row():
        caps = row(key_cap, 4.2)
	bases = row(key_base, 4.2)
	return caps.fuse(bases)
	return caps


def button_row_cutout():
	h = keycap_h + 2 * buttons_step_y
	caps_t = keycap_t - key_gap
	bases_t = keybase_t + 2 * key_gap + 10	# clear out other items
	caps = rrect_center_xy(keycap_w + 2 * key_gap, h + 2 * key_gap,
	    caps_t, keycap_r)
	bases = rrect_center_xy(keycap_w + 2 * (keybase_w + key_gap),
	    h + keybase_w + 2 * key_gap, bases_t, keycap_r + keybase_w)
	move(bases, 0, keybase_w / 2.0, -bases_t)
	caps = caps.fuse(bases)
	move(caps, buttons_x, buttons_y, 4.2 - caps_t)
	return caps

# ----- rfkill switch -----

def make_switch():
	r1 = rect(sw_body_w, sw_body_h, sw_body_t)
	r2 = rect(sw_knob_w, sw_knob_h, sw_knob_t)

	move(r2, (sw_body_w - sw_knob_w - sw_knob_travel) / 2.0,
	    -sw_knob_h, sw_knob_z)

	sw = r1.fuse(r2)

	cut = None
	for dx in (-1, 1):
		cx = (sw_body_w + dx * sw_boss_w) / 2.0
		cy = sw_body_h / 2.0

		cyl = cylinder(sw_boss_r, sw_boss_t)
		move(cyl, cx, cy, -sw_boss_t)
		sw = sw.fuse(cyl)

		cyl = cylinder(sw_boss_hole_r, pcb_t)
		move(cyl, cx, cy, -pcb_t)
		if cut is None:
			cut = cyl
		else:
			cut = cut.fuse(cyl)

	return sw, cut


#
# During construction:
#
# Wall side of slider is on the X axis, center at x = 0.
# Knob points to Y+, slide to Y-
# Bottom of slider on z = 0
#

def make_knob():
	slider = rect(knob_slider_w, -knob_slider_h, knob_t)
	move(slider, -knob_slider_w / 2.0, 0, 0)

	hole = rect(knob_hole_w, knob_hole_h, knob_t)
	move(hole, -knob_hole_w / 2.0, -knob_slider_h, 0)
	slider = slider.cut(hole)

	knob = rect(knob_body_w, knob_body_h, knob_t)
	move(knob, -knob_body_w / 2.0, 0, 0)

	slider = slider.fuse(knob)

	slider_cut = rect(
	    knob_slider_w + 2 * knob_slide_cutout_gap + sw_knob_travel,
	    -knob_slider_h - knob_cutout_gap, knob_t + 2 * knob_cutout_gap)
	move(slider_cut,
	    -knob_slider_w / 2.0 - knob_slide_cutout_gap - sw_knob_travel,
	    knob_cutout_gap, -knob_cutout_gap)
	knob_cut = rect(
	    knob_body_w + 2 * knob_slide_cutout_gap + sw_knob_travel,
	    knob_body_h, knob_t + 2 * knob_cutout_gap)
	move(knob_cut,
	    -knob_body_w / 2.0 - knob_slide_cutout_gap - sw_knob_travel, 0,
	    -knob_cutout_gap)
	cut = slider_cut.fuse(knob_cut)

	slider.rotate(v(0, 0, 0), v(0, 0, 1), 180)
	move(slider, knob_x, knob_y, knob_z)

	cut.rotate(v(0, 0, 0), v(0, 0, 1), 180)
	move(cut, knob_x, knob_y, knob_z)

	return slider, cut


# ----- LED -----

def make_led():
	base = rect(led_base_w, led_base_h, led_base_t)
	move(base, -led_base_w / 2.0, -led_base_h / 2.0, 0)

	lens = rect(led_lens_w, led_lens_h, led_lens_t)
	move(lens, -led_lens_w / 2.0, -led_lens_h / 2.0, led_base_t)

	return base.fuse(lens)

# ----- Antenna -----

def make_antenna():
	return rect(antenna_w, antenna_h, antenna_t)


# ===== Reduce top part by jiggling (and cutting) bottom part =================


def jiggle(top, bottom, dist):
	s = sin(base_a / 180.0 * pi)
	c = cos(base_a / 180.0 * pi)
	cut = bottom.copy()
	for v in ((1, 1, 1), (-2, 0, 0), (0, -2, 0), (2, 0, 0),
	    (0, 0, -1), (-2, 0, 0), (0, 2, 0), (2, 0, 0)):
		dx = dist * v[0]
		dy = dist * v[1]
		dz = dist * v[2]
		move(cut, dx, c * dy - s * dz, c * dz + s * dy)
		top = top.cut(cut)
	return top


# ===== Top half ==============================================================


#
# Coordinate system:
#
# x = 0: center
# y = 0: edge of face at thick end
# z = 0: outside top of case (inside faces downward !)
#


def top_face(w, h, t):
	r = rect(w, h, t)
	move(r, -w / 2.0, 0, 0)
	return r


#
# In bottom coordinates !
#
# (0, 0, z0) is the screw center
#

def make_screw_barrel(x, y, z0):
	cyl = cylinder(screw_mount_r, screw_mount_len)
	move(cyl, x, y, z0 + screw_hole_pass_t + screw_mount_gap)
	return cyl


def make_screw_barrel_cutout(x, y, z0):
	hole = cylinder(screw_hole_thread_r, screw_mount_len + screw_mount_gap)
	move(hole, x, y, z0 + screw_hole_pass_t)
	return hole


def make_screw_barrels():
	all = None
	for x in screw_offsets:
		barrel = make_screw_barrel(x, screw_y, screw_z)
		if all is None:
			all = barrel
		else:
			all = all.fuse(barrel)
	return all


def make_screw_barrel_cutouts():
	all = None
	for x in screw_offsets:
		barrel = make_screw_barrel_cutout(x, screw_y, screw_z)
		if all is None:
			all = barrel
		else:
			all = all.fuse(barrel)
	return all


def make_top():
#	bat = half_tube(tube_ro, tube_ri, width - 2 * side_wall,
#	    -batt_cut_a - base_a)
#
#	face = top_face(width, face_h, wall)
#	top = top.fuse(face)

	top = side(width)
	top.rotate(v(0, 0, 0), v(1, 0, 0), -90)
	top.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(top, width / 2.0, 0, 0)

	# Note: here, we implicitly hard-code the battery tube tilt to be equal
	# to base_a, i.e., the cut of the battery opening is parallel to the
	# top face.

	cut = rect(width - 2 * side_wall, base + tube_ro + top_ro, -tube_ro)
	move(cut, -width / 2.0 + side_wall, -tube_ro, 0)
	top = top.common(cut)

	bat = cylinder(tube_ri, width - 2 * side_wall)
	bat.rotate(v(0, 0, 0), v(0, 1, 0), 90)
	move(bat, -width / 2.0 + side_wall, 0, -tube_ro)
	top = top.cut(bat)

	thin = arc_tube(top_ro, top_ri, width - 2 * side_wall,
	    -90, 90 - 2 * base_a)
	move(thin, 0, face_h, -top_ro)
	top = top.fuse(thin)

	left = side(side_wall)

	left.rotate(v(0, 0, 0), v(1, 0, 0), -90)
	left.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(left, side_wall - width / 2.0, 0, 0)
	top = top.fuse(left)

	right = side(side_wall)

	right.rotate(v(0, 0, 0), v(1, 0, 0), -90)
	right.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(right, width / 2.0, 0, 0)
	top = top.fuse(right)

	return top


# ===== Catched to hold the main PCB ==========================================

#
# In PCB coordinate system
#

#
# "Catch" to hold the main PCB
#
# During construction (2D):
# - ramp begins at (0, 0)
# - support is parallel to Y
#
# At the end (3D):
# - origin is on the support surface, at the base of the catch
# - the base is on the XZ plane, with the catch extending into Y+
#

def top_catch():
	xfb = catch_ramp_h + catch_flat_h
	xsb = xfb + catch_r

	vrf = v(catch_ramp_h, catch_t, 0)
	vfb = v(xfb, catch_t, 0)
	vsb = v(xsb, catch_t - catch_r, 0)

	ramp = Part.Line(v(0, 0, 0), vrf)
	flat = Part.Line(vrf, vfb)
# alas, it seems that we can't use makeCircle here
#	bend = Part.makeCircle(catch_r, v(xsb, catch_t - catch_r, 0),
#	    v(0, 0, 1))
	bend = Part.Arc(vfb,
	    v(xfb + catch_r / sqrt(2), catch_t - catch_r + catch_r / sqrt(2),
	      0),
	    v(xsb, catch_t - catch_r, 0))
	support = Part.Line(vsb, v(xsb, 0, 0))
	close = Part.Line(v(xsb, 0, 0), v(0, 0, 0))

	s = Part.Shape([ ramp, flat, bend, support, close ])

	catch = extrude_shape(s, catch_w)
	move(catch, -xsb, 0, -catch_w / 2.0)
	catch.rotate(v(0, 0, 0), v(0, 1, 0), -90)

	return catch


def make_catches():
	all = None
	for dx in (-1, 1):
		lower = top_catch()
		move(lower, dx * catch_offset_lower, -pcb_xy_clear, 0)

		if all is None:
			all = lower
		else:
			all = all.fuse(lower)

		upper = top_catch()
		upper.rotate(v(0, 0, 0), v(0, 0, 1), 180)
		move(upper, dx * catch_offset_upper, pcb_h + pcb_xy_clear, 0)

		all = all.fuse(upper)

	move(all, pcb_w / 2.0, 0, -pcb_t - pcb_z_clear)

	return all


# ===== Bottom half ===========================================================


#
# Coordinate system:
#
# x = 0: center
# y = 0: edge of face at thick end
# z = 0: outside bottom of case (inside faces upward)
#


def make_lanyard_corner(t):
	vul = v(0, lanyard_bottom_cut_h, 0)
	vur = v(lanyard_bottom_cut_w, lanyard_bottom_cut_h, 0)
	vll = v(0, 0, 0)
	vlry = v(lanyard_bottom_cut_w, lanyard_bottom_cut_r, 0)
	vlrx = v(lanyard_bottom_cut_w - lanyard_bottom_cut_r, 0, 0)

	vm = v(lanyard_bottom_cut_w - lanyard_bottom_cut_r * (1 - 1 / sqrt(2)),
	    lanyard_bottom_cut_r * (1 - 1 / sqrt(2)), 0)

	top = Part.Line(vul, vur)
	right = Part.Line(vur, vlry)
	left = Part.Line(vul, vll)
	bottom = Part.Line(vll, vlrx)
	arc = Part.Arc(vlrx, vm, vlry)

	s = Part.Shape([ top, right, left, bottom, arc ])
	corner = extrude_shape(s, t)

	return corner


#
# @@@ Very ugly. We have the same calculation in make_bottom and bottom_face.
# Alas, it would be messy to share this.
#

def place_lanyard_corner(obj):
	move(obj, -width / 2.0 + wall + top_bottom_gap,
	    face_h - top_bottom_gap - lanyard_bottom_cut_h, 0)
	return place_bottom(obj)


def bottom_face(w, h, t):
	r = rect(w, h, t)

	cut = make_lanyard_corner(t)
	move(cut, 0, h - lanyard_bottom_cut_h, 0)
	r = r.cut(cut)

	move(r, -w / 2.0, 0, 0)
	return r


#
# While constructing the lug, xy is the inside of the bottom plate, with its
# (top) edge at y = 0.
#

def make_lug(offset):
	lug = rect(lug_w, lug_base_h + lug_gap, lug_t + lug_gap)
	move(lug, 0, -lug_base_h, 0)

	cut = cylinder(lug_gap, lug_w)
	cut.rotate(v(0, 0, 0), v(0, 1, 0), 90)
	move(cut, 0, lug_gap, 0)
	lug = lug.cut(cut)

	r = rect(lug_w, lug_h, lug_t)
	move(r, 0, lug_gap, lug_gap)
	lug = lug.fuse(r)

	cyl = cylinder(lug_t / 2.0, lug_w)
	cyl.rotate(v(0, 0, 0), v(0, 1, 0), 90)
	move(cyl, 0, lug_gap + lug_h, lug_t / 2.0 + lug_gap)
	lug = lug.fuse(cyl)

	move(lug, offset - lug_w / 2.0, face_h - top_bottom_gap, wall)

	return lug


def make_bottom():
	bat = half_tube(tube_ro, tube_ri, width - 2 * (wall + top_bottom_gap),
	    batt_cut_a - base_a)

	for dx in (-1, 1):
		trim = rect(dx * (side_wall - wall), tube_ro, tube_ro)
		move(trim, dx * (width / 2.0 - side_wall - top_bottom_gap),
		    0, wall)
		bat = bat.cut(trim)

	face = bottom_face(width - 2 * (wall + top_bottom_gap),
	    face_h - top_bottom_gap, wall)
	bot = bat.fuse(face)

	offset = screw_hole_head_t + screw_hole_pass_t + screw_mount_gap

	all_cuts = None

	for x in screw_offsets:
		solid, cut = screw_head_area(x, screw_y, screw_z)
		bot = bot.fuse(solid)
		if all_cuts is None:
			all_cuts = cut
		else:
			all_cuts = all_cuts.fuse(cut)

	left = make_lug(-lug_offset)
	bot = bot.fuse(left)
	middle = make_lug(0)
	bot = bot.fuse(middle)
	right = make_lug(lug_offset)
	bot = bot.fuse(right)

	return bot, all_cuts


# ===== PCB ===================================================================

#
# Coordinate system:
#
# origin = top side lower left corner of PCB
#


def make_pcb_main():
	return pcb_main_outline(0, -pcb_t)


# ===== Placement =============================================================


def place_top(obj):
	move(obj, 0, 0, tube_ro)
	obj.rotate(v(0, 0, 0), v(1, 0, 0), -base_a)
	return obj


def place_bottom(obj):
	move(obj, 0, 0, -tube_ro)
	obj.rotate(v(0, 0, 0), v(1, 0, 0), base_a)
	return obj


#
# Place components with respect to the main PCB as follows:
# x, y = 0, 0:	lower left corner
# z = 0:	top of PCB
#
# Z+ is above board, X and Y correspond to position in top view
#

def place_pcb(obj):
	move(obj, pcb_x, pcb_y, pcb_z)
	return place_top(obj)


def place_oled(obj):
	move(obj, oled_x, oled_y, oled_z)
	return place_pcb(obj)


def place_memcard(obj):
	obj.rotate(v(0, 0, 0), v(0, 0, 1), 180)
	obj.rotate(v(0, 0, 0), v(0, 1, 0), 180)
	move(obj, card_holder_x, card_holder_y + card_holder_h, -pcb_t)
	return place_pcb(obj)


def place_switch(obj):
	obj.rotate(v(sw_body_w / 2.0, 0, -pcb_t / 2.0), v(0, 1, 0), 180)
	obj.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(obj, sw_x, sw_y, 0)
	return place_pcb(obj)


def place_usb(obj):
	obj.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(obj, pcb_w, usb_y - usb_rcpt_w / 2.0, -pcb_t - usb_rcpt_t)
	return place_pcb(obj)


# ===== Top cutouts, and glass ================================================

#
# Glass cutout, in top coordinate system
#

glass_overlap = 1.0
glass_xy_gap = 0.1
glass_z_gap = 0.1

glass_w = oled_w + 2 * glass_overlap
glass_h = oled_h + 2 * glass_overlap
glass_t = 1.0

glass_x = pcb_x + oled_x - glass_w / 2.0
glass_y = pcb_y + oled_y + oled_off + - glass_h / 2.0
glass_z = -glass_t

def make_glass():
	r = rect(glass_w, glass_h, glass_t)
	move(r, glass_x, glass_y, glass_z)
	return r


def make_glass_cutout():
	r = rect(glass_w + 2 * glass_xy_gap, glass_h + 2 * glass_xy_gap,
	    glass_t + glass_z_gap)
	move(r, glass_x - glass_xy_gap, glass_y - glass_xy_gap,
	    glass_z - glass_z_gap)
	return r

#
# OLED panel cutout, in PCB coordinate system
#

oled_xy_gap = 0.1
oled_z_overshoot = 1.6

def make_oled_cutout():
	r = rect(oled_w + 2 * oled_xy_gap, oled_h + 2 * oled_xy_gap,
	    oled_t + oled_z_overshoot + 0.1)
	# add extra 0.1 mm to make sure we punch through the top
		# (this is for the 50 um of slack we allow)
	move(r, oled_x - oled_w / 2.0 - oled_xy_gap,
	    oled_y - oled_h / 2.0 + oled_off - oled_xy_gap,
	    oled_z - oled_z_overshoot)
	return r


#
# PCB top-side components cut-out, in PCB coordinate system
#

def make_pcb_top_comp_cutout():
	r = pcb_main_outline(pcb_overlap, oled_z - pcb_z_clear)
	move(r, 0, 0, pcb_z_clear)
	return r


#
# Main PCB cut-out, in PCB coordinate system
#

def make_pcb_main_top_cutout():
	r = pcb_main_outline(-pcb_xy_clear, -2 * tube_ro)
		# 2 * tube_ro to make sure we cut through everything
	move(r, 0, 0, pcb_z_clear)
	return r


#
# Lanyard cut-out, in global coordinate system
#

def make_lanyard_cutout():
	left = Part.Line(v(0, lanyard_r, 0), v(0, lanyard_h - lanyard_r, 0))
	right = Part.Line(v(lanyard_w, lanyard_r, 0),
	    v(lanyard_w, lanyard_h - lanyard_r, 0))
	lower = Part.Arc(v(0, lanyard_r, 0), v(lanyard_r, 0, 0),
	    v(lanyard_w, lanyard_r, 0))
	upper = Part.Arc(v(0, lanyard_h - lanyard_r, 0),
	    v(lanyard_r, lanyard_h, 0), v(lanyard_w, lanyard_h - lanyard_r, 0))

	s = Part.Shape([ left, right, lower, upper ])
	cut = extrude_shape(s, -2 * tube_ro)

	move(cut, lanyard_x - width / 2.0, lanyard_y, 0)
	cut = place_top(cut)

	return cut

def make_lanyard_cutout_cylinders():
	a = cylinder(lanyard_r2, lanyard_r1 + lanyard_r2)
	b = cylinder(lanyard_r2, lanyard_r1 + lanyard_r2)
	a.rotate(v(0, 0, 0), v(0, 1, 0), -90)
	b.rotate(v(0, 0, 0), v(1, 0, 0), -90)
	move(a, lanyard_r2, 0, 0)
	move(b, 0, -lanyard_r2, 0)
	a = a.fuse(b)
	move(a, -width / 2.0 + lanyard_r1, base + top_ro - lanyard_r1, 0)
	return a


# ===== Put everything together ===============================================


#
# Global coordinate system:
#
# X axis is battery axis
# X = 0 is battery center
# Case is symmetric with respect to XY
# Z+ is top
#


def assemble():
	top = make_top()
	top = place_top(top)

	bottom, bottom_cut = make_bottom()
	bottom = place_bottom(bottom)

	pcb_main = make_pcb_main()
	move(pcb_main, pcb_x, pcb_y, pcb_z)
	pcb_main = place_top(pcb_main)

	screw = make_screw()
	move(screw, screw_offsets[0], screw_y, wall)
	screw = place_bottom(screw)

	oled_panel = make_oled_panel()
	oled_panel = place_oled(oled_panel)

	oled_visual = make_oled_visual()
	oled_visual = place_oled(oled_visual)

	oled_fpc = make_fpc(oled_z)
	oled_fpc = place_oled(oled_fpc)

	usb = make_usb()
	usb = place_usb(usb)

	card = make_memcard()
	card = place_memcard(card)

	cut = make_memcard_cutout()
	cut = place_memcard(cut)
	top = top.cut(cut)
	bottom = bottom.cut(cut)

	tr = tact_row()
	tr = place_pcb(tr)

	br = button_row()
	br = place_pcb(br)
	cut = button_row_cutout()
	cut = place_pcb(cut)
	top = top.cut(cut)

	sw, cut = make_switch()
	sw = place_switch(sw)
	cut = place_switch(cut)
	pcb_main = pcb_main.cut(cut)

	knob, cut = make_knob()
	knob = place_switch(knob)
	cut = place_switch(cut)
	top = top.cut(cut)

	led = make_led()
	led.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(led, buttons_x, buttons_y + buttons_step_y * 1.5 - 0.5, 0)
	led = place_pcb(led)

	antenna = make_antenna()
	antenna.rotate(v(0, 0, 0), v(0, 0, 1), 90)
	move(antenna, antenna_h, antenna_y, 0)
	antenna = place_pcb(antenna)

	bat = make_aaa()
	move(bat, 0, 0, -bat_tot_h / 2.0)
	bat.rotate(v(0, 0, 0), v(0, 1, 0), 90)

	glass = make_glass()
	place_top(glass)

	cut = make_glass_cutout()
	place_top(cut)
	top = top.cut(cut)

	cut = make_oled_cutout()
	place_pcb(cut)
	top = top.cut(cut)

	cut = make_pcb_top_comp_cutout()
	place_pcb(cut)
	top = top.cut(cut)

	cut = make_pcb_main_top_cutout()
	place_pcb(cut)
	top = top.cut(cut)

	corner = make_lanyard_corner(2 * wall)  # 2 * wall is handwaving
	place_lanyard_corner(corner)
	top = top.fuse(corner)

	cut = make_lanyard_cutout()
	top = top.cut(cut)

	cut = make_fpc_cutout(oled_z)
	place_oled(cut)
	top = top.cut(cut)

	cut = make_usb_cutout()
	cut = place_usb(cut)
	top = top.cut(cut)

	catches = make_catches()
	place_pcb(catches)
	top = top.fuse(catches)

	barrels = make_screw_barrels()
	barrels = place_bottom(barrels)
	top = top.fuse(barrels)

	cut = make_screw_barrel_cutouts()
	cut = place_bottom(cut)
	top = top.cut(cut)

	top = jiggle(top, bottom, 0.1)

	place_bottom(bottom_cut)
	bottom = bottom.cut(bottom_cut)

	visualize(doc, top, "Top", (1.0, 1.0, 1.0), 50)
	visualize(doc, bottom, "Bottom", (1.0, 1.0, 1.0), 50)
	visualize(doc, pcb_main, "Main PCB", (0.0, 1.0, 0.0), 50)
	visualize(doc, oled_panel, "OLED Panel", (1.0, 1.0, 1.0), 70)
	visualize(doc, oled_visual, "OLED Display", (0.0, 0.0, 0.0), 50)
	visualize(doc, oled_fpc, "OLED FPC", (1.0, 0.5, 0.0), 50)
	visualize(doc, glass, "Glass", (1.0, 0.8, 0.2), 80)
	visualize(doc, usb, "USB", (0.5, 0.5, 0.5), 60)
	visualize(doc, card, "Card", (0.5, 0.5, 0.5), 60)
	visualize(doc, tr, "Tactile", (0.4, 0.4, 0.4), 50)
	visualize(doc, br, "Buttons", (0.3, 0.3, 1.0), 50)
	visualize(doc, sw, "Switch", (0.5, 0.5, 0.5), 60)
	visualize(doc, knob, "Knob", (0.8, 0.5, 1.0), 50)
	visualize(doc, led, "LED", (1.0, 1.0, 1.0), 80)
	visualize(doc, antenna, "Antenna", (0.0, 0.0, 0.0), 80)
	visualize(doc, bat, "Battery", (1.0, 0.7, 0.7), 50)
	visualize(doc, screw, "Screw", (0.0, 0.0, 0.0), 50)

	top.exportStl("top.stl")
	bottom.exportStl("bottom.stl")
	knob.exportStl("knob.stl")
	br.exportStl("br.stl")

	top.rotate(v(0, 0, 0), v(1, 0, 0), 180 + base_a)
	bottom.rotate(v(0, 0, 0), v(1, 0, 0), -base_a)
	knob.rotate(v(0, 0, 0), v(1, 0, 0), base_a)
	br.rotate(v(0, 0, 0), v(1, 0, 0), 180 + base_a)

	top.exportStl("top-flat.stl")
	bottom.exportStl("bottom-flat.stl")
	knob.exportStl("knob-flat.stl")
	br.exportStl("br-flat.stl")


doc = FreeCAD.newDocument()

pcb_main_gp(0)
assemble()
