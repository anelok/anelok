#!/usr/bin/perl

sub usage
{
	print STDERR "usage: $0 mailbox msg-id ...\n";
	exit(1);
}

$mbox = shift @ARGV;
&usage unless defined $mbox;
&usage unless $#ARGV >= 0;


sub flush
{
	if (defined $mid && grep($_ eq $mid, @ARGV)) {
		if ($b =~ /^[A-Za-z0-9+\/\n]+=*\s*$/s) {
			$b = `echo "$b" | base64 -d`;
		}
		my $t = "";
		for (split("\n", $b)) {
			if (/^(\s*)(http\S+\.jpg)\s*$/) {
				$t .= <<"EOF";
$1</pre><a href="$2"><img src="$2" height="100"></a><pre>
EOF
				next;
			}
			while (/^(.*?)(http\S+)/) {
				$t .= <<"EOF";
$1</pre><a href="$2"><code>$2</code></a><pre>
EOF
				$_ = $';
			}
			$t .= $_ . "\n";
		}
		$msg{$mid} = <<"EOF";
<div>${from}Subject: $subj$date</div></pre>
<pre>$t
EOF
		push(@subject, $subj);
	}
	undef $mid;
	undef $b;
	undef $from;
	undef $subj;
	undef $date;
	$hdr = 1;
}


sub escape
{
	local ($s) = $_[0];

	$s =~ s/\&/\&amp;/g;
	$s =~ s/</\&lt;/g;
	$s =~ s/>/\&gt;/g;
	return $s;
}


$hdr = 1;
open(MBOX, $mbox) || die "$mbox: $!";
while (<MBOX>) {
	if (/^From /) {
		&flush;
		next;
	}
	$hdr = 0 if /^\s*$/;
	if ($hdr) {
		# @@@ handle continuation lines
		if (/^Subject:\s+/) {
			$subj = &escape($');
			next;
		}
		if (/^Date:\s+/) {
			$date = &escape($_);
			next;
		}
		if (/^From:\s+/) {
			s/\s*<[^>]+>//g;
			$from = &escape($_);
			next;
		}
		if (/^Message-ID:\s+<([^>]+)>/) {
			$mid = $1;
			next;
		}
		next;
	} else {
		$b .= &escape($_);
	}
}
close(MBOX);
&flush;

print <<"EOF";
<html>
<head>
<title>$subject[0]</title>
<style>
div {
	background-color: #e0e0e0;
	padding-left:	1px;
	padding-top:	4px;
	padding-bottom:	4px;
}
</style>
</head>
<body>
EOF

$i = 0;
for (@subject) {
	print "<a href=\"#$i\">$_</a><br>\n";
	$i++;
}

print <<"EOF";
<p>
<hr>
<p>
EOF

$i = 0;
for (@ARGV) {
	print <<"EOF";
<a name="$i"></a>
<pre>$msg{$_}</pre>
<p>
<hr>
<p>
EOF
	$i++;
}

print <<"EOF";
</body>
</html>
EOF
