EESchema Schematic File Version 2
LIBS:pwr
LIBS:r
LIBS:c
LIBS:led
LIBS:powered
LIBS:kl25-48
LIBS:memcard8
LIBS:micro_usb_b
LIBS:varistor
LIBS:pmosfet-gsd
LIBS:antenna
LIBS:er-oled-fpc30
LIBS:at86rf231
LIBS:balun-smt6
LIBS:xtal-4
LIBS:testpoint
LIBS:tswa
LIBS:device_sot
LIBS:gencon
LIBS:aat1217
LIBS:inductor
LIBS:switch
LIBS:anelok-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Anelok Password Safe"
Date "5 oct 2013"
Rev ""
Comp "Werner Almesberger"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 9300 2200 2    60   Output ~ 0
VBAT
Text GLabel 4900 4800 2    60   Input ~ 0
PWR_EN
$Comp
L GND #PWR046
U 1 1 52364D82
P 4400 2700
F 0 "#PWR046" H 4400 2700 30  0001 C CNN
F 1 "GND" H 4400 2630 30  0001 C CNN
F 2 "" H 4400 2700 60  0000 C CNN
F 3 "" H 4400 2700 60  0000 C CNN
	1    4400 2700
	1    0    0    -1  
$EndComp
$Comp
L AAT1217 U3
U 1 1 5236563C
P 2250 4650
F 0 "U3" H 2050 4950 60  0000 C CNN
F 1 "AAT1217" H 2250 4300 60  0000 C CNN
F 2 "~" H 2250 4650 60  0000 C CNN
F 3 "~" H 2250 4650 60  0000 C CNN
	1    2250 4650
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 5236564B
P 2250 3900
F 0 "L1" H 2250 4050 60  0000 C CNN
F 1 "4.7uH" H 2250 3800 60  0000 C CNN
F 2 "" H 2250 3900 60  0000 C CNN
F 3 "" H 2250 3900 60  0000 C CNN
F 4 "600mA" H 2250 3700 60  0000 C CNN "Imax"
	1    2250 3900
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 5236565A
P 3300 5200
F 0 "C13" H 3350 5300 60  0000 L CNN
F 1 "4.7uF" H 3350 5100 60  0000 L CNN
F 2 "" H 3300 5200 60  0000 C CNN
F 3 "" H 3300 5200 60  0000 C CNN
	1    3300 5200
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 52365667
P 3800 5200
F 0 "C14" H 3850 5300 60  0000 L CNN
F 1 "4.7uF" H 3850 5100 60  0000 L CNN
F 2 "" H 3800 5200 60  0000 C CNN
F 3 "" H 3800 5200 60  0000 C CNN
	1    3800 5200
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR047
U 1 1 523656A7
P 3800 4300
F 0 "#PWR047" H 3800 4260 30  0001 C CNN
F 1 "3V3" H 3800 4450 60  0000 C CNN
F 2 "" H 3800 4300 60  0000 C CNN
F 3 "" H 3800 4300 60  0000 C CNN
	1    3800 4300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR048
U 1 1 523656F9
P 3800 5600
F 0 "#PWR048" H 3800 5600 30  0001 C CNN
F 1 "GND" H 3800 5530 30  0001 C CNN
F 2 "" H 3800 5600 60  0000 C CNN
F 3 "" H 3800 5600 60  0000 C CNN
	1    3800 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR049
U 1 1 5236570F
P 3300 5600
F 0 "#PWR049" H 3300 5600 30  0001 C CNN
F 1 "GND" H 3300 5530 30  0001 C CNN
F 2 "" H 3300 5600 60  0000 C CNN
F 3 "" H 3300 5600 60  0000 C CNN
	1    3300 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 5236581E
P 1300 5600
F 0 "#PWR050" H 1300 5600 30  0001 C CNN
F 1 "GND" H 1300 5530 30  0001 C CNN
F 2 "" H 1300 5600 60  0000 C CNN
F 3 "" H 1300 5600 60  0000 C CNN
	1    1300 5600
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5236584B
P 4300 5250
F 0 "R5" H 4430 5300 60  0000 C CNN
F 1 "100" H 4450 5200 60  0000 C CNN
F 2 "" H 4300 5250 60  0000 C CNN
F 3 "" H 4300 5250 60  0000 C CNN
	1    4300 5250
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 52365858
P 4300 5900
F 0 "C15" H 4350 6000 60  0000 L CNN
F 1 "220uF" H 4350 5800 60  0000 L CNN
F 2 "" H 4300 5900 60  0000 C CNN
F 3 "" H 4300 5900 60  0000 C CNN
	1    4300 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 52365868
P 4300 6300
F 0 "#PWR051" H 4300 6300 30  0001 C CNN
F 1 "GND" H 4300 6230 30  0001 C CNN
F 2 "" H 4300 6300 60  0000 C CNN
F 3 "" H 4300 6300 60  0000 C CNN
	1    4300 6300
	1    0    0    -1  
$EndComp
Text GLabel 4900 4500 2    60   Input ~ 0
VSYS
$Comp
L R R6
U 1 1 52365966
P 2250 5400
F 0 "R6" H 2380 5450 60  0000 C CNN
F 1 "0" H 2400 5350 60  0000 C CNN
F 2 "" H 2250 5400 60  0000 C CNN
F 3 "" H 2250 5400 60  0000 C CNN
F 4 "DNP" V 2150 5400 60  0000 C CNN "Field4"
	1    2250 5400
	0    -1   -1   0   
$EndComp
$Comp
L ZX62-B-5PA CON2
U 1 1 5236BC26
P 9550 4800
F 0 "CON2" H 9550 5300 60  0000 C CNN
F 1 "ZX62R-AB-5P" H 9550 4300 60  0000 C CNN
F 2 "" H 9550 4800 60  0000 C CNN
F 3 "" H 9550 4800 60  0000 C CNN
	1    9550 4800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR052
U 1 1 5236BC2C
P 10300 5300
F 0 "#PWR052" H 10300 5300 30  0001 C CNN
F 1 "GND" H 10300 5230 30  0001 C CNN
F 2 "" H 10300 5300 60  0000 C CNN
F 3 "" H 10300 5300 60  0000 C CNN
	1    10300 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR053
U 1 1 5236BC32
P 8750 5300
F 0 "#PWR053" H 8750 5300 30  0001 C CNN
F 1 "GND" H 8750 5230 30  0001 C CNN
F 2 "" H 8750 5300 60  0000 C CNN
F 3 "" H 8750 5300 60  0000 C CNN
	1    8750 5300
	1    0    0    -1  
$EndComp
Text GLabel 6450 4600 0    60   3State ~ 0
USB_DM
Text GLabel 6450 4800 0    60   3State ~ 0
USB_DP
Text GLabel 6450 5000 0    60   Output ~ 0
USB_ID
Text GLabel 6450 4450 0    60   Output ~ 0
USB_VBUS
$Comp
L VARISTOR V4
U 1 1 5236BC54
P 8400 5650
F 0 "V4" V 8300 5650 60  0000 C CNN
F 1 "VARISTOR" V 8500 5650 60  0000 C CNN
F 2 "" H 8400 5650 60  0000 C CNN
F 3 "" H 8400 5650 60  0000 C CNN
	1    8400 5650
	1    0    0    -1  
$EndComp
$Comp
L VARISTOR V3
U 1 1 5236BC5A
P 8000 5650
F 0 "V3" V 7900 5650 60  0000 C CNN
F 1 "VARISTOR" V 8100 5650 60  0000 C CNN
F 2 "" H 8000 5650 60  0000 C CNN
F 3 "" H 8000 5650 60  0000 C CNN
	1    8000 5650
	1    0    0    -1  
$EndComp
$Comp
L VARISTOR V2
U 1 1 5236BC60
P 7600 5650
F 0 "V2" V 7500 5650 60  0000 C CNN
F 1 "VARISTOR" V 7700 5650 60  0000 C CNN
F 2 "" H 7600 5650 60  0000 C CNN
F 3 "" H 7600 5650 60  0000 C CNN
	1    7600 5650
	1    0    0    -1  
$EndComp
$Comp
L VARISTOR V1
U 1 1 5236BC66
P 7200 5650
F 0 "V1" V 7100 5650 60  0000 C CNN
F 1 "VARISTOR" V 7300 5650 60  0000 C CNN
F 2 "" H 7200 5650 60  0000 C CNN
F 3 "" H 7200 5650 60  0000 C CNN
	1    7200 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR054
U 1 1 5236BC6C
P 8400 6000
F 0 "#PWR054" H 8400 6000 30  0001 C CNN
F 1 "GND" H 8400 5930 30  0001 C CNN
F 2 "" H 8400 6000 60  0000 C CNN
F 3 "" H 8400 6000 60  0000 C CNN
	1    8400 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR055
U 1 1 5236BC72
P 8000 6000
F 0 "#PWR055" H 8000 6000 30  0001 C CNN
F 1 "GND" H 8000 5930 30  0001 C CNN
F 2 "" H 8000 6000 60  0000 C CNN
F 3 "" H 8000 6000 60  0000 C CNN
	1    8000 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 5236BC78
P 7600 6000
F 0 "#PWR056" H 7600 6000 30  0001 C CNN
F 1 "GND" H 7600 5930 30  0001 C CNN
F 2 "" H 7600 6000 60  0000 C CNN
F 3 "" H 7600 6000 60  0000 C CNN
	1    7600 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR057
U 1 1 5236BC7E
P 7200 6000
F 0 "#PWR057" H 7200 6000 30  0001 C CNN
F 1 "GND" H 7200 5930 30  0001 C CNN
F 2 "" H 7200 6000 60  0000 C CNN
F 3 "" H 7200 6000 60  0000 C CNN
	1    7200 6000
	1    0    0    -1  
$EndComp
Text Notes 900  1150 0    200  ~ 40
Power + USB
$Comp
L SW-SPDT SW2
U 1 1 5236D6C5
P 7150 2200
F 0 "SW2" H 7200 2400 60  0000 C CNN
F 1 "JS102011SAQN" H 7150 2000 60  0000 C CNN
F 2 "~" H 7150 2200 60  0000 C CNN
F 3 "~" H 7150 2200 60  0000 C CNN
	1    7150 2200
	-1   0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 523B029E
P 7150 1700
F 0 "R9" H 7280 1750 60  0000 C CNN
F 1 "10k" H 7300 1650 60  0000 C CNN
F 2 "" H 7150 1700 60  0000 C CNN
F 3 "" H 7150 1700 60  0000 C CNN
	1    7150 1700
	0    -1   -1   0   
$EndComp
Text Notes 7500 1200 0    60   ~ 0
Determine exact value of R9 by measurement.\nMust be high enough to not allow transceiver to send/receive.\nMust be low enough to supply sleeping MCU with RTC.
$Comp
L POWERED #FLG058
U 1 1 523D38C6
P 8500 1700
F 0 "#FLG058" H 8700 1600 60  0001 C CNN
F 1 "POWERED" H 8500 1750 60  0000 C CNN
F 2 "" H 8500 1700 60  0000 C CNN
F 3 "" H 8500 1700 60  0000 C CNN
	1    8500 1700
	1    0    0    -1  
$EndComp
$Comp
L POWERED #FLG059
U 1 1 523D393A
P 8500 4050
F 0 "#FLG059" H 8700 3950 60  0001 C CNN
F 1 "POWERED" H 8500 4100 60  0000 C CNN
F 2 "" H 8500 4050 60  0000 C CNN
F 3 "" H 8500 4050 60  0000 C CNN
	1    8500 4050
	1    0    0    -1  
$EndComp
$Comp
L PMOSFET-GSD Q1
U 1 1 524300BD
P 5700 1800
F 0 "Q1" V 6000 1950 60  0000 C CNN
F 1 "NTS2101P" V 5400 1500 60  0000 C CNN
F 2 "" H 5700 1800 60  0000 C CNN
F 3 "" H 5700 1800 60  0000 C CNN
	1    5700 1800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR060
U 1 1 524301D5
P 5700 2700
F 0 "#PWR060" H 5700 2700 30  0001 C CNN
F 1 "GND" H 5700 2630 30  0001 C CNN
F 2 "" H 5700 2700 60  0000 C CNN
F 3 "" H 5700 2700 60  0000 C CNN
	1    5700 2700
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 CON4
U 1 1 5243AAB8
P 3800 2400
F 0 "CON4" H 3800 2250 60  0000 C CNN
F 1 "CR2032" H 3800 2550 60  0000 C CNN
F 2 "" H 3800 2400 60  0000 C CNN
F 3 "" H 3800 2400 60  0000 C CNN
	1    3800 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 1500 5000 2350
Wire Wire Line
	2800 4500 4900 4500
Wire Wire Line
	2800 4650 3800 4650
Wire Wire Line
	3800 4300 3800 5000
Wire Wire Line
	3300 5000 3300 4500
Connection ~ 3300 4500
Wire Wire Line
	3300 5400 3300 5600
Connection ~ 3800 4650
Wire Wire Line
	3800 5400 3800 5600
Wire Wire Line
	2800 4800 4900 4800
Wire Wire Line
	2600 3900 3000 3900
Wire Wire Line
	3000 3900 3000 4500
Connection ~ 3000 4500
Wire Wire Line
	1700 4500 1500 4500
Wire Wire Line
	1500 4500 1500 3900
Wire Wire Line
	1500 3900 1900 3900
Wire Wire Line
	1300 5600 1300 4650
Wire Wire Line
	1300 4650 1700 4650
Wire Wire Line
	4300 5500 4300 5700
Wire Wire Line
	4300 6100 4300 6300
Wire Wire Line
	3000 4800 3000 5400
Wire Wire Line
	3000 5400 2500 5400
Connection ~ 3000 4800
Wire Wire Line
	1700 4800 1500 4800
Wire Wire Line
	1500 4800 1500 5400
Wire Wire Line
	1500 5400 2000 5400
Wire Wire Line
	10100 4500 10300 4500
Wire Wire Line
	10300 4500 10300 5300
Wire Wire Line
	10100 4600 10300 4600
Connection ~ 10300 4600
Wire Wire Line
	10100 4750 10300 4750
Connection ~ 10300 4750
Wire Wire Line
	10100 4850 10300 4850
Connection ~ 10300 4850
Wire Wire Line
	10100 5000 10300 5000
Connection ~ 10300 5000
Wire Wire Line
	10100 5100 10300 5100
Connection ~ 10300 5100
Wire Wire Line
	8750 5300 8750 5100
Wire Wire Line
	8750 5100 8950 5100
Wire Wire Line
	6450 4600 6700 4600
Wire Wire Line
	6700 4600 6700 4650
Wire Wire Line
	6700 4650 8950 4650
Wire Wire Line
	6450 4800 8950 4800
Wire Wire Line
	6450 5000 6650 5000
Wire Wire Line
	6650 5000 6650 4950
Wire Wire Line
	6650 4950 8950 4950
Wire Wire Line
	6450 4450 6800 4450
Wire Wire Line
	6800 4450 6800 4500
Wire Wire Line
	6800 4500 8950 4500
Wire Wire Line
	7200 5500 7200 4500
Connection ~ 7200 4500
Wire Wire Line
	7600 5500 7600 4650
Connection ~ 7600 4650
Wire Wire Line
	8000 5500 8000 4800
Connection ~ 8000 4800
Wire Wire Line
	8400 5500 8400 4950
Connection ~ 8400 4950
Wire Wire Line
	7200 5800 7200 6000
Wire Wire Line
	7600 5800 7600 6000
Wire Wire Line
	8000 5800 8000 6000
Wire Wire Line
	8400 5800 8400 6000
Wire Wire Line
	6100 1700 6900 1700
Wire Wire Line
	7400 1700 7800 1700
Wire Wire Line
	4300 5000 4300 4500
Connection ~ 4300 4500
Wire Wire Line
	8500 4250 8500 4500
Connection ~ 8500 4500
Wire Wire Line
	5000 1700 5300 1700
Connection ~ 6500 1700
Wire Wire Line
	5700 2100 5700 2700
Wire Wire Line
	6500 1700 6500 2100
Wire Wire Line
	6500 2100 6700 2100
Wire Wire Line
	7600 2200 9300 2200
Wire Wire Line
	7800 1700 7800 2200
Connection ~ 7800 2200
Wire Wire Line
	8500 1900 8500 2200
Connection ~ 8500 2200
NoConn ~ 6700 2300
$Comp
L TESTPOINT TP9
U 1 1 524DAF15
P 5000 1500
F 0 "TP9" H 5000 1750 60  0000 C CNN
F 1 "TESTPOINT" H 5000 1450 60  0001 C CNN
F 2 "" H 5000 1500 60  0000 C CNN
F 3 "" H 5000 1500 60  0000 C CNN
	1    5000 1500
	1    0    0    -1  
$EndComp
$Comp
L TESTPOINT TP10
U 1 1 524DAF24
P 2600 2400
F 0 "TP10" H 2600 2650 60  0000 C CNN
F 1 "TESTPOINT" H 2600 2350 60  0001 C CNN
F 2 "" H 2600 2400 60  0000 C CNN
F 3 "" H 2600 2400 60  0000 C CNN
	1    2600 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR061
U 1 1 524DAF33
P 2600 2600
F 0 "#PWR061" H 2600 2600 30  0001 C CNN
F 1 "GND" H 2600 2530 30  0001 C CNN
F 2 "" H 2600 2600 60  0000 C CNN
F 3 "" H 2600 2600 60  0000 C CNN
	1    2600 2600
	1    0    0    -1  
$EndComp
Connection ~ 5000 1700
Wire Wire Line
	2600 2400 2600 2600
$Comp
L TESTPOINT TP11
U 1 1 524F4121
P 9100 1900
F 0 "TP11" H 9100 2150 60  0000 C CNN
F 1 "TESTPOINT" H 9100 1850 60  0001 C CNN
F 2 "" H 9100 1900 60  0000 C CNN
F 3 "" H 9100 1900 60  0000 C CNN
	1    9100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1900 9100 2200
Connection ~ 9100 2200
$Comp
L TESTPOINT TP12
U 1 1 524F45A5
P 4500 4300
F 0 "TP12" H 4500 4550 60  0000 C CNN
F 1 "TESTPOINT" H 4500 4250 60  0001 C CNN
F 2 "" H 4500 4300 60  0000 C CNN
F 3 "" H 4500 4300 60  0000 C CNN
	1    4500 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4300 4500 4500
Connection ~ 4500 4500
Wire Wire Line
	5000 2350 4200 2350
Wire Wire Line
	4200 2450 4400 2450
Wire Wire Line
	4400 2450 4400 2700
Text Label 4400 2350 0    60   ~ 0
BATTERY
$EndSCHEMATC
