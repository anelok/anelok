#!/usr/bin/perl
#
# tps.pl- Test point position checker and extractor
#
# Written 2014, 2017 by Werner Almesberger
# Copyright 2014, 2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


$MIN_DISTANCE_MM = 4;


#----- Command line -----------------------------------------------------------

sub usage
{
	print STDERR "usage: $0 [-c] file.pos file.net comp-name mod-name\n\n";
	print STDERR "Example: $0 foo-all.pos foo.net TESTPOINT PAD_C_80x80\n";
	exit(1);
}


if ($ARGV[0] eq "-c") {
	$cad = 1;
	shift @ARGV;
}
&usage if $ARGV[0] =~ /^-/;
&usage unless $#ARGV == 3;
($pos, $net, $comp, $mod) = @ARGV;

#----- Read and validate test point positions ---------------------------------

open(POS, $pos) || die "$pos: $!";
while (<POS>) {
	$f = 25.4 if /^##\s+Unit\s+=\s+inch/;
	$f = 1 if /^##\s+Unit\s+=\s+mm/;
	next if /^#/;
	next unless /\b$comp\s+$mod\b/;
	@a = split /\s+/;
	$x{$a[0]} = $a[3]*$f;
	$y{$a[0]} = $a[4]*$f;
}
close POS;
@k = sort keys %x;

for ($i = 0; $i != @k; $i++) {
	for ($j = $i+1; $j != @k; $j++) {
		$dx = $x{$k[$i]}-$x{$k[$j]};
		$dy = $y{$k[$i]}-$y{$k[$j]};
		$d = sqrt($dx*$dx+$dy*$dy);
		print STDERR "$k[$i] to $k[$j]: $d mm\n"
		    if $d < $MIN_DISTANCE_MM;
		$dmin = $d unless defined $dmin && $d > $dmin;
	}
}

#----- Read netlist -----------------------------------------------------------

open(NET, $net) || die "$net: $!";
while (<NET>) {
	$n = "$2" if /^\s*\(net.*\(name\s+\/?([^)\/]*\/)*([^)]+)\)/;
	next unless /^\s*\(node\s+\(ref\s+([^)]+)\)/;
	next unless defined $x{$1};
	die if defined $net{$1} && $net{$1} ne $n;
	$net{$1} = $n;
}
close NET;

#----- Print locations of test points -----------------------------------------

undef $xmin;
for (values %x) {
	$xmin = $_ unless defined $xmin && $xmin < $_;
}
undef $ymax;
for (values %y) {
	$ymax = $_ unless defined $ymax && $ymax > $_;
}

for (@k) {
	if ($cad) {
		printf("%5.1f\t%5.1f\t%s\n", $x{$_}, $y{$_}, $net{$_});
	} else {
		printf("%5.1f\t%5.1f\t%s\t%s\n",
		    $x{$_}-$xmin, $ymax-$y{$_}, $_, $net{$_});
	}
}

printf STDERR "Minimum distance %.2f\n", $dmin;
