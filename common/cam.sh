#!/bin/sh
while [ "$1" ]; do
	eval "$1"
	shift
done

: ${MILL:=125mil}
: ${OVERLAP:=1.6mm}
: ${X0:=0mm}
: ${Y0:=0mm}
: ${Z1:=0mm}
: ${PIECE_Z:=0mm}
: ${Z_OFFSET:=0mm}
: ${ROTATE:=0}
: ${CAMEO:=cameo}
: ${OUT:=out.gp}

if [ "$FLIP" ]; then
	FLIP_CMD="flip $FLIP"
else
	FLIP_CMD=
fi

$CAMEO <<EOF
gnuplot $MILL $PART-piece.gp
gnuplot $MILL $PART.gp
rotate $ROTATE
$REVERSE
$FLIP_CMD
align 1 $X0 $Y0
z 0 $Z1
z $Z_OFFSET
z -$PIECE_Z
area $OVERLAP
write $OUT
EOF
