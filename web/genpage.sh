#!/bin/sh

LIST_ARCH=http://lists.en.qi-hardware.com/pipermail/discussion/

usage()
{
	cat <<EOF 1>&2
usage: $0 [-c file.css ...] [-h file.html ...] number file.html ...
EOF
	exit 1
}


off=menu
on=selected
c0=$off
c1=$off
c2=$off
c3=$off

css=
head=
while [ "$1" ]; do
	case "$1" in
	-c)	[ "$2" ] || usage
		css="$css $2"
		shift 2;;
	-h)	[ "$2" ] || usage
		head="$head $2"
		shift 2;;
	*)	break;;
	esac
done

# top=$LIST_ARCH/2013-September/010243.html;;
top=index.html
case "$1" in
0)	c0=$on;;
1)	c1=$on;;
2)	c2=$on;;
3)	c3=$on;;
*)	usage;;
esac
shift

cat <<EOF
<!DOCTYPE HTML PUBLIC>
<HTML>
<HEAD>
	<TITLE>Anelok</TITLE>
	<STYLE type="text/css">

#logo {
	padding-right:		32px;
	vertical-align:		top;
}

#header {
	font-family:		Helvetica, Sans-serif, Arial;
	font-weight:		bold;
	vertical-align:		top;
	border-collapse:	collapse;
	border-spacing:		0px 0px;
	padding:		0px;
	margin:			0px;
	border:			0px;
}

#header tr, #header td.pps {
	font-size:		22px;
	color:			black;
	background-color:	white;
	text-align:		left;
	vertical-align:		top;
}

#header td.pps {
	position:		relative;
	top:			-4px;
}

#header td.menu a, #header td.selected a {
	font-size:		16px;
	text-decoration:	none;
	padding:		3px 30px 3px 30px;
	margin-right:		4px;
	text-align:		center;
	border-top:		100px;
	position:		relative;
	bottom:			-12px;

}

#header td.menu a {
	color:			black;
	background-color:	#a0a0a0;
}

#header td.menu a:hover {
	color:			white;
}

#header td.selected a {
	color:			white;
	background-color:	#202020;
}

hr.divider {
	margin-top:		8px;
	border-width:		1px;
	border-style:		solid;
}
EOF

[ "$css" ] && cat $css

echo '</STYLE>'

[ "$head" ] && cat $head

cat <<EOF
</HEAD>
<BODY>
<TABLE id="header">
  <TR class="pps">
    <TD id="logo" rowspan="2">
    <A href="$top">
EOF
echo '<IMG src="logo.png">' | ./imgsize.pl
cat <<EOF
    </A></TD>
    <TD class="pps" colspan="3">Personal Password Safe</TD>
  <TR>
    <TD class="$c0"><A href="index.html">Home</A></TD>
    <TD class="$c1"><A href="about.html">About</A></TD>
    <TD class="$c2"><A href="source.html">Source</A></TD>
    <TD class="$c3"><A href="people.html">People</A></TD>
  </TR>
</TABLE>
<HR class="divider">
<P>
EOF

cat "$@"

cat <<EOF
</BODY>
</HTML>
EOF
