The project follows an "open
everything" approach, with source code, hardware design, artwork,
development process, and also the tools we use being openly available.
<P>
Overview:<BR>
<UL>
  <LI><A href="block/">Block diagrams</A>
  <LI><A href="http://downloads.qi-hardware.com/people/werner/anelok/tmp/mk3-partial/anelok.pdf">Schematics</A>
    (<A href="http://downloads.qi-hardware.com/people/werner/anelok/tmp/anelok-20150107.pdf">old</A>)
  <LI><A href="http://downloads.qi-hardware.com/people/werner/anelok/tmp/anelok-20150108.png">Layout</A> (new version still pending)
  <LI><A href="http://downloads.qi-hardware.com/people/werner/anelok/tmp/case-20160531.png">CAD drawing of the case</A>
  (<A href="http://downloads.qi-hardware.com/people/werner/anelok/tmp/anelok-case-20150114.png">old</A>)
</UL>
<P>
Repositories:
<UL>
  <LI><A href="https://gitlab.com/anelok/anelok">Main repository</A>
    on <A href="https://gitlab.com/">GitLab</A><BR>
    <SAMP>git clone https://gitlab.com/anelok/anelok.git</SAMP>
  <LI><A href="https://gitlab.com/anelok/mexp">CAD models</A>
    (directory <SAMP>alt13/</SAMP>)<BR>
    <SAMP>git clone https://gitlab.com/anelok/mexp.git</SAMP>
  <LI><A href="http://projects.qi-hardware.com/index.php/p/kicad-libs/">Component
    and footprint libraries</A><BR>
    <SAMP>git clone git://projects.qi-hardware.com/kicad-libs.git</SAMP>
  <LI><A href="http://projects.qi-hardware.com/index.php/p/ben-wpan/">USB
    stack</A> (directory <SAMP>atusb/fw/usb/</SAMP>)<BR>
    <SAMP>git clone git://projects.qi-hardware.com/ben-wpan.git</SAMP>
</UL>
<P>
Tools:
<UL>
  <LI>For software development, we use the
    <A href="https://en.wikipedia.org/wiki/GNU_toolchain">GNU toolchain</A>
  <LI>Schematics and layout are made with
    <A href="http://www.kicad-pcb.org/">KiCAD</A>
  <LI>The 3D CAD work is done mainly with
    <A href="http://solvespace.com/">SolveSpace</A>,<BR>
    some older parts were designed with
    <A href="http://www.freecadweb.org/">FreeCAD</A>.
  <LI>We use a few additional
    <A href="http://projects.qi-hardware.com/index.php/p/eda-tools/">EDA
    tools</A><BR>
    <SAMP>git clone git://projects.qi-hardware.com/eda-tools.git</SAMP>
  <LI><A href="http://projects.qi-hardware.com/index.php/p/cae-tools/">CAE
    tools</A><BR>
    <SAMP>git clone git://projects.qi-hardware.com/cae-tools.git</SAMP>
  <LI>Documentation is produced using
    <A href="http://www.latex-project.org/">LaTeX</A> and
    <A href="http://www.xfig.org/">Xfig</A>
</UL>
