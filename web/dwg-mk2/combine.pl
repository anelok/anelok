#!/usr/bin/perl


sub split
{
	local ($s) = @_;

	die unless $s =~ /^(-?\d+)\s+(-?\d+)$/;
	return ($1, $2);
}


sub pick
{
	local @prev = (@{ $_[0] }[0], @{ $_[0] }[1]);

	my $a = "$prev[0] $prev[1]";
	return undef unless defined @{ $v{$a} }[0];
	my $p = shift(@{ $v{$a} });
	my $b = "@{ $p }[0] @{ $p }[1]";
	@{ $v{$b} } =
	    grep(@{ $_ }[0] != $prev[0] || @{ $_ }[1] != $prev[1], @{ $v{$b} });
	return $p;
}


sub merge
{
	my $first = (keys %v)[0];
	die unless defined $first;

	my @p = ( &pick([ &split($first) ]) );

	while (1) {
		my $next = &pick($p[0]);
		if (defined $next) {
			unshift(@p, $next);
			next;
		}
		my $next = &pick($p[$#p]);
		if (defined $next) {
			push(@p, $next);
			next;
		}
		last;
	}
	undef %v;

	return @p;
}


sub add
{
	local ($a, $b, $c, $d) = @_;

	push(@{ $v{"$a $b"} }, [ $c, $d ]);
	push(@{ $v{"$c $d"} }, [ $a, $b ]);
}


sub do_poly
{
	local ($n) = @_;

	#
	#      polyline     pen_color         area_fill      forward_arrow
	#      | polyline   |   fill_color    |     style_val| backward_arrow
	#      | | line_style   |   depth     |     | join_style npoints
	#      | | | thickness  |   |      pen_style| | cap_style|
	#      | | | |      |   |   |      |  |     | | | radius |
	#      | | | |      |   |   |      |  |     | | | |  | | |
	print "2 1 0 $thick $fg $bg $depth -1 $fill 0 1 1 -1 0 0 $n\n";
	#      0 1 2 3      4   5   6      7  8     9 101112 131415
}


sub sel_poly
{
	local ($n) = @_;

	local $thick = 4;	# thickness
	local $fg = 14;		# Pen color, Green 2
	local $bg = 0;		# Background color, black (don't care)
	local $depth = 110;	# depth
	local $fill = -1;	# area fill, none

	&do_poly($n);
}


sub sel_floor
{
	local ($n) = @_;

	local $thick = 0;	# thickness
	local $fg = 0;		# Pen color, black (don't care)
	local $bg = 2;		# Background color, green
	local $depth = 190;	# depth
	local $fill = 36;	# 80% white

	&do_poly($n);
}


while (<>) {
	if (/^\S/ && defined $this_poly) {
		&sel_poly($a[15]);
		print $vertices;
		&sel_floor($a[15]);
		print $vertices;
		undef $this_poly;
	}
	if (/^#\s+(\S+)\s*$/) {
		print;
		$comment = $1;
		next;
	}

	# continuation line

	if (/^\s+/) {
		if ($this_poly) {
			$vertices .= $_;
			next;
		}
		if (!$this) {
			print;
			next;
		}
		@a = split(/\s+/, $_);
		die unless $#a == 4;
		shift @a;
		&add(@a);
		&add($a[2], $a[3], $a[0], $a[1]);
		undef $this;
		next;
	}
	die if defined $this;

	# begin compound

	$collect = $comment if /^6\s/;

	# end compound

	if (/^-6/ && defined $collect) {
		if (keys %v) {
			@p = &merge;

			# @@@ dirty hack: for some reason all vertices are
			# duplicated. Just pretend they aren't ;)
			splice(@p, 0, @p/2);
			push(@p, $p[0]);

			print "# href=\"$collect\" alt=\"\"\n";

			&sel_poly($#p + 1);
			for (@p) {
				print "\t@{ $_ }[0] ${ $_ }[1]\n";
			}

			&sel_floor($#p + 1);
			for (@p) {
				print "\t@{ $_ }[0] ${ $_ }[1]\n";
			}
		}
		undef $collect;
	}

	# polyline

	if (/^2\s/ && defined $collect) {
		@a = split(/\s+/, $_);
		# polyline, red, width = 1, two points
		if ($a[1] == 1 && $a[3] == 1 && $a[4] == 4 && $a[15] == 2) {
			$this = 1;
			next;
		}
		# polygon, blue, width = 1
		if ($a[1] == 3 && $a[3] == 1 && $a[4] == 1) {
			print "# href=\"$collect\" alt=\"\"\n";
			undef $vertices;
			$this_poly = 1;
			next;
		}
	}
	print;

	undef $comment;
}
