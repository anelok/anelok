#!/usr/bin/perl

sub css
{
	local ($id, $w, $x, $y) = @_;

	print <<EOF;
#t_$id {
	position:	absolute;
	left:		${x}px;
	top:		${y}px;
	width:		${w}px;
	display:	none;
	background:	url(${dir}blank.gif);
}
EOF
}


sub flush
{
	return unless $div;
	return unless defined $id;
	print <<EOF;
<DIV id="t_$id">
$s
</SPAN>
</DIV>
EOF
	undef $id;
}


$dir = "";
if ($ARGV[0] eq "-d") {
	shift @ARGV;
	$dir = shift @ARGV;
	$dir .= "/";
}
if ($ARGV[0] eq "-c") {
	$css = 1;
	shift @ARGV;
} elsif ($ARGV[0] eq "-p") {
	$preload = 1;
	shift @ARGV;
} else {
	$div = 1;
}

while (<>) {
	s/#.*//;
	next if /^\s+$/;
	if (/^(\S+?):\s*(\d+)\+(\d+)\+(\d+)\s*$/) {
		&flush;
		$discard = 0;
		&css($1, $2, $3, $4) if $css;
		$id = $1;
		push(@ids, $id);
		undef $s;
		next;
	}
	if (/^(\S+?)\*\s*$/) {
		&flush;
		$discard = 1;
		undef $id;
		next;
	}
	die if /^\S/;
	next if $discard;
	if (defined $s) {
		$s .= $_;
	} else {
		chop;
		$s = "<SPAN class=\"head\">$_</SPAN><BR>\n";
		$s .= "<SPAN class=\"body\">\n";
	}
}
&flush;

if ($preload) {
	$i = 0;
	for (@ids) {
		print <<EOF;
	img$i = new Image();
	img$i.src = "${dir}dwg-overlay-$_.png";
EOF
		$i++;
	}
}
