#!/usr/bin/perl
#
# imgsize.pl - Explicitly size images in HTML files
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

while (<>) {
	if (/(<img\s+src="([^"]+)"[^>]*)/i) {
		($a, $b, $c) = ($`, $1, $');
		$id = `identify "$2"`;
		die unless $id =~ /(\d+)x(\d+)/;
		print "$a$b width=\"$1\" height=\"$2\"$c";
		
	} else {
		print;
	}
}
