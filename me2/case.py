#!/usr/bin/python
#
# me2/case.py - Construct Anelok case with FreeCAD
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import FreeCAD, Part
from FreeCAD import Base
from math import sqrt, sin, cos, atan2

#
# Coordinate system:
#
# x = 0: left edge of PCB (short side, next to the antenna)
# y = 0: bottom edge of PCB (where USB comes out)
# z = 0:
#	globally: bottom surface of PCB
#	during bottom shell construction: rim of the bottom shell
#	durint top shell construction: rim of the top shell
#
#

# ----- Components (for openings and cut-outs) ---------------------------------


# USB connector

#usb_x = 30.9
usb_y = 5.1
usb_w = 8 + 1
#usb_z0 = -2.75 - 0.3		# z = 0: bottom surface of the PCB
#usb_z1 = 0.3 + 0.3

# Switch

#sw_x = 18.3
#sw_w = 2.2 + 2.8
#sw_z0 = -1.2 - 0.3
#sw_z1 = -0.1 + 0.3

# OLED

#oled_x = 24.2
#oled_y = 

oled_w = 34.5
oled_h = 23.0
oled_t = 1.45

oled_off = (23 - 19.2) / 2.0    # panel offset from center

oled_vis_w = 31.42
oled_vis_h = 16.7

fpc_w = 12 + 2

# LED

led_base_w = 1.6
led_base_h = 3.2
led_base_t = 0.28

led_lens_w = 1.6
led_lens_h = 1.5
led_lens_t = 1.1 - led_base_t

led_cut_w = 2.1
led_cut_h = 2.3

# Battery

# Based on Energizer EN92 data sheet

bat_cyl_d = 10		# 10.0 +/- 0.5 mm
bat_tot_h = 43.9	# 43.9 +/- 0.6 mm
bat_but_d = 3.8		# 3.8 mm max
bat_but_h = 0.8		# 0.8 mm min
bat_bot_d = 4.3		# 4.3 mm min
bat_bot_fake_h = 0.1	# fake height of bottom

# Battery tube

contact = 2.0
tube_h = 44.5 + 0.5 + contact
			# 43.9 +/- 0.6 mm, tolerance + 0.5 mm
tube_d = 10.5 + 0.5	# 10.0 +/- 0.5 mm, tolerance + 0.5 mm
tube_w = 1.5		# tube thickness

# PCBs

# Clearances: left/right = 0.25 mm, top/bottom = 0.3 mm

pcb_h = 26
pcb_w = 46.6
pcb_t = 0.8

pcb_sub_w = 6.2
pcb_sub_h = 25.5
pcb_sub_t = 1.6

# Top curvature

top_r = 4.5

# Micro USB receptacle (with funnel-shaped entry)

usb_rcpt_w = 7.5	# nominal
usb_rcpt_h = 5		# 5.00 +/- 0.15 mm
usb_rcpt_t = 2.5

usb_entry_w = 8.06
usb_entry_h = 0.6
usb_entry_t = 3.07

# Memory card

card_w = 14
card_h = 15.2
card_t = 1.93

# Slide switch

sw_body_w = 6.7
sw_body_h = 2.6
sw_body_t = 1.4

sw_knob_w = 0.8
sw_knob_h = 0.5
sw_knob_t = 1.1

sw_knob_travel = 3
sw_knob_z = 0.1

#
# Board-to-board connector (paired)
#
# Hirose DF40C-10DS-0.4V(51) + DF40C-10DP-0.4V(51)
#

b2b_w = 4.6
b2b_h = 3.38
b2b_t = 1.5		# 1.5 mm stacking height

# Antenna

antenna_w = 16.8
antenna_h = 5.3
antenna_t = 0.05


# ----- Helper functions ------------------------------------------------------


def v(x, y, z):
	return Base.Vector(x, y, z)


def extrude_shape(shape, z):
	wire = Part.Wire(shape.Edges)
	face = Part.Face(wire)

	return face.extrude(v(0, 0, z))


def rect(x, y, z):
	bottom = Part.Line(v(0, 0, 0), v(x, 0, 0))
	top = Part.Line(v(0, y, 0), v(x, y, 0))
	left = Part.Line(v(0, 0, 0), v(0, y, 0))
	right = Part.Line(v(x, 0, 0), v(x, y, 0))

	s = Part.Shape([ bottom, top, left, right ])
	return extrude_shape(s, z)


def cylinder(r, z):
	return Part.makeCylinder(r, z)


def move(obj, x, y, z):
	obj.translate(v(x, y, z))


# ----- Components ------------------------------------------------------------


def aaa():
	cyl = cylinder(bat_cyl_d / 2.0, bat_tot_h - bat_but_h - bat_bot_fake_h)
	but = cylinder(bat_but_d / 2.0, bat_but_h)
	bot = cylinder(bat_bot_d / 2.0, bat_bot_fake_h)

	move(cyl, 0, 0, bat_bot_fake_h)
	move(but, 0, 0, bat_tot_h - bat_but_h)

	bat = cyl.fuse(bot)
	bat = bat.fuse(but)

	return bat


def oled_panel():
	r = rect(oled_w, oled_h, oled_t - 0.05)
	move(r, -oled_w / 2.0, -oled_h / 2.0 - oled_off, 0)
	return r


def oled_display():
	r = rect(oled_vis_w, oled_vis_h, 0.05)
	move(r, -oled_vis_w / 2.0, -oled_vis_h / 2.0, oled_t - 0.05)
	return r


def pcb_main():
	r = rect(pcb_w, pcb_h, pcb_t)
	return r


def pcb_sub():
	r = rect(pcb_sub_w, pcb_sub_h, pcb_sub_t)

	cut = rect(led_cut_w, led_cut_h, pcb_sub_t)
	move(cut, (pcb_sub_w - led_cut_w) / 2.0, pcb_sub_h - led_cut_h, 0)

	return r.cut(cut)


def usb():
	r1 = rect(usb_rcpt_w, usb_rcpt_h, usb_rcpt_t)
	r2 = rect(usb_entry_w, usb_entry_h, usb_entry_t)

	move(r2, (usb_rcpt_w - usb_entry_w) / 2.0, -usb_entry_h,
	    (usb_rcpt_t - usb_entry_t) / 2.0)

	return r1.fuse(r2)

def memcard():
	r = rect(card_w, card_h, card_t)
	return r


def switch():
	r1 = rect(sw_body_w, sw_body_h, sw_body_t)
	r2 = rect(sw_knob_w, sw_knob_h, sw_knob_t)

	move(r2, (sw_body_w - sw_knob_w - sw_knob_travel) / 2,
	    -sw_knob_h, sw_knob_z)

	return r1.fuse(r2)


def b2b():
	return rect(b2b_w, b2b_h, b2b_t)


def led():
	base = rect(led_base_w, led_base_h, led_base_t)
	move(base, -led_base_w / 2.0, -led_base_h / 2.0, -led_base_t)

	lens = rect(led_lens_w, led_lens_h, led_lens_t)
	move(lens, -led_lens_w / 2.0, -led_lens_h / 2.0, 0)

	return base.fuse(lens)


def antenna():
	return rect(antenna_w, antenna_h, antenna_t)


# ----- Shift

#
# Calculate x for a given y and radius r
#
#    | x |
#   ________
#  (   ) | y
# (     )---
#  (___)
#
# x^2 = 2ry - y^2
#

def circ_shift(r, y):
	return sqrt(2 * r * y - y * y)


# ----- Case ------------------------------------------------------------------


def tube():
	inner = cylinder(tube_d / 2.0, tube_h)
	outer = cylinder(tube_d / 2.0 + tube_w, tube_h)

	return outer.cut(inner)


def top():
	outer = cylinder(top_r, tube_h)
	inner = cylinder(top_r - tube_w, tube_h)
	cyl = outer.cut(inner)

	r = rect(2 * top_r, top_r, tube_h)
	move(r, -top_r, -top_r, 0)

	return cyl.cut(r)

#
#   |x|     t   _______----
#   ______------   ( \  )
#---( \ )     c _ (_ _   )
#  (   - ) -  -   (      )
#   (   ) r1       (    ) r2
# ---------------------------------------------
#     |       b      |
#
# This can be simplified by (r1, r2) -> (0, r2 - r1)
#
#             _____
#       t  __- (\a  )
#       __-   (     )
#      -a      (   )
# ---------------------------
#     |       b      |
#
# a = 2 * atan (r / t)
# t = b
#

def side(r1, r2, b):
	a = 2 * atan2(r2 - r1, b)
	t1x = -r1 * sin(a)
	t1y = r1 + r1 * cos(a)
	t2x = b - r2 * sin(a)
	t2y = r2 + r2 * cos(a)
	h1 = Part.Line(v(0, 0, 0), v(b, 0, 0))
	a2 = Part.Arc(v(b, 0, 0), v(b + r2, r2, 0), v(t2x, t2y, 0))
	h2 = Part.Line(v(t2x, t2y, 0), v(t1x, t1y, 0))
	a1 = Part.Arc(v(t1x, t1y, 0), v(-r1, r1, 0), v(0, 0, 0))

	s = Part.Shape([ h1, a2, h2, a1 ])
	return extrude_shape(s, 1.5)


# ----- Assemble things -------------------------------------------------------


def visualize(shape, name, color, trans):
	obj = doc.addObject("Part::Feature", name)
	obj.Shape = shape
	obj.ViewObject.ShapeColor = color
	obj.ViewObject.Transparency = trans


doc = FreeCAD.newDocument()

bat = aaa()
bat.rotate(v(0, 0, 0), v(0, 1, 0), 90)
move(bat, contact, 0, -tube_d / 2.0 - tube_w)

tube = tube()
tube.rotate(v(0, 0, 0), v(0, 1, 0), 90)
move(tube, 0, 0, -tube_d / 2.0 - tube_w)

shift = circ_shift(tube_d / 2.0 + tube_w, 2.5) + 2

top = top()
top.rotate(v(0, 0, 0), v(0, 1, 0), 90)
move(top, 0, oled_h + shift, -top_r)

case = tube.fuse(top)

panel_shift = shift + oled_h / 2 - oled_off - 0.4

panel = oled_panel()
panel.rotate(v(0, 0, 0), v(0, 0, 1), 180)
move(panel, oled_w / 2.0 + 5.9, panel_shift, -oled_t - 1)
disp = oled_display()
disp.rotate(v(0, 0, 0), v(0, 0, 1), 180)
move(disp, oled_w / 2.0 + 5.9, panel_shift, -oled_t - 1)

pcb_x = contact / 2.0 - 0.8
pcb_y = 7
pcb_top = -4.1
pcb_bot = pcb_top - pcb_t

pcb = pcb_main()
move(pcb, pcb_x, pcb_y, pcb_bot)

pcb_sub_x = tube_h - 6.2
pcb_sub_y = 5
pcb_sub_z = -1 - pcb_sub_t

pcb_sub = pcb_sub()
move(pcb_sub, pcb_sub_x, pcb_sub_y, pcb_sub_z)

left = side(top_r, tube_d / 2.0 + tube_w, oled_h + shift)
left.rotate(v(0, 0, 0), v(1, 0, 0), -90)
left.rotate(v(0, 0, 0), v(0, 0, 1), -90)
move(left, -1.5, oled_h + shift, 0)

right = side(top_r, tube_d / 2.0 + tube_w, oled_h + shift)
right.rotate(v(0, 0, 0), v(1, 0, 0), -90)
right.rotate(v(0, 0, 0), v(0, 0, 1), -90)
move(right, tube_h, oled_h + shift, 0)

glass = rect(tube_h, oled_h + shift, 1)
move(glass, 0, 0, -1)

case = case.fuse(left)
case = case.fuse(right)

usb = usb()
usb.rotate(v(0, 0, 0), v(0, 0, 1), 90)
move(usb, pcb_x + pcb_w, pcb_y + usb_y - usb_rcpt_w / 2.0, pcb_bot - usb_rcpt_t)

card = memcard()
card.rotate(v(0, 0, 0), v(0, 0, 1), -90)
move(card, pcb_x + pcb_w - card_h, pcb_y + pcb_h - 1, pcb_bot - card_t)

sw = switch()
sw.rotate(v(0, 0, 0), v(0, 0, 1), 90)
move(sw, pcb_x + pcb_w - 1, pcb_y + 1.75, pcb_top)

b2b = b2b()
b2b.rotate(v(0, 0, 0), v(0, 0, 1), 90)
move(b2b, pcb_x + pcb_w - (pcb_sub_w - b2b_h) / 2.0,
  pcb_y + pcb_h - b2b_w - 5, pcb_top)

led = led()
led.rotate(v(0, 0, 0), v(0, 0, 1), 90)
move(led, pcb_sub_x + pcb_sub_w / 2.0,
    pcb_sub_y + pcb_sub_h - led_cut_h / 2.0, pcb_sub_z)

ant = antenna()
ant.rotate(v(0, 0, 0), v(0, 0, 1), 90)
move(ant, pcb_x + antenna_h, pcb_y + 2.5, pcb_top)

visualize(case, "Case", (0.7, 0.7, 0.7), 50)
visualize(bat, "Battery", (1.0, 0.7, 0.7), 50)
visualize(panel, "OLED Panel", (1.0, 1.0, 1.0), 70)
visualize(disp, "OLED Display", (0.0, 0.0, 0.0), 50)
visualize(pcb, "PCB Main", (0.2, 0.8, 0.2), 50)
visualize(pcb_sub, "PCB Sub", (0.2, 0.8, 0.2), 50)
#visualize(left, "Left", (1.0, 0.8, 0.2), 50)
#visualize(right, "Right", (1.0, 0.8, 0.2), 50)
visualize(glass, "Glass", (1.0, 0.8, 0.2), 80)
visualize(usb, "USB", (0.5, 0.5, 0.5), 60)
visualize(card, "Card", (0.5, 0.5, 0.5), 60)
visualize(sw, "Switch", (0.5, 0.5, 0.5), 60)
visualize(b2b, "B2B", (0.5, 0.5, 0.5), 60)
visualize(led, "LED", (1.0, 1.0, 1.0), 80)
visualize(ant, "Antenna", (0.0, 0.0, 0.0), 80)

#case.exportStl("case.stl")
