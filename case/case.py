#!/usr/bin/python
#
# case/case.py - Construct Anelok case with FreeCAD
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import FreeCAD, Part
from FreeCAD import Base
from math import sqrt

#
# Case variant configuration
#

board = 2014	# Board type, 2014 or 2015
taller = False	# Add height to the model, to compensate for machining problems
buttons = True	# Thicken case to accommodate mechanical switches

#
# Coordinate system:
#
# x = 0: left edge of PCB (short side, next to the antenna)
# y = 0: bottom edge of PCB (where USB comes out)
# z = 0:
#	globally: bottom surface of PCB
#	during bottom shell construction: rim of the bottom shell
#	durint top shell construction: rim of the top shell
#
#

#----- Parameters -------------------------------------------------------------


# General parameters

project_y = 50		# size used for projections (for holes)

# PCB dimensions

pcb_w = 64		# overall circuit area, considering overlaps
pcb_main_w = 42		# main PCB, without bridge to sub board
pcb_sub_w = 20		# sub PCB, without bridge to main board
pcb_h = 31
pcb_r = 2		# corners are chamfered for radius 2 mm

# PCB placement and support

pcb_sub_x = pcb_main_w + 2
pcb_gap_x = 0.2		# gap around the PCB on each side
pcb_gap_y = 0.2
pcb_gap_z = 0.1		# vertical tolerance
pcb_overlap = 0.5	# overlap under PCB
pcb_chamfer = 2		# corners are rounded
pcb_sub_z = 2.9

# case properties

top_wall_x = 1.3
top_wall_y = 1.3
top_rim_x = 1
top_rim_y = 1
rim_gap = 0.15		# gap between rims

# HACK: adjustments for 2014 boards in 2015 cases

if board == 2014:
	pcb_h -= 1
	top_wall_y += 0.5
	#top_rim_y += 0.5

# outline

tx = pcb_gap_x + top_wall_x
ty = pcb_gap_y + top_wall_y
out_x = pcb_w + 2 * tx
out_y = pcb_h + 2 * ty
out_r_x = pcb_r + tx
out_r_y = pcb_r + ty
out_r = out_r_x		# x radius determines curvature


#----- Common (top and bottom) geometry ---------------------------------------


# case parts that grip the PCB

grip_x = pcb_w - 2 * pcb_overlap
grip_y = pcb_h - 2 * pcb_overlap
grip_r = pcb_r - pcb_overlap


#----- Top geometry -----------------------------------------------------------


oled_gap = 0.4		# gap around the OLED display

bezel_x_gap = 3.5	# vertical (!) gap beween side bars and bezel y rim
			# (to let the mill pass)

bezel_x_gap += 2.5	# extra space to make room for the bridge between the
			# two PCBs

#----- Bottom geometry --------------------------------------------------------


# bottom rim

tx = top_rim_x + rim_gap
ty = top_rim_y + rim_gap
bot_rim_x = out_x - 2 * tx
bot_rim_y = out_y - 2 * ty
bot_rim_r_x = out_r_x - tx
bot_rim_r_y = out_r_y - ty
bot_rim_r = bot_rim_r_x		# x radius determines curvature


#----- Components (for openings and cut-outs) ---------------------------------


# USB connector

usb_x = 30.9
usb_w = 8 + 1
usb_z0 = -2.75 - 0.3		# z = 0: bottom surface of the PCB
usb_z1 = 0.3 + 0.3

# switch

sw_x = 18.3
sw_w = 2.2 + 2.8
sw_z0 = -1.2 - 0.3
sw_z1 = -0.1 + 0.3

# OLED

oled_x = 24.2
oled_w = 34.5
oled_h = 23.0

oled_off = (23 - 19.2) / 2.0	# panel offset from center

oled_vis_w = 31.42
oled_vis_h = 16.7

fpc_w = 12 + 2

# battery

bat_x = 54.0
bat_r = 10
bat_z = 3.2
bat_clear = 0.5
bat_h = 5.51 + 0.5	# Keystone 1059

# tall silo capacitors

cap1_x = 16.9
cap3_x = 25.9
cap_y = 2.6
cap_z = 2.5	# they're 1.6 mm, but let's play it safe and assume 2.0 + 0.5 mm
cap_r = 1.6	# leave room for 1/8" mil
cap_w = 3.2 + 2 * 0.5 + 2 * cap_r
cap_h = max(1.6 + 2 * 0.5, 2 * cap_r + 0.05)

# LED light pipe

led_h = 0.8	# height of top of the LED above PCB
led_r = 0.7	# radius of light pipe (LED head is 0.8 x 1.1 mm)
led_x = 40.6
led_y = 2.6

# @@@ HACK for 2014 board (components are closer to the PCB edge)

if board == 2014:
	cap_y -= 0.5
	led_y -= 0.5

# @@@ HACK: grow height to compensate for machining inaccuracies

if taller:
	usb_z0 -= 0.5

# lanyard hole
#
# We have three parameters we can vary:
# - lan_hole_r: the radius of the hole
# - lan_cyl_r: the radius of the cylinder surrounding it
# - lan_off: the position of the center
#
# There are the following constraints:
#
# - lan_hole_r must be at least 1.6 mm so that a 1/8 in (3.175 mm) endmill
#   can get inside:
#
# - the wall around the hole (lan_cyl_r - lan_hole_r) must be at least
#   the standard wall thickness (1 mm)
#
# - the hole must leave at least 1 mm of rim
#
# - the cylinder containing the lanyard hole must fit in the 5.4 mm x 5.4 mm
#   cut-out with 2.2 mm chamfer (lan_cut_x, lan_cut_y, lan_cut_chamf) in the
#   PCB (we can take some liberties with the chamfer if necessary)
#
# - in all this we only consider the x axis since the x/y asymmetry of the
#   case is only temporary
#
# We therefore get:
#
#   lan_hole_r >= 3.2 / 2.0
#   lan_cyl_r - lan_hole_r >= 1
#   lan_pos - lan_hole_r >= 1 - bot_rim_r
#   lan_pos + 2 * lan_cyl_r <= lan_cut_x - pcb_gap_x
#

lan_cut_x = 5.4		# PCB cut-out for the lanyard
lan_cut_y = 5.4
lan_cut_chamfer = pcb_chamfer + pcb_gap_x

print "lan_hole_r >= ", 3.2 / 2.0
print "lan_cyl_r - lan_hole_r >= ", 1
print "lan_pos - lan_hole_r >= ", 1 - bot_rim_r
print "lan_pos + 2 * lan_cyl_r <= ", lan_cut_x - pcb_gap_x

#
# This yields:
#  lan_hole_r >= 1.6
#  lan_cyl_r - lan_hole_r >= 1
#  lan_pos + lan_hole_r >= 1.35
#  lan_pos + 2 * lan_cyl_r <= 5.2
#
# We insert:
#
#   lan_hole_r = 1.6
#   lan_cyl_r - lan_hole_r >= 1		-> lan_cyl_r >= 2.6
#   lan_pos - lan_hole_r >= -1.35	-> lan_pos >= 0.25
#   lan_pos + lan_cyl_r <= 5.2		-> lan_pos <= 2.6

# @@@ the math above is wrong but the values here are right :-)

lan_hole_r = 1.7
lan_cyl_r = 2.7
lan_pos = 2.2		# +x, +y offset from the origin


#----- Helper functions -------------------------------------------------------


def v(x, y, z):
	return Base.Vector(x, y, z)


def corner(x, y, a, b):
	va = v(x + a[0], y + a[1], 0)
	f = 1 - 1 / sqrt(2)
	vb = v(x + (a[0] + b[0]) * f, y + (a[1] + b[1]) * f, 0)
	vc = v(x + b[0], y + b[1], 0)
	return Part.Arc(va, vb, vc)


def extrude_shape(shape, z):
	wire = Part.Wire(shape.Edges)
	face = Part.Face(wire)

	return face.extrude(v(0, 0, z))


def move(obj, x, y, z):
	obj.translate(v(x, y, z))


def moveto(x, y):
	global curr_x, curr_y

	curr_x = x
	curr_y = y


def lineto(x, y):
	global curr_x, curr_y

	res = Part.Line(v(curr_x, curr_y, 0), v(x, y, 0))
	curr_x = x
	curr_y = y
	return res


def rlineto(dx, dy):
	return lineto(curr_x + dx, curr_y + dy)


def rrect(x, y, z, r):
	bottom = Part.Line(v(r, 0, 0), v(x - r, 0, 0))
	top = Part.Line(v(r, y, 0), v(x - r, y, 0))
	left = Part.Line(v(0, r, 0), v(0, y - r, 0))
	right = Part.Line(v(x, r, 0), v(x, y - r, 0))

	ll = corner(0, 0, (r, 0), (0, r))
	ul = corner(0, y, (0, -r), (r, 0))
	ur = corner(x, y, (-r, 0), (0, -r))
	lr = corner(x, 0, (0, r), (-r, 0))

	s = Part.Shape([ bottom, top, left, right, ll, ul, ur, lr ])
	return extrude_shape(s, z)


def rrect_center_xy(x, y, z, r):
	r = rrect(x, y, z, r)
	move(r, -x / 2.0, -y / 2.0, 0)
	return r


def rect(x, y, z):
	bottom = Part.Line(v(0, 0, 0), v(x, 0, 0))
	top = Part.Line(v(0, y, 0), v(x, y, 0))
	left = Part.Line(v(0, 0, 0), v(0, y, 0))
	right = Part.Line(v(x, 0, 0), v(x, y, 0))

	s = Part.Shape([ bottom, top, left, right ])
	return extrude_shape(s, z)


def rect_center_xy(x, y, z):
	r = rect(x, y, z)
	move(r, -x / 2.0, -y / 2.0, 0)
	return r


def cylinder(r, z):
	return Part.makeCylinder(r, z)


def adjust(obj, x, y, z):
	move(obj, pcb_r - x, pcb_r - y, z)


def project(obj, up):
	if up:
		move(obj, 0, pcb_h / 2.0, 0)
	else:
		move(obj, 0, pcb_h / 2.0 - project_y, 0)


#----- Components (for cut-outs) ----------------------------------------------


def usb():
	r = rect(usb_w, project_y, usb_z1 - usb_z0)
	move(r, usb_x - usb_w / 2.0, 0, usb_z0)
	project(r, False)
	return r


def switch():
	r = rect(sw_w, project_y, sw_z1 - sw_z0)
	move(r, sw_x - sw_w / 2.0, 0, sw_z0)
	project(r, True)
	return r


def bat():
	cyl = cylinder(bat_r + bat_clear, bat_h)
	move(cyl, bat_x, pcb_h / 2.0, 3.0 - bat_h)
	return cyl


#----- Buttons ----------------------------------------------------------------


# Tactile switch

tact_w = 4.5
tact_h = 4.5
tact_t = 0.55

# The button array

buttons_x = pcb_sub_x + 13.0	# center
buttons_y = pcb_h / 2.0
buttons_step_y = 10.0		# center-to-center distance

# The cut-out for the button array

buttons_w = 10.0
buttons_h = 10.0 + 2 * buttons_step_y
buttons_r = 1.6		# just use tool radius + epsilon

# The keys

key_overlap = 0.8	# overlap between base and case
key_gap = 0.2		# gap between keys and case
key_cap_t = 0.5		# key cap thickness
key_align_t = 0.5	# key alignment area thickness
key_base_t = 0.9	# key base thickness
key_act_t = 0.9		# actuator length
key_fix_side = 6.0
key_fix_r = 1.6		# tool radius
key_fix_gap = 0.1	# gap between caps and base
key_act_r = 1.0		# actuator must be small


def button_hole():
	hole = rrect_center_xy(buttons_w, buttons_h, top_z_top - top_z_bezel,
	    buttons_r)
	r = rrect_center_xy(buttons_w + 2 * key_overlap, buttons_h, 2.0,
	    buttons_r)
	move(r, 0, 0, top_z_top - top_z_bezel - key_cap_t - 2.0)
	hole = hole.fuse(r)
	move(hole, buttons_x, buttons_y, top_z_bezel)
	return hole


def tact(dy):
	return rect_center_xy(tact_w, tact_h, tact_t)


def key_cap():
	bar = rrect_center_xy(buttons_w - 2 * key_gap, buttons_h - 2 * key_gap,
	    key_cap_t, buttons_r - key_gap)
	for dy in (-1, 0, 1):
		f = rrect_center_xy(key_fix_side, key_fix_side, key_align_t,
		    key_fix_r)
		move(f, 0, buttons_step_y * dy, -key_align_t)
		bar = bar.fuse(f)
		p = cylinder(key_act_r, key_act_t - key_align_t)
		move(p, 0, buttons_step_y * dy, -key_act_t)
		bar = bar.fuse(p)
	move(bar, buttons_x, buttons_y, top_z_top - key_cap_t)
	move(bar, 0, 0, pcb_gap_z - top_z_pcb_bot)
	return bar


def key_base():
	bar = rrect_center_xy(buttons_w + 2 * key_overlap - 2 * key_gap,
	    buttons_h - 2 * key_gap, key_base_t, buttons_r - key_gap)
	for dy in (-1, 0, 1):
		p = rrect_center_xy(key_fix_side + 2 * key_fix_gap,
		    key_fix_side + 2 * key_fix_gap, key_base_t,
		    key_fix_r + key_fix_gap)
		move(p, 0, buttons_step_y * dy, 0)
		bar = bar.cut(p)
	move(bar, buttons_x, buttons_y, top_z_top - key_cap_t - key_base_t)
	move(bar, 0, 0, pcb_gap_z - top_z_pcb_bot)
	return bar


def row(fn, z):
	r = None
	for dy in (-1, 0, 1):
		t = fn(dy)
		move(t, buttons_x, buttons_y + buttons_step_y * dy, z)
		if r is None:
			r = t
		else:
			r = r.fuse(t)
	return r


def tact_row():
	return row(tact, pcb_sub_z + 0.8)


#----- Bottom shell -----------------------------------------------------------


#
# Z stacking during shell construction:
#
# 0.0	outside (bottom)
# 1.0	floor
# 3.2	rim
# 4.0	top of rim (and end of bottom shell)
#

bot_z_top	= 0
bot_z_rim	= bot_z_top - 0.8
bot_z_floor	= bot_z_rim - 2.2

if taller:
	bot_z_floor -= 1

bot_z_bottom	= bot_z_floor - 1.0


def bottom():
	base = rrect(out_x, out_y, bot_z_rim - bot_z_bottom, out_r)
	rim = rrect(bot_rim_x, bot_rim_y, bot_z_top - bot_z_rim, bot_rim_r)
	inside = rrect(grip_x, grip_y, bot_z_top - bot_z_floor, grip_r)

	adjust(base, out_r_x, out_r_y, bot_z_bottom)
	adjust(rim, bot_rim_r_x, bot_rim_r_y, bot_z_rim)
	adjust(inside, grip_r, grip_r, bot_z_floor)

	shell = base.fuse(rim)

	# lanyard

	cyl = cylinder(lan_cyl_r, bot_z_top - bot_z_bottom)
	move(cyl, lan_pos, lan_pos, bot_z_bottom)
	hole = cylinder(lan_hole_r, bot_z_top - bot_z_bottom)
	move(hole, lan_pos, lan_pos, bot_z_bottom)

	# @@@ HACK: tweak lanyard for PCB asymmetry
	if board == 2014:
		move(cyl, 0, -0.5, 0)
		move(hole, 0, -0.5, 0)

	# trim off wall protruding over the rim
	cyl = cyl.common(shell)

	shell = shell.cut(inside)
	shell = shell.fuse(cyl)
	shell = shell.cut(hole)

	# move to correct position

	move(shell, 0, 0, -pcb_gap_z)

	# openings

	u = usb()
	shell = shell.cut(u)
	sw = switch()
	shell = shell.cut(sw)
	b = bat()
	shell = shell.cut(b)

	return shell

#----- Top shell --------------------------------------------------------------


#
# Z stacking during shell construction:
#
# 5.8	outside (top)
# 4.8	ceiling for main PCB
# 4.3	window (display goes into this)
# 3.3	bezel (display goes into this; leave room for FPC)
# 1.8	pcb top surface
# 1.0	pcb bottom surface
# 0.0	top of rim
#

top_z_rim	= 0
top_z_pcb_bot	= top_z_rim + 1.0
top_z_pcb_top	= top_z_pcb_bot + 0.8
top_z_bezel	= top_z_pcb_top + 1.5

if taller:
	top_z_bezel += 0.5

top_z_ceiling	= top_z_bezel + 1.5
top_z_window	= top_z_ceiling - 0.5
top_z_top	= top_z_ceiling + 1.0

if buttons:
	top_z_top += 1.0


def top():
	base = rrect(out_x, out_y, top_z_top - top_z_rim, out_r)
	inside = rrect(grip_x, grip_y, top_z_ceiling - top_z_pcb_top, grip_r)

	top_wall_r_x = out_r_x - top_wall_x
	top_wall_r_y = out_r_y - top_wall_y
	top_rim_r_x = out_r_x - top_rim_x
	top_rim_r_y = out_r_y - top_rim_y

	wall = rrect(out_x - 2 * top_wall_x, out_y - 2 * top_wall_y,
	    top_z_ceiling - top_z_pcb_bot, top_wall_r_x)
	rim = rrect(out_x - 2 * top_rim_x, out_y - 2 * top_rim_y,
	    top_z_pcb_bot - top_z_rim, top_rim_r_x)

	adjust(base, out_r_x, out_r_y, top_z_rim)
	adjust(inside, grip_r, grip_r, top_z_pcb_top)

	adjust(wall, top_wall_r_x, top_wall_r_y, top_z_pcb_bot)
	adjust(rim, top_rim_r_x, top_rim_r_y, top_z_rim)

	wall_trim = rect(2 * pcb_w, 2 * pcb_h, top_z_ceiling - top_z_pcb_top)
	    # xy size is just "big"
	move(wall_trim, pcb_main_w - 2 * pcb_w, -pcb_h / 2, top_z_pcb_top)
	wall = wall.cut(wall_trim)

	shell = base.cut(inside)
	shell = shell.cut(wall)
	shell = shell.cut(rim)

	# bezel y

	by_h = oled_h + 2 * oled_gap
	by_base = rect(grip_x - (pcb_w - pcb_main_w), grip_y,
	    top_z_ceiling - top_z_bezel)
	by_cut = rrect(grip_x, by_h, top_z_ceiling - top_z_bezel, grip_r)
	d = grip_y - by_h
	move(by_cut, 0, d / 2.0 + oled_off, 0)
	by = by_base.cut(by_cut)
	adjust(by, grip_r, grip_r, top_z_bezel)

	shell = shell.fuse(by)

	# bezel x (rondels)

	# center of the panel
	by_cy = pcb_h / 2.0 + oled_off

	bx_wt = pcb_main_w + 2 - (oled_x + oled_w / 2.0) - oled_gap - pcb_gap_x
	bx_wb = pcb_sub_x - pcb_main_w - 2 * pcb_gap_x
	bx_h = by_h - 2 * bezel_x_gap
	bx_r = bx_wt / 3.0
	bx_l = rrect(bx_wt, bx_h, top_z_ceiling - top_z_bezel, bx_r)
	bx_rt = rrect(bx_wt, bx_h, top_z_ceiling - top_z_pcb_top, bx_r)
	bx_rb = rrect(bx_wb, bx_h, top_z_pcb_top - top_z_rim + 0.2, bx_r)
	move(bx_l, oled_x - oled_w / 2.0 - oled_gap - bx_wt,
	    by_cy - bx_h / 2.0, top_z_bezel)
	move(bx_rt, oled_x + oled_w / 2.0 + oled_gap, by_cy - bx_h / 2.0,
	    top_z_pcb_top)
	move(bx_rb, pcb_main_w + pcb_gap_x, by_cy - bx_h / 2.0,
	    top_z_pcb_bot - 0.2)

	shell = shell.fuse(bx_l)
	shell = shell.fuse(bx_rt)
	shell = shell.fuse(bx_rb)

	# cut-out for tall silo capacitors

	cap = rrect(cap3_x - cap1_x + cap_w, cap_h, cap_z, cap_r)
	move(cap, cap1_x - cap_w / 2.0, cap_y - cap_h / 2.0, top_z_pcb_top)
	shell = shell.cut(cap)

	# FPC

	fpc = rect(fpc_w, 5, top_z_window - top_z_rim)
	move(fpc, oled_x - fpc_w / 2.0, pcb_h, top_z_rim)
	fpc = fpc.common(shell)

	move(fpc, 0, -1, 0)
	shell = shell.cut(fpc)

	move(fpc, 0, -1, 0)
	shell = shell.cut(fpc)

	# lanyard

	cyl = cylinder(lan_cyl_r, top_z_top - top_z_pcb_bot)
	move(cyl, lan_pos, lan_pos, top_z_pcb_bot)
	hole = cylinder(lan_hole_r, top_z_top - top_z_pcb_bot)
	move(hole, lan_pos, lan_pos, top_z_pcb_bot)

	# @@@ HACK: tweak lanyard for PCB asymmetry
	if board == 2014:
		move(cyl, 0, -0.5, 0)
		move(hole, 0, -0.5, 0)

	shell = shell.fuse(cyl)
	shell = shell.cut(hole)

	# LED light pipe

	h = led_h + 0.5 + top_z_pcb_top
	led = cylinder(led_r, top_z_ceiling - h)
	move(led, led_x, led_y, h)

	# @@@ It'll just be a very minor (0.2 mm) bump that doesn't do much.
	# @@@ Leave out for now.
	#shell = shell.fuse(led)

	if buttons:
		b = button_hole()
		shell = shell.cut(b)

	# move to correct position

	move(shell, 0, 0, pcb_gap_z - top_z_pcb_bot)

	# openings

	u = usb()
	shell = shell.cut(u)
	sw = switch()
	shell = shell.cut(sw)
	b = bat()
	shell = shell.cut(b)

	return shell


#----- PCBs -------------------------------------------------------------------


def pcb_main():
	e = []
	moveto(pcb_main_w, pcb_h)
	e.append(lineto(pcb_chamfer, pcb_h))
	e.append(rlineto(-pcb_chamfer, -pcb_chamfer))
	e.append(lineto(0, lan_cut_y))
	e.append(lineto(lan_cut_x - lan_cut_chamfer, lan_cut_y))
	e.append(rlineto(lan_cut_chamfer, -lan_cut_chamfer))
	e.append(lineto(lan_cut_x, 0))
	e.append(lineto(pcb_main_w, 0))
	e.append(rlineto(0, pcb_h))

	s = Part.Shape(e)
	return extrude_shape(s, 0.8)


def pcb_sub():
	e = []
	moveto(pcb_w - pcb_chamfer, 0)
	e.append(rlineto(pcb_chamfer, pcb_chamfer))
	e.append(rlineto(0, pcb_h - 2 * pcb_chamfer))
	e.append(rlineto(-pcb_chamfer, pcb_chamfer))
	e.append(lineto(pcb_sub_x, pcb_h))
	e.append(rlineto(0, -pcb_h))
	e.append(lineto(pcb_w - pcb_chamfer, 0))

	s = Part.Shape(e)
	sub = extrude_shape(s, 0.8)
	move(sub, 0, 0, pcb_sub_z)
	return sub


#----- OLED -------------------------------------------------------------------


def oled_panel():
	r = rect(oled_w, oled_h, 1.45)
	move(r, oled_x - oled_w / 2.0, (pcb_h - oled_h) / 2.0 + oled_off, 2.3)
	return r


def oled_display():
	r = rect(oled_vis_w, oled_vis_h, 0.05)
	move(r, oled_x - oled_vis_w / 2.0, (pcb_h - oled_vis_h) / 2.0, 3.75)
	return r


#----- Battery ----------------------------------------------------------------


#
# Approximate position:
# - ceiling	 3.8
# - PCB		 2.8
# - Keystone 1059 has a total height of 5.51 mm, so that's 2.3 mm above and
#   below the battery. Let's assume the base is about 1.2 mm
#		 1.6
# - CR2032	-1.6
#

def battery():
	cyl = cylinder(bat_r, bat_z)
	move(cyl, bat_x, pcb_h / 2.0, -1.6)
	return cyl

#----- Assemble things --------------------------------------------------------


def visualize(shape, name, color, trans):
	obj = doc.addObject("Part::Feature", name)
	obj.Shape = shape
	obj.ViewObject.ShapeColor = color
	obj.ViewObject.Transparency = trans


doc = FreeCAD.newDocument()
b = bottom()
t = top()

pm = pcb_main()
ps = pcb_sub()
op = oled_panel()
od = oled_display()
bt = battery()

visualize(b, "Bottom", (0.2, 0.2, 0.8), 80)
visualize(t, "Top", (0.8, 0.2, 0.2), 80)
visualize(pm, "PCB Main", (0.2, 0.8, 0.2), 50)
visualize(ps, "PCB Sub", (0.2, 0.8, 0.2), 50)
visualize(op, "OLED Panel", (1.0, 1.0, 1.0), 70)
visualize(od, "OLED Display", (0.0, 0.0, 0.0), 50)
visualize(bt, "Battery", (0.7, 0.7, 0.7), 50)

if buttons:
	tr = tact_row()
	visualize(tr, "Tactile", (0.4, 0.4, 0.4), 50)
	kc = key_cap()
	visualize(kc, "Keycaps", (0.3, 0.3, 1.0), 50)
	kb = key_base()
	visualize(kb, "Key base", (0.8, 0.0, 0.8), 50)

t.exportStl("top.stl")
b.exportStl("bot.stl")

if buttons:
	kc.exportStl("keycaps.stl")
	kb.exportStl("keybase.stl")
