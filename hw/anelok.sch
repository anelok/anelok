EESchema Schematic File Version 2
LIBS:pwr
LIBS:r
LIBS:c
LIBS:led
LIBS:powered
LIBS:kl25-48
LIBS:memcard8
LIBS:micro_usb_b
LIBS:varistor
LIBS:pmosfet-gsd
LIBS:antenna
LIBS:er-oled-fpc30
LIBS:cc2543
LIBS:balun-smt6
LIBS:xtal-4
LIBS:testpoint
LIBS:nmosfet-gsd
LIBS:diode
LIBS:gencon
LIBS:aat1217
LIBS:inductor
LIBS:crystal
LIBS:switch
LIBS:anelok-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Anelok Password Safe"
Date "28 mar 2015"
Rev ""
Comp "Werner Almesberger"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9600 1200 1000 500 
U 5235E128
F0 "Power+USB" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 9600 2100 1000 500 
U 5235DC63
F0 "Display" 50
F1 "disp.sch" 50
$EndSheet
$Sheet
S 9600 3000 1000 500 
U 5235DC65
F0 "RF" 50
F1 "rf.sch" 50
$EndSheet
$Sheet
S 9600 3900 1000 500 
U 5235DEF3
F0 "Memcard" 50
F1 "memcard.sch" 50
$EndSheet
$Comp
L C C4
U 1 1 5235EA1A
P 2500 4550
F 0 "C4" H 2550 4650 60  0000 L CNN
F 1 "100nF" H 2550 4450 60  0000 L CNN
F 2 "" H 2500 4550 60  0000 C CNN
F 3 "" H 2500 4550 60  0000 C CNN
	1    2500 4550
	1    0    0    -1  
$EndComp
Text GLabel 1200 3250 0    60   Input ~ 0
USB_VBUS
Text GLabel 1200 2800 0    60   3State ~ 0
USB_DP
Text GLabel 1200 2950 0    60   3State ~ 0
USB_DM
$Comp
L C C3
U 1 1 5235EA2B
P 2000 4550
F 0 "C3" H 2050 4650 60  0000 L CNN
F 1 "1uF" H 2050 4450 60  0000 L CNN
F 2 "" H 2000 4550 60  0000 C CNN
F 3 "" H 2000 4550 60  0000 C CNN
	1    2000 4550
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5235EA31
P 1500 4550
F 0 "C2" H 1550 4650 60  0000 L CNN
F 1 "2.2uF" H 1550 4450 60  0000 L CNN
F 2 "" H 1500 4550 60  0000 C CNN
F 3 "" H 1500 4550 60  0000 C CNN
	1    1500 4550
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5235EA76
P 1500 5850
F 0 "C5" H 1550 5950 60  0000 L CNN
F 1 "1uF" H 1550 5750 60  0000 L CNN
F 2 "" H 1500 5850 60  0000 C CNN
F 3 "" H 1500 5850 60  0000 C CNN
	1    1500 5850
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5235EA7C
P 2800 5850
F 0 "C6" H 2850 5950 60  0000 L CNN
F 1 "1uF" H 2850 5750 60  0000 L CNN
F 2 "" H 2800 5850 60  0000 C CNN
F 3 "" H 2800 5850 60  0000 C CNN
	1    2800 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5235EA99
P 2500 4950
F 0 "#PWR01" H 2500 4950 30  0001 C CNN
F 1 "GND" H 2500 4880 30  0001 C CNN
F 2 "" H 2500 4950 60  0000 C CNN
F 3 "" H 2500 4950 60  0000 C CNN
	1    2500 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5235EA9F
P 2000 4950
F 0 "#PWR02" H 2000 4950 30  0001 C CNN
F 1 "GND" H 2000 4880 30  0001 C CNN
F 2 "" H 2000 4950 60  0000 C CNN
F 3 "" H 2000 4950 60  0000 C CNN
	1    2000 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5235EAA5
P 1500 4950
F 0 "#PWR03" H 1500 4950 30  0001 C CNN
F 1 "GND" H 1500 4880 30  0001 C CNN
F 2 "" H 1500 4950 60  0000 C CNN
F 3 "" H 1500 4950 60  0000 C CNN
	1    1500 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5235EB49
P 2800 6250
F 0 "#PWR04" H 2800 6250 30  0001 C CNN
F 1 "GND" H 2800 6180 30  0001 C CNN
F 2 "" H 2800 6250 60  0000 C CNN
F 3 "" H 2800 6250 60  0000 C CNN
	1    2800 6250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5235EB4F
P 1500 6250
F 0 "#PWR05" H 1500 6250 30  0001 C CNN
F 1 "GND" H 1500 6180 30  0001 C CNN
F 2 "" H 1500 6250 60  0000 C CNN
F 3 "" H 1500 6250 60  0000 C CNN
	1    1500 6250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5235EB55
P 6200 5350
F 0 "#PWR06" H 6200 5350 30  0001 C CNN
F 1 "GND" H 6200 5280 30  0001 C CNN
F 2 "" H 6200 5350 60  0000 C CNN
F 3 "" H 6200 5350 60  0000 C CNN
	1    6200 5350
	1    0    0    -1  
$EndComp
Text Notes 2400 6500 0    60   ~ 0
Near pins 1/2
$Comp
L C C1
U 1 1 5235EBEE
P 2900 4250
F 0 "C1" H 2950 4350 60  0000 L CNN
F 1 "100nF" H 2950 4150 60  0000 L CNN
F 2 "" H 2900 4250 60  0000 C CNN
F 3 "" H 2900 4250 60  0000 C CNN
	1    2900 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5235EC2F
P 2900 4950
F 0 "#PWR07" H 2900 4950 30  0001 C CNN
F 1 "GND" H 2900 4880 30  0001 C CNN
F 2 "" H 2900 4950 60  0000 C CNN
F 3 "" H 2900 4950 60  0000 C CNN
	1    2900 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5235EC35
P 3300 4950
F 0 "#PWR08" H 3300 4950 30  0001 C CNN
F 1 "GND" H 3300 4880 30  0001 C CNN
F 2 "" H 3300 4950 60  0000 C CNN
F 3 "" H 3300 4950 60  0000 C CNN
	1    3300 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5235EC59
P 3500 4950
F 0 "#PWR09" H 3500 4950 30  0001 C CNN
F 1 "GND" H 3500 4880 30  0001 C CNN
F 2 "" H 3500 4950 60  0000 C CNN
F 3 "" H 3500 4950 60  0000 C CNN
	1    3500 4950
	1    0    0    -1  
$EndComp
Text GLabel 6050 1300 1    60   Output ~ 0
DISP_SDIN
Text GLabel 4850 1300 1    60   Input ~ 0
RF_MISO
Text GLabel 5900 1300 1    60   Output ~ 0
RF_DC
Text GLabel 5750 1300 1    60   Output ~ 0
RF_nRESET
Text GLabel 4700 1300 1    60   Output ~ 0
RF_MOSI
Text GLabel 5150 1300 1    60   BiDi ~ 0
RF_DD
Text GLabel 5300 1300 1    60   Output ~ 0
RF_nSEL
Text GLabel 5000 1300 1    60   Output ~ 0
RF_SCLK
Text GLabel 6200 1300 1    60   Output ~ 0
DISP_SCLK
Text GLabel 7550 2950 2    60   Output ~ 0
DISP_nRES
Text GLabel 7550 3100 2    60   Output ~ 0
DISP_nCS
$Comp
L TESTPOINT TP1
U 1 1 5235F91E
P 8100 4000
F 0 "TP1" V 8100 4250 60  0000 C CNN
F 1 "TESTPOINT" H 8100 3950 60  0001 C CNN
F 2 "" H 8100 4000 60  0000 C CNN
F 3 "" H 8100 4000 60  0000 C CNN
	1    8100 4000
	0    1    1    0   
$EndComp
$Comp
L TESTPOINT TP3
U 1 1 5235F92B
P 5750 6000
F 0 "TP3" H 5750 6250 60  0000 C CNN
F 1 "TESTPOINT" H 5750 5950 60  0001 C CNN
F 2 "" H 5750 6000 60  0000 C CNN
F 3 "" H 5750 6000 60  0000 C CNN
	1    5750 6000
	-1   0    0    1   
$EndComp
$Comp
L TESTPOINT TP2
U 1 1 5235F931
P 5300 6000
F 0 "TP2" H 5300 6250 60  0000 C CNN
F 1 "TESTPOINT" H 5300 5950 60  0001 C CNN
F 2 "" H 5300 6000 60  0000 C CNN
F 3 "" H 5300 6000 60  0000 C CNN
	1    5300 6000
	-1   0    0    1   
$EndComp
Text GLabel 7550 3550 2    60   3State ~ 0
CARD_DAT0
Text GLabel 7550 3700 2    60   3State ~ 0
CARD_DAT1
Text GLabel 4700 5400 3    60   3State ~ 0
CARD_DAT2
Text GLabel 4850 5400 3    60   3State ~ 0
CARD_DAT3
Text GLabel 5150 5400 3    60   Output ~ 0
CARD_CLK
Text GLabel 5000 5400 3    60   3State ~ 0
CARD_CMD
Text GLabel 7550 3400 2    60   Output ~ 0
CARD_ON
Text GLabel 7550 2650 2    60   Output ~ 0
DISP_DnC
Text GLabel 1200 3550 0    60   Input ~ 0
USB_ID
Text GLabel 5450 5400 3    60   UnSpc ~ 0
CAP_A
Text GLabel 5600 5400 3    60   UnSpc ~ 0
CAP_B
Text GLabel 7550 2500 2    60   Input ~ 0
CARD_SW
Text GLabel 7550 3850 2    60   Output ~ 0
LED
Text GLabel 5450 1300 1    60   Output ~ 0
PWR_EN
Text Notes 5350 6350 3    60   ~ 0
SWD_CLK
Text Notes 5800 6350 3    60   ~ 0
SWD_DIO
Text Notes 8500 4050 0    60   ~ 0
nRESET
Text GLabel 1200 3100 0    60   Output ~ 0
VSYS
$Comp
L GND #PWR010
U 1 1 5236D8A3
P 6700 5350
F 0 "#PWR010" H 6700 5350 30  0001 C CNN
F 1 "GND" H 6700 5280 30  0001 C CNN
F 2 "" H 6700 5350 60  0000 C CNN
F 3 "" H 6700 5350 60  0000 C CNN
	1    6700 5350
	1    0    0    -1  
$EndComp
Text Notes 1050 6500 0    60   ~ 0
Near pins 22/23
Text Label 5300 5850 1    60   ~ 0
SWD_CLK
Text Label 5750 5850 1    60   ~ 0
SWD_DIO
Text Label 7550 4000 0    60   ~ 0
nRESET
$Comp
L R R2
U 1 1 536CFF4E
P 1850 2950
F 0 "R2" V 1900 3200 60  0000 C CNN
F 1 "33" V 1850 2950 60  0000 C CNN
F 2 "" H 1850 2950 60  0000 C CNN
F 3 "" H 1850 2950 60  0000 C CNN
	1    1850 2950
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 536CFF84
P 1850 2800
F 0 "R1" V 1900 3050 60  0000 C CNN
F 1 "33" V 1850 2800 60  0000 C CNN
F 2 "" H 1850 2800 60  0000 C CNN
F 3 "" H 1850 2800 60  0000 C CNN
	1    1850 2800
	0    -1   -1   0   
$EndComp
Text GLabel 7550 2800 2    60   Input ~ 0
USB_nSENSE
Text GLabel 7550 3250 2    60   Output ~ 0
DISP_ON
$Sheet
S 9600 4800 1000 500 
U 5371C006
F0 "Sub-board" 50
F1 "sub.sch" 50
$EndSheet
$Comp
L CRYSTAL X1
U 1 1 537B7DCD
P 6800 5800
F 0 "X1" H 6800 5950 60  0000 C CNN
F 1 "32.768kHz" H 6800 5650 60  0000 C CNN
F 2 "" H 6800 5800 60  0000 C CNN
F 3 "" H 6800 5800 60  0000 C CNN
F 4 "<=16pF" H 6800 5550 60  0000 C CNN "Field4"
	1    6800 5800
	1    0    0    -1  
$EndComp
Text Notes 6450 6250 0    60   ~ 0
SC32S-12.5PF20PPM or similar
Text Label 2300 2800 0    60   ~ 0
MCU_DP
Text Label 2300 2950 0    60   ~ 0
MCU_DM
$Comp
L KL25-48 U1
U 1 1 5235CAC5
P 5500 3300
F 0 "U1" H 4100 4900 60  0000 C CNN
F 1 "MKL26Z128VFT4" H 5500 3350 60  0000 C CNN
F 2 "~" H 6300 3150 60  0000 C CNN
F 3 "~" H 6300 3150 60  0000 C CNN
	1    5500 3300
	1    0    0    -1  
$EndComp
NoConn ~ 5900 5150
Text GLabel 6350 1300 1    60   Input ~ 0
VRF
Wire Wire Line
	3300 3400 3700 3400
Wire Wire Line
	3300 3400 3300 4950
Connection ~ 3300 4000
Wire Wire Line
	3700 3550 1200 3550
Wire Wire Line
	5150 5150 5150 5400
Wire Wire Line
	7550 2500 7350 2500
Wire Wire Line
	5750 1500 5750 1300
Wire Wire Line
	6350 1500 6350 1300
Wire Wire Line
	5300 1300 5300 1500
Wire Wire Line
	5900 1300 5900 1500
Wire Wire Line
	5600 5150 5600 5400
Wire Wire Line
	5450 5150 5450 5400
Wire Wire Line
	5000 5150 5000 5400
Wire Wire Line
	4850 5150 4850 5400
Wire Wire Line
	4700 5150 4700 5400
Wire Wire Line
	6350 5800 6500 5800
Wire Wire Line
	6350 5150 6350 5800
Wire Wire Line
	7550 5800 7550 4150
Wire Wire Line
	7100 5800 7550 5800
Wire Wire Line
	7350 3850 7550 3850
Wire Wire Line
	7350 2950 7550 2950
Wire Wire Line
	7550 4150 7350 4150
Wire Wire Line
	3700 2950 2100 2950
Wire Wire Line
	2100 2800 3700 2800
Wire Wire Line
	1600 2950 1200 2950
Wire Wire Line
	1200 2800 1600 2800
Wire Wire Line
	2500 3700 2500 4350
Connection ~ 2000 3250
Wire Wire Line
	2000 4350 2000 3250
Wire Wire Line
	6700 5150 6700 5350
Connection ~ 1500 3100
Wire Wire Line
	7350 3100 7550 3100
Wire Wire Line
	4700 1300 4700 1500
Wire Wire Line
	7350 4000 8100 4000
Wire Wire Line
	7350 3700 7550 3700
Wire Wire Line
	7350 3550 7550 3550
Wire Wire Line
	7350 3400 7550 3400
Wire Wire Line
	7350 3250 7550 3250
Wire Wire Line
	7350 2800 7550 2800
Wire Wire Line
	7350 2650 7550 2650
Wire Wire Line
	6200 1300 6200 1500
Wire Wire Line
	6050 1300 6050 1500
Wire Wire Line
	4850 1500 4850 1300
Wire Wire Line
	5150 1300 5150 1500
Wire Wire Line
	5000 1300 5000 1500
Wire Wire Line
	5450 1300 5450 1500
Wire Wire Line
	5750 5150 5750 6000
Wire Wire Line
	5300 5150 5300 6000
Connection ~ 3500 4150
Wire Wire Line
	3700 2650 3500 2650
Wire Wire Line
	3500 2650 3500 4950
Wire Wire Line
	3700 4150 3500 4150
Wire Wire Line
	2900 4450 2900 4950
Connection ~ 2900 3850
Connection ~ 2500 3850
Wire Wire Line
	2900 4050 2900 3850
Wire Wire Line
	2800 6050 2800 6250
Wire Wire Line
	1500 6050 1500 6250
Wire Wire Line
	1500 5650 1500 5450
Wire Wire Line
	2800 5450 2800 5650
Wire Wire Line
	6200 5150 6200 5350
Wire Wire Line
	6050 5150 6050 5550
Wire Wire Line
	1500 4750 1500 4950
Wire Wire Line
	2000 4750 2000 4950
Wire Wire Line
	2500 4750 2500 4950
Wire Wire Line
	1500 3100 1500 4350
Wire Wire Line
	1200 3100 3700 3100
Wire Wire Line
	1200 3250 3700 3250
Wire Wire Line
	3300 4000 3700 4000
Wire Wire Line
	2500 3700 3700 3700
Wire Wire Line
	2500 3850 3700 3850
Text Notes 1000 7300 0    60   ~ 0
Unless indicated otherwise, all capacitors should be X5R or X6S, or better (X7R, NP0, etc.)
Text Notes 1000 7450 0    60   ~ 0
Y5V is cheaper but very sensitive to temperature, ages rapidly, etc.
$Comp
L TESTPOINT TP11
U 1 1 54AB5E88
P 5600 1300
F 0 "TP11" V 5600 1500 60  0000 L CNN
F 1 "TESTPOINT" H 5600 1250 60  0001 C CNN
F 2 "" H 5600 1300 60  0000 C CNN
F 3 "" H 5600 1300 60  0000 C CNN
	1    5600 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1300 5600 1500
Text Notes 1000 7600 0    60   ~ 0
If no voltage is specified, use >= 6.3 V
Text Notes 1000 7100 0    60   ~ 0
Capacitor selection
Wire Notes Line
	1000 7150 1900 7150
$Comp
L 3V3 #PWR011
U 1 1 54DEC0B7
P 3100 2300
F 0 "#PWR011" H 3100 2260 30  0001 C CNN
F 1 "3V3" H 3100 2450 60  0000 C CNN
F 2 "" H 3100 2300 60  0000 C CNN
F 3 "" H 3100 2300 60  0000 C CNN
	1    3100 2300
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR012
U 1 1 54DEC0C4
P 1500 5450
F 0 "#PWR012" H 1500 5410 30  0001 C CNN
F 1 "3V3" H 1500 5600 60  0000 C CNN
F 2 "" H 1500 5450 60  0000 C CNN
F 3 "" H 1500 5450 60  0000 C CNN
	1    1500 5450
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR013
U 1 1 54DEC0CA
P 2800 5450
F 0 "#PWR013" H 2800 5410 30  0001 C CNN
F 1 "3V3" H 2800 5600 60  0000 C CNN
F 2 "" H 2800 5450 60  0000 C CNN
F 3 "" H 2800 5450 60  0000 C CNN
	1    2800 5450
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR014
U 1 1 54DEC0D0
P 6050 5550
F 0 "#PWR014" H 6050 5510 30  0001 C CNN
F 1 "3V3" H 6050 5700 60  0000 C CNN
F 2 "" H 6050 5550 60  0000 C CNN
F 3 "" H 6050 5550 60  0000 C CNN
	1    6050 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 2300 3100 3700
Wire Wire Line
	3100 2500 3700 2500
Connection ~ 3100 3700
Connection ~ 3100 2500
$EndSCHEMATC
