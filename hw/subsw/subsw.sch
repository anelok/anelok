EESchema Schematic File Version 2
LIBS:pwr
LIBS:gencon
LIBS:sw4
LIBS:subsw-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "12 sep 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_4 CON2
U 1 1 55F32AA0
P 2100 3950
F 0 "CON2" H 2100 4200 60  0000 C CNN
F 1 "CONN_4" H 2100 3700 60  0000 C CNN
F 2 "" H 2100 3950 60  0000 C CNN
F 3 "" H 2100 3950 60  0000 C CNN
	1    2100 3950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 CON1
U 1 1 55F32ACD
P 3900 2950
F 0 "CON1" H 3900 2800 60  0000 C CNN
F 1 "1059/1060" H 3900 3100 60  0000 C CNN
F 2 "" H 3900 2950 60  0000 C CNN
F 3 "" H 3900 2950 60  0000 C CNN
	1    3900 2950
	1    0    0    1   
$EndComp
$Comp
L SW4 SW1
U 1 1 55F32ADC
P 5500 3100
F 0 "SW1" H 5500 3350 60  0000 C CNN
F 1 "TL3315" H 5500 2800 60  0000 C CNN
F 2 "" H 5500 3100 60  0000 C CNN
F 3 "" H 5500 3100 60  0000 C CNN
	1    5500 3100
	1    0    0    -1  
$EndComp
$Comp
L SW4 SW3
U 1 1 55F32AE9
P 5500 4600
F 0 "SW3" H 5500 4850 60  0000 C CNN
F 1 "TL3315" H 5500 4300 60  0000 C CNN
F 2 "" H 5500 4600 60  0000 C CNN
F 3 "" H 5500 4600 60  0000 C CNN
	1    5500 4600
	1    0    0    -1  
$EndComp
$Comp
L SW4 SW2
U 1 1 55F32AEF
P 5500 3900
F 0 "SW2" H 5500 4150 60  0000 C CNN
F 1 "TL3315" H 5500 3600 60  0000 C CNN
F 2 "" H 5500 3900 60  0000 C CNN
F 3 "" H 5500 3900 60  0000 C CNN
	1    5500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3800 2700 3800
Wire Wire Line
	2700 3800 2700 2900
Wire Wire Line
	2700 2900 3500 2900
Wire Wire Line
	2500 4100 2800 4100
Wire Wire Line
	2800 3000 2800 5100
Wire Wire Line
	2800 3000 3500 3000
Text Label 3000 2900 0    60   ~ 0
SUB_BATTERY
Text Label 3000 3000 0    60   ~ 0
SUB_GND
Wire Wire Line
	2800 5100 5900 5100
Connection ~ 2800 4100
Wire Wire Line
	5900 5100 5900 2700
Wire Wire Line
	5300 5100 5300 4800
Connection ~ 5300 5100
Wire Wire Line
	5700 4800 5700 5100
Connection ~ 5700 5100
Wire Wire Line
	5300 3300 5300 3700
Wire Wire Line
	5700 3700 5700 3300
Wire Wire Line
	5300 4100 5300 4400
Wire Wire Line
	5700 4100 5700 4400
Wire Wire Line
	5300 3500 4400 3500
Wire Wire Line
	4400 3500 4400 3900
Wire Wire Line
	4400 3900 2500 3900
Connection ~ 5300 3500
Wire Wire Line
	5300 4250 4400 4250
Wire Wire Line
	4400 4250 4400 4000
Wire Wire Line
	4400 4000 2500 4000
Connection ~ 5300 4250
Text Label 3200 3900 0    60   ~ 0
SUB_SW_A
Text Label 3200 4000 0    60   ~ 0
SUB_SW_B
Wire Wire Line
	5300 2900 5300 2700
Wire Wire Line
	5300 2700 5900 2700
Wire Wire Line
	5700 2900 5700 2700
Connection ~ 5700 2700
Wire Notes Line
	7100 5000 7200 5000
Wire Notes Line
	7150 5000 7150 4850
Wire Notes Line
	7150 4850 7100 4750
Wire Notes Line
	7150 4650 7250 4650
Wire Notes Line
	7250 4650 7350 4600
Wire Notes Line
	7350 4650 7450 4650
Wire Notes Line
	7400 5000 7500 5000
Wire Notes Line
	7450 5000 7450 4850
Wire Notes Line
	7450 4850 7400 4750
Wire Notes Line
	7150 4750 7150 4450
Wire Notes Line
	7450 4750 7450 4450
Text Notes 7100 4400 0    60   ~ 0
A
Text Notes 7400 4400 0    60   ~ 0
B
Text Notes 6850 4850 0    60   ~ 0
SW1
Text Notes 7550 4850 0    60   ~ 0
SW3
Text Notes 7200 4550 0    60   ~ 0
SW2
$EndSCHEMATC
